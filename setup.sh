#! /bin/bash

# Toolchain setup
sudo apt-get -y install gcc-arm-none-eabi gdb-arm-none-eabi binutils-arm-none-eabi

# Python deps setup
sudo apt-get -y install python-pip
sudo pip install msgpack pyqtgraph pyudev 

# Docs gen setup
sudo apt-get -y install doxygen texlive-latex-base dvips
