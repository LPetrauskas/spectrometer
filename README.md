# Spectrometer

## Overview

Final Proyect: Spectrometer

## Setup

Go to the repo root and run:

```sh
./setup.sh
```

This will setup the development environment, including 
the toolchain, the doc-generating tools and the python 
dependencies for the UI.

## Build

Once within ```Firmware/```, to build everything:

```sh
make all 
```

Or you can build a specific package with:

```sh
make package
```

## Test

Testing is performed on a package basis. If a given 
package contains executables, then run:

```sh
make test-package
```

## Document

Documentation is performed on a package basis. To generate
it, run:

```sh
make html-package
```

At the time of writing, only HTML output is supported

## Clean up

To clean up means to delete all generated files (binaries, 
manifests, documentation, etc.). To do it on the entire firmware, 
run:

```sh
make clean
```

Or you can do it on a specific package with:

```sh
make clean-package
```

## TODO 

* Everything.

### Quirks

* Take care of initialization of misc peripherials.

### Hardware

* Change hole size and annular ring for CCD connector
* Change schmidt trigger footprint
* Make bigger hole sizes for screws
* Make plug's hole bigger
 
### Software

* Pretty much everything. Test modules on their own.

