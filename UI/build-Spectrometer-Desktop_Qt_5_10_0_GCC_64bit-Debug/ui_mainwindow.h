/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QCheckBox *chkIntTime;
    QSpinBox *sbIntegrationTime;
    QComboBox *cbTimeUnit;
    QPushButton *btnSetIntTime;
    QSpacerItem *horizontalSpacer;
    QCheckBox *chkAverage;
    QSpinBox *sbAverage;
    QSpacerItem *horizontalSpacer_7;
    QLabel *label;
    QComboBox *comboBox;
    QSpacerItem *horizontalSpacer_2;
    QToolButton *btnPlay;
    QSpacerItem *horizontalSpacer_3;
    QToolButton *btnPause;
    QSpacerItem *horizontalSpacer_4;
    QToolButton *btnStop;
    QSpacerItem *horizontalSpacer_5;
    QToolButton *btnRefresh;
    QSpacerItem *horizontalSpacer_6;
    QHBoxLayout *horizontalLayout_3;
    QTabWidget *tabLeft;
    QWidget *tab;
    QFrame *line;
    QLabel *label_2;
    QLabel *label_3;
    QFrame *line_2;
    QWidget *layoutWidget1;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_4;
    QComboBox *cbXAxis;
    QWidget *layoutWidget2;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_5;
    QComboBox *cbYAxis;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *chkStrobe;
    QCheckBox *chkLamp;
    QCheckBox *chkUV;
    QLabel *label_9;
    QFrame *line_4;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout_2;
    QCheckBox *chkSpectral;
    QWidget *tab_3;
    QLabel *label_6;
    QComboBox *cbSerial;
    QLabel *label_7;
    QLabel *lblSerial;
    QPushButton *btnConnect;
    QToolButton *toolButton;
    QFrame *line_3;
    QLabel *label_8;
    QWidget *tab_4;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout_3;
    QTableWidget *table;
    QCustomPlot *customPlot;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1118, 727);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        chkIntTime = new QCheckBox(centralWidget);
        chkIntTime->setObjectName(QStringLiteral("chkIntTime"));

        horizontalLayout->addWidget(chkIntTime);

        sbIntegrationTime = new QSpinBox(centralWidget);
        sbIntegrationTime->setObjectName(QStringLiteral("sbIntegrationTime"));
        sbIntegrationTime->setMaximum(1000);

        horizontalLayout->addWidget(sbIntegrationTime);

        cbTimeUnit = new QComboBox(centralWidget);
        cbTimeUnit->addItem(QString());
        cbTimeUnit->addItem(QString());
        cbTimeUnit->setObjectName(QStringLiteral("cbTimeUnit"));

        horizontalLayout->addWidget(cbTimeUnit);

        btnSetIntTime = new QPushButton(centralWidget);
        btnSetIntTime->setObjectName(QStringLiteral("btnSetIntTime"));

        horizontalLayout->addWidget(btnSetIntTime);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        chkAverage = new QCheckBox(centralWidget);
        chkAverage->setObjectName(QStringLiteral("chkAverage"));

        horizontalLayout->addWidget(chkAverage);

        sbAverage = new QSpinBox(centralWidget);
        sbAverage->setObjectName(QStringLiteral("sbAverage"));

        horizontalLayout->addWidget(sbAverage);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_7);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        comboBox = new QComboBox(centralWidget);
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->setObjectName(QStringLiteral("comboBox"));

        horizontalLayout->addWidget(comboBox);

        horizontalSpacer_2 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        btnPlay = new QToolButton(centralWidget);
        btnPlay->setObjectName(QStringLiteral("btnPlay"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/play.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnPlay->setIcon(icon);
        btnPlay->setIconSize(QSize(25, 25));

        horizontalLayout->addWidget(btnPlay);

        horizontalSpacer_3 = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);

        btnPause = new QToolButton(centralWidget);
        btnPause->setObjectName(QStringLiteral("btnPause"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/icons/pause.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnPause->setIcon(icon1);
        btnPause->setIconSize(QSize(25, 25));

        horizontalLayout->addWidget(btnPause);

        horizontalSpacer_4 = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);

        btnStop = new QToolButton(centralWidget);
        btnStop->setObjectName(QStringLiteral("btnStop"));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/icons/stop.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnStop->setIcon(icon2);
        btnStop->setIconSize(QSize(25, 25));

        horizontalLayout->addWidget(btnStop);

        horizontalSpacer_5 = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_5);

        btnRefresh = new QToolButton(centralWidget);
        btnRefresh->setObjectName(QStringLiteral("btnRefresh"));
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/icons/refresh.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnRefresh->setIcon(icon3);
        btnRefresh->setIconSize(QSize(25, 25));

        horizontalLayout->addWidget(btnRefresh);

        horizontalSpacer_6 = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_6);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        tabLeft = new QTabWidget(centralWidget);
        tabLeft->setObjectName(QStringLiteral("tabLeft"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(tabLeft->sizePolicy().hasHeightForWidth());
        tabLeft->setSizePolicy(sizePolicy);
        tabLeft->setMinimumSize(QSize(240, 0));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        line = new QFrame(tab);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(80, 160, 111, 20));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        label_2 = new QLabel(tab);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 160, 66, 20));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        label_2->setFont(font);
        label_2->setLineWidth(2);
        label_2->setTextFormat(Qt::RichText);
        label_3 = new QLabel(tab);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(10, 10, 66, 20));
        label_3->setFont(font);
        label_3->setLineWidth(2);
        label_3->setTextFormat(Qt::RichText);
        line_2 = new QFrame(tab);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setGeometry(QRect(50, 10, 141, 20));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);
        layoutWidget1 = new QWidget(tab);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(10, 50, 197, 30));
        horizontalLayout_4 = new QHBoxLayout(layoutWidget1);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(0, 0, 6, 0);
        label_4 = new QLabel(layoutWidget1);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_4->addWidget(label_4);

        cbXAxis = new QComboBox(layoutWidget1);
        cbXAxis->addItem(QString());
        cbXAxis->addItem(QString());
        cbXAxis->setObjectName(QStringLiteral("cbXAxis"));

        horizontalLayout_4->addWidget(cbXAxis);

        layoutWidget2 = new QWidget(tab);
        layoutWidget2->setObjectName(QStringLiteral("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(10, 100, 190, 30));
        horizontalLayout_5 = new QHBoxLayout(layoutWidget2);
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(0, 0, 0, 0);
        label_5 = new QLabel(layoutWidget2);
        label_5->setObjectName(QStringLiteral("label_5"));

        horizontalLayout_5->addWidget(label_5);

        cbYAxis = new QComboBox(layoutWidget2);
        cbYAxis->addItem(QString());
        cbYAxis->addItem(QString());
        cbYAxis->setObjectName(QStringLiteral("cbYAxis"));

        horizontalLayout_5->addWidget(cbYAxis);

        layoutWidget = new QWidget(tab);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 190, 128, 92));
        verticalLayout_2 = new QVBoxLayout(layoutWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        chkStrobe = new QCheckBox(layoutWidget);
        chkStrobe->setObjectName(QStringLiteral("chkStrobe"));

        verticalLayout_2->addWidget(chkStrobe);

        chkLamp = new QCheckBox(layoutWidget);
        chkLamp->setObjectName(QStringLiteral("chkLamp"));

        verticalLayout_2->addWidget(chkLamp);

        chkUV = new QCheckBox(layoutWidget);
        chkUV->setObjectName(QStringLiteral("chkUV"));

        verticalLayout_2->addWidget(chkUV);

        label_9 = new QLabel(tab);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(10, 300, 66, 20));
        label_9->setFont(font);
        label_9->setLineWidth(2);
        label_9->setTextFormat(Qt::RichText);
        line_4 = new QFrame(tab);
        line_4->setObjectName(QStringLiteral("line_4"));
        line_4->setGeometry(QRect(80, 300, 111, 20));
        line_4->setFrameShape(QFrame::HLine);
        line_4->setFrameShadow(QFrame::Sunken);
        horizontalLayoutWidget = new QWidget(tab);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(10, 330, 251, 61));
        horizontalLayout_2 = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        chkSpectral = new QCheckBox(horizontalLayoutWidget);
        chkSpectral->setObjectName(QStringLiteral("chkSpectral"));

        horizontalLayout_2->addWidget(chkSpectral);

        tabLeft->addTab(tab, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        label_6 = new QLabel(tab_3);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(10, 60, 41, 31));
        cbSerial = new QComboBox(tab_3);
        cbSerial->setObjectName(QStringLiteral("cbSerial"));
        cbSerial->setGeometry(QRect(50, 60, 86, 28));
        label_7 = new QLabel(tab_3);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(10, 120, 66, 31));
        lblSerial = new QLabel(tab_3);
        lblSerial->setObjectName(QStringLiteral("lblSerial"));
        lblSerial->setGeometry(QRect(80, 120, 91, 31));
        lblSerial->setTextFormat(Qt::RichText);
        btnConnect = new QPushButton(tab_3);
        btnConnect->setObjectName(QStringLiteral("btnConnect"));
        btnConnect->setGeometry(QRect(20, 180, 141, 28));
        toolButton = new QToolButton(tab_3);
        toolButton->setObjectName(QStringLiteral("toolButton"));
        toolButton->setGeometry(QRect(140, 60, 28, 27));
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/icons/refreshGreen.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton->setIcon(icon4);
        line_3 = new QFrame(tab_3);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setGeometry(QRect(50, 20, 141, 20));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);
        label_8 = new QLabel(tab_3);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(10, 20, 66, 20));
        label_8->setFont(font);
        label_8->setLineWidth(2);
        label_8->setTextFormat(Qt::RichText);
        tabLeft->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        verticalLayoutWidget = new QWidget(tab_4);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 10, 181, 561));
        verticalLayout_3 = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        table = new QTableWidget(verticalLayoutWidget);
        table->setObjectName(QStringLiteral("table"));

        verticalLayout_3->addWidget(table);

        tabLeft->addTab(tab_4, QString());

        horizontalLayout_3->addWidget(tabLeft);

        customPlot = new QCustomPlot(centralWidget);
        customPlot->setObjectName(QStringLiteral("customPlot"));
        customPlot->setMinimumSize(QSize(600, 600));

        horizontalLayout_3->addWidget(customPlot);


        verticalLayout->addLayout(horizontalLayout_3);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1118, 25));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        tabLeft->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Spectrometer", nullptr));
        chkIntTime->setText(QApplication::translate("MainWindow", "Integration Time", nullptr));
        cbTimeUnit->setItemText(0, QApplication::translate("MainWindow", "microseconds", nullptr));
        cbTimeUnit->setItemText(1, QApplication::translate("MainWindow", "miliseconds", nullptr));

        btnSetIntTime->setText(QApplication::translate("MainWindow", "SET", nullptr));
        chkAverage->setText(QApplication::translate("MainWindow", "Average", nullptr));
        label->setText(QApplication::translate("MainWindow", "Trigger:", nullptr));
        comboBox->setItemText(0, QApplication::translate("MainWindow", "Continuous", nullptr));
        comboBox->setItemText(1, QApplication::translate("MainWindow", "External", nullptr));

        btnPlay->setText(QApplication::translate("MainWindow", "...", nullptr));
        btnPause->setText(QApplication::translate("MainWindow", "...", nullptr));
        btnStop->setText(QApplication::translate("MainWindow", "...", nullptr));
        btnRefresh->setText(QApplication::translate("MainWindow", "...", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "External", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "Plot", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "X Axis", nullptr));
        cbXAxis->setItemText(0, QApplication::translate("MainWindow", "Wavelength [nm]", nullptr));
        cbXAxis->setItemText(1, QApplication::translate("MainWindow", "Pixel Number [ ]", nullptr));

        label_5->setText(QApplication::translate("MainWindow", "Y Axis", nullptr));
        cbYAxis->setItemText(0, QApplication::translate("MainWindow", "Intensity [counts]", nullptr));
        cbYAxis->setItemText(1, QApplication::translate("MainWindow", "Absorbance [au]", nullptr));

        chkStrobe->setText(QApplication::translate("MainWindow", "Strobe", nullptr));
        chkLamp->setText(QApplication::translate("MainWindow", "Tungsten Lamp", nullptr));
        chkUV->setText(QApplication::translate("MainWindow", "UV Lamp", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "Data", nullptr));
        chkSpectral->setText(QApplication::translate("MainWindow", "Spectral Response Correction", nullptr));
        tabLeft->setTabText(tabLeft->indexOf(tab), QApplication::translate("MainWindow", "Device", nullptr));
        label_6->setText(QApplication::translate("MainWindow", "Port:", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "Status:", nullptr));
        lblSerial->setText(QApplication::translate("MainWindow", "Disconnected", nullptr));
        btnConnect->setText(QApplication::translate("MainWindow", "Connect", nullptr));
        toolButton->setText(QApplication::translate("MainWindow", "...", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "Com", nullptr));
        tabLeft->setTabText(tabLeft->indexOf(tab_3), QApplication::translate("MainWindow", "Serial", nullptr));
        tabLeft->setTabText(tabLeft->indexOf(tab_4), QApplication::translate("MainWindow", "Page", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
