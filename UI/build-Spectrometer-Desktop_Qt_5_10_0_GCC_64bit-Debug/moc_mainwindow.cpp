/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Spectrometer/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[32];
    char stringdata0[525];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 14), // "addRandomGraph"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 12), // "graphClicked"
QT_MOC_LITERAL(4, 40, 21), // "QCPAbstractPlottable*"
QT_MOC_LITERAL(5, 62, 9), // "plottable"
QT_MOC_LITERAL(6, 72, 9), // "dataIndex"
QT_MOC_LITERAL(7, 82, 9), // "timerTick"
QT_MOC_LITERAL(8, 92, 10), // "mousePress"
QT_MOC_LITERAL(9, 103, 10), // "mouseWheel"
QT_MOC_LITERAL(10, 114, 12), // "showResponse"
QT_MOC_LITERAL(11, 127, 14), // "const quint16*"
QT_MOC_LITERAL(12, 142, 4), // "data"
QT_MOC_LITERAL(13, 147, 9), // "bytesRead"
QT_MOC_LITERAL(14, 157, 21), // "on_btnRefresh_clicked"
QT_MOC_LITERAL(15, 179, 21), // "on_chkIntTime_toggled"
QT_MOC_LITERAL(16, 201, 7), // "checked"
QT_MOC_LITERAL(17, 209, 21), // "on_chkAverage_toggled"
QT_MOC_LITERAL(18, 231, 21), // "on_toolButton_clicked"
QT_MOC_LITERAL(19, 253, 21), // "on_btnConnect_clicked"
QT_MOC_LITERAL(20, 275, 30), // "on_cbXAxis_currentIndexChanged"
QT_MOC_LITERAL(21, 306, 5), // "index"
QT_MOC_LITERAL(22, 312, 18), // "on_btnPlay_clicked"
QT_MOC_LITERAL(23, 331, 19), // "on_btnPause_clicked"
QT_MOC_LITERAL(24, 351, 18), // "on_btnStop_clicked"
QT_MOC_LITERAL(25, 370, 24), // "on_btnSetIntTime_clicked"
QT_MOC_LITERAL(26, 395, 23), // "on_chkLamp_stateChanged"
QT_MOC_LITERAL(27, 419, 4), // "arg1"
QT_MOC_LITERAL(28, 424, 21), // "on_chkUV_stateChanged"
QT_MOC_LITERAL(29, 446, 25), // "on_sbAverage_valueChanged"
QT_MOC_LITERAL(30, 472, 24), // "on_checkBox_stateChanged"
QT_MOC_LITERAL(31, 497, 27) // "on_chkSpectral_stateChanged"

    },
    "MainWindow\0addRandomGraph\0\0graphClicked\0"
    "QCPAbstractPlottable*\0plottable\0"
    "dataIndex\0timerTick\0mousePress\0"
    "mouseWheel\0showResponse\0const quint16*\0"
    "data\0bytesRead\0on_btnRefresh_clicked\0"
    "on_chkIntTime_toggled\0checked\0"
    "on_chkAverage_toggled\0on_toolButton_clicked\0"
    "on_btnConnect_clicked\0"
    "on_cbXAxis_currentIndexChanged\0index\0"
    "on_btnPlay_clicked\0on_btnPause_clicked\0"
    "on_btnStop_clicked\0on_btnSetIntTime_clicked\0"
    "on_chkLamp_stateChanged\0arg1\0"
    "on_chkUV_stateChanged\0on_sbAverage_valueChanged\0"
    "on_checkBox_stateChanged\0"
    "on_chkSpectral_stateChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  119,    2, 0x08 /* Private */,
       3,    2,  120,    2, 0x08 /* Private */,
       7,    0,  125,    2, 0x08 /* Private */,
       8,    0,  126,    2, 0x08 /* Private */,
       9,    0,  127,    2, 0x08 /* Private */,
      10,    2,  128,    2, 0x08 /* Private */,
      14,    0,  133,    2, 0x08 /* Private */,
      15,    1,  134,    2, 0x08 /* Private */,
      17,    1,  137,    2, 0x08 /* Private */,
      18,    0,  140,    2, 0x08 /* Private */,
      19,    0,  141,    2, 0x08 /* Private */,
      20,    1,  142,    2, 0x08 /* Private */,
      22,    0,  145,    2, 0x08 /* Private */,
      23,    0,  146,    2, 0x08 /* Private */,
      24,    0,  147,    2, 0x08 /* Private */,
      25,    0,  148,    2, 0x08 /* Private */,
      26,    1,  149,    2, 0x08 /* Private */,
      28,    1,  152,    2, 0x08 /* Private */,
      29,    1,  155,    2, 0x08 /* Private */,
      30,    1,  158,    2, 0x08 /* Private */,
      31,    1,  161,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4, QMetaType::Int,    5,    6,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11, QMetaType::Short,   12,   13,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   16,
    QMetaType::Void, QMetaType::Bool,   16,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   21,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   27,
    QMetaType::Void, QMetaType::Int,   27,
    QMetaType::Void, QMetaType::Int,   27,
    QMetaType::Void, QMetaType::Int,   27,
    QMetaType::Void, QMetaType::Int,   27,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->addRandomGraph(); break;
        case 1: _t->graphClicked((*reinterpret_cast< QCPAbstractPlottable*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: _t->timerTick(); break;
        case 3: _t->mousePress(); break;
        case 4: _t->mouseWheel(); break;
        case 5: _t->showResponse((*reinterpret_cast< const quint16*(*)>(_a[1])),(*reinterpret_cast< qint16(*)>(_a[2]))); break;
        case 6: _t->on_btnRefresh_clicked(); break;
        case 7: _t->on_chkIntTime_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->on_chkAverage_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->on_toolButton_clicked(); break;
        case 10: _t->on_btnConnect_clicked(); break;
        case 11: _t->on_cbXAxis_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->on_btnPlay_clicked(); break;
        case 13: _t->on_btnPause_clicked(); break;
        case 14: _t->on_btnStop_clicked(); break;
        case 15: _t->on_btnSetIntTime_clicked(); break;
        case 16: _t->on_chkLamp_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->on_chkUV_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 18: _t->on_sbAverage_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 20: _t->on_chkSpectral_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QCPAbstractPlottable* >(); break;
            }
            break;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
