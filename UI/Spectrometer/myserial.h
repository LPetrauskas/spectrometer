#ifndef MYSERIAL_H
#define MYSERIAL_H

#include <QMainWindow>
#include <QSerialPort>
#include <QDebug>
#include <QThread>
#include <QMessageBox>
#include <QObject>


#define SERIAL_BUFFER_SIZE 3600

class mySerial
{
public:
    mySerial();
    ~mySerial();

    bool connect(const QString &portName);

    bool setIntegrationTime(quint16 time);
    bool setLamp(int value);

    qint64 readData(quint16* data, quint64 size);

public:
    bool isConnected();

private:

    QSerialPort serial;


    QString currentPortName;
    int m_waitTimeout = -1;
};

#endif // MYSERIAL_H
