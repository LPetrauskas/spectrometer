#include "myserial.h"

mySerial::mySerial()
{

}

qint64 mySerial::readData(quint16* data, quint64 size)
{
    quint64 read = 0;
    qint16 flag=0;
    if(serial.isOpen())
    {

        /* Flush recieve buffer */
        serial.clear(QSerialPort::AllDirections);
        // write request

       // serial.read((char*)&flag, sizeof(flag));
        serial.write("Z", 1);
        if (serial.waitForBytesWritten(-1))
        {
            // read response
            QThread::usleep(100000);
            if (serial.waitForReadyRead(2000))
            {
    //            serial.read((char*)&flag, sizeof(flag));

           //     if(flag == -1){
                    do{
                        //serial.waitForReadyRead(2000);
                        QThread::usleep(10000);
                        read += serial.read((char*) data, size);
                      }while(read < size);
                  //  QThread::usleep(1000000);
               //
             //   }
            }else
            {

                qDebug() << "Wait read response timeout";
            }
        }else
        {

            qDebug() << "Wait write request timeout";
        }

    }
    return read;
}



bool mySerial::connect(const QString &portName)
{
    bool currentPortNameChanged = false;
    bool isConnected = true;
    qint16 ticks = 10;

    QString currentPortName;

    /* Checkeo si cambió el nombre del puerto */
    if (currentPortName != portName)
    {
        currentPortName = portName;
        currentPortNameChanged = true;
    }else
    {
        currentPortNameChanged = false;
    }


    if (currentPortName.isEmpty())
    {
        isConnected = false;
    }


    if (currentPortNameChanged)
    {
        serial.close();
        serial.setPortName(currentPortName);
        /* Attempt to open several times in order to give time
         * to enumerate properly */
        do{
            if (!serial.open(QIODevice::ReadWrite))
            {

                qDebug() << serial.error();
                isConnected = false;
            }
            QThread::usleep(100);

        }while(ticks-- && !isConnected);
        /* If unable to open port, throw error message */
        if(!isConnected)
        {
            QMessageBox msgBox;
            msgBox.setText("Could not open Serial Port");
            msgBox.setInformativeText((serial.errorString()));
            msgBox.setStandardButtons(QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);
            msgBox.exec();
        }else
        {

            serial.flush();
        }

    }

    return isConnected;
}


bool mySerial::setIntegrationTime(quint16 time)
{
    if(serial.isOpen())
    {
        serial.write("S", 1);
        serial.write((char*)&time, sizeof(time));

    }
    return true;
}

bool mySerial::setLamp(int value)
{
    if(serial.isOpen())
    {
        serial.write("K", 1);
        serial.write((char*)&value, sizeof(value));

    }
    return true;
}



bool mySerial::isConnected(void)
{
    return serial.isOpen();
}

 mySerial::~mySerial(void)
{
     if(serial.isOpen()){
        serial.flush();
        serial.close();
     }

}
