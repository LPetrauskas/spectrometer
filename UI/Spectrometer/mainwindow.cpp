#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSerialPortInfo>
#include <QTimer>
#include <QtMath>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    /* Resize QVector for appropiate dimensions */
    x.resize(PIXEL_AMOUNT);
    y.resize(PIXEL_AMOUNT);
    calibration.resize(PIXEL_AMOUNT);

    int mu = 550;
    int sd = 220;
    for(int i=0 ; i < PIXEL_AMOUNT ; i++)
    {
        double j = MIN_WAVELENGTH + ((MAX_WAVELENGTH-MIN_WAVELENGTH)*i/PIXEL_AMOUNT);
        calibration[i] = qExp(-((j-mu)*(j-mu))/(2*sd*sd));
    }
    /* Initializes table to be populated with data */
    ui->table->setRowCount(PIXEL_AMOUNT);
    ui->table->setColumnCount(1);
    ui->table->setColumnWidth(0,120);
    ui->table->setHorizontalHeaderLabels(QStringList("Data"));

    ui->table->setItem(0, 0, new QTableWidgetItem("Hello"));
    ui->lblSerial->setStyleSheet("font-weight: normal; color: red;");

    /* Setup User interface default values */
    ui->sbIntegrationTime->setValue(DEFAULT_INTEGRATION_TIME);
    ui->sbAverage->setValue(DEFAULT_AVERAGE_VALUE);
    if(!(ui->chkIntTime->isChecked()))
    {
        ui->cbTimeUnit->setDisabled(true);
        ui->sbIntegrationTime->setDisabled(true);
    }

    QHeaderView* header = ui->table->horizontalHeader();
    header->setSectionResizeMode(QHeaderView::Stretch);

    if(!(ui->chkAverage->isChecked()))
    {
        ui->sbAverage->setDisabled(true);
    }

    ui->sbAverage->setMaximum(MAX_AVERAGE_VALUE);
    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos)
        ui->cbSerial->addItem(info.portName());


    ui->customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes | QCP::iSelectLegend | QCP::iSelectPlottables);
    ui->customPlot->xAxis->setRange(MIN_WAVELENGTH, MAX_WAVELENGTH);
    ui->customPlot->yAxis->setRange(0, MAX_COUNTS);
    ui->customPlot->axisRect()->setupFullAxesBox();

    ui->customPlot->xAxis->setLabel("Wavelength [nm]");
    ui->customPlot->yAxis->setLabel("Intensity [counts]");


    connect(&timer, SIGNAL(timeout()), this, SLOT(timerTick()));
    // connect slots that takes care that when an axis is selected, only that direction can be dragged and zoomed:
    connect(ui->customPlot, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(mousePress()));
    connect(ui->customPlot, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(mouseWheel()));
    /* connect slot that shows a message on the status bar when data is clicked */
    connect(ui->customPlot, SIGNAL(plottableClick(QCPAbstractPlottable*,int,QMouseEvent*)), this, SLOT(graphClicked(QCPAbstractPlottable*,int)));


    addRandomGraph();
    updateAxis();
    populateTable();


}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::showResponse(const quint16 *data, qint16 bytesRead)
{
    quint16 aux = 0;


        for(int i=0;i<bytesRead/2;i++)
        {
             y[i] = 0;
        }

   for (int i=0; i<bytesRead/2; i++)
   {
    //  x[i] = i;
        aux  = (data[i] & 0x10  ) << 7;       // D11
        aux |= (data[i] & 0x20  ) << 5;       // D10
        aux |= (data[i] & 0x40  ) << 3;       // D09
        aux |= (data[i] & 0x80  ) << 1;       // D08
        aux |= (data[i] & 0x100 ) >> 1;       // D07
        aux |= (data[i] & 0x200 ) >> 3;       // D06
        aux |= (data[i] & 0x400 ) >> 5;       // D05
        aux |= (data[i] & 0x800 ) >> 7;       // D04
        aux |= (data[i] & 0x001 ) << 3;       // D03
        aux |= (data[i] & 0x002 ) << 1;       // D02
        aux |= (data[i] & 0x004 ) >> 1;       // D01
        aux |= (data[i] & 0x008 ) >> 3;       // D00
  //      y[bytesRead*0.5-i-1] = aux;
        yDataArray[currentAverage][(int)(bytesRead*0.5-i-1)] = aux;

          //y[i] = data[i];
    }
   for(int k=0 ; k < average ; k++)
   {
       for(int i=0;i<bytesRead/2;i++)
       {
            y[i] += yDataArray[k][i]/average;
       }
   }
    /* Increment average counter and limit it to the desired average value */
   currentAverage++;
   currentAverage %= average;

    if(calibIsLoaded)
        for (int i=0; i<bytesRead/2; i++)
            if(y[i] > 800)
                y[i] = y[i]*calibration[i];

    ui->customPlot->graph()->data().clear();
    ui->customPlot->graph()->setData(x, y);
    ui->customPlot->replot();

    populateTable();
}

void MainWindow::timerTick()
{
    qint16 bytesRead;
    if( my_serial.isConnected())
    {
        bytesRead = my_serial.readData(data, size);
        if(bytesRead > 0)
            showResponse(data, bytesRead);
    }
}

void MainWindow::mousePress()
{
  // if an axis is selected, only allow the direction of that axis to be dragged
  // if no axis is selected, both directions may be dragged

  if (ui->customPlot->xAxis->selectedParts().testFlag(QCPAxis::spAxis))
    ui->customPlot->axisRect()->setRangeDrag(ui->customPlot->xAxis->orientation());
  else if (ui->customPlot->yAxis->selectedParts().testFlag(QCPAxis::spAxis))
    ui->customPlot->axisRect()->setRangeDrag(ui->customPlot->yAxis->orientation());
  else
    ui->customPlot->axisRect()->setRangeDrag(Qt::Horizontal|Qt::Vertical);
}

void MainWindow::mouseWheel()
{
  // if an axis is selected, only allow the direction of that axis to be zoomed
  // if no axis is selected, both directions may be zoomed

  if (ui->customPlot->xAxis->selectedParts().testFlag(QCPAxis::spAxis))
    ui->customPlot->axisRect()->setRangeZoom(ui->customPlot->xAxis->orientation());
  else if (ui->customPlot->yAxis->selectedParts().testFlag(QCPAxis::spAxis))
    ui->customPlot->axisRect()->setRangeZoom(ui->customPlot->yAxis->orientation());
  else
    ui->customPlot->axisRect()->setRangeZoom(Qt::Horizontal|Qt::Vertical);
}

void MainWindow::graphClicked(QCPAbstractPlottable *plottable, int dataIndex)
{
  // since we know we only have QCPGraphs in the plot, we can immediately access interface1D()
  // usually it's better to first check whether interface1D() returns non-zero, and only then use it.
  QString message;
  double dataValue = plottable->interface1D()->dataMainValue(dataIndex);
  if(ui->cbXAxis->currentIndex() == 0)
    message = QString(" Wavelength: %2 nm,  Intensity: %3 counts").arg(MIN_WAVELENGTH + ((MAX_WAVELENGTH-MIN_WAVELENGTH)*dataIndex/PIXEL_AMOUNT)).arg(dataValue);
  else
    message = QString(" Count: %2 ,  Intensity: %3 counts").arg(dataIndex).arg(dataValue);
  ui->statusBar->showMessage(message, 0);

}


void MainWindow::addRandomGraph()
{
  int n = PIXEL_AMOUNT; // number of points in graph
  double xScale = (rand()/(double)RAND_MAX + 0.5)*2000;
  double yScale = (rand()/(double)RAND_MAX + 0.5)*400;
  double xOffset = (rand()/(double)RAND_MAX - 0.5)*4;
  double yOffset = (rand()/(double)RAND_MAX - 0.5)*10;
  double r1 = (rand()/(double)RAND_MAX - 0.5)*2;
  double r2 = (rand()/(double)RAND_MAX - 0.5)*2;
  double r3 = (rand()/(double)RAND_MAX - 0.5)*2;
  double r4 = (rand()/(double)RAND_MAX - 0.5)*2;
  for (int i=0; i<n; i++)
  {
    x[i] = (i/(double)n-0.5)*10.0*xScale + xOffset;
    y[i] = (qSin(x[i]*r1*5)*qSin(qCos(x[i]*r2)*r4*3)+r3*qCos(qSin(x[i])*r4*2))*yScale + yOffset;
  }

  ui->customPlot->addGraph();
  ui->customPlot->graph()->setName(QString("New graph %1").arg(ui->customPlot->graphCount()-1));
  ui->customPlot->graph()->setData(x, y);
  ui->customPlot->graph()->setLineStyle(QCPGraph::lsLine);
//  if (rand()%100 > 50)
 //   ui->customPlot->graph()->setScatterStyle(QCPScatterStyle((QCPScatterStyle::ScatterShape)(rand()%14+1)));
//  QPen graphPen;
//  graphPen.setColor(QColor(rand()%245+10, rand()%245+10, rand()%245+10));
//  graphPen.setWidthF(rand()/(double)RAND_MAX*2+1);
//  ui->customPlot->graph()->setPen(graphPen);
  ui->customPlot->replot();
}


void MainWindow::on_btnRefresh_clicked()
{
    updateAxis();
    ui->customPlot->yAxis->setRange(0, MAX_COUNTS);
    ui->customPlot->replot();
}



void MainWindow::on_chkIntTime_toggled(bool checked)
{
    if(checked)
    {
        ui->cbTimeUnit->setDisabled(false);
        ui->sbIntegrationTime->setDisabled(false);
    }else
    {
        ui->cbTimeUnit->setDisabled(true);
        ui->sbIntegrationTime->setDisabled(true);
        my_serial.setIntegrationTime(0);
    }
}

void MainWindow::on_chkAverage_toggled(bool checked)
{
    if(checked)
    {
        ui->sbAverage->setDisabled(false);
        average = ui->sbAverage->value();
    }else
    {
        ui->sbAverage->setDisabled(true);
        average = DISABLED_AVERAGE_VALUE;
    }
}

void MainWindow::on_toolButton_clicked()
{
    ui->cbSerial->clear();
    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos)
        ui->cbSerial->addItem(info.portName());
}

void MainWindow::on_btnConnect_clicked()
{
    bool isConnected;
    isConnected = my_serial.connect(ui->cbSerial->currentText());
    if(isConnected)
    {
        ui->lblSerial->setText(tr("Connected"));
        ui->lblSerial->setStyleSheet("font-weight: normal; color: green;");
    }else
    {
        ui->lblSerial->setText(tr("Disconnected"));
        ui->lblSerial->setStyleSheet("font-weight: normal; color: red;");

    }
}


void MainWindow::updateAxis(void)
{
    if (ui->cbXAxis->currentIndex() == 0)
    {
        /* X Axis is wavelength */
        for(int i = 0; i < PIXEL_AMOUNT; i++)
        {
            x[i] = MIN_WAVELENGTH + ((MAX_WAVELENGTH-MIN_WAVELENGTH)*i/PIXEL_AMOUNT);
        }
        ui->customPlot->xAxis->setRange(MIN_WAVELENGTH, MAX_WAVELENGTH);
        ui->customPlot->xAxis->setLabel("Wavelength [nm]");

    }else
    {
        /* X Axis is counts */
        for(int i = 0; i < PIXEL_AMOUNT; i++)
        {
            x[i] = i;
        }

        ui->customPlot->xAxis->setRange(0, PIXEL_AMOUNT);
        ui->customPlot->xAxis->setLabel("Counts [ ]");
    }
    ui->customPlot->graph()->data().clear();
    ui->customPlot->graph()->setData(x, y);
    ui->customPlot->replot();

}

void MainWindow::on_cbXAxis_currentIndexChanged(int index)
{
    updateAxis();
}


void MainWindow::populateTable(void)
{
    for(int i=0;i<PIXEL_AMOUNT;i++)
        ui->table->setItem(i, 0, new QTableWidgetItem(QString::number(y[i])));

}

void MainWindow::on_btnPlay_clicked()
{
    timer.start(SCREEN_REFRESH_RATE_MS);
}

void MainWindow::on_btnPause_clicked()
{
    timer.stop();
}

void MainWindow::on_btnStop_clicked()
{
    timer.stop();
    ui->customPlot->graph()->data().clear();

    ui->customPlot->replot();
}


void MainWindow::on_btnSetIntTime_clicked()
{
    if(ui->chkIntTime->isChecked())
        if(ui->cbTimeUnit->currentIndex() == 0)
            my_serial.setIntegrationTime(ui->sbIntegrationTime->text().toShort());
        else
            my_serial.setIntegrationTime(ui->sbIntegrationTime->text().toShort()*1000);
    else
        my_serial.setIntegrationTime(0);
}

void MainWindow::on_chkLamp_stateChanged(int arg1)
{
    if(arg1)
        my_serial.setLamp(2);
    else
        my_serial.setLamp(3);

}

void MainWindow::on_chkUV_stateChanged(int arg1)
{

    if(arg1)
        my_serial.setLamp(1);
    else
        my_serial.setLamp(4);
}

void MainWindow::on_sbAverage_valueChanged(int arg1)
{
    if(ui->chkAverage->isChecked())
        average = arg1;
}

void MainWindow::on_chkSpectral_stateChanged(int arg1)
{
    if(ui->chkSpectral->isChecked() && !calibIsLoaded)
    {
         calibIsLoaded = true;
    }else
    {
        calibIsLoaded = false;
    }
}
