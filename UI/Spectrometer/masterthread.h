#ifndef MASTERTHREAD_H
#define MASTERTHREAD_H

#include <QMutex>
#include <QThread>
#include <QWaitCondition>

class MasterThread : public QThread
{
    Q_OBJECT

public:
    explicit MasterThread(QObject *parent = nullptr);
    ~MasterThread();

    bool connect(const QString &portName);
    quint16 data[3600];

signals:
    void response(const quint16 *data);
    void error(const QString &s);
    void timeout(const QString &s);

public slots:
    void readData(void);

private:
    void run() override;

    QString m_portName;
    QString m_request;
    int m_waitTimeout = 0;
    QMutex m_mutex;
    QWaitCondition m_cond;
    bool m_quit = false;
    bool isConnected;
};

#endif // MASTERTHREAD_H
