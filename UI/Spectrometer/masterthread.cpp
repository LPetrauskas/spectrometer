
#include "masterthread.h"

#include <QSerialPort>
#include <QTime>
#include <QDebug>

MasterThread::MasterThread(QObject *parent) :
    QThread(parent)
{
}

MasterThread::~MasterThread()
{
    m_mutex.lock();
    m_quit = true;
    m_cond.wakeOne();
    m_mutex.unlock();
    wait();
}

bool MasterThread::connect(const QString &portName)
{
    const QMutexLocker locker(&m_mutex);
    m_portName = portName;
    m_waitTimeout = 30000;

    if (!isRunning())
        start();
    else
        m_cond.wakeOne();

    return isConnected;
}

void MasterThread::run()
{
    bool currentPortNameChanged = false;

    m_mutex.lock();
    QString currentPortName;
    if (currentPortName != m_portName) {
        currentPortName = m_portName;
        currentPortNameChanged = true;
    }

    int currentWaitTimeout = m_waitTimeout;
    QString currentRequest = m_request;
    m_mutex.unlock();
    QSerialPort serial;

    if (currentPortName.isEmpty()) {
        emit error(tr("No port name specified"));
        isConnected = false;
        return;
    }

    while (!m_quit) {
        if (currentPortNameChanged) {
            serial.close();
            serial.setPortName(currentPortName);

            if (!serial.open(QIODevice::ReadWrite)) {
                emit error(tr("Can't open %1, error code %2").arg(m_portName).arg(serial.error()));
                qDebug() << serial.error();
                isConnected = false;
                return;
            }

        }

        m_mutex.lock();
        m_cond.wait(&m_mutex);
        if (currentPortName != m_portName) {
            currentPortName = m_portName;
            currentPortNameChanged = true;
        } else {
            currentPortNameChanged = false;
        }
        currentWaitTimeout = m_waitTimeout;
        currentRequest = m_request;
        m_mutex.unlock();
    }

    isConnected = true;
    return;
}

void MasterThread::readData(void)
{
    qDebug() << "Inside readData";


    // write request
    serial.write("sweep");
    if (serial.waitForBytesWritten(m_waitTimeout))
    {
        // read response
        if (serial.waitForReadyRead(m_waitTimeout))
        {
            serial.read((char*) data, 3600);
            emit this->response(data);
        }else
        {
            emit timeout(tr("Wait read response timeout %1").arg(QTime::currentTime().toString()));
            qDebug() << "Wait read response timeout";
        }
    }else
    {
        emit timeout(tr("Wait write request timeout %1").arg(QTime::currentTime().toString()));
        qDebug() << "Wait write request timeout";
    }

}

