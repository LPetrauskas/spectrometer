#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include "qcustomplot.h"
#include "myserial.h"

#define MAX_WAVELENGTH  766
#define MIN_WAVELENGTH  318
#define MAX_COUNTS      4096
#define DEFAULT_INTEGRATION_TIME 100
#define DEFAULT_AVERAGE_VALUE 5
#define DISABLED_AVERAGE_VALUE 1
#define PIXEL_AMOUNT 1024
#define SCREEN_REFRESH_RATE_MS 1000
#define MAX_AVERAGE_VALUE 10

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void addRandomGraph();
    void graphClicked(QCPAbstractPlottable *plottable, int dataIndex);
    void timerTick();
    void mousePress();
    void mouseWheel();

    void showResponse(const quint16 *data, qint16 bytesRead);

    void on_btnRefresh_clicked();

    void on_chkIntTime_toggled(bool checked);

    void on_chkAverage_toggled(bool checked);

    void on_toolButton_clicked();

    void on_btnConnect_clicked();

    void on_cbXAxis_currentIndexChanged(int index);

    void on_btnPlay_clicked();

    void on_btnPause_clicked();

    void on_btnStop_clicked();

    void on_btnSetIntTime_clicked();

    void on_chkLamp_stateChanged(int arg1);

    void on_chkUV_stateChanged(int arg1);

    void on_sbAverage_valueChanged(int arg1);

    void on_checkBox_stateChanged(int arg1);

    void on_chkSpectral_stateChanged(int arg1);

private:
    void updateAxis(void);
    void populateTable(void);
    Ui::MainWindow *ui;

    qint64 size = PIXEL_AMOUNT*2;
    quint16 data[PIXEL_AMOUNT];
    qint16 average = DISABLED_AVERAGE_VALUE;
    qint16 currentAverage = 0;

    mySerial my_serial;
    QVector<double> x;
    QVector<double> y;
    qint16 yDataArray[MAX_AVERAGE_VALUE][PIXEL_AMOUNT];
    QTimer timer;
    bool calibIsLoaded = false;
    QVector<double> calibration;


};

#endif // MAINWINDOW_H
