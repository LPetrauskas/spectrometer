PACKAGE  := $(notdir $(shell pwd))

SRC_PATH  := src
OUT_PATH  := out
DOC_PATH  := docs 
OBJ_PATH  := $(OUT_PATH)/obj

CPPFLAGS  += -Iinc/ 
CPPFLAGS  += -DDEBUG -DCORE_M3 -D__LPC17XX__ -MMD -MP -MT "$@"
CFLAGS    += -Wall -ggdb3 -mcpu=cortex-m3 -mthumb -fdata-sections -ffunction-sections
LDFLAGS   += -nostdlib -fno-builtin -mcpu=cortex-m3 -mthumb -Xlinker -Map="$@" -Wl,--gc-sections

SRC_FILES := $(wildcard $(SRC_PATH)/*.c)
OBJ_FILES := $(patsubst $(SRC_PATH)/%.c,$(OBJ_PATH)/%.o,$(SRC_FILES)) 

LIB_FILE := $(OUT_PATH)/lib$(PACKAGE).a
MFS_FILE := manifest.mk

vpath %.c $(SRC_PATH)

all:	$(LIB_FILE) $(MFS_FILE)
	@echo "*** Built package $(PACKAGE) ***"

html:
	@echo "*** Generate package $(PACKAGE) documentation ***"
	$(DXMOD) $(DXFLAGS) | $(DXGEN) -

clean: 
	@echo "*** Clean package $(PACKAGE) ***"
	$(RM) $(OUT_PATH) $(MFS_FILE) $(DOC_PATH)

$(LIB_FILE): $(OBJ_FILES)

$(OUT_PATH)/%.a: 
	@echo "*** Archiving library $@ ***"
	@$(MKDIR) $(@D)
	$(AR) $@ $^
	$(SIZE) $@

$(OBJ_PATH)/%.o: %.c
	@echo "*** Compiling C file $< ***"
	@$(MKDIR) $(@D)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

$(OBJ_PATH)/%.o: %.S
	@echo "*** Compiling Assembly file $< ***"
	@$(MKDIR) $(@D)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

export

%.mk: %.mk.in
	@echo "*** Writing Manifest file $@ ***"	
	cat < $< | envsubst > $@ 

-include $(OBJ_FILES:.o=.d)
