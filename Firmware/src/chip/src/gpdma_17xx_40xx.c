/*
 * @brief LPC17xx/40xx GPDMA driver
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2014
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#include "chip.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/* Handles per channel, per transfer  */ 
static GPDMA_HANDLE_T GPDMA_HNDPerChannel[GPDMA_NUMBER_CHANNELS];

/* Callbacks per channel  */ 
static struct {
  GPDMA_CBK_T Call;
  void *Arg;
} GPDMA_CBKPerChannel[GPDMA_NUMBER_CHANNELS];

/* Handle macros  */ 
#define GPDMA_HANDLE_Init(ch) GPDMA_HNDPerChannel[(ch)] = 0
#define GPDMA_HANDLE_Make(ch) ((++GPDMA_HNDPerChannel[(ch)] << 8) & (ch))
#define GPDMA_HANDLE_Check(hnd) (GPDMA_HNDPerChannel[GPDMA_HANDLE_Channel(hnd)] == (hnd))
#define GPDMA_HANDLE_Channel(hnd)  ((uint8_t)((hnd) && 0xFF))

#if defined(CHIP_LPC177X_8X) || defined(CHIP_LPC40XX)
/* Optimized Peripheral Source and Destination burst size (177x_8x,407x_8x) */
static const uint8_t GPDMA_LUTPerBurst[] = {
	0,								/* Reserved */
	GPDMA_BSIZE_8,					/* SD Card */
	GPDMA_BSIZE_4,					/* SSP0 Tx */
	GPDMA_BSIZE_4,					/* SSP0 Rx */
	GPDMA_BSIZE_4,					/* SSP1 Tx */
	GPDMA_BSIZE_4,					/* SSP1 Rx */
	GPDMA_BSIZE_4,					/* SSP2 Tx */
	GPDMA_BSIZE_4,					/* SSP2 Rx */
	GPDMA_BSIZE_1,					/* ADC */
	GPDMA_BSIZE_1,					/* DAC */
	GPDMA_BSIZE_1,					/* UART0 Tx */
	GPDMA_BSIZE_1,					/* UART0 Rx */
	GPDMA_BSIZE_1,					/* UART1 Tx */
	GPDMA_BSIZE_1,					/* UART1 Rx */
	GPDMA_BSIZE_1,					/* UART2 Tx */
	GPDMA_BSIZE_1,					/* UART2 Rx */
	GPDMA_BSIZE_1,					/* MAT0.0 */
	GPDMA_BSIZE_1,					/* MAT0.1 */
	GPDMA_BSIZE_1,					/* MAT1.0 */
	GPDMA_BSIZE_1,					/* MAT1.1 */
	GPDMA_BSIZE_1,					/* MAT2.0 */
	GPDMA_BSIZE_1,					/* MAT2.1 */
	GPDMA_BSIZE_32,					/* I2S channel 0 */
	GPDMA_BSIZE_32,					/* I2S channel 1 */
	0,								/* Reserved */
	0,								/* Reserved */
	GPDMA_BSIZE_1,					/* UART3 Tx */
	GPDMA_BSIZE_1,					/* UART3 Rx */
	GPDMA_BSIZE_1,					/* UART4 Tx */
	GPDMA_BSIZE_1,					/* UART4 Rx */
	GPDMA_BSIZE_1,					/* MAT3.0 */
	GPDMA_BSIZE_1,					/* MAT3.1 */
};

/* Optimized Peripheral Source and Destination transfer width (177x_8x,407x_8x) */
static const uint8_t GPDMA_LUTPerWid[] =  {
	0,									/* Reserved */
	GPDMA_WIDTH_WORD,					/* SD Card */
	GPDMA_WIDTH_BYTE,					/* SSP0 Tx */
	GPDMA_WIDTH_BYTE,					/* SSP0 Rx */
	GPDMA_WIDTH_BYTE,					/* SSP1 Tx */
	GPDMA_WIDTH_BYTE,					/* SSP1 Rx */
	GPDMA_WIDTH_BYTE,					/* SSP2 Tx */
	GPDMA_WIDTH_BYTE,					/* SSP2 Rx */
	GPDMA_WIDTH_WORD,					/* ADC */
	GPDMA_WIDTH_HALFWORD,   				/* DAC */
	GPDMA_WIDTH_BYTE,					/* UART0 Tx */
	GPDMA_WIDTH_BYTE,					/* UART0 Rx */
	GPDMA_WIDTH_BYTE,					/* UART1 Tx */
	GPDMA_WIDTH_BYTE,					/* UART1 Rx */
	GPDMA_WIDTH_BYTE,					/* UART2 Tx */
	GPDMA_WIDTH_BYTE,					/* UART2 Rx */
	GPDMA_WIDTH_WORD,					/* MAT0.0 */
	GPDMA_WIDTH_WORD,					/* MAT0.1 */
	GPDMA_WIDTH_WORD,					/* MAT1.0 */
	GPDMA_WIDTH_WORD,					/* MAT1.1 */
	GPDMA_WIDTH_WORD,					/* MAT2.0 */
	GPDMA_WIDTH_WORD,					/* MAT2.1 */
	GPDMA_WIDTH_WORD,					/* I2S channel 0 */
	GPDMA_WIDTH_WORD,					/* I2S channel 1 */
	0,									/* Reserved */
	0,									/* Reserved */
	GPDMA_WIDTH_BYTE,					/* UART3 Tx */
	GPDMA_WIDTH_BYTE,					/* UART3 Rx */
	GPDMA_WIDTH_BYTE,					/* UART4 Tx */
	GPDMA_WIDTH_BYTE,					/* UART4 Rx */
	GPDMA_WIDTH_WORD,					/* MAT3.0 */
	GPDMA_WIDTH_WORD,					/* MAT3.1 */
};

/* Lookup Table of Connection Type matched with (177x_8x,407x_8x) Peripheral Data (FIFO) register base address */
volatile static const void *GPDMA_LUTPerAddr[] = {
	0,									/* Reserved */
	(&LPC_SDC->FIFO),					/* SD Card */
	(&LPC_SSP0->DR),					/* SSP0 Tx */
	(&LPC_SSP0->DR),					/* SSP0 Rx */
	(&LPC_SSP1->DR),					/* SSP1 Tx */
	(&LPC_SSP1->DR),					/* SSP1 Rx */
	(&LPC_SSP2->DR),					/* SSP2 Tx */
	(&LPC_SSP2->DR),					/* SSP2 Rx */
	(&LPC_ADC->GDR),					/* ADC */
	(&LPC_DAC->CR),						/* DAC */
	(&LPC_UART0-> /*RBTHDLR.*/ THR),	/* UART0 Tx */
	(&LPC_UART0-> /*RBTHDLR.*/ RBR),	/* UART0 Rx */
	(&LPC_UART1-> /*RBTHDLR.*/ THR),	/* UART1 Tx */
	(&LPC_UART1-> /*RBTHDLR.*/ RBR),	/* UART1 Rx */
	(&LPC_UART2-> /*RBTHDLR.*/ THR),	/* UART2 Tx */
	(&LPC_UART2-> /*RBTHDLR.*/ RBR),	/* UART2 Rx */
	(&LPC_TIMER0->MR[0]),				/* MAT0.0 */
	(&LPC_TIMER0->MR[1]),				/* MAT0.1 */
	(&LPC_TIMER1->MR[0]),				/* MAT1.0 */
	(&LPC_TIMER1->MR[1]),				/* MAT1.1 */
	(&LPC_TIMER2->MR[0]),				/* MAT2.0 */
	(&LPC_TIMER2->MR[1]),				/* MAT2.1 */
	(&LPC_I2S->TXFIFO),					/* I2S Tx */
	(&LPC_I2S->RXFIFO),					/* I2S Rx */
	0,									/* Reverse */
	0,									/* Reverse */
	(&LPC_UART3-> /*RBTHDLR.*/ THR),	/* UART3 Tx */
	(&LPC_UART3-> /*RBTHDLR.*/ RBR),	/* UART3 Rx */
	(&LPC_UART4-> /*RBTHDLR.*/ THR),	/* UART4 Tx */
	(&LPC_UART4-> /*RBTHDLR.*/ RBR),	/* UART4 Rx */
	(&LPC_TIMER3->MR[0]),				/* MAT3.0 */
	(&LPC_TIMER3->MR[1])				/* MAT3.1 */
};

#elif defined(CHIP_LPC175X_6X)
const uint8_t GPDMA_LUTPerBurst[] = {
	GPDMA_BSIZE_4,					// SSP0 Tx
	GPDMA_BSIZE_4,					// SSP0 Rx
	GPDMA_BSIZE_4,					// SSP1 Tx
	GPDMA_BSIZE_4,					// SSP1 Rx
	GPDMA_BSIZE_1,					// ADC
	GPDMA_BSIZE_32,					// I2S channel 0
	GPDMA_BSIZE_32,					// I2S channel 1
	GPDMA_BSIZE_1,					// DAC
	GPDMA_BSIZE_1,					// UART0 Tx
	GPDMA_BSIZE_1,					// UART0 Rx
	GPDMA_BSIZE_1,					// UART1 Tx
	GPDMA_BSIZE_1,					// UART1 Rx
	GPDMA_BSIZE_1,					// UART2 Tx
	GPDMA_BSIZE_1,					// UART2 Rx
	GPDMA_BSIZE_1,					// UART3 Tx
	GPDMA_BSIZE_1,					// UART3 Rx
	GPDMA_BSIZE_1,					// MAT0.0
	GPDMA_BSIZE_1,					// MAT0.1
	GPDMA_BSIZE_1,					// MAT1.0
	GPDMA_BSIZE_1,					// MAT1.1
	GPDMA_BSIZE_1,					// MAT2.0
	GPDMA_BSIZE_1,					// MAT2.1
	GPDMA_BSIZE_1,					// MAT3.0
	GPDMA_BSIZE_1					// MAT3.1
};

/**
 * @brief Optimized Peripheral Source and Destination transfer width
 */
const uint8_t GPDMA_LUTPerWid[] = {
	GPDMA_WIDTH_BYTE,					// SSP0 Tx
	GPDMA_WIDTH_BYTE,					// SSP0 Rx
	GPDMA_WIDTH_BYTE,					// SSP1 Tx
	GPDMA_WIDTH_BYTE,					// SSP1 Rx
	GPDMA_WIDTH_WORD,					// ADC
	GPDMA_WIDTH_WORD,					// I2S channel 0
	GPDMA_WIDTH_WORD,					// I2S channel 1
	GPDMA_WIDTH_HALFWORD,		       		        // DAC
	GPDMA_WIDTH_BYTE,					// UART0 Tx
	GPDMA_WIDTH_BYTE,					// UART0 Rx
	GPDMA_WIDTH_BYTE,					// UART1 Tx
	GPDMA_WIDTH_BYTE,					// UART1 Rx
	GPDMA_WIDTH_BYTE,					// UART2 Tx
	GPDMA_WIDTH_BYTE,					// UART2 Rx
	GPDMA_WIDTH_BYTE,					// UART3 Tx
	GPDMA_WIDTH_BYTE,					// UART3 Rx
	GPDMA_WIDTH_WORD,					// MAT0.0
	GPDMA_WIDTH_WORD,					// MAT0.1
	GPDMA_WIDTH_WORD,					// MAT1.0
	GPDMA_WIDTH_WORD,					// MAT1.1
	GPDMA_WIDTH_WORD,					// MAT2.0
	GPDMA_WIDTH_WORD,					// MAT2.1
	GPDMA_WIDTH_WORD,					// MAT3.0
	GPDMA_WIDTH_WORD					// MAT3.1
};

/**
 * @brief Peripheral Source and Destination address
 */
volatile const void *GPDMA_LUTPerAddr[] = {
	(&LPC_SSP0->DR),					// SSP0 Tx
	(&LPC_SSP0->DR),					// SSP0 Rx
	(&LPC_SSP1->DR),					// SSP1 Tx
	(&LPC_SSP1->DR),					// SSP1 Rx
	(&LPC_ADC->DR),  				// ADC
	(&LPC_I2S->TXFIFO),				// I2S Tx
	(&LPC_I2S->RXFIFO),				// I2S Rx
	(&LPC_DAC->CR),					// DAC
	(&LPC_UART0-> /*RBTHDLR.*/ THR),	// UART0 Tx
	(&LPC_UART0-> /*RBTHDLR.*/ RBR),	// UART0 Rx
	(&LPC_UART1-> /*RBTHDLR.*/ THR),	// UART1 Tx
	(&LPC_UART1-> /*RBTHDLR.*/ RBR),	// UART1 Rx
	(&LPC_UART2-> /*RBTHDLR.*/ THR),	// UART2 Tx
	(&LPC_UART2-> /*RBTHDLR.*/ RBR),	// UART2 Rx
	(&LPC_UART3-> /*RBTHDLR.*/ THR),	// UART3 Tx
	(&LPC_UART3-> /*RBTHDLR.*/ RBR),	// UART3 Rx
	(&LPC_TIMER0->MR[0]),					// MAT0.0
	(&LPC_TIMER0->MR[1]),					// MAT0.1
	(&LPC_TIMER1->MR[0]),					// MAT1.0
	(&LPC_TIMER1->MR[1]),					// MAT1.1
	(&LPC_TIMER2->MR[0]),					// MAT2.0
	(&LPC_TIMER2->MR[1]),					// MAT2.1
	(&LPC_TIMER3->MR[0]),					// MAT3.0
	(&LPC_TIMER3->MR[1])					// MAT3.1

};
#endif

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/* Control which set of peripherals is connected to the DMA controller */
STATIC uint8_t configDMAMux(uint32_t gpdma_peripheral_connection_number)
{
  gpdma_peripheral_connection_number = GPDMA_CONN_BASE(gpdma_peripheral_connection_number);
#if defined(CHIP_LPC175X_6X)
  if (gpdma_peripheral_connection_number > 15) {
    LPC_SYSCTL->DMAREQSEL |= (1 << (gpdma_peripheral_connection_number - 16));
    return gpdma_peripheral_connection_number - 8;
  }
  else {
    if (gpdma_peripheral_connection_number > 7) {
      LPC_SYSCTL->DMAREQSEL &= ~(1 << (gpdma_peripheral_connection_number - 8));
    }
    return gpdma_peripheral_connection_number;
  }
#elif defined(CHIP_LPC177X_8X) || defined(CHIP_LPC40XX)
  if (gpdma_peripheral_connection_number > 15) {
    LPC_SYSCTL->DMAREQSEL |= (1 << (gpdma_peripheral_connection_number - 16));
    return gpdma_peripheral_connection_number - 16;
  }
  else {
    LPC_SYSCTL->DMAREQSEL &= ~(1 << (gpdma_peripheral_connection_number));
    return gpdma_peripheral_connection_number;
  }
#endif
}

STATIC uint8_t configDMAChannel(LPC_GPDMA_T *pGPDMA, uint8_t gpdma_min_priority) {  
  uint8_t channelNum = GPDMA_NUMBER_CHANNELS - 1;
  if (gpdma_min_priority < channelNum) {
    channelNum = gpdma_min_priority;
  } 
  do {
    if (!(pGPDMA->ENBLDCHNS & GPDMA_DMACHx_BitMask(channelNum))) {
      return channelNum;
    }
  } while(channelNum--);
  return GPDMA_NUMBER_CHANNELS;
}


STATIC void handleDMAInts(LPC_GPDMA_T *pGPDMA, uint8_t gpdma_channel) {
  GPDMA_STATUS_T status; uint32_t gpdma_channel_mask;
  gpdma_channel_mask = GPDMA_DMACHx_BitMask(gpdma_channel);
  if (pGPDMA->INTSTAT & gpdma_channel_mask) {
    if (pGPDMA->INTTCSTAT & gpdma_channel_mask) {
      status = GPDMA_STAT_TC;
      pGPDMA->INTTCCLEAR = gpdma_channel_mask;
    } 
    if (pGPDMA->INTERRSTAT & gpdma_channel_mask) {
      status = GPDMA_STAT_ERR;
      pGPDMA->INTERRCLR = gpdma_channel_mask;
    } 
    if (GPDMA_CBKPerChannel[gpdma_channel].Call) {
      GPDMA_CBKPerChannel[gpdma_channel].Call(status, GPDMA_CBKPerChannel[gpdma_channel].Arg);
    }
  }
}

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/* Initialize the GPDMA */
void Chip_GPDMA_Init(LPC_GPDMA_T *pGPDMA) {
  uint8_t channelNum;

  Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_GPDMA);

  /* Reset all channel configuration register */
  channelNum = GPDMA_NUMBER_CHANNELS;
  while (channelNum--) {
    pGPDMA->CH[channelNum].CONFIG = 0;
    GPDMA_HANDLE_Init(channelNum);
  }
  
  /* Clear all DMA interrupt and error flag */
  pGPDMA->INTTCCLEAR = 0xFF;
  pGPDMA->INTERRCLR = 0xFF;
}

/* Shutdown the GPDMA */
void Chip_GPDMA_DeInit(LPC_GPDMA_T *pGPDMA) {
  Chip_Clock_DisablePeriphClock(SYSCTL_CLOCK_GPDMA);
}

/* The GPDMA stream interrupt status checking */
void Chip_GPDMA_Interrupt(LPC_GPDMA_T *pGPDMA) {
  uint8_t channelNum;
  for (channelNum = 0;channelNum < GPDMA_NUMBER_CHANNELS;channelNum++) {
    handleDMAInts(pGPDMA, channelNum);
  }
}

Status Chip_GPDMA_PrepareDescriptor(LPC_GPDMA_T *pGPDMA, GPDMA_CFG_T *pCFG, GPDMA_DESC_T *pDESC)
{
   switch (pCFG->XferType) {
	/* Memory to memory */
	case GPDMA_TRANSFERTYPE_M2M_CONTROLLER_DMA:
	  pDESC->CtrlWord = GPDMA_DMACCxControl_TransferSize(pDESC->Size)
	    | GPDMA_DMACCxControl_SBSize((4UL))				/**< Burst size = 32 */
	    | GPDMA_DMACCxControl_DBSize((4UL))				/**< Burst size = 32 */
	    | GPDMA_DMACCxControl_SWidth(pCFG->XferWidth)
	    | GPDMA_DMACCxControl_DWidth(pCFG->XferWidth)
	    | GPDMA_DMACCxControl_SI
	    | GPDMA_DMACCxControl_DI
	    | GPDMA_DMACCxControl_I;
	  break;
	case GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA:
	case GPDMA_TRANSFERTYPE_M2P_CONTROLLER_PERIPHERAL:
	  pDESC->DstAddr = (uint32_t)(((uint32_t *) GPDMA_LUTPerAddr[GPDMA_CONN_BASE(pCFG->DstConn)]) + GPDMA_CONN_IDX(pCFG->DstConn));
	  pDESC->CtrlWord = GPDMA_DMACCxControl_TransferSize(pDESC->Size)
	    | GPDMA_DMACCxControl_SBSize(GPDMA_LUTPerBurst[GPDMA_CONN_BASE(pCFG->DstConn)])
	    | GPDMA_DMACCxControl_DBSize(GPDMA_LUTPerBurst[GPDMA_CONN_BASE(pCFG->DstConn)])
	    | GPDMA_DMACCxControl_SWidth(GPDMA_LUTPerWid[GPDMA_CONN_BASE(pCFG->DstConn)])
	    | GPDMA_DMACCxControl_DWidth(GPDMA_LUTPerWid[GPDMA_CONN_BASE(pCFG->DstConn)])
	    | GPDMA_DMACCxControl_DestTransUseAHBMaster1
	    | GPDMA_DMACCxControl_SI
	    | GPDMA_DMACCxControl_I;
	  break;
	case GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA:
	case GPDMA_TRANSFERTYPE_P2M_CONTROLLER_PERIPHERAL:
	  pDESC->SrcAddr = (uint32_t)(((uint32_t *) GPDMA_LUTPerAddr[GPDMA_CONN_BASE(pCFG->SrcConn)]) + GPDMA_CONN_IDX(pCFG->SrcConn));
	  pDESC->CtrlWord = GPDMA_DMACCxControl_TransferSize((uint32_t) pDESC->Size)
	    | GPDMA_DMACCxControl_SBSize(GPDMA_LUTPerBurst[GPDMA_CONN_BASE(pCFG->SrcConn)])
	    | GPDMA_DMACCxControl_DBSize(GPDMA_LUTPerBurst[GPDMA_CONN_BASE(pCFG->SrcConn)])
	    | GPDMA_DMACCxControl_SWidth(GPDMA_LUTPerWid[GPDMA_CONN_BASE(pCFG->SrcConn)])
	    | GPDMA_DMACCxControl_DWidth(GPDMA_LUTPerWid[GPDMA_CONN_BASE(pCFG->SrcConn)])
	    | GPDMA_DMACCxControl_SrcTransUseAHBMaster1
	    | GPDMA_DMACCxControl_DI
	    | GPDMA_DMACCxControl_I;
	  break;
	case GPDMA_TRANSFERTYPE_P2P_CONTROLLER_DMA:
	case GPDMA_TRANSFERTYPE_P2P_CONTROLLER_DestPERIPHERAL:
	case GPDMA_TRANSFERTYPE_P2P_CONTROLLER_SrcPERIPHERAL:
	  pDESC->SrcAddr = (uint32_t)(((uint32_t *) GPDMA_LUTPerAddr[GPDMA_CONN_BASE(pCFG->SrcConn)]) + GPDMA_CONN_IDX(pCFG->SrcConn));
	  pDESC->DstAddr = (uint32_t)(((uint32_t *) GPDMA_LUTPerAddr[GPDMA_CONN_BASE(pCFG->DstConn)]) + GPDMA_CONN_IDX(pCFG->DstConn));
	  pDESC->CtrlWord = GPDMA_DMACCxControl_TransferSize(pDESC->Size)
	    | GPDMA_DMACCxControl_SBSize(GPDMA_LUTPerBurst[GPDMA_CONN_BASE(pCFG->SrcConn)])
	    | GPDMA_DMACCxControl_DBSize(GPDMA_LUTPerBurst[GPDMA_CONN_BASE(pCFG->DstConn)])
	    | GPDMA_DMACCxControl_SWidth(GPDMA_LUTPerWid[GPDMA_CONN_BASE(pCFG->SrcConn)])
	    | GPDMA_DMACCxControl_DWidth(GPDMA_LUTPerWid[GPDMA_CONN_BASE(pCFG->DstConn)])
	    | GPDMA_DMACCxControl_SrcTransUseAHBMaster1
	    | GPDMA_DMACCxControl_DestTransUseAHBMaster1
	    | GPDMA_DMACCxControl_I;
	  break;
   }   
   /* By default set interrupt only for last transfer */
   if (pDESC->LinkAddr) {
      pDESC->CtrlWord &= ~GPDMA_DMACCxControl_I;
   }

   return SUCCESS;
}

/* Do a DMA scatter-gather transfer M2M, M2P,P2M or P2P using DMA descriptors */
GPDMA_HANDLE_T Chip_GPDMA_Transfer(LPC_GPDMA_T *pGPDMA, GPDMA_CFG_T *pCFG, GPDMA_DESC_T *pDESC, uint32_t minPriority) {
  uint8_t SrcPeriph, DstPeriph;
  GPDMA_CH_T *pCH; uint32_t channelNum;
  
  if (pCFG->SrcConn) {
    SrcPeriph = configDMAMux(pCFG->SrcConn);
  } else SrcPeriph = 0;
  
  if (pCFG->DstConn) {
    DstPeriph = configDMAMux(pCFG->DstConn);
  } else DstPeriph = 0;

  channelNum = configDMAChannel(pGPDMA, (uint8_t) minPriority);
  if (channelNum < GPDMA_NUMBER_CHANNELS) {
    pGPDMA->INTTCCLEAR = GPDMA_DMACHx_BitMask(channelNum);
    pGPDMA->INTERRCLR = GPDMA_DMACHx_BitMask(channelNum);
  
    /* Enable DMA channels, little endian */
    pGPDMA->CONFIG = GPDMA_DMACConfig_E;
    while (!(pGPDMA->CONFIG & GPDMA_DMACConfig_E));
	
    pCH = (GPDMA_CH_T *) &(pGPDMA->CH[channelNum]);
    /* Assign Transfer Addresses*/
    pCH->SRCADDR = pDESC->SrcAddr;
    pCH->DESTADDR = pDESC->DstAddr;
    /* Setup Transfer */
    pCH->CONTROL = pDESC->CtrlWord;
    /* Assign Linked List Item value */
    pCH->LLI = pDESC->LinkAddr;	
    /* Configure DMA Channel, enable Error Counter and Terminate counter */
    pCH->CONFIG = GPDMA_DMACCxConfig_IE
      | GPDMA_DMACCxConfig_ITC		/*| GPDMA_DMACCxConfig_E*/
      | GPDMA_DMACCxConfig_TransferType((uint32_t)pCFG->XferType)
      | GPDMA_DMACCxConfig_SrcPeripheral(SrcPeriph)
      | GPDMA_DMACCxConfig_DestPeripheral(DstPeriph);
    /* Setup callback + arg */
    GPDMA_CBKPerChannel[channelNum].Call = pCFG->Callback;
    GPDMA_CBKPerChannel[channelNum].Arg = pCFG->Arg;

    pCH->CONFIG |= GPDMA_DMACCxConfig_E;  
    return GPDMA_HANDLE_Make(channelNum);
  }
  return GPDMA_HANDLE_NULL;
}

/* Stop a stream DMA transfer */
Status Chip_GPDMA_Stop(LPC_GPDMA_T *pGPDMA, GPDMA_HANDLE_T Handle) {
  GPDMA_CH_T *pDMAch; uint8_t channelNum;
  if (GPDMA_HANDLE_Check(Handle)) {
    return ERROR;
  }
  channelNum = GPDMA_HANDLE_Channel(Handle);
  if (!(pGPDMA->ENBLDCHNS & GPDMA_DMACHx_BitMask(channelNum))) {
      return ERROR;
  }
  pDMAch = (GPDMA_CH_T *) &(pGPDMA->CH[channelNum]);
  pDMAch->CONFIG &= ~GPDMA_DMACCxConfig_E;

  handleDMAInts(pGPDMA, channelNum);
  return SUCCESS;
}
