#ifndef ANALYSIS_H
#define ANALYSIS_H

#include "cmpaction.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup app_analysis Analysis Actions
 * @ingroup app
 * @brief Analysis actions implementation
 * @related app_cmp_action
 * @{
 */

/**
 * @brief Analysis window (chunk) max size
 */
#define ANALYSIS_WINDOW_SIZE 50

/**
 * @brief Filtering window size
 */
#define FILTER_WINDOW_SIZE 7

/**
 * @brief Analysis actions table
 */
extern const cmp_action_t analysis_actions[];

/**
 * @}
 */


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* ANALYSIS_H  */
