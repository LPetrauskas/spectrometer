#ifndef IO_H
#define IO_H

#include "chip.h"
#include "os.h"
#include "board.h"

#ifdef __cplusplus
extern "C"{
#endif

/**
 * @defgroup app_io Buffered IO 
 * @ingroup app
 * @brief A buffered stream implementation to avoid bloating performance 
 *        due to direct IO access.
 * @{
 */

/** 
 * @brief IO buffer max size 
 */
#define IOBUFFER_MAX_SIZE   60

/** 
 * @brief IO buffer
 */
typedef struct {
  uint8_t mem[IOBUFFER_MAX_SIZE]; /**< Underlying memory */
  uint16_t count;                 /**< Available/remaining bytes */ 
  uint16_t next;                  /**< Next byte available/consumable */ 
} iobuffer_t;

/** 
 * @brief Read callback
 * @param data : Buffer for data to be read into
 * @param size : Max amount of data to be read
 * @param flags : Additional flags
 * @return Amount of data read, or error 
 */  
typedef size_t (*ioread_t)(void *data, size_t size, uint32_t flags);

/** 
 * @brief Write callback
 * @param data : Data to be written
 * @param size : Amount of data to be written
 * @param flags : Additional flags
 * @return Amount of data written, or error 
 */  
typedef size_t (*iowrite_t)(const void *data, size_t size, uint32_t flags);

/** 
 * @brief IO stream
 */  
typedef struct {
  ioread_t read;        /**< Read function */
  iowrite_t write;      /**< Write function */
  iobuffer_t ibuffer;   /**< In buffer */
  iobuffer_t obuffer;   /**< Out buffer */
} iostream_t;

/**
 * @brief Initialize IO stream
 * @param stream   : Stream to be initialized
 * @param read     : Read function to attach
 * @param write    : Write function to attach
 * @return Nothing
 */
EXTERN void   iostream_init(iostream_t *stream, ioread_t read, iowrite_t write);

/**
 * @brief Read from IO stream
 * @param stream   : Stream to be read
 * @param data     : Buffer to read data into
 * @param size     : Max amount of readable data
 * @return Amount of data read, or error
 */
EXTERN size_t iostream_read(iostream_t *stream, void *data, size_t size);

/**
 * @brief Write to IO stream
 * @param stream   : Stream to write to
 * @param data     : Data to be written
 * @param size     : Amount of data to be written
 * @return Amount of data written, or error
 */
EXTERN size_t iostream_write(iostream_t *stream, const void *data, size_t size);

/**
 * @brief Flush out IO stream (buffered data written).
 * @param stream   : Stream to be flushed
 * @return Amount of data flushed, or error
 */
EXTERN size_t iostream_flush(iostream_t *stream);

/**
 * @}
 */


#ifdef __cplusplus
}
#endif

#endif /* IO_H */
