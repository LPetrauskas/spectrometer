

#include "os.h"
#include "board.h"

#include "io.h"
#include "cmp.h"
#include "cmpaction.h"
#include "analysis.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
#define DETECTOR_SIZE 1024
static BaseType_t ledStatus = LED_STATUS_OK;
#define CHUNK 64
/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*
struct __attribute__((__packed__)) dataStruct{
	
	uint32_t data1[600];
	uint32_t data2[600];
	uint32_t data3[600];
} data; */

union Data
{
	char charData[DETECTOR_SIZE*2];
	int16_t intData[DETECTOR_SIZE];
} data, rampa;

char errorMsg[] = "Error de lectura, soy un string muy largo que intenta replicar y el fucking CDC funciona para el ojete shalalala fucking shit\n";
char comMsg[] = "Comienzo lectura";
//char errorMsg[] = "Error de lectura";
/*****************************************************************************
 * Private functions
 ****************************************************************************/
void HardFault_Handler(void) {
	ledStatus = LED_STATUS_HARDFAULT;
	while(1);
}


static int32_t triggerReadCCD(void)
{
	vBoard_CCD_Read();
	return xBoard_ADC_Read();

}


static bool cmp_stream_reader(cmp_ctx_t *ctx, void *data, size_t limit) {      
  iostream_t *stream;
  stream = ((iostream_t *)ctx->buf);  
  return (iostream_read(stream, data, limit) == limit);
}

static size_t cmp_stream_writer(cmp_ctx_t *ctx, const void *data, size_t count) {
  iostream_t *stream;
  stream = ((iostream_t *)ctx->buf);  
  if (count > 0) {
    count = iostream_write(stream, data, count);      
  } else {
    count = iostream_flush(stream);
  }
  return count;
}

/* LED1 togglee thread */
static void vLEDTask1(void *pvParameters) {
	BaseType_t prevLedStatus = LED_STATUS_OK;
	vBoard_LED_ClearGreen();
	vBoard_LED_ClearRed();

	while (1) {
		if(prevLedStatus != ledStatus)
		{
			prevLedStatus = ledStatus;
			vBoard_LED_ClearGreen();
			vBoard_LED_ClearRed();

		}
		switch(ledStatus)
		{
			case LED_STATUS_HARDFAULT:
				vBoard_LED_ToggleRed();
				vTaskDelay(configTICK_RATE_HZ/5);
			break;

			case LED_STATUS_OK:
				vBoard_LED_ToggleGreen();
				vTaskDelay(configTICK_RATE_HZ);
			break;

			case LED_STATUS_BUSY:
				vBoard_LED_ToggleRed();
				vBoard_LED_ToggleGreen();	
				vTaskDelay(configTICK_RATE_HZ/3);			
			break;

			default:
				vBoard_LED_ToggleRed();
				vBoard_LED_ToggleGreen();	
				vTaskDelay(configTICK_RATE_HZ);			
			break;

		}
	}
}

/* LED1 togglee thread */
static void vDataTask(void *pvParameters) {
	char in;
	BaseType_t result, i;
	int16_t flag = 0xFFFF; 

	for(i = 0 ; i < DETECTOR_SIZE ; i++ )
		rampa.intData[i] = i % 4096;

	while (1) {

		xBoard_COM_Read((void*)&in, sizeof(in), 0);
		ledStatus = LED_STATUS_OK;			
		
		if(in == 'Z')
		{
			result = triggerReadCCD();
			if (result == pdFREERTOS_ERRNO_NONE)
			{
				ledStatus = LED_STATUS_BUSY;
				//xBoard_COM_Write(&flag, sizeof(flag), 0);	

				for(i = 0; i < 2*DETECTOR_SIZE; i+=CHUNK)	
				{
				//	result += xBoard_COM_Write(&rampa.charData[i], CHUNK, 0);
					result += xBoard_COM_Write(&data.charData[i], CHUNK, 0);
					vTaskDelay(configTICK_RATE_HZ/1000);
				}
				//flag = 0xFFFF;
				/* Send Zero Length packet */
				xBoard_COM_Write(&flag,0,0);	

				if(result < sizeof(data))
					ledStatus = LED_STATUS_HARDFAULT;
				else
					ledStatus = LED_STATUS_OK;
				
			}else
			{
				xBoard_COM_Write(errorMsg, sizeof(errorMsg), 0);
			}
		}
		
		if(in == 'L')
		{

			xBoard_COM_Write(errorMsg, 64, 0);
			xBoard_COM_Write(&errorMsg[64], sizeof(errorMsg)-64, 0);

		}

		/* Set integration time */
		if(in == 'S')
		{
			xBoard_COM_Read((void*)&flag, sizeof(flag), 0);
			if(flag == 0)
				xBoard_CCD_DisableElectronicShutter();
			else{
				xBoard_CCD_SetIntegrationTime(flag*1000);
				xBoard_CCD_EnableElectronicShutter();
			}
		}
		/* Set Lighting */
		if(in == 'K')
		{
			xBoard_COM_Read((void*)&flag, sizeof(flag), 0);
			switch(flag)
			{
				case 1:
					vBoard_LED_SetUV();
				break;

				case 2:
					vBoard_LED_SetHalogen();
				break;

				case 3:
					vBoard_LED_ClearHalogen();
				break;

				case 4:
					vBoard_LED_ClearUV();
				break;

				default:
					vBoard_LED_ClearUV();
					vBoard_LED_ClearHalogen();
				break;
			}

		}
		
	//	vTaskDelay(configTICK_RATE_HZ);
	}
}


static portTASK_FUNCTION(vServerTask, pvParameters) {     
  cmp_ctx_t ctx;
  cmp_action_ctx_t actx; 
  iostream_t stream;
  // setup stream
  iostream_init(&stream, (ioread_t)&xBoard_COM_Read, (iowrite_t)&xBoard_COM_Write);
  // setup mp context
  ctx.buf = &stream;
  ctx.read = &cmp_stream_reader;
  ctx.write = &cmp_stream_writer;
  // setup action mp context
  actx.ctx = &ctx;

  // serve mp actions
  while(1) {
    cmp_action_serve(&actx, analysis_actions);
  }
}



/*****************************************************************************
 * Public functions
 ****************************************************************************/

/**
 * @brief	main routine for FreeRTOS blinky example
 * @return	Nothing, function should not exit
 */
int main(void)
{

  	BaseType_t xOutcome;
	xOutcome = xBoard_Init();

// 	xTaskCreate(&vServerTask, "ActionServer", 1024, 0, tskIDLE_PRIORITY+2, 0);  
	/* LED1 toggle thread */
	xTaskCreate(vLEDTask1, (signed char *) "vTaskLed1",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	xTaskCreate(vDataTask, (signed char *) "vDataTask",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 2UL),
				(xTaskHandle *) NULL);



	/* Start the scheduler */
	vTaskStartScheduler();

	/* Should never arrive here */
	return 1;
}

/**
 * @}
 */
