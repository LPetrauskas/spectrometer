
#ifndef __ADC_H_
#define __ADC_H_


#ifdef __cplusplus
extern "C" {
#endif

/** @defgroup CCD TCD1304 block driver
 * @ingroup board
 * @{
 */
#include "hal/adc.h"
#include "chip.h"
#include "os.h"

#define ADC_DATA_MASK 0xFFF
#define ADC_DATA_OFF 18

/**
 * @brief   Initialize ADC block.
 * @return  FreeRTOS-like return
 */
BaseType_t xBoard_ADC_Init(void);

void vBoard_ADC_Enable(void);
void vBoard_ADC_Disable(void);
BaseType_t xBoard_ADC_Read(void);


/**
 * @}
 */

#ifdef __cplusplus
}
#endif


#endif /* __ADC_H_ */
