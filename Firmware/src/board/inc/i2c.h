#ifndef I2C_H
#define I2C_H

#include "chip.h"
#include "os.h"

#ifdef __cplusplus
extern "C" {
#endif

/** @defgroup board_i2c I2C Bus driver
 * @ingroup board
 * @{
 */

/**
 * @brief Chip re-definition.
 */
typedef I2C_XFER_T I2C_Transfer_t;

/**
 * @brief Chip re-definition.
 */
typedef I2C_BUFF_T I2C_Buffer_t;

/**
 * @brief	Initialize the I2C bus.
 * @return	Nothing
 */
EXTERN BaseType_t xBoard_I2C_Init(void);

/**
 * @brief	Initiate transfer on the I2C bus.
 * @param	pxTransfer : I2C transfer to be initiated
 * @param       Flags      : Flags for BSP driver operation
 * @return	FreeRTOS-like return value
 */
EXTERN BaseType_t xBoard_I2C_Transfer(I2C_Transfer_t *pxTransfer, UBaseType_t Flags);

/**
 * @brief	Deinitialize the I2C bus.
 * @return	Nothing
 */
EXTERN void vBoard_I2C_Deinit(void);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* I2C_H */
