#ifndef USB_SETUP_H
#define USB_SETUP_H

#include "os.h"
#include "chip.h"

#ifdef __cplusplus
extern "C"
{
#endif

/** @defgroup board_usb USB stack setup
 * @ingroup board
 * @{
 */

/**
 * @brief Initialize the USB stack
 * @return FreeRTOS-like return value
 */
EXTERN BaseType_t xBoard_USB_Init(void);

/**
 * @brief Deinitialize the USB stack
 * @return Nothing
 */
EXTERN void vBoard_USB_Deinit(void);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* USB_SETUP_H */
