
#ifndef __CCD_H_
#define __CCD_H_


#ifdef __cplusplus
extern "C" {
#endif

/** @defgroup CCD TCD1304 block driver
 * @ingroup board
 * @{
 */
#include "hal/ccd.h"
#include "chip.h"
#include "os.h"


/**
 * @brief typedef for match timer configuration
 */
typedef enum{
    MATCH0, MATCH1, MATCH2, MATCH3
}CCD_TIMER_MATCH_CONFIG_T;


/**
 * @brief TIMER utilized for the master clock sequence
 */
#define CCD_TIMER_MCLK LPC_TIMER2

/**
 * @brief TIMER match utilized for the master clock sequence
 */
#define MCLK_TIM_MATCH MATCH0

/**
 * @brief TIMER utilized for the ccd timing sequence
 */
#define CCD_TIMER_ICG LPC_TIMER1

/**
 * @brief TIMER match utilized for the ccd timing sequence
 */
#define ICG_TIM_MATCH MATCH1


/**
 * @brief Number of CCD elements (including dummies)
 */
#define CCD_PIXEL_QTY 3694

/**
 * @brief Minimum Clock Period por Master Clock (in nanoseconds)
 */
#define CCD_CLK_PERIOD_MIN 50

/**
 * @brief Maximum Clock Period por Master Clock (in nanoseconds)
 */
#define CCD_CLK_PERIOD_MAX 3000

/**
 * @brief Minimum settable integration time, in nS
 */
#define CCD_INT_TIME_MIN 10000

/**
 * @brief   Initialize CCD block.
 * @return  FreeRTOS-like return
 */
BaseType_t xBoard_CCD_Init(void);

/**
 * @brief   Set integration time of CCD block.
 * @param   intTime      : integration time in uS
 * @return  FreeRTOS-like return value.
 * @note
 * For any time setting, the following relation must always
 * hold: @ref CCD_INT_TIME_MIN <= intTime
 */
BaseType_t xBoard_CCD_SetIntegrationTime(uint32_t intTime);

/**
 * @brief   Set Master clock Period from CCD block.
 * @param   clkPeriod      : Period time in nS
 * @return  FreeRTOS-like return value.
 * @note
 * For any time setting, the following relation must always
 * hold: @ref CCD_CLK_PERIOD_MIN <= clkPeriod && clkPeriod < CCD_CLK_PERIOD_MAX
 */
BaseType_t xBoard_CCD_SetClkPeriod(uint32_t clkPeriod);


/**
 * @brief   Disables Electronic Shutter from CCD block.
 * @return  FreeRTOS-like return
 */
BaseType_t xBoard_CCD_ShutterDisable(void);

/**
 * @brief   Enables Electronic Shutter from CCD block.
 * @return  FreeRTOS-like return
 */
BaseType_t xBoard_CCD_ShutterEnable(void);


/**
 * @brief   Triggers readout sequence for CCD
 * @return  void.
 * @note
 */
void vBoard_CCD_Read(void);

BaseType_t xBoard_CCD_EnableElectronicShutter(void);

BaseType_t xBoard_CCD_DisableElectronicShutter(void);

                                                 



/**
 * @}
 */

#ifdef __cplusplus
}
#endif


#endif /* __CCD_H_ */
