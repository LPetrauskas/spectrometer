

#ifndef __LED_H_
#define __LED_H_


#ifdef __cplusplus
extern "C" {
#endif

/** @defgroup Leds in board front end Block
 * @ingroup board
 * @{
 */
#include "hal/led.h"
#include "chip.h"



/**
 * @brief   Initialized GPIO pins connected to the LEDs
 * @return	void.
 * @note
 */
void vBoard_LED_Init(void);


/**
 * @brief   Toggles green led
 * @return	void.
 * @note
 */
void vBoard_LED_ToggleGreen(void);


/**
 * @brief   Toggles red led
 * @return	void.
 * @note
 */
void vBoard_LED_ToggleRed(void);


/**
 * @brief   Turns on green led
 * @return	void.
 * @note
 */
void vBoard_LED_SetGreen(void);

/**
 * @brief   Turns on red led
 * @return	void.
 * @note
 */
void vBoard_LED_SetRed(void);

/**
 * @brief   Turns off green led
 * @return	void.
 * @note
 */
void vBoard_LED_ClearGreen(void);

/**
 * @brief   Turns off red led
 * @return	void.
 * @note
 */
void vBoard_LED_ClearRed(void);

/**
 * @brief   Turns off UV led
 * @return	void.
 * @note
 */
void vBoard_LED_ClearUV(void);

/**
 * @brief   Turns ON UV led
 * @return	void.
 * @note
 */
void vBoard_LED_SetUV(void);

/**
 * @brief   Turns ON halogen lamp
 * @return	void.
 * @note
 */
void vBoard_LED_SetHalogen(void);

/**
 * @brief   Turns OFF halogen lamp
 * @return	void.
 * @note
 */
void vBoard_LED_ClearHalogen(void);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif


#endif /* __LED_H_ */

