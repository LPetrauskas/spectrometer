
#ifndef __HAL_ADC_H_
#define __HAL_ADC_H_


/** @defgroup Hardware abstraction layer for lt1415 ADC block driver
 * @ingroup board
 * @{
 */

/**
 * @brief ADC conversion start port
 */
#define ADC_CONVST_PORT 3
/**
 * @brief ADC conversion start pin
 */
#define ADC_CONVST_PIN 25
/**
 * @brief ADC conversion start pinmode
 */
#define ADC_CONVST_MODE IOCON_MODE_INACT
/**
 * @brief ADC conversion start function (TIMER)
 */
#define ADC_CONVST_FUNC IOCON_FUNC2
/**
 * @brief ADC conversion start Match number
 */
#define ADC_CONVST_MAT 0
/**
 * @brief ADC conversion start TIMER number
 */
#define ADC_CONVST_TIMER LPC_TIMER0
/**
 * @brief ADC conversion start Timer IRQ
 */
#define ADC_CONVST_TIMER_IRQ TIMER0_IRQn
/**
 * @brief ADC conversion start Timer Clock
 */
#define ADC_CONVST_TIMER_CLK SYSCTL_PCLK_TIMER0


/**
 * @brief ADC BUSY port
 */
#define ADC_BUSY_PORT 2
/**
 * @brief ADC BUSY pin
 */
#define ADC_BUSY_PIN 13
/**
 * @brief ADC BUSY pinmode
 */
#define ADC_BUSY_MODE IOCON_MODE_INACT
/**
 * @brief ADC BUSY function (GPIO)
 */
#define ADC_BUSY_FUNC IOCON_FUNC0

/**
 * @brief ADC Read Data port
 */
#define ADC_RD_PORT 2
/**
 * @brief ADC Read Data pin
 */
#define ADC_RD_PIN 12
/**
 * @brief ADC Read Data pinmode
 */
#define ADC_RD_MODE IOCON_MODE_INACT
/**
 * @brief ADC Read Data function 
 */
#define ADC_RD_FUNC IOCON_FUNC0


/**
 * @brief ADC Chip Select port
 */
#define ADC_CS_PORT 2
/**
 * @brief ADC Chip Select pin
 */
#define ADC_CS_PIN 11
/**
 * @brief ADC Chip Select pinmode
 */
#define ADC_CS_MODE IOCON_MODE_INACT
/**
 * @brief ADC Chip Select function 
 */
#define ADC_CS_FUNC IOCON_FUNC0


/**
 * @brief ADC NAP port
 */
#define ADC_NAP_PORT 0
/**
 * @brief ADC NAP pin
 */
#define ADC_NAP_PIN 10
/**
 * @brief ADC NAP pinmode
 */
#define ADC_NAP_MODE IOCON_MODE_INACT
/**
 * @brief ADC NAP function 
 */
#define ADC_NAP_FUNC IOCON_FUNC0

/**
 * @brief ADC Shutdown port
 */
#define ADC_SHDN_PORT 0
/**
 * @brief ADC Shutdown pin
 */
#define ADC_SHDN_PIN 1
/**
 * @brief ADC Shutdown pinmode
 */
#define ADC_SHDN_MODE IOCON_MODE_INACT
/**
 * @brief ADC Shutdown function 
 */
#define ADC_SHDN_FUNC IOCON_FUNC0


 /************* DATA ***************/


/**
 * @brief ADC DATA0 port
 */
#define ADC_DATA0_PORT 1
/**
 * @brief ADC DATA0 pin
 */
#define ADC_DATA0_PIN 21
/**
 * @brief ADC DATA0 pinmode
 */
#define ADC_DATA0_MODE IOCON_MODE_INACT
/**
 * @brief ADC DATA0 function 
 */
#define ADC_DATA0_FUNC IOCON_FUNC0

/**
 * @brief ADC DATA1 port
 */
#define ADC_DATA1_PORT 1
/**
 * @brief ADC DATA1 pin
 */
#define ADC_DATA1_PIN 20
/**
 * @brief ADC DATA1 pinmode
 */
#define ADC_DATA1_MODE IOCON_MODE_INACT
/**
 * @brief ADC DATA1 function 
 */
#define ADC_DATA1_FUNC IOCON_FUNC0

/**
 * @brief ADC DATA2 port
 */
#define ADC_DATA2_PORT 1
/**
 * @brief ADC DATA2 pin
 */
#define ADC_DATA2_PIN 19
/**
 * @brief ADC DATA2 pinmode
 */
#define ADC_DATA2_MODE IOCON_MODE_INACT
/**
 * @brief ADC DATA2 function 
 */
#define ADC_DATA2_FUNC IOCON_FUNC0

/**
 * @brief ADC DATA3 port
 */
#define ADC_DATA3_PORT 1
/**
 * @brief ADC DATA3 pin
 */
#define ADC_DATA3_PIN 18
/**
 * @brief ADC DATA3 pinmode
 */
#define ADC_DATA3_MODE IOCON_MODE_INACT
/**
 * @brief ADC DATA3 function 
 */
#define ADC_DATA3_FUNC IOCON_FUNC0


/**
 * @brief ADC DATA4 port
 */
#define ADC_DATA4_PORT 1
/**
 * @brief ADC DATA4 pin
 */
#define ADC_DATA4_PIN 29
/**
 * @brief ADC DATA4 pinmode
 */
#define ADC_DATA4_MODE IOCON_MODE_INACT
/**
 * @brief ADC DATA4 function 
 */
#define ADC_DATA4_FUNC IOCON_FUNC0

/**
 * @brief ADC DATA5 port
 */
#define ADC_DATA5_PORT 1
/**
 * @brief ADC DATA5 pin
 */
#define ADC_DATA5_PIN 28
/**
 * @brief ADC DATA5 pinmode
 */
#define ADC_DATA5_MODE IOCON_MODE_INACT
/**
 * @brief ADC DATA5 function 
 */
#define ADC_DATA5_FUNC IOCON_FUNC0

/**
 * @brief ADC DATA6 port
 */
#define ADC_DATA6_PORT 1
/**
 * @brief ADC DATA6 pin
 */
#define ADC_DATA6_PIN 27
/**
 * @brief ADC DATA6 pinmode
 */
#define ADC_DATA6_MODE IOCON_MODE_INACT
/**
 * @brief ADC DATA6 function 
 */
#define ADC_DATA6_FUNC IOCON_FUNC0

/**
 * @brief ADC DATA7 port
 */
#define ADC_DATA7_PORT 1
/**
 * @brief ADC DATA7 pin
 */
#define ADC_DATA7_PIN 26
/**
 * @brief ADC DATA7 pinmode
 */
#define ADC_DATA7_MODE IOCON_MODE_INACT
/**
 * @brief ADC DATA7 function 
 */
#define ADC_DATA7_FUNC IOCON_FUNC0

/**
 * @brief ADC DATA8 port
 */
#define ADC_DATA8_PORT 1
/**
 * @brief ADC DATA8 pin
 */
#define ADC_DATA8_PIN 25
/**
 * @brief ADC DATA8 pinmode
 */
#define ADC_DATA8_MODE IOCON_MODE_INACT
/**
 * @brief ADC DATA8 function 
 */
#define ADC_DATA8_FUNC IOCON_FUNC0

/**
 * @brief ADC DATA9 port
 */
#define ADC_DATA9_PORT 1
/**
 * @brief ADC DATA9 pin
 */
#define ADC_DATA9_PIN 24
/**
 * @brief ADC DATA9 pinmode
 */
#define ADC_DATA9_MODE IOCON_MODE_INACT
/**
 * @brief ADC DATA9 function 
 */
#define ADC_DATA9_FUNC IOCON_FUNC0

/**
 * @brief ADC DATA10 port
 */
#define ADC_DATA10_PORT 1
/**
 * @brief ADC DATA10 pin
 */
#define ADC_DATA10_PIN 23
/**
 * @brief ADC DATA10 pinmode
 */
#define ADC_DATA10_MODE IOCON_MODE_INACT
/**
 * @brief ADC DATA10 function 
 */
#define ADC_DATA10_FUNC IOCON_FUNC0

/**
 * @brief ADC DATA11 port
 */
#define ADC_DATA11_PORT 1
/**
 * @brief ADC DATA11 pin
 */
#define ADC_DATA11_PIN 22
/**
 * @brief ADC DATA11 pinmode
 */
#define ADC_DATA11_MODE IOCON_MODE_INACT
/**
 * @brief ADC DATA11 function 
 */
#define ADC_DATA11_FUNC IOCON_FUNC0




/** 
 * @}
 */

#endif /* HAL_ADC_H  */

