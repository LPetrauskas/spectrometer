#ifndef HAL_LO74HC40XX_H
#define HAL_LO74HC40XX_H

/** @defgroup board_i2c_hal I2C block Hardware Abstraction Layer
 * @ingroup board_i2c
 * @{
 */

/**
 * @brief I2C controller to be used
 */
#define I2C_BUS_CONTROLLER           I2C1

/**
 * @brief I2C bus clock rate, in Hz
 */
#define I2C_BUS_CONTROLLER_CLOCKRATE 100000


/**
 * @brief I2C controller SDA gpio port
 */
#define I2C_BUS_CONTROLLER_SDA_PORT  0

/**
 * @brief I2C controller SDA gpio pin
 */
#define I2C_BUS_CONTROLLER_SDA_PIN   19

/**
 * @brief I2C controller SDA gpio mode
 */
#define I2C_BUS_CONTROLLER_SDA_MODE  0

/**
 * @brief I2C controller SDA gpio function
 */
#define I2C_BUS_CONTROLLER_SDA_FUNC  FUNC3

/**
 * @brief I2C controller SCL gpio port
 */
#define I2C_BUS_CONTROLLER_SCL_PORT  0

/**
 * @brief I2C controller SCL gpio pin
 */
#define I2C_BUS_CONTROLLER_SCL_PIN   20

/**
 * @brief I2C controller SCL gpio mode
 */
#define I2C_BUS_CONTROLLER_SCL_MODE  0

/**
 * @brief I2C controller SCL gpio function
 */
#define I2C_BUS_CONTROLLER_SCL_FUNC  FUNC3


/**
 * @brief I2C controller WP gpio port
 */
#define I2C_BUS_CONTROLLER_WP_PORT  0

/**
 * @brief I2C controller WP gpio pin
 */
#define I2C_BUS_CONTROLLER_WP_PIN   21

/**
 * @brief I2C controller WP gpio mode
 */
#define I2C_BUS_CONTROLLER_WP_MODE  0

/**
 * @brief I2C controller WP gpio function
 */
#define I2C_BUS_CONTROLLER_WP_FUNC  FUNC0

/** 
 * @}
 */

#endif /* HAL_LO74HC40XX_H */
