
#ifndef __HAL_CCD_H_
#define __HAL_CCD_H_


/** @defgroup Hardware abstraction layer for TCD1304 block driver
 * @ingroup board
 * @{
 */

/**
 * @brief CCD shutter port
 */

#define CCD_SH_PORT 0
/**
 * @brief CCD shutter pin
 */
#define CCD_SH_PIN 11
/**
 * @brief CCD shutter pinmode
 */
#define CCD_SH_MODE IOCON_MODE_INACT
/**
 * @brief CCD shutter function (TIMER)
 */
#define CCD_SH_FUNC IOCON_FUNC3
/**
 * @brief CCD Master clock Match number
 */
#define CCD_SH_MAT 1
/**
 * @brief CCD Master clock TIMER number
 */
#define CCD_SH_TIMER LPC_TIMER3
/**
 * @brief CCD Master clock Timer IRQ
 */
#define CCD_SH_TIMER_IRQ TIMER3_IRQn
/**
 * @brief CCD Master clock Timer Clock
 */
#define CCD_SH_TIMER_CLK SYSCTL_PCLK_TIMER3



/**
 * @brief CCD integration clear gate port
 */
#define CCD_ICG_PORT 0
/**
 * @brief CCD integration clear gate pin
 */
#define CCD_ICG_PIN 8
/**
 * @brief CCD integration clear gate pinmode
 */
#define CCD_ICG_MODE IOCON_MODE_INACT
/**
 * @brief CCD integration clear gate function
 */
#define CCD_ICG_FUNC IOCON_FUNC0



/**
 * @brief CCD Master clock port
 */
#define CCD_MCLK_PORT 0
/**
 * @brief CCD Master clock pin
 */
#define CCD_MCLK_PIN 7
/**
 * @brief CCD Master clock pinmode
 */
#define CCD_MCLK_MODE IOCON_MODE_INACT
/**
 * @brief CCD Master clock function (TIMER)
 */
#define CCD_MCLK_FUNC IOCON_FUNC3
/**
 * @brief CCD Master clock Match number
 */
#define CCD_MCLK_MAT 1
/**
 * @brief CCD Master clock TIMER number
 */
#define CCD_MCLK_TIMER LPC_TIMER2
/**
 * @brief CCD Master clock Timer IRQ
 */
#define CCD_MCLK_TIMER_IRQ TIMER2_IRQn
/**
 * @brief CCD Master clock Timer Clock
 */
#define CCD_MCLK_TIMER_CLK SYSCTL_PCLK_TIMER2

/** 
 * @}
 */

#endif /* HAL_CCD_H  */

