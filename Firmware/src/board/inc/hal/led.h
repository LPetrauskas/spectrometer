#ifndef HAL_LED_H
#define HAL_LED_H

/** @defgroup  Hardware Abstraction Layer
 * @ingroup board_led
 * @{
 */


/**
 * @brief Microcontroler Green LED port
 */
#define LED_GREEN_PORT 0
/**
 * @brief  Microcontroler Green LED pin
 */
#define LED_GREEN_PIN 2
/**
 * @brief Microcontroler Green LED pull up/down mode
 */
#define LED_GREEN_MODE IOCON_MODE_PULLUP
/**
 * @brief  Microcontroler Green LED function
 */
#define LED_GREEN_FUNC IOCON_FUNC0


/**
 * @brief  Microcontroler Red LED port
 */
#define LED_RED_PORT 0
/**
 * @brief Microcontroler Red LED pin
 */
#define LED_RED_PIN 3
/**
 * @brief Microcontroler Red LED pull up/down mode
 */
#define LED_RED_MODE IOCON_MODE_PULLUP
/**
 * @brief Microcontroler Red LED function
 */
#define LED_RED_FUNC IOCON_FUNC0

/**
 * @brief  Microcontroler Red LED port
 */
#define HAL_LAMP_PORT 0
/**
 * @brief Microcontroler Red LED pin
 */
#define HAL_LAMP_PIN 23
/**
 * @brief Microcontroler Red LED pull up/down mode
 */
#define HAL_LAMP_MODE IOCON_MODE_PULLDOWN
/**
 * @brief Microcontroler Red LED function
 */
#define HAL_LAMP_FUNC IOCON_FUNC0

/**
 * @brief  Microcontroler Red LED port
 */
#define UV_LAMP_PORT 0
/**
 * @brief Microcontroler Red LED pin
 */
#define UV_LAMP_PIN 25
/**
 * @brief Microcontroler Red LED pull up/down mode
 */
#define UV_LAMP_MODE IOCON_MODE_PULLDOWN
/**
 * @brief Microcontroler Red LED function
 */
#define UV_LAMP_FUNC IOCON_FUNC0





#endif /* __HAL_LED_H */
