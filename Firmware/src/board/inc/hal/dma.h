#ifndef HAL_DMA_H
#define HAL_DMA_H

/** @defgroup board_dma_hal DMA block Hardware Abstraction Layer
 * @ingroup board_dma
 * @{
 */

/**
 * @brief DMA controller to be used
 */
#define DMA_CONTROLLER DMA

/** 
 * @}
 */


#endif /* HAL_DMA_H  */
