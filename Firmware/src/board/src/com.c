#include "hal/usb.h"
#include "usb_config.h"
#include "usb_desc.h"
#include "usb_setup.h"
#include "string.h"
#include "buffer.h"
#include "stream.h"
#include "common.h"
#include "chip.h"
#include "com.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/**
 * Structure containing Virtual Comm port control data
 */
STATIC struct {
  UBaseType_t xInits;
  SemaphoreHandle_t xRxMutex;
  SemaphoreHandle_t xRxWaitq;
  SemaphoreHandle_t xTxMutex;
  SemaphoreHandle_t xTxWaitq;
  USBD_HANDLE_T xUSBHandle;
  USBD_HANDLE_T xCDCHandle;
  Buffer_t xRxBuffer;
  Buffer_t xTxBuffer;
  volatile BaseType_t xBusy;
  volatile BaseType_t xConnected;
} COMData = {0};

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/* VCOM bulk endpoint handler */
STATIC ErrorCode_t xBoard_Bulk_Endpoint_Handler(USBD_HANDLE_T xUsb, void *pvData, uint32_t ulEvent) {
   BaseType_t xHigherPriorityTaskWoken = pdFALSE;
   UBaseType_t xSize; void *pvBuffer;
   switch (ulEvent) {
      case USB_EVT_IN_NAK:        
      case USB_EVT_IN:
   if (!xBuffer_IsEmpty(&COMData.xTxBuffer)) {
      xSize = xBuffer_BeginPop(&COMData.xTxBuffer, &pvBuffer, USB_FS_MAX_BULK_PACKET);
      xSize = (UBaseType_t) USBD_API->hw->WriteEP(COMData.xUSBHandle, USB_ENDPOINT_IN(USB_CDC_IN_EP), pvBuffer, (uint32_t) xSize);      
      xBuffer_EndPop(&COMData.xTxBuffer, xSize);            
   } else COMData.xBusy = pdFALSE;
   xSemaphoreGiveFromISR(COMData.xTxWaitq, &xHigherPriorityTaskWoken);
   break;
      case USB_EVT_OUT:
   xSize = xBuffer_BeginPush(&COMData.xRxBuffer, &pvBuffer, USB_FS_MAX_BULK_PACKET);
   if (xSize >= USB_FS_MAX_BULK_PACKET) {
      xSize = (UBaseType_t) USBD_API->hw->ReadEP(COMData.xUSBHandle, USB_ENDPOINT_OUT(USB_CDC_OUT_EP), pvBuffer);       
   } else xSize = 0;
   xBuffer_EndPush(&COMData.xRxBuffer, xSize);   
   if (xSize > 0) xSemaphoreGiveFromISR(COMData.xRxWaitq, &xHigherPriorityTaskWoken);
   break;
      case USB_EVT_OUT_NAK:
   xSize = xBuffer_BeginPush(&COMData.xRxBuffer, &pvBuffer, USB_FS_MAX_BULK_PACKET);
   if (xSize > 0) {
      xSize = (UBaseType_t) USBD_API->hw->ReadReqEP(COMData.xUSBHandle, USB_ENDPOINT_OUT(USB_CDC_OUT_EP), pvBuffer, (uint32_t) xSize);   
   }   
   xBuffer_EndPush(&COMData.xRxBuffer, xSize);
   break;
      default:
   break;
   }
   portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
   return LPC_OK;
}

/* Set line coding call back routine */
STATIC ErrorCode_t xCDC_SetLineCode(USBD_HANDLE_T hCDC, CDC_LINE_CODING *line_coding) {
   /* Called when baud rate is changed/set. Using it to know host connection state */
   COMData.xConnected = pdTRUE;
   return LPC_OK;
}

STATIC void vBoard_COM_ForceWrite(UBaseType_t xSize) {
  void *pvBuffer;

  if (COMData.xBusy == pdFALSE) {
    NVIC_DisableIRQ(IRQSource(USB_CONTROLLER)); /* enter critical section */      
    xSize = xBuffer_BeginPop(&COMData.xTxBuffer, &pvBuffer, xSize);
    xSize = (UBaseType_t) USBD_API->hw->WriteEP(COMData.xUSBHandle, USB_ENDPOINT_IN(USB_CDC_IN_EP), pvBuffer, (uint32_t) xSize);
    xBuffer_EndPop(&COMData.xTxBuffer, xSize);              
    COMData.xBusy = pdTRUE;
    NVIC_EnableIRQ(IRQSource(USB_CONTROLLER)); /* exit critical section */
  }
}

STATIC BaseType_t xBoard_COM_WLock(void) {
  return xSemaphoreTake(COMData.xTxMutex, portMAX_DELAY);   
}
STATIC void vBoard_COM_WUnlock(void) {
  xSemaphoreGive(COMData.xTxMutex);   
}

STATIC void vBoard_COM_Put(uint32_t ulValue, BaseType_t xFlags) {
  void *pvBuffer;
  if (ulValue != pdEOF) {
    if (xBuffer_BeginPush(&COMData.xTxBuffer, &pvBuffer, sizeof(ulValue)) < sizeof(ulValue)) {
      if (!(xFlags & NOSTALL)) xStreamStall(NULL);
      xBuffer_EndPush(&COMData.xTxBuffer, 0);
    } else {
      memcpy(pvBuffer, &ulValue, sizeof(ulValue));
      xBuffer_EndPush(&COMData.xTxBuffer, sizeof(ulValue));
      /*In case of no ongoing transfer */
      vBoard_COM_ForceWrite(sizeof(ulValue));            
    }
  }
}

/*****************************************************************************
 * Interrupt handlers
 ****************************************************************************/

IRQHandler(USB_CONTROLLER) {
   USBD_API->hw->ISR(COMData.xUSBHandle);
}

/*****************************************************************************
 * Public functions
 ****************************************************************************/

const StreamSink_t COMSink = {
  &xBoard_COM_WLock,
  &vBoard_COM_WUnlock,

  &vBoard_COM_Put
};

BaseType_t xBoard_COM_Init(void) {   
   USBD_API_INIT_PARAM_T xUSBParam;
   USBD_CDC_INIT_PARAM_T xCDCParam;
   USB_CORE_DESCS_T xUSBDesc;
   ErrorCode_t xRet;

   if (COMData.xInits) {
     COMData.xInits++;
     return pdFREERTOS_ERRNO_NONE;
   }

   xBoard_USB_Init();

   memset((void *) &xUSBParam, 0, sizeof(USBD_API_INIT_PARAM_T));
   xUSBParam.usb_reg_base = LPC_USB_BASE + 0x200;
   xUSBParam.max_num_ep = USB_CDC_EP_COUNT;
   xUSBParam.mem_base = USB_STACK_MEM_BASE;
   xUSBParam.mem_size = USB_STACK_MEM_SIZE;
   
   /* Set the USB descriptors */
   xUSBDesc.device_desc = (uint8_t *) &USB_DeviceDescriptor[0];
   xUSBDesc.string_desc = (uint8_t *) &USB_StringDescriptor[0];
   /* Note, to pass USBCV test full-speed only devices should have both
      descriptor arrays point to same location and device_qualifier set to 0.
   */
   xUSBDesc.high_speed_desc = (uint8_t *) &USB_FsConfigDescriptor[0];
   xUSBDesc.full_speed_desc = (uint8_t *) &USB_FsConfigDescriptor[0];
   xUSBDesc.device_qualifier = 0;

   xRet = USBD_API->hw->Init(&COMData.xUSBHandle, &xUSBDesc, &xUSBParam);   
   if (xRet != LPC_OK) {
     return pdFREERTOS_ERRNO_EIO;    
   }
     
   /* setup cdc class */
   memset((void *) &xCDCParam, 0, sizeof(USBD_CDC_INIT_PARAM_T));
   xCDCParam.mem_base = xUSBParam.mem_base;
   xCDCParam.mem_size = xUSBParam.mem_size;
   xCDCParam.cif_intf_desc = (uint8_t *) Find_IntfDescriptor(xUSBDesc.high_speed_desc, CDC_COMMUNICATION_INTERFACE_CLASS);
   xCDCParam.dif_intf_desc = (uint8_t *) Find_IntfDescriptor(xUSBDesc.high_speed_desc, CDC_DATA_INTERFACE_CLASS);
   xCDCParam.SetLineCode = &xCDC_SetLineCode;
   
   xRet = USBD_API->cdc->init(COMData.xUSBHandle, &xCDCParam, &COMData.xCDCHandle);   
   if (xRet != LPC_OK) {  
        return pdFREERTOS_ERRNO_EIO;
   }
   
   /* allocate transfer buffers */
   xBuffer_Init(&COMData.xRxBuffer, (void *) xCDCParam.mem_base, sizeof(uint8_t), COM_RX_BUFFER_SIZE);
   xCDCParam.mem_base += COM_RX_BUFFER_SIZE;
   xCDCParam.mem_size -= COM_RX_BUFFER_SIZE;
   xBuffer_Init(&COMData.xTxBuffer, (void *) xCDCParam.mem_base, sizeof(uint8_t), COM_TX_BUFFER_SIZE);
   xCDCParam.mem_base += COM_TX_BUFFER_SIZE;
   xCDCParam.mem_size -= COM_TX_BUFFER_SIZE;
       
   /* register endpoint interrupt handler */
   xRet = USBD_API->core->RegisterEpHandler(COMData.xUSBHandle, USB_OUT_EP_IDX(USB_CDC_OUT_EP), &xBoard_Bulk_Endpoint_Handler, NULL);   
   if (xRet != LPC_OK) {
     return pdFREERTOS_ERRNO_EIO;
   }
   
   /* register endpoint interrupt handler */
   xRet = USBD_API->core->RegisterEpHandler(COMData.xUSBHandle, USB_IN_EP_IDX(USB_CDC_IN_EP), &xBoard_Bulk_Endpoint_Handler, NULL);   
   if (xRet != LPC_OK) {
     return pdFREERTOS_ERRNO_EIO;
   }
   /* update mem_base and size variables for cascading calls. */
   xUSBParam.mem_base = xCDCParam.mem_base;
   xUSBParam.mem_size = xCDCParam.mem_size;
   
   COMData.xRxMutex = xSemaphoreCreateMutex();
   
   if (COMData.xRxMutex == NULL) {
     return pdFREERTOS_ERRNO_ENOMEM;
   }
   
   COMData.xRxWaitq = xSemaphoreCreateBinary();
   
   if (COMData.xRxWaitq == NULL) {
     return pdFREERTOS_ERRNO_ENOMEM;
   }

   COMData.xTxMutex = xSemaphoreCreateMutex();
   if (COMData.xTxMutex == NULL) {
     return pdFREERTOS_ERRNO_ENOMEM;
   }   

   COMData.xTxWaitq = xSemaphoreCreateBinary();
   if (COMData.xTxWaitq == NULL) {
     return pdFREERTOS_ERRNO_ENOMEM;
   }
   
   NVIC_EnableIRQ(IRQSource(USB_CONTROLLER));
   USBD_API->hw->Connect(COMData.xUSBHandle, 1);
   while (COMData.xConnected != pdTRUE); // shit code, shit stack, USB first on TODO list
   COMData.xInits++;
   return pdFREERTOS_ERRNO_NONE;
}

BaseType_t xBoard_COM_Read(void *pvData, BaseType_t xSize, UBaseType_t uxFlags) {
   BaseType_t xOutcome;  
   TickType_t xTimeout;
   void *pvBuffer;
      
   xTimeout = (uxFlags & NONBLOCKING)? 0:portMAX_DELAY;
   do {
     if (xSemaphoreTake(COMData.xRxMutex, xTimeout) == pdTRUE){   
       if (!xBuffer_IsEmpty(&COMData.xRxBuffer)) {
   xSize = xBuffer_BeginPop(&COMData.xRxBuffer, &pvBuffer, xSize);    
   memcpy(pvData, pvBuffer, xSize);
   xBuffer_EndPop(&COMData.xRxBuffer, xSize);
   xOutcome = pdFREERTOS_ERRNO_NONE;
       }else xOutcome = pdEOF;
       xSemaphoreGive(COMData.xRxMutex);
     } else xOutcome = pdFREERTOS_ERRNO_EBUSY;
   } while (
      !(uxFlags & NONBLOCKING) && 
      xOutcome != pdFREERTOS_ERRNO_NONE &&      
      xSemaphoreTake(COMData.xRxWaitq, xTimeout) != pdFALSE
   );
   return (xOutcome == pdFREERTOS_ERRNO_NONE)? xSize:xOutcome;
}

BaseType_t xBoard_COM_Write(const void *pvData, BaseType_t xSize, UBaseType_t uxFlags) {
   BaseType_t xOutcome;
   TickType_t xTimeout;
   void *pvBuffer;

   xTimeout = (uxFlags & NONBLOCKING)? 0:portMAX_DELAY;
   do {
      if (xSemaphoreTake(COMData.xTxMutex, xTimeout) == pdTRUE) {   
   if (!xBuffer_IsFull(&COMData.xTxBuffer)) {
      xSize = xBuffer_BeginPush(&COMData.xTxBuffer, &pvBuffer, xSize);
      memcpy(pvBuffer, pvData, xSize);
      xBuffer_EndPush(&COMData.xTxBuffer, xSize);
      vBoard_COM_ForceWrite(xSize);  
      xOutcome = pdFREERTOS_ERRNO_NONE;
   } else xOutcome = pdFREERTOS_ERRNO_ENOBUFS;
   xSemaphoreGive(COMData.xTxMutex);
      } else xOutcome = pdFREERTOS_ERRNO_EBUSY;
   } while (
      !(uxFlags & NONBLOCKING) && 
      xOutcome != pdFREERTOS_ERRNO_NONE &&      
      xSemaphoreTake(COMData.xTxWaitq, xTimeout) != pdFALSE
   );
   return (xOutcome == pdFREERTOS_ERRNO_NONE)? xSize:xOutcome;
}

void vBoard_COM_Deinit(void) {
   COMData.xInits--;
   
   if(!COMData.xInits) {
      NVIC_DisableIRQ(IRQSource(USB_CONTROLLER));

      vSemaphoreDelete(COMData.xTxMutex);
      vSemaphoreDelete(COMData.xTxWaitq);
      vSemaphoreDelete(COMData.xRxMutex);
      vSemaphoreDelete(COMData.xRxWaitq);
      
      vBoard_USB_Deinit();
   }
}