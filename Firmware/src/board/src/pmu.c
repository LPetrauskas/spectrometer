#include "pmu.h"

void vBoard_PMU_Init(void)
{
	Chip_IOCON_PinMux(LPC_IOCON, PMU_ENABLE_PORT, PMU_ENABLE_PIN, PMU_ENABLE_MODE, PMU_ENABLE_FUNC);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, PMU_ENABLE_PORT, PMU_ENABLE_PIN);
}

void vBoard_PMU_PowerUp(void)
{
	Chip_GPIO_WritePortBit(LPC_GPIO, PMU_ENABLE_PORT, PMU_ENABLE_PIN, 1);
}

void vBoard_PMU_PowerDown(void)
{
	Chip_GPIO_WritePortBit(LPC_GPIO, PMU_ENABLE_PORT, PMU_ENABLE_PIN, 0);
}
