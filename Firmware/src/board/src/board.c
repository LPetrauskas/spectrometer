#include "board.h"
#include "chip.h" 

/* System oscillator rate and RTC oscillator rate (fucking LPCOpen) */
const uint32_t OscRateIn = 12000000;
const uint32_t RTCOscRateIn = 32768;



BaseType_t xBoard_Init(void) {
	BaseType_t xOutcome;
	//Setup clock
	Chip_SetupXtalClocking();
	Chip_SYSCTL_SetFLASHAccess(FLASHTIM_120MHZ_CPU);
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock/1000);

	vBoard_LED_Init();
	vBoard_PMU_Init();
	xOutcome = xBoard_COM_Init();
	if(xOutcome == pdFREERTOS_ERRNO_NONE)
	{
		vBoard_PMU_PowerUp();
		xBoard_CCD_Init();
		xBoard_ADC_Init();
		vBoard_ADC_Enable();
		//	
		
	}


	return xOutcome;
}

