#include "hal/i2c.h"
#include "i2c.h"
#include "common.h"
 
STATIC struct{
  UBaseType_t       xInits;
  SemaphoreHandle_t xMutex;
  TaskHandle_t      xOwner;
} I2CData = {0};

STATIC void vBoard_I2C_EventHandler(I2C_ID_T id, I2C_EVENT_T event) {
  switch (event) {
     case I2C_EVENT_LOCK:
	I2CData.xOwner = xTaskGetCurrentTaskHandle();
	break;
     case I2C_EVENT_WAIT:	 
	while (Chip_I2C_MasterStatus(I2C_BUS_CONTROLLER) == I2C_STATUS_BUSY) {
	  xTaskNotifyWait(0x00, 0xFF, NULL, portMAX_DELAY);
	}
	break;
     case I2C_EVENT_UNLOCK:
	I2CData.xOwner = NULL;    	
	break;
     case I2C_EVENT_DONE:
	break;
     default: 
	break;
   }
   return;
}

/* Hardware interrupt handler */
IRQHandler(I2C_BUS_CONTROLLER)
{
  BaseType_t xHigherPriorityTaskWoken = pdFALSE;
  if (Chip_I2C_MasterStateHandler(I2C_BUS_CONTROLLER) != I2C_STATUS_BUSY) {
    xTaskNotifyFromISR(I2CData.xOwner, 0x00, eNoAction, &xHigherPriorityTaskWoken);
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
  }	
}

/* Initializes the I2C bus */
BaseType_t xBoard_I2C_Init(void) 
{
  if (!I2CData.xInits) {
     Chip_I2C_Init(I2C_BUS_CONTROLLER);	
     Chip_I2C_SetClockRate(I2C_BUS_CONTROLLER, I2C_BUS_CONTROLLER_CLOCKRATE);
     Chip_I2C_SetMasterEventHandler(I2C_BUS_CONTROLLER, &vBoard_I2C_EventHandler);
     Chip_IOCON_PinMux(LPC_IOCON, I2C_BUS_CONTROLLER_SDA_PORT, I2C_BUS_CONTROLLER_SDA_PIN, 
		       I2C_BUS_CONTROLLER_SDA_MODE, I2C_BUS_CONTROLLER_SDA_FUNC);
     Chip_IOCON_EnableOD(LPC_IOCON, I2C_BUS_CONTROLLER_SDA_PORT, I2C_BUS_CONTROLLER_SDA_PIN);
     Chip_IOCON_PinMux(LPC_IOCON, I2C_BUS_CONTROLLER_SCL_PORT, I2C_BUS_CONTROLLER_SCL_PIN, 
		       I2C_BUS_CONTROLLER_SCL_MODE, I2C_BUS_CONTROLLER_SCL_FUNC);	     
     Chip_IOCON_EnableOD(LPC_IOCON, I2C_BUS_CONTROLLER_SCL_PORT, I2C_BUS_CONTROLLER_SCL_PIN);
     Chip_IOCON_PinMux(LPC_IOCON, I2C_BUS_CONTROLLER_WP_PORT, I2C_BUS_CONTROLLER_WP_PIN, I2C_BUS_CONTROLLER_WP_MODE, I2C_BUS_CONTROLLER_WP_FUNC);
	  Chip_GPIO_SetPinDIROutput(LPC_GPIO, I2C_BUS_CONTROLLER_WP_PORT, I2C_BUS_CONTROLLER_WP_PIN);
     I2CData.xMutex = xSemaphoreCreateMutex();
     if (I2CData.xMutex == NULL) {
       return pdFREERTOS_ERRNO_ENOMEM;
     }
     NVIC_EnableIRQ(IRQSource(I2C_BUS_CONTROLLER));	 
  }
  I2CData.xInits++;
  return pdFREERTOS_ERRNO_NONE;
}

BaseType_t xBoard_I2C_Transfer(I2C_Transfer_t *pxTransfer, UBaseType_t uxFlags) {
  BaseType_t xOutcome;
  if(xSemaphoreTake(I2CData.xMutex, (uxFlags & NONBLOCKING)? 0:portMAX_DELAY) == pdTRUE) {
    switch((I2C_STATUS_T)Chip_I2C_MasterTransfer(I2C_BUS_CONTROLLER, pxTransfer))
    {   
       case I2C_STATUS_BUSERR:
	  xOutcome = pdFREERTOS_ERRNO_EIO;
	  break;
       case I2C_STATUS_BUSY:
	  xOutcome = pdFREERTOS_ERRNO_EBUSY;
	  break;
       case I2C_STATUS_NAK:
       case I2C_STATUS_ARBLOST:
	  xOutcome = pdFREERTOS_ERRNO_EAGAIN;
	  break;
       case I2C_STATUS_DONE:
         xOutcome = pdFREERTOS_ERRNO_NONE;
         break;
    }
    xSemaphoreGive(I2CData.xMutex);
  } else xOutcome = pdFREERTOS_ERRNO_EBUSY;
  return xOutcome;
}

/* De-initializes the I2C bus */
void vBoard_I2C_Deinit(void)
{
   I2CData.xInits--;

   if(!I2CData.xInits) {
      NVIC_DisableIRQ(IRQSource(I2C_BUS_CONTROLLER));
      vSemaphoreDelete(I2CData.xMutex);
      Chip_I2C_DeInit(I2C_BUS_CONTROLLER);
   } 
}

