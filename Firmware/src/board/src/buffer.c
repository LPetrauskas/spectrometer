#include "buffer.h"

/*****************************************************************************
 * Public functions
 ****************************************************************************/

BaseType_t xBuffer_Init(Buffer_t *pxBuff, void *pvItems, UBaseType_t uxItemSize, UBaseType_t uxItemCnt) {
  pxBuff->pvItems = pvItems;
  pxBuff->uxItemSize = uxItemSize;
  pxBuff->uxItemCnt = pxBuff->uxWrap = uxItemCnt;
  pxBuff->uxHead = pxBuff->uxTail = 0;
  return pdTRUE;
}

UBaseType_t xBuffer_BeginPush(Buffer_t *pxBuff, void **ppvItems, UBaseType_t uxSize) {
  UBaseType_t uxAvailable;
  
  if (pxBuff->uxHead > pxBuff->uxTail) {
    uxAvailable = pxBuff->uxHead - pxBuff->uxTail - 1;
  } else {
    uxAvailable = pxBuff->uxItemCnt - pxBuff->uxTail - 1;
    if (uxAvailable < uxSize && uxAvailable < pxBuff->uxHead - 1) {
      pxBuff->uxWrap = pxBuff->uxTail;
      uxAvailable = pxBuff->uxHead - 1;
    } else pxBuff->uxWrap = pxBuff->uxItemCnt;
  }

  if (uxSize < uxAvailable) {
    uxAvailable = uxSize;
  }
  
  if (uxAvailable) {
    *ppvItems = pxBuff->pvItems + ( pxBuff->uxTail % pxBuff->uxWrap ) * pxBuff->uxItemSize;
  } else *ppvItems = NULL;
  
  return uxAvailable;
}

UBaseType_t xBuffer_EndPush(Buffer_t *pxBuff, UBaseType_t uxSize) {
  pxBuff->uxTail = (pxBuff->uxTail + uxSize) % pxBuff->uxWrap;
  return uxSize;
}

UBaseType_t xBuffer_BeginPop(Buffer_t *pxBuff, void **ppvItems, UBaseType_t uxSize) {
  UBaseType_t uxAvailable;
  
  if (pxBuff->uxHead > pxBuff->uxTail) {
    uxAvailable = pxBuff->uxWrap - pxBuff->uxHead;
  }else {
    uxAvailable = pxBuff->uxTail - pxBuff->uxHead;    
  }
  
  if (uxSize < uxAvailable) {
    uxAvailable = uxSize;
  }
  
  if (uxAvailable) {
    *ppvItems = pxBuff->pvItems + pxBuff->uxHead * pxBuff->uxItemSize;
  } else *ppvItems = NULL;

  return uxAvailable;
}

UBaseType_t xBuffer_EndPop(Buffer_t *pxBuff, UBaseType_t uxSize) {
  pxBuff->uxHead = (pxBuff->uxHead + uxSize) % pxBuff->uxWrap;
  return uxSize;
}

INLINE BaseType_t xBuffer_IsEmpty(Buffer_t *pxBuff) {
  return (pxBuff->uxTail == pxBuff->uxHead)? pdTRUE:pdFALSE;
}

INLINE BaseType_t xBuffer_IsFull(Buffer_t *pxBuff) {
  return (pxBuff->uxHead == (pxBuff->uxTail + 1) % pxBuff->uxWrap)? pdTRUE:pdFALSE;
}
