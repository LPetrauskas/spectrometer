
#include "led.h"


void vBoard_LED_Init(void)
{

	Chip_IOCON_PinMux(LPC_IOCON, LED_RED_PORT, LED_RED_PIN, LED_RED_MODE, LED_RED_FUNC);
	Chip_IOCON_PinMux(LPC_IOCON, LED_GREEN_PORT, LED_GREEN_PIN, LED_GREEN_MODE, LED_GREEN_FUNC);
	
	Chip_IOCON_PinMux(LPC_IOCON, HAL_LAMP_PORT, HAL_LAMP_PIN, HAL_LAMP_MODE, HAL_LAMP_FUNC);
	Chip_IOCON_PinMux(LPC_IOCON, UV_LAMP_PORT, UV_LAMP_PIN, UV_LAMP_MODE, UV_LAMP_FUNC);

	Chip_GPIO_SetPortDIROutput(LPC_GPIO, LED_RED_PORT, LED_RED_PIN);
	Chip_GPIO_SetPortDIROutput(LPC_GPIO, LED_GREEN_PORT, LED_GREEN_PIN);

	Chip_GPIO_SetPortDIROutput(LPC_GPIO, HAL_LAMP_PORT, HAL_LAMP_PIN);
	Chip_GPIO_SetPortDIROutput(LPC_GPIO, UV_LAMP_PORT, UV_LAMP_PIN);

	Chip_IOCON_EnableOD(LPC_IOCON, LED_RED_PORT, LED_RED_PIN);
	Chip_IOCON_EnableOD(LPC_IOCON, LED_GREEN_PORT, LED_GREEN_PIN);
	Chip_IOCON_EnableOD(LPC_IOCON, HAL_LAMP_PORT, HAL_LAMP_PIN);
	Chip_IOCON_EnableOD(LPC_IOCON, UV_LAMP_PORT, UV_LAMP_PIN);
	

	Chip_GPIO_SetPinState(LPC_GPIO, LED_RED_PORT, LED_RED_PIN, 0);
	Chip_GPIO_SetPinState(LPC_GPIO, LED_GREEN_PORT, LED_GREEN_PIN, 1);


	Chip_GPIO_SetPinState(LPC_GPIO, UV_LAMP_PORT, UV_LAMP_PIN, 0);
	Chip_GPIO_SetPinState(LPC_GPIO, HAL_LAMP_PORT, HAL_LAMP_PIN, 0);
}



void vBoard_LED_ToggleGreen(void)
{

	Chip_GPIO_SetPinToggle(LPC_GPIO, LED_GREEN_PORT, LED_GREEN_PIN);
}


void vBoard_LED_ToggleRed(void)
{
	Chip_GPIO_SetPinToggle(LPC_GPIO, LED_RED_PORT, LED_RED_PIN);
}

void vBoard_LED_SetGreen(void)
{

	Chip_GPIO_SetPinState(LPC_GPIO, LED_GREEN_PORT, LED_GREEN_PIN, 0);

}

void vBoard_LED_SetRed(void)
{
	Chip_GPIO_SetPinState(LPC_GPIO, LED_RED_PORT, LED_RED_PIN, 0);
}

void vBoard_LED_ClearGreen(void)
{

	Chip_GPIO_SetPinState(LPC_GPIO, LED_GREEN_PORT, LED_GREEN_PIN, 1);
}

void vBoard_LED_ClearRed(void)
{
	Chip_GPIO_SetPinState(LPC_GPIO, LED_RED_PORT, LED_RED_PIN, 1);
}


void vBoard_LED_ClearUV(void)
{
	Chip_GPIO_SetPinState(LPC_GPIO, UV_LAMP_PORT, UV_LAMP_PIN, 0);
}

void vBoard_LED_SetUV(void)
{

	Chip_GPIO_SetPinState(LPC_GPIO, UV_LAMP_PORT, UV_LAMP_PIN, 1);

}

void vBoard_LED_SetHalogen(void)
{
	Chip_GPIO_SetPinState(LPC_GPIO, HAL_LAMP_PORT, HAL_LAMP_PIN, 1);
}

void vBoard_LED_ClearHalogen(void)
{

	Chip_GPIO_SetPinState(LPC_GPIO, HAL_LAMP_PORT, HAL_LAMP_PIN, 0);
}

