#include "hal/usb.h"
#include "usb_setup.h"

BaseType_t xBoard_USB_Init(void) {
   Chip_USB_Init();

  // Chip_IOCON_PinMux(LPC_IOCON, USB_CONTROLLER_VBUS_PORT,  USB_CONTROLLER_VBUS_PIN,  USB_CONTROLLER_VBUS_MODE,  USB_CONTROLLER_VBUS_FUNC);

   Chip_IOCON_PinMux(LPC_IOCON, USB_CONTROLLER_DPLUS_PORT,  USB_CONTROLLER_DPLUS_PIN,  USB_CONTROLLER_DPLUS_MODE,  USB_CONTROLLER_DPLUS_FUNC);
   Chip_IOCON_PinMux(LPC_IOCON, USB_CONTROLLER_DMINUS_PORT, USB_CONTROLLER_DMINUS_PIN, USB_CONTROLLER_DMINUS_MODE, USB_CONTROLLER_DMINUS_FUNC);
   Chip_IOCON_PinMux(LPC_IOCON, USB_CONTROLLER_DEVICE_EN_PORT, USB_CONTROLLER_DEVICE_EN_PIN, USB_CONTROLLER_DEVICE_EN_MODE, USB_CONTROLLER_DEVICE_EN_FUNC);   
 //  Chip_GPIO_SetPinDIROutput(LPC_GPIO, USB_CONTROLLER_DEVICE_EN_PORT, USB_CONTROLLER_DEVICE_EN_PIN);
  // Chip_GPIO_WritePortBit(LPC_GPIO, USB_CONTROLLER_DEVICE_EN_PORT, USB_CONTROLLER_DEVICE_EN_PIN, 1);
   
   LPC_USB->USBClkCtrl = 0x12;                /* Dev, AHB clock enable */
   while ((LPC_USB->USBClkSt & 0x12) != 0x12);
   
 //  Chip_GPIO_WritePortBit(LPC_GPIO, USB_CONTROLLER_DEVICE_EN_PORT, USB_CONTROLLER_DEVICE_EN_PIN, 0);
   return pdFREERTOS_ERRNO_NONE;
}

void vBoard_USB_Deinit(void) {
  LPC_USB->USBClkCtrl = 0;                /* Dev, AHB clock enable */
}