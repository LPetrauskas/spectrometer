#include "ccd.h"

static volatile BaseType_t shCount = 0;
static volatile BaseType_t mclkCount = 0;

static struct{
	UBaseType_t xInits;
	uint32_t intTime;
	uint32_t clkPeriod;
	xSemaphoreHandle xMutex;
}CCDData = {0};


void TIMER1_IRQHandler(void)
{

}

BaseType_t xBoard_CCD_Init(void)
{
	BaseType_t error = pdFREERTOS_ERRNO_NONE;
	if(!CCDData.xInits)
	{
		CCDData.xMutex = xSemaphoreCreateMutex();
		if(CCDData.xMutex == NULL)
		{
	      return pdFREERTOS_ERRNO_ENOMEM;
		}
	    if ((error = xBoard_CCD_SetClkPeriod(1000)) != pdFREERTOS_ERRNO_NONE)
	    		return error;
	    if ((error = xBoard_CCD_SetIntegrationTime(15000)) != pdFREERTOS_ERRNO_NONE)
		    	return error;

	    Chip_IOCON_PinMux(LPC_IOCON, CCD_ICG_PORT, CCD_ICG_PIN, CCD_ICG_MODE, CCD_ICG_FUNC);
	    Chip_GPIO_SetPinDIROutput(LPC_GPIO, CCD_ICG_PORT, CCD_ICG_PIN);
		Chip_GPIO_WritePortBit(LPC_GPIO, CCD_ICG_PORT, CCD_ICG_PIN, true);

	    Chip_IOCON_PinMux(LPC_IOCON, CCD_MCLK_PORT, CCD_MCLK_PIN, CCD_MCLK_MODE, CCD_MCLK_FUNC);
	    Chip_GPIO_SetPinDIROutput(LPC_GPIO, CCD_MCLK_PORT, CCD_MCLK_PIN);
		Chip_GPIO_WritePortBit(LPC_GPIO, CCD_MCLK_PORT, CCD_MCLK_PIN, false);

	    Chip_IOCON_PinMux(LPC_IOCON, CCD_SH_PORT, CCD_SH_PIN, CCD_SH_MODE, CCD_SH_FUNC);
	    Chip_GPIO_SetPinDIROutput(LPC_GPIO, CCD_SH_PORT, CCD_SH_PIN);
		Chip_GPIO_WritePortBit(LPC_GPIO, CCD_SH_PORT, CCD_SH_PIN, false);

		/* Configure Timer for Master Clock */
	    Chip_TIMER_Init(CCD_MCLK_TIMER);
	    Chip_Clock_SetPCLKDiv(CCD_MCLK_TIMER_CLK , SYSCTL_CLKDIV_1);
		NVIC_DisableIRQ(CCD_MCLK_TIMER_IRQ);
		Chip_TIMER_TIMER_SetCountClockSrc(CCD_MCLK_TIMER, TIMER_CAPSRC_RISING_PCLK, 0);
		Chip_TIMER_PrescaleSet(CCD_MCLK_TIMER, 0);
		Chip_TIMER_SetMatch(CCD_MCLK_TIMER, CCD_MCLK_MAT, 22);
		Chip_TIMER_ResetOnMatchEnable(CCD_MCLK_TIMER, CCD_MCLK_MAT);
		Chip_TIMER_ExtMatchControlSet(CCD_MCLK_TIMER, 0, TIMER_EXTMATCH_TOGGLE, CCD_MCLK_MAT);
		Chip_TIMER_Enable(CCD_MCLK_TIMER);

		/* Configure Timer for Shutter */	
	    Chip_TIMER_Init(CCD_SH_TIMER);
	    Chip_Clock_SetPCLKDiv(CCD_SH_TIMER_CLK , SYSCTL_CLKDIV_1);
	    NVIC_DisableIRQ(CCD_SH_TIMER_IRQ);
		Chip_TIMER_TIMER_SetCountClockSrc(CCD_SH_TIMER, TIMER_CAPSRC_RISING_PCLK, 0);
		Chip_TIMER_PrescaleSet(CCD_SH_TIMER, 0);
		Chip_TIMER_SetMatch(CCD_SH_TIMER, CCD_SH_MAT,  ((Chip_Clock_GetSystemClockRate()/1000000)*CCDData.intTime)/2000);
		Chip_TIMER_ResetOnMatchEnable(CCD_SH_TIMER, CCD_SH_MAT);
		Chip_TIMER_ExtMatchControlSet(CCD_SH_TIMER, 0, TIMER_EXTMATCH_CLEAR, CCD_SH_MAT);
		Chip_TIMER_Enable(CCD_SH_TIMER);


	}
	CCDData.xInits++;	
	return error;
}




void vBoard_CCD_Read(void)
{
	BaseType_t wait = 150;
	xSemaphoreTake(CCDData.xMutex, portMAX_DELAY);

	Chip_TIMER_Disable(CCD_SH_TIMER);
	Chip_TIMER_SetMatchDrive(CCD_SH_TIMER, CCD_SH_MAT, false);
	Chip_GPIO_WritePortBit(LPC_GPIO, CCD_ICG_PORT, CCD_ICG_PIN, false);
	Chip_TIMER_SetMatchDrive(CCD_SH_TIMER, CCD_SH_MAT, true);
	Chip_TIMER_Reset(CCD_SH_TIMER);

	/* Insert Awful delay to allow SH pin to go up before pulling it to ground */
	while(wait--);
	Chip_TIMER_SetMatchDrive(CCD_SH_TIMER, CCD_SH_MAT, false);
	Chip_TIMER_Enable(CCD_SH_TIMER);
	Chip_GPIO_WritePortBit(LPC_GPIO, CCD_ICG_PORT, CCD_ICG_PIN, true);
	xSemaphoreGive(CCDData.xMutex);
	
}


BaseType_t xBoard_CCD_SetIntegrationTime(uint32_t intTime)
{
	if (intTime <= CCD_INT_TIME_MIN)
	{
		return pdFREERTOS_ERRNO_EINVAL;
	}

	xSemaphoreTake(CCDData.xMutex, portMAX_DELAY);
	CCDData.intTime = intTime;
	if(CCDData.xInits)
	{
		Chip_TIMER_SetMatch(CCD_SH_TIMER, CCD_SH_MAT, ((Chip_Clock_GetSystemClockRate()/1000000)*CCDData.intTime)/2000);
		Chip_TIMER_ExtMatchControlSet(CCD_SH_TIMER, 0, TIMER_EXTMATCH_TOGGLE, CCD_SH_MAT);

	}

	xSemaphoreGive(CCDData.xMutex);

	return pdFREERTOS_ERRNO_NONE;
}


BaseType_t xBoard_CCD_SetClkPeriod(uint32_t clkPeriod)
{
	if ((clkPeriod <= CCD_CLK_PERIOD_MIN) || (clkPeriod >= CCD_CLK_PERIOD_MAX))
	{
		return pdFREERTOS_ERRNO_EINVAL;
	}

	xSemaphoreTake(CCDData.xMutex, portMAX_DELAY);
	CCDData.clkPeriod = clkPeriod;
	if(CCDData.xInits)
		Chip_TIMER_SetMatch(CCD_MCLK_TIMER, CCD_MCLK_MAT,((Chip_Clock_GetSystemClockRate()/1000000)*clkPeriod)/1000);
	xSemaphoreGive(CCDData.xMutex);

	return pdFREERTOS_ERRNO_NONE;
}

BaseType_t xBoard_CCD_EnableElectronicShutter(void)
{
	if(CCDData.xInits)
	{
		xSemaphoreTake(CCDData.xMutex, portMAX_DELAY);

		Chip_TIMER_ExtMatchControlSet(CCD_SH_TIMER, 0, TIMER_EXTMATCH_TOGGLE, CCD_SH_MAT);	
		xSemaphoreGive(CCDData.xMutex);
		return pdFREERTOS_ERRNO_NONE;

	}	
	
	return pdFREERTOS_ERRNO_ENODEV;
}




BaseType_t xBoard_CCD_DisableElectronicShutter(void)
{
	if(CCDData.xInits)
	{
		xSemaphoreTake(CCDData.xMutex, portMAX_DELAY);

		Chip_TIMER_ExtMatchControlSet(CCD_SH_TIMER, 0, TIMER_EXTMATCH_CLEAR, CCD_SH_MAT);	
		xSemaphoreGive(CCDData.xMutex);

		return pdFREERTOS_ERRNO_NONE;
	}

	return pdFREERTOS_ERRNO_ENODEV;
}
