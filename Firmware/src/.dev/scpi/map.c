


typedef struct __scpi_table_entry_t
{
	scpi_object_t *key;
	scpi_object_t *value;	
} scpi_map_entry_t;

typedef struct _scpi_table_object_t
{
	SCPI_OBJECT_PROLOGUE;
	scpi_map_entry_t entries[];	
	int size;	
} scpi_map_object_t;

const scpi_type_t scpi_map_type_t;

