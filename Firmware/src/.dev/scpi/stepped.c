#include "scpi.h"

typedef struct _scpi_step_t{
	SCPI_OBJECT_PROLOGUE;
	scpi_object_t *incr;
	scpi_object_t *pdec;
	scpi_object_t *mode;
} scpi_step_t;

typedef struct _scpi_stepped_t{
	SCPI_OBJECT_PROLOGUE;
	scpi_object_t *step;
} scpi_stepped_t;

static int scpi_stepped_perform(scpi_object_t *this, scpi_object_t *args) {
	int len; scpi_object_t *arg;

	scpi_object_t *value = scpi_object_query(this, NULL);
	scpi_object_t *step = STEPPED_STEP(this);

	len = scpi_array_len(args);
	while(len--) {
		arg = scpi_array_pop(args);
		
		
		if (arg == scpi_up || arg == scpi_down) {
			op = (arg == scpi_up)? '+':'-';
			if (STEP_MODE(step) == scpi_linear) {
				scpi_object_binary_op()
			} else if (STEP_MODE(step) == scpi_linear) {

			} else {
				scpi_error_set();
				return -1;
			}
			arg = scpi_object_binary_op(, STEPPED_STEP(this), '+');
		}
		if () {

			
		}
		if (arg == scpi_down) {
			
			arg = scpi_object_binary_op(scpi_object_query(this, NULL), STEPPED_STEP(this), '-');	
		}
		scpi_array_push(args, arg);
	}
	return scpi_object_perform(SCPI_PROTO(this), args);
}

const scpi_type_t scpi_



scpi_object_t *scpi_set_step(scpi_object_t *obj) {
	scpi_object_t *new = scpi_object_new(&scpi_stepped_type);
	if (new != NULL) {
		if (SCPI_INSTANCE_OF(obj, scpi_numeric_type))
	}
	
	return new;
}