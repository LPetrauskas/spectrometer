{%
	
	#include "scpi_parser.tab.h";

	#define intchar(c) ((c)-0x30)

	int  yytextindex, yytextsize;
	char yytextbuffer[YY_BUF_SIZE];
%}

/* Declarations section */

%option   8bit reentrant 
%option   bison-bridge bison-locations
%option   case-insensitive

SIGN "+"|"-"
DIGIT [0-9]

MULT "EX"|"PE"|"MA"|[TGKMUNPFA]

%%
/* Rules section */

BEGIN(INITIAL);

\"							{ yytextbuffer[0] = '\0'; BEGIN(DQSTRING); }
<DQSTRING>{
	[^\"]+					{ strcat(yytextbuffer, yytext); }
	\"/\"					{ strcat(yytextbuffer, "\""); }
	\"						{ yylval = scpi_string_from_chars(yytextbuffer); return STRING; }					
}

\'							{ yytextbuffer[0] = '\0'; BEGIN(QSTRING); }
<QSTRING>{
	[^\']+					{ strcat(yytextbuffer, yytext); }
	\'/\'					{ strcat(yytextbuffer, "'"); }
	\'						{ yylval = scpi_string_from_chars(yytextbuffer); return STRING; }					
}

"#"[1-9] 			{ yytextsize = intchar(yytext[1]); yytextindex = 0; BEGIN(RAWHEAD); }
<RAWHEAD>{
	[0-9]			{ 
						if(yytextindex == yytextsize) { 
							yytextbuffer[yytextindex] = '\0';
							yytextsize = atoi(yytextbuffer);
							yytextindex = 0;
							BEGIN(RAWBODY);  							
						} else {
							yytextbuffer[yytextindex++] = yytext[0];							
						}
					}
	[^0-9]			
}
<RAWBODY>{
	.|\n			{ 
						if (yytextindex == yytextsize) {
							yylval = scpi_flat_from_bytes(yytextbuffer, yytextsize);
							return RAW;					  	
					  	} else {
							yytextbuffer[yytextindex++] = yytext[0];							
					  	} 					  	
					}
}

"#B"				{ BEGIN(BINARY); }
<BINARY>{
	[0-1]+			{ yylval = scpi_integer_from_long(strtol(&yytext[2], NULL, 2)); return INTEGER; }
	[^0-1]				
}

"#Q"				{ BEGIN(OCTAL); }
<OCTAL>{
	[0-7]+			{ yylval = scpi_integer_from_long(strtol(&yytext[2], NULL, 8)); return INTEGER; }	
	[^0-7]				
}

"#H"				{ BEGIN(HEX); }
<HEX>{
	[0-9A-F]+		{ yylval = scpi_integer_from_long(strtol(&yytext[2], NULL, 16)); return INTEGER; }
	[^0-9A-F]
}

{SIGN}?{DIGIT}+		{ yylval = scpi_integer_from_long(strtol(yytext, NULL, 10)); return INTEGER; }

"NAN"												|
"INF"												|
"NINF"												|
{SIGN}?{DIGIT}+"."{DIGIT}+("E"{SIGN}{DIGIT}+)?		{ 
														if (!strcmp(yytext, "NINF")) yytext[0] = '-'; 
														yylval = scpi_numeric_from_double(strtod(yytext, NULL)); 
														return DECIMAL; 
													}
 
[A-Z][0-9A-Z_]*			{ 
							yylval = scpi_object_attr(&scpi_local_scope, yytext); 

							if (yylval != NULL) {
								return MNEMONIC;								
							}

							yylval = scpi_object_attr(&scpi_global_scope, yytext); 

							if (yylval != NULL) {
								return MNEMONIC;								
							}

							yylval = scpi_unit_from_symbol(yytext);
							return UNIT;
						}

"MOD"				{ return MOD; }
"DIV"				{ return DIV; }
"REM"				{ return REM; }
"NOT"				{ return NOT; }
[ \t]				{ return WHITESPACE; }
.|\n				{ return yytext[0]; }

%%
/* Code section */