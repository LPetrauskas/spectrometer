
#define SI_UNITS_COUNT 7
#define UNIT_SI_UNITS(obj) (((scpi_unit_t *)object)->si_units)
#define UNIT_SI_UNIT(obj, i) (UNIT_SI_UNITS(obj)[i])
#define UNIT_SI_MULT(obj)	(((scpi_unit_t *)object)->si_pfx.mult)
#define UNIT_SI_DB(obj)		(((scpi_unit_t *)object)->si_pfx.db)

typedef struct _scpi_unit_t {
	SCPI_OBJECT_EPILOGUE;	
	char si_units[SI_UNITS_COUNT];	
	struct { 
		unsigned db: 1;
		signed mult: 7;
	} si_pfx;
} scpi_unit_t;

static scpi_object_t *scpi_unit_integer_binary_op(scpi_object_t *this, scpi_object_t *other, char op)
{
	scpi_object_t *new;
	int i; long rvalue;	
	if (op == '^' && !UNIT_SI_DB(object)) {
		new = scpi_object_new(&scpi_unit_type);
		if (new != NULL) {		
			rvalue = scpi_integer_as_long(other);				
			for (i = 0 ; i < SI_UNITS_COUNT ; i++) {
				UNIT_SI_UNIT(new, i) = UNIT_SI_UNIT(this, i) + rvalue;				
			}
			UNIT_SI_MULT(new) = UNIT_SI_MULT(this);
			UNIT_SI_DB(new) = 0;
		}
		return new;
	}
	scpi_error_set();
	return NULL;
}

static scpi_object_t *scpi_unit_unit_binary_op(scpi_object_t *this, scpi_object_t *other, char op) {
	int i; 
	scpi_object_t *new = NULL;	

	switch(op)
	{
		case '+':
		case '-':
			if (memcmp(UNIT_SI_UNITS(this), UNIT_SI_UNITS(other), SI_UNITS_COUNT)) {
				scpi_error_set();
				break;
			}
			new = scpi_object_new(&scpi_unit_type);
			if (new != NULL) {
				memcpy(UNIT_SI_UNITS(new), UNIT_SI_UNITS(other), SI_UNITS_COUNT);
				UNIT_SI_MULT(new) = MIN(UNIT_SI_MULT(this), UNIT_SI_MULT(other));
				UNIT_SI_DB(new) = UNIT_SI_DB(this) && UNIT_SI_DB(other);
			}
			break;
		case '*':			
			new = scpi_object_new(&scpi_unit_type);
			if (new != NULL) {						
				for (i = 0 ; i < SI_UNITS_COUNT ; i++) {
					UNIT_SI_UNIT(new, i) = UNIT_SI_UNIT(this, i) + UNIT_SI_UNIT(other, i);				
				}
				UNIT_SI_MULT(new) = UNIT_MULT(this) + UNIT_MULT(other);
				UNIT_SI_DB(new) = UNIT_SI_DB(this) && UNIT_SI_DB(other);
			}
			break;
		case '/':
			new = scpi_object_new(&scpi_unit_type);
			if (new != NULL) {						
				for (i = 0 ; i < SI_UNITS_COUNT ; i++) {
					UNIT_SI_UNIT(new, i) = UNIT_SI_UNIT(this, i) - UNIT_SI_UNIT(other, i);				
				}
				UNIT_SI_MULT(new) = UNIT_SI_MULT(this) - UNIT_SI_MULT(other);
				UNIT_SI_DB(new) = UNIT_SI_DB(this) && UNIT_SI_DB(other);
			}
			break;
		default:
			scpi_error_set();
			break;
	}
	return new;
}

static scpi_object_t *scpi_unit_binary_op(scpi_object_t *this, scpi_object_t *other, char op) {
	if (SCPI_IS_INSTANCE(other, scpi_unit_type)) {
		return scpi_unit_unit_binary_op(this, other, op);
	}
	if (SCPI_IS_INSTANCE(other, scpi_integer_type)) {
		return scpi_unit_integer_binary_op(this, other, op);	
	}
	scpi_error_set();
	return NULL;
}



const scpi_type_t scpi_unit_type = {

	sizeof(scpi_unit_t), /* object size */

	/* base primitives */
	&scpi_base_type.new,	/* new method */
	&scpi_base_type.clone,  /* clone method */
	&scpi_base_type.del,	/* delete method */
	
	/* attr primitives */
	NULL,					/* get method */
	NULL,					/* set method */
	
//	/*serialize primitives */
//	&scpi_string_load		/* load method */
//	&scpi_string_dump		/* dump method */
	
	/* expression primitives */
	&scpi_unit_binary_op,	/*binary operation method*/
	NULL,					/*unary operation method*/
	
	/*command primitives */
	NULL,					/* perform method */ 
	NULL,					/* query method */ 
}


#define SI_MULT_SYMBOL_SIZE 3
#define SI_UNIT_SYMBOL_SIZE 5

typedef struct _scpi_unit_symbol_t {
	char symbol[SI_MULT_SYMBOL_SIZE];
	char mult;
} scpi_mult_def_t;

typedef struct _scpi_unit_def_t {
	char symbol[SI_UNIT_SYMBOL_SIZE];
	char si_units[SI_UNITS_COUNT];	
} scpi_unit_def_t;

static scpi_mult_def_t *_base_mults;
static int _base_mults_qty;

static scpi_unit_def_t *_base_units;
static int _base_units_qty;

static char mult_backsearch_by_symbol(char *symbol, char **next) {
	int i;
	for (i = 0;i < _base_mults_qty;i++) {
		if (!sstrcmp(symbol, _base_mults[i].symbol, next, -1)) {
			return _base_mults[i].mult;
		}
	}
	return -1;	
}
	
static char *mult_search_by_mult(char mult) {
	int i;
	for (i = 0;i < _base_mults_qty;i++) {
		if (mult = _base_mults[i].mult) {
			return _base_mults[i].symbol;
		}
	}
	return NULL;
}



scpi_object_t *scpi_unit_from_symbol(const char *_symbol) {
	char *symbol, *units, mult, db;	

	units = unit_backsearch_by_symbol(&symbol);
	if (units == NULL) {
		scpi_error_set();
		return NULL;
	}

	mult = mult_backsearch_by_symbol(&symbol);
	if (mult == -1) {
		scpi_error_set();
		return NULL;	
	}

	db = (!sstrcmp("DB", _symbol, NULL, 0));			 		

	scpi_object_t *new = scpi_object_new(&scpi_unit_type);
	if (new != NULL) {
		memcpy(UNIT_SI_UNITS(new), units, SI_UNITS_COUNT);
		UNIT_SI_MULT(new) = mult;
		UNIT_SI_DB(new) = db;
	}

	return new;	
}

int scpi_unit_as_symbol(scpi_object_t *object, char *data, int max_size) {
	char *symbol;
	int size, next_size;
	
	if (!SCPI_INSTANCE_OF(object, scpi_unit_type)) {
		scpi_error_set();
		return -1;
	}
	if (UNIT_SI_DB(unit)) {
		if (size + 2 > max_size) {
			scpi_error_set();
			return size + 2;
		}	
		strcpy(data, "dB");
		size += 2;
	}
	if (UNIT_SI_MULT(unit)) {
		symbol = symbol_search_by_mult(UNIT_SI_MULT(unit));
		if (strlen(symbol) + size > max_size) {
			scpi_error_set();
			return strlen(symbol) + size;
		}
		strcpy(data, symbol);		
		size += strlen(symbol);
	}

	symbol = symbol_search_by_units(UNIT_SI_UNITS(unit));
	if (symbol == NULL) {
		snprintf(data, max_size - size, "m^%u*kg^%u*s^%u*A^%u*K*%u*mol^%u*cd^%u", 
			UNIT_SI_UNIT(unit, 0), UNIT_SI_UNIT(unit, 1), UNIT_SI_UNIT(unit, 2), 
			UNIT_SI_UNIT(unit, 3), UNIT_SI_UNIT(unit, 4), UNIT_SI_UNIT(unit, 5),
			UNIT_SI_UNIT(unit, 6));
	}

	if (strlen(symbol) + size > max_size) {
		strcpy(data, symbol);
		
	}
	size += strlen(symbol);
	
	return size;
}

scpi_unit_ratio()