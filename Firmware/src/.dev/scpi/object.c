


#define SCPI_INSTANCE_OF(this, type)  ((this)->_type == &(type))
#define SCPI_INCREF(obj)	((obj)->_ref)++
#define SCPI_DECREF(obj)	if (--((obj)->_ref)) scpi_object_delete(obj)


scpi_object_t *scpi_object_new(scpi_type_t *type) {

}

scpi_object_t *scpi_object_unary_op(scpi_object_t *this, char op) {
	while (this != NULL) {
		if (this->_type->_u_op != NULL) {
			return this->_type->_u_op(this, op);	
		}	
		this = this->_proto;
	}
	scpi_error_set();
	return -1;
}

scpi_object_t *scpi_object_binary_op(scpi_object_t *this, scpi_object_t *other, char op) {
	while (this != NULL) {
		if (this->_type->_b_op != NULL) {
			return this->_type->_b_op(this, other, op);	
		}	
		this = this->_proto;
	}
	scpi_error_set();
	return NULL;
}

int scpi_object_perform(scpi_object_t *this, scpi_object_t *args) {
	while (this != NULL) {
		if (this->_type->_perform != NULL) {
			return this->_type->_perform(this, args);	
		}	
		this = this->_proto;
	}
	scpi_error_set();
	return -1;
}

scpi_object_t *scpi_object_attr(scpi_object_t *this, scpi_object_t *name) {
	while (this != NULL) {
		if (this->_type->_attr != NULL) {
			return this->_type->_attr(this, name);	
		}	
		this = this->_proto;
	}
	scpi_error_set();
	return NULL;
}

scpi_object_t *scpi_object_query(scpi_object_t *this, scpi_object_t *args) {
	while (this != NULL) {
		if (this->_type->_query != NULL) {
			return this->_type->_query(this, args);	
		}	
		this = this->_proto;
	}
	scpi_error_set();
	return NULL;
}

int scpi_object_delete(scpi_object_t *this) {
	int outcome; scpi_object_t *that;
	do {
		that = this->_proto;
		outcome = this->_type->_delete(this);
		this = that;
	} while (this != NULL && !outcome);
	return outcome;
}
