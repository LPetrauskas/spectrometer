#include "scpi.h"

typedef struct _scpi_var_t {
	SCPI_OBJECT_PROLOGUE;
} scpi_var_t;


static int scpi_var_perform(scpi_object_t *this, scpi_object_t *args) {
	scpi_object_t *old, *new;

	if (scpi_array_len(args) != 1) {
		scpi_error_set();
		return -1;
	}
	new = scpi_array_index(args, 0);
	
	old = SCPI_PROTO(this)
	SCPI_DECREF(old);
	SCPI_INCREF(new);
	SCPI_PROTO(this) = new;
	return -1;
}

static scpi_object_t *scpi_var_query(scpi_object_t *this, scpi_object_t *args) {
	return SCPI_PROTO(this);
}

/*** Exported symbols ***/

const scpi_type_t scpi_var_type = {

	sizeof(scpi_var_t), /* object size */

	/* base primitives */
	&scpi_base_type.new,	/* new method */
	&scpi_base_type.clone,  /* clone method */
	&scpi_base_type.del,	/* delete method */
	
	/* attr primitives */
	NULL,					/* get method */
	NULL,					/* set method */
	
	// /*serialize primitives */
	// &scpi_integer_load		/* load method */
	// &scpi_integer_dump		/* dump method */
	
	/* expression primitives */
	NULL,		/*binary operation method*/
	NULL,		/*unary operation method*/
	
	/*command primitives */
	&scpi_var_perform,		/* perform method */ 
	&scpi_var_query,		/* query method */ 
}
