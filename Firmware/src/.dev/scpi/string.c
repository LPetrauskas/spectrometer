#include "scpi.h"

#define STRING_VALUE(obj) (((scpi_string_t *)obj)->value)

typedef struct _scpi_string_t {
	SCPI_OBJECT_PROLOGUE;
	char *value;	
} scpi_string_t;

static scpi_object_t *scpi_string_string_binary_op(scpi_object_t *this, scpi_object_t *other, char op) {		
	scpi_object_t *new;	

	if (op == '+') {	
		new = scpi_object_new(&scpi_mutable_string_type);	
		if (new != NULL) {
			STRING_VALUE(new) = malloc(strlen(STRING_VALUE(this)) + strlen(STRING_VALUE(other)) + 1);
			if (STRING_VALUE(new) == NULL) {
				scpi_object_delete(new);
				scpi_error_set();
				return NULL;
			}
			strcpy(STRING_VALUE(new), STRING_VALUE(this));
			strcat(STRING_VALUE(new), STRING_VALUE(other));	
		}
		return new;
	}	
	scpi_error_set();
	return NULL;
}

static scpi_object_t *scpi_string_integer_binary_op(scpi_object_t *this, scpi_object_t *other, char op) {			
	scpi_object_t *new;
	char *lvalue; long rvalue;
	
	if (op == '*') {
		rvalue = scpi_integer_as_long(other);
		new = scpi_object_new(&scpi_mutable_string_type);
		if (new != NULL) {
			STRING_VALUE(new) = malloc(strlen(STRING_VALUE(this)) * rvalue + 1);
			if (STRING_VALUE(new) == NULL) {
				scpi_object_delete(new);
				scpi_error_set();
				return NULL;
			}
			while (!rvalue) {
				strcat(STRING_VALUE(new), STRING_VALUE(this));
				rvalue--;
			}
		}
		return new;					
	}
	scpi_error_set("");
	return NULL;
}

static scpi_object_t *scpi_string_binary_op(scpi_object_t *this, scpi_object_t *other, char op) {	
	if (SCPI_INSTANCE_OF(other, scpi_integer_type)) {	
		return scpi_string_integer_binary_op(this, other, op);
	}
	if (SCPI_INSTANCE_OF(other, scpi_mutable_string_type) || 
			SCPI_INSTANCE_OF(other, scpi_immutable_string_type)) {
		return scpi_string_string_binary_op(this, other, op);
	}
	scpi_error_set();
	return NULL;	
}


static int scpi_mutable_string_delete(scpi_object_t *this) {
	free(STRING_VALUE(this));	
	free(this);
	return 0;
}

/*** Exported symbols ***/

const scpi_type_t scpi_mutable_string_type = {

	sizeof(scpi_string_t), /* object size */

	/* base primitives */
	&scpi_base_type.new,	/* new method */
	&scpi_string_clone,  	/* clone method */
	&scpi_string_delete,	/* delete method */
	
	/* attr primitives */
	NULL,					/* get method */
	NULL,					/* set method */
	
//	/*serialize primitives */
//	&scpi_string_load		/* load method */
//	&scpi_string_dump		/* dump method */
	
	/* expression primitives */
	&scpi_string_binary_op,	/*binary operation method*/
	&scpi_string_unary_op,		/*unary operation method*/
	
	/*command primitives */
	NULL,					/* perform method */ 
	NULL,					/* query method */ 
}

const scpi_type_t scpi_immutable_string_type = {

	sizeof(scpi_string_t), /* object size */

	/* base primitives */
	&scpi_base_type.new,	/* new method */	
	&scpi_base_type.del,	/* delete method */
	
	/* attr primitives */
	NULL,					/* get method */
	NULL,					/* set method */
	
//	/*serialize primitives */
//	&scpi_string_load		/* load method */
//	&scpi_string_dump		/* dump method */
	
	/* expression primitives */
	&scpi_string_binary_op,	/*binary operation method*/
	&scpi_string_unary_op,	/*unary operation method*/
	
	/*command primitives */
	NULL,					/* perform method */ 
	NULL,					/* query method */ 
}


const char *scpi_string_as_chars(scpi_object_t *object) {	
	if (SCPI_INSTANCE_OF(object, scpi_mutable_string_type) || 
			SCPI_INSTANCE_OF(object, scpi_immutable_string_type)) {
		return STRING_VALUE(object);	
	}	 		
	scpi_error_set();		
	return NULL;		
}

scpi_object_t *scpi_string_from_const_chars(const char *value) {
	scpi_object_t *object = scpi_object_new(&scpi_immutable_string_type);
	if (object != NULL) {
		 STRING_VALUE(object) = value;		 
	}
	return object;
}

scpi_object_t *scpi_string_from_chars(const char *value) {	
	scpi_object_t *object = scpi_object_new(&scpi_mutable_string_type);
	if (object != NULL) {
		STRING_VALUE(object) = malloc(strlen(value) + 1);
		if (STRING_VALUE(object) == NULL) {
			scpi_object_delete(object);
			scpi_error_set();
			return NULL;
		}
		strcpy(STRING_VALUE(object), value);				
	}
	return object;
}
