#ifndef DEVICES_H
#define DEVICES_H

#ifdef __cplusplus
extern "C" {
#endif


struct DevDescriptor_t;

typedef BaseType_t (*xDevGenericInit)(const DevDescriptor_t *pxDesc);
typedef BaseType_t (*xDevGenericOpen)(const DevDescriptor_t *pxDesc, Dev_t *pxDev, UBaseType_t usMode);
typedef BaseType_t (*xDevGenericWrite)(const DevDescriptor_t *pxDesc, void *pvData, UBaseType_t usSize);
typedef BaseType_t (*xDevGenericRead)(const DevDescriptor_t *pxDesc, void *pvData, UBaseType_t usSize);
typedef BaseType_t (*xDevGenericSeek)(const DevDescriptor_t *pxDesc, BaseType_t sWhence, BaseType_t sOffset);
typedef BaseType_t (*xDevGenericIoctl)(const DevDescriptor_t *pxDesc, BaseType_t sCmd, ...);
typedef BaseType_t (*xDevGenericClose)(const DevDescriptor_t *pxDesc *pxDev);
typedef BaseType_t (*xDevGenericDeinit)(const DevDescriptor_t *pxDesc);

struct DevOperations_t
{
	xDevGenericInit  pxInit;
	xDevGenericOpen  pxOpen;
	xDevGenericWrite pxWrite;
	xDevGenericRead  pxRead;
	xDevGenericSeek  pxSeek;
	xDevGenericIoctl pxIoctl;
	xDevGenericClose pxClose;
	xDevGenericDeinit pxDeinit;
};

struct DevDescriptor_t
{
	void *pvPrivate;
	DevOperations_t *pxOperations;
};

extern const DevDescriptor_t pxNullDevice;

#ifdef __cplusplus
}
#endif

#endif /* DEVICES_H */
