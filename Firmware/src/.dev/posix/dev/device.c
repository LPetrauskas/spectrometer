
#include "devices.h"

static BaseType_t xNullDeviceInit(const DevDescriptor_t *pxDescriptor);
static BaseType_t xNullDeviceOpen(const DevDescriptor_t *pxDescriptor, Dev_t *pxDev, UBaseType_t usMode);
static BaseType_t xNullDeviceWrite(Dev_t *pxDev, void *pvData, UBaseType_t usSize);
static BaseType_t xNullDeviceRead(Dev_t *pxdev, void *pvData, UBaseType_t usSize);
static BaseType_t xNullDeviceClose(Dev_t *pxDev);

const struct DevOperations_t xNullDeviceOps = {
	.pxInit = &xNullDeviceInit,
	.pxOpen = &xNullDeviceOpen,
	.pxWrite = &xNullDeviceWrite,
	.pxRead = &xNullDeviceRead,
	.pxSeek = &xNullDeviceSeek,
	.pxIoctl = &xNullDeviceIoctl,
	.pxClose = &xNullDeviceClose,
	.pxDeinit = &xNullDeviceDeinit
};

const struct DevDescriptor_t xNullDevice = {
	.pxPrivate = NULL,
	.pxOperations = &xNullDeviceOps
};


static BaseType_t xNullDeviceInit(const DevDescriptor_t * pxDescriptor)
{
	return pdPass;
}

static BaseType_t xNullDeviceOpen(const DevDescriptor_t * pxDescriptor, Dev_t *pxDev, UBaseType_t usMode, UBaseType_t usFlags)
{
	return pdPass;
}

static BaseType_t xNullDeviceWrite(Dev_t *pxDev, void *pvData, UBaseType_t usSize)
{
	return (BaseType_t)usSize;
}

static BaseType_t xNullDeviceRead(Dev_t *pxDev, void *pvData, UBaseType_t usSize)
{
	return -1;
}

static BaseType_t xNullDeviceSeek(Dev_t *pxDev, BaseType_t sOffset, BaseType_t sWhence)
{
	return sOffset;
}

static BaseType_t xNullDeviceIoctl(Dev_t *pxDev, BaseType_t sCmd, ...)
{
	return pdPass;
}

static BaseType_t xNullDeviceClose(Dev_t *pxDev)
{
	return pdPass;
}

static BaseType_t xNullDeviceDeinit(const DevDescriptor_t * pxDescriptor)
{
	return pdPass;
}