#!/bin/bash
usage()
{
cat <<EOF
usage: `basename $SCRIPT_NAME` options

This script loads a MCU device

Options:
	-n mcu_name		: name of device to be loaded
	-v mcu_vendor 		: as NXP, ST
	-d driver		: filename of driver to use
	-w wire 	        : name of script
        -h 			: show help
        [-f file]               : filename of binary to load
	[ -p ] 			: print (display the command only)
EOF
}

terminate() {
	if [ -e "$EMU_UTIL_OUTPUT" ] ; then
		rm $EMU_UTIL_OUTPUT
	fi
	exit $1
}

SCRIPT_NAME=$0
SCRIPT_HOME="$( cd "$( dirname "$0" )" && pwd )" 

LOGGING=0
PRINT=0

EMU_UTIL=
EMU_UTIL_OUTPUT=`mktemp -t emu$$XXX`

optspec="hpw:f:n:v:d:s:"
while getopts "$optspec" OPTION
do
	case $OPTION in
	s)
		SCRIPT_NAME=${OPTARG}
		;;
	n)
		EMU_NAME=${OPTARG}
		;;
	v)
		EMU_VENDOR=${OPTARG}
		;;
	d)	
		EMU_DRIVER=${OPTARG}
		;;
	w)
		EMU_WIRE=${OPTARG}
		;;
	f)
		EMU_BINARY=${OPTARG}
		;;
	p)
		PRINT=1
		;;
	h)
		usage
		terminate 0
		;;
	?)
		usage
		terminate 0
		;;
	esac	
done


if [ -z "$EMU_NAME" ] ; then
	usage
	terminate 1
fi
if [ -z "$EMU_DRIVER" ] ; then
	usage
	terminate 1
fi
if [ -z "$EMU_VENDOR" ] ; then
	usage
	terminate 1
fi

case $EMU_NAME in
    LPC17*)
	EMU_UTIL=$SCRIPT_HOME/crt_emu_cm3_nxp
	;;
    LPC11*|LPC12*|LPC13*)
	EMU_UTIL=$SCRIPT_HOME/crt_emu_lpc11_lpc13
	;;
    LPC18*|LPC43*)
	EMU_UTIL=$SCRIPT_HOME/crt_emu_lpc18_43_nxp
	;;
    LPC21*|LPC22*|LPC23*|LPC24*)
	EMU_UTIL=$SCRIPT_HOME/crt_emu_a7_nxp
	;;
    LPC31*|LPC32*|LPC29*)
	EMU_UTIL=$SCRIPT_HOME/crt_emu_a9_nxp
	;;
    LM3S*)
	EMU_UTIL=$SCRIPT_HOME/crt_emu_cm3_lmi
	;;
    STM32F1)
	EMU_UTIL=$SCRIPT_HOME/crt_emu_cm3_stm32f1
	;;
    STM32F2)
	EMU_UTIL=$SCRIPT_HOME/crt_emu_cm3_stm32f2
	;;
    *)
	EMU_UTIL=$SCRIPT_HOME/crt_emu_cm3_gen
	;;
esac

driver=`ls $EMU_DRIVER`
driver_count=`echo $driver | wc -w`
if [ $driver_count -ne 1 ] ; then
	echo "Cannot find unique flash driver. Found '$driver'"
	terminate 12
fi


if [ ! -z "$EMU_BINARY" ] ; then
    emu_options="-p$EMU_NAME -vendor=$EMU_VENDOR -wire=$EMU_WIRE -flash-driver=$EMU_DRIVER -flash-load-exec=$EMU_BINARY -s250"
else
    emu_options="-2 -e0  -p$EMU_NAME -wire=$EMU_WIRE -flash-driver=$EMU_DRIVER"
fi

echo "$EMU_UTIL $emu_options"
if [ $PRINT -eq 0 ] ; then 
	$EMU_UTIL $emu_options > $EMU_UTIL_OUTPUT
	if [ $LOGGING -eq 1 ] ; then
		cat $EMU_UTIL_OUTPUT
	fi
	emu_exit_code=$?
	if [ $emu_exit_code -ne 0 ] ; then
		echo "`basename $EMU_UTIL` exit code was: $emu_exit_code"
	elif [ ! -z "$EMU_BINARY" ] ; then
		echo "Loaded $EMU_NAME ($EMU_VENDOR) with `basename $EMU_BINARY`"
	else
	    	echo "Loaded $EMU_NAME ($EMU_VENDOR)"
	fi
fi
terminate 0
