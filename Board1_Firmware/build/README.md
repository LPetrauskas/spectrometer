# Build system

The build system is make-based.

## Usage

Every make run must be performed at a root level. Standard
targets should be implemented on each package, then reached
running:

```sh
make target-package
```

## Layout

It should be structured as follows:

/Makefile
/src
   package1/
      Makefile
      manifest.mk
   package2/
      Makefile
      manifest.mk
   ...

## Concept

A root Makefile defines the compilation rules for the packages 
within the /src directory. Also sets the toolchain and common 
build constants.

At a package level, another Makefile (one for each package) manages
its own scope, overriding root Makefile definitions as needed.

Inter-dependency among packages is resolved through manifests. A manifest
is a special Makefile, intended for each package to expose itself (meaning 
include paths, libraries, executables, linking scripts, compilation flags, etc.) 
to the rest using GNU standard Make variables. 

When make is called at the root level to build a package, all the packages
in which the former relies are built first. Then, the manifests of this packages
are then preprocessed before building the intended package, leaving all necessary
in the proper variables.


