#ifndef CMPACTION_H
#define CMPACTION_H

#include "cmp.h"
#include "string.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup app_cmp_action MessagePack-based Actions
 * @ingroup app
 * @brief RPC-like implementation using MessagePack serialization protocol
 * @{
 */

typedef struct {
  cmp_ctx_t *ctx;
  uint32_t nargs;
  uint32_t nrets;
  uint32_t uid;  
} cmp_action_ctx_t;

typedef struct {
  const char *name;
  void (*handle)(cmp_action_ctx_t *ctx);
} cmp_action_t;
  
#define ACTION_NAME_MAX_SIZE  20

/*
 * ============================================================================
 * === Action API
 * ============================================================================
 */

#define base(ctx) ((ctx)->ctx)

/* Return result from CMP action ctx */
#define cmp_action_result(ctx) \
  cmp_action_reply(ctx, 0, false)

/* Start n-fragmented result */
#define cmp_action_start_result(ctx, n) \
  cmp_action_reply(ctx, n, false)

/* Next result fragment */
#define cmp_action_next_result(ctx) \
  cmp_action_reply(ctx, -1, false) 

/* Throw an error from CMP action ctx */
#define cmp_action_error(ctx, msg)                      \
  cmp_action_reply(ctx, 0, true);			\
  cmp_write_str(base(ctx), msg, strlen(msg))

/*
 * ============================================================================
 * === Main API
 * ============================================================================
 */

/* Serves a set of actions on the given CMP context */
bool cmp_action_serve(cmp_action_ctx_t *ctx, const cmp_action_t *actions); 

/* Returns a result from CMP action ctx */
bool cmp_action_reply(cmp_action_ctx_t *ctx, size_t n, bool error);

/**
 * @}
 */

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* CMPACTION_H */
