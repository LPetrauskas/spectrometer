#include "io.h"
#include "string.h"

void iostream_init(iostream_t *stream, ioread_t read, iowrite_t write) {  
  stream->obuffer.count = IOBUFFER_MAX_SIZE;
  stream->obuffer.next = 0;
  stream->ibuffer.count = 0;
  stream->ibuffer.next = 0;
  stream->read = read;
  stream->write = write;
}

size_t iostream_read(iostream_t *stream, void *data, size_t size) {
  size_t read, remaining;

  remaining = size;
  while (remaining > 0) {
    if (stream->ibuffer.count == 0) {      
      read = stream->read(stream->ibuffer.mem, IOBUFFER_MAX_SIZE, 0);
      if (read < 0) return read;
      stream->ibuffer.count = read;
      stream->ibuffer.next = 0;
    } 
    read = (stream->ibuffer.count < remaining)? stream->ibuffer.count:remaining;
    memcpy(data, &stream->ibuffer.mem[stream->ibuffer.next], read);          
    stream->ibuffer.next += read;
    stream->ibuffer.count -= read;
    remaining -= read;
    data += read;
  }
  return size - remaining;
}

size_t iostream_write(iostream_t *stream, const void *data, size_t size) {
  size_t write, remaining;
  
  remaining = size;
  while (remaining > 0) {
    if (stream->obuffer.count == 0) {
      write = iostream_flush(stream);
      if (write < 0) break;
      stream->obuffer.count = write;
      stream->obuffer.next = 0;
    }   
    write = (stream->obuffer.count < remaining)? stream->obuffer.count:remaining;
    memcpy(&stream->obuffer.mem[stream->obuffer.next], data, write); 
    stream->obuffer.next += write;
    stream->obuffer.count -= write;    
    remaining -= write;
    data += write;
  }
  return size - remaining;
}

size_t iostream_flush(iostream_t *stream) {
  size_t write, size, remaining, index;

  index = 0; size = remaining = stream->obuffer.next;  
  while (remaining > 0) {    
    write = stream->write(&stream->obuffer.mem[index], remaining, 0);
    if (write < 0) return -1;
    remaining -= write;
    index += write;
  }
  stream->obuffer.count = IOBUFFER_MAX_SIZE;  
  stream->obuffer.next = 0;
  return size - remaining;
}