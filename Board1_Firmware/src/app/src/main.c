

#include "os.h"
#include "board.h"

#include "io.h"
#include "cmp.h"
#include "cmpaction.h"
#include "analysis.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

static char buf[] = "Hello there\t";
/*****************************************************************************
 * Private functions
 ****************************************************************************/



static bool cmp_stream_reader(cmp_ctx_t *ctx, void *data, size_t limit) {      
  iostream_t *stream;
  stream = ((iostream_t *)ctx->buf);  
  return (iostream_read(stream, data, limit) == limit);
}

static size_t cmp_stream_writer(cmp_ctx_t *ctx, const void *data, size_t count) {
  iostream_t *stream;
  stream = ((iostream_t *)ctx->buf);  
  if (count > 0) {
    count = iostream_write(stream, data, count);      
  } else {
    count = iostream_flush(stream);
  }
  return count;
}

/* LED1 togglee thread */
static void vLEDTask1(void *pvParameters) {
	bool LedState = false;

	while (1) {
		vBoard_LED_ToggleGreen();
		/* About a 1Hz on/off toggle rate */
		vTaskDelay(configTICK_RATE_HZ/3);
	//	xBoard_COM_Write(buf, sizeof(buf), NULL);
	}
}

static void vTaskCCD(void *pvParameters) {

	while (1) {
		/* About a 1Hz on/off toggle rate */
		vTaskDelay(configTICK_RATE_HZ/2);
		vBoard_LED_ToggleRed();
	//	Chip_GPIO_SetPinOutLow(LPC_GPIO, 1, 4);
	//	Chip_GPIO_SetPinOutHigh(LPC_GPIO, 1, 8);
		//vBoard_CCD_HighSpeed_Read();
		//vBoard_CCD_Read();
	}
}


static portTASK_FUNCTION(vServerTask, pvParameters) {     
  cmp_ctx_t ctx;
  cmp_action_ctx_t actx; 
  iostream_t stream;
  // setup stream
  iostream_init(&stream, (ioread_t)&xBoard_COM_Read, (iowrite_t)&xBoard_COM_Write);
  // setup mp context
  ctx.buf = &stream;
  ctx.read = &cmp_stream_reader;
  ctx.write = &cmp_stream_writer;
  // setup action mp context
  actx.ctx = &ctx;

  // serve mp actions
  while(1) {
    cmp_action_serve(&actx, analysis_actions);
  }
}



/*****************************************************************************
 * Public functions
 ****************************************************************************/

/**
 * @brief	main routine for FreeRTOS blinky example
 * @return	Nothing, function should not exit
 */
int main(void)
{

  BaseType_t xOutcome;
	xOutcome = xBoard_Init();
	vBoard_AFE_PowerDown();

 	xTaskCreate(&vServerTask, "ActionServer", 1024, 0, tskIDLE_PRIORITY+2, 0);  
	/* LED1 toggle thread */
	xTaskCreate(vLEDTask1, (signed char *) "vTaskLed1",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	xTaskCreate(vTaskCCD, (signed char *) "vTaskCCD",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);


	/* Start the scheduler */
	vTaskStartScheduler();

	/* Should never arrive here */
	return 1;
}

/**
 * @}
 */
