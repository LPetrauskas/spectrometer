  #include "board.h"
#include "analysis.h"

/* Action definitions */
static void start_sweep(cmp_action_ctx_t *ctx);
static void next_sweep(cmp_action_ctx_t *ctx);
static void adjust_gain(cmp_action_ctx_t *ctx);

/* Action table */
const cmp_action_t analysis_actions[] = {
  {"start_sweep", &start_sweep},
  {"next_sweep", &next_sweep},
  {"adjust_gain", &adjust_gain},
  {NULL, NULL}
};


/* Action implementation */
static struct {
  uint32_t steps;
} analysis_ctx;



static void next_sweep(cmp_action_ctx_t *ctx) {
   int32_t unfiltered[ANALYSIS_WINDOW_SIZE];
   int32_t filtered[ANALYSIS_WINDOW_SIZE];
   int32_t window[FILTER_WINDOW_SIZE];
   uint32_t read, index;
   
   read = (analysis_ctx.steps < ANALYSIS_WINDOW_SIZE)? analysis_ctx.steps:ANALYSIS_WINDOW_SIZE;
 //  read = xBoard_HET_Read(unfiltered, read, 0);
   analysis_ctx.steps -= read;
   // filter out adc glitches (credit to A. Demski)
 //  medianfilter(unfiltered, filtered, read, window, FILTER_WINDOW_SIZE);
  for(index=0;index<ANALYSIS_WINDOW_SIZE;index++)
  {
    unfiltered[index] = index % 4096;
  }
   // output next window
   cmp_action_result(ctx);
   cmp_write_array(base(ctx), 2);      
   cmp_write_array(base(ctx), read);      
   for (index = 0;index < read;index++) {
     cmp_write_int(base(ctx), filtered[index]);
   }
   cmp_write_bool(base(ctx), (analysis_ctx.steps == 0));
}

static void start_sweep(cmp_action_ctx_t *ctx) {      
   uint32_t low, high, step;
   // process args
 
}

static void adjust_gain(cmp_action_ctx_t *ctx) {
  int32_t gain;
  if (ctx->nargs != 1) {
    cmp_action_error(ctx, "Expected only gain argument");
    return;
  }
  if(!cmp_read_int(base(ctx), &gain)) {
    cmp_action_error(ctx, "Gain arg must be an integer");
    return;
  }
  // output 
  cmp_action_result(ctx);
  cmp_write_array(base(ctx), 0);
}