#include "chip.h"
#include "os.h"
#include "board.h"

#include "cmpaction.h"

static const cmp_action_t *cmp_action_lookup(const cmp_action_t *actions, const char *name) {
  uint32_t index;
  for (index = 0;actions[index].name != NULL; index++) {
    if (!strcmp(name, actions[index].name)) {
      return &actions[index];
    }
  }
  return NULL;
}

static bool internal_cmp_action_serve(cmp_action_ctx_t *ctx, const cmp_action_t *actions) {  
  const cmp_action_t *action; uint32_t size;   
  char name[ACTION_NAME_MAX_SIZE + 1]; // added NULL termination char
  
  if (!cmp_read_array(base(ctx), &size)) {
    cmp_action_error(ctx, cmp_strerror(base(ctx)));    
    return false;
  }

  if (size != 3) {
    cmp_action_error(ctx, "Malformed message");
    return false;
  }

  if (!cmp_read_uint(base(ctx), &ctx->uid)) {
    cmp_action_error(ctx, "Id field missing");
    return false;
  }    

  if (!cmp_read_str_size(base(ctx), &size)) {
    cmp_action_error(ctx, cmp_strerror(base(ctx)));
    return false;
  }

  if (size > ACTION_NAME_MAX_SIZE) {
    cmp_action_error(ctx, "Action name too long");
    return false;
  }  

  if (!base(ctx)->read(base(ctx), name, size)) {
    cmp_action_error(ctx, cmp_strerror(base(ctx)));
    return false;
  }  
  name[size] = '\0'; // add missing null

  if(!cmp_read_array(base(ctx), &ctx->nargs)) {
    cmp_action_error(ctx, cmp_strerror(base(ctx)));
    return false;
  }  
  
  action = cmp_action_lookup(actions, name);
  if (action != NULL) {
    action->handle(ctx);
  } else {
    cmp_action_error(ctx, "No matching action");
    return false;
  }

  return true;
}

bool cmp_action_serve(cmp_action_ctx_t *ctx, const cmp_action_t *actions) {  
  bool served, flushed;
  served = internal_cmp_action_serve(ctx, actions);
  flushed = !(base(ctx)->write(base(ctx), NULL, 0) < 0);
  return served && flushed;
}

bool cmp_action_reply(cmp_action_ctx_t *ctx, size_t n, bool error) {
  if (n < 0) ctx->nrets--;
  else ctx->nrets = n;
  // write reply header
  cmp_write_array(base(ctx), 4);
  cmp_write_uint(base(ctx), ctx->uid); // uid
  cmp_write_bool(base(ctx), error); // error?
  cmp_write_bool(base(ctx), (ctx->nrets == 0)); // last?

  return NULL;
}