PACKAGE  := $(notdir $(shell pwd))

SRC_PATH  := src
OUT_PATH  := out
DOC_PATH  := docs 
OBJ_PATH  := $(OUT_PATH)/obj
ETC_PATH  := etc

CPPFLAGS  += -Iinc/
CPPFLAGS  += -DDEBUG -DCORE_M3 -D__LPC17XX__ -MMD -MP -MT "$@"
CFLAGS    += -Wall -ggdb3 -mcpu=cortex-m3 -mthumb -fdata-sections -ffunction-sections
LDFLAGS   += -nostdlib -fno-builtin -mcpu=cortex-m3 -mthumb -Wl,--gc-sections

SRC_FILES := $(wildcard $(SRC_PATH)/*.c)
OBJ_FILES := $(patsubst $(SRC_PATH)/%.c,$(OBJ_PATH)/%.o,$(SRC_FILES))
AXF_FILE := $(OUT_PATH)/$(PACKAGE).axf
BIN_FILE := $(OUT_PATH)/$(PACKAGE).bin

TEST_FILE := $(ETC_PATH)/main.gdb

vpath %.c $(SRC_PATH)

# Explicit rules

all:	$(AXF_FILE) $(BIN_FILE)
	@echo "*** Built package $(PACKAGE) ***"

install: $(AXF_FILE) $(BIN_FILE)
	@echo "*** Installing executable $< ***"
	$(FLASH) $<

test: $(AXF_FILE) $(BIN_FILE)
	@echo "*** Testing executable $< ***"
	$(DEBUG) -x $(TEST_FILE) $<

html:
	@echo "*** Generate package $(PACKAGE) documentation ***"
	$(DXMOD) $(DXFLAGS) | $(DXGEN) -

clean:
	@echo "*** Clean package $(PACKAGE) ***"
	$(RM) $(OUT_PATH) $(DOC_PATH)

$(AXF_FILE): $(OBJ_FILES)
	@echo "*** Linking executable $@ ***"
	@$(MKDIR) $(@D)
	$(CC) -Xlinker -Map="$(basename $@).map" $(LDFLAGS) -o $@ $(OBJ_FILES) $(LDLIBS)
	$(MD) $(basename $@).map -t $@
	$(SIZE) $@
	@echo ""

# Implicit rules

$(OUT_PATH)/%.bin: $(OUT_PATH)/%.axf
	@echo "*** Translate executable $< ***"
	@$(MKDIR) $(@D)
	$(OBJCP) $< $@
	@echo ""

$(OBJ_PATH)/%.o: %.c
	@echo "*** Compiling C file $< ***"
	@$(MKDIR) $(@D)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@
	@echo ""

-include $(OBJ_FILES:.o=.d)
-include $(AXF_FILE:.axf=.d)
