<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="dir">
    <name>inc</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/crt/inc/</path>
    <filename>dir_bfccd401955b95cf8c75461437045ac0.html</filename>
    <file>crt0.h</file>
    <file>syscalls.h</file>
  </compound>
  <compound kind="dir">
    <name>src</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/crt/src/</path>
    <filename>dir_68267d1309a1af8e8297ef4c3efbcdba.html</filename>
    <file>crt0.c</file>
    <file>syscalls.c</file>
  </compound>
  <compound kind="page">
    <name>index</name>
    <title>C Runtime Package</title>
    <filename>index</filename>
    <docanchor file="index">crt_pkg</docanchor>
    <docanchor file="index" title="Overview">crt_overview</docanchor>
    <docanchor file="index">Dependencies</docanchor>
  </compound>
</tagfile>
