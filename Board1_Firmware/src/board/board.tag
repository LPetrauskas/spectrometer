<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="group">
    <name>board</name>
    <title>Board Drivers</title>
    <filename>group__board.html</filename>
    <subgroup>board_buffer</subgroup>
    <subgroup>board_com</subgroup>
    <subgroup>board_common</subgroup>
    <subgroup>board_config</subgroup>
    <subgroup>board_dma</subgroup>
    <subgroup>board_het</subgroup>
    <subgroup>board_i2c</subgroup>
    <subgroup>board_lit</subgroup>
    <subgroup>board_lo</subgroup>
    <subgroup>board_mem</subgroup>
    <subgroup>board_pmu</subgroup>
    <subgroup>board_rssi</subgroup>
    <subgroup>board_usb</subgroup>
    <subgroup>board_vga</subgroup>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_Init</name>
      <anchorfile>group__board.html</anchorfile>
      <anchor>gadac73cb8ed616398a4d81b71ac0cca79</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_Minimal</name>
      <anchorfile>group__board.html</anchorfile>
      <anchor>ga3ba182e38182978ce22bc0e33c0c85cc</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_buffer</name>
    <title>Two-phase ring buffer</title>
    <filename>group__board__buffer.html</filename>
    <class kind="struct">Buffer_t</class>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBuffer_Init</name>
      <anchorfile>group__board__buffer.html</anchorfile>
      <anchor>ga63fad1807b08c5e335ec521dd90cf59b</anchor>
      <arglist>(Buffer_t *pxBuff, void *pvItems, UBaseType_t uxItemSize, UBaseType_t uxItemCnt)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN UBaseType_t</type>
      <name>xBuffer_BeginPush</name>
      <anchorfile>group__board__buffer.html</anchorfile>
      <anchor>ga33fc40fcbb7983628c2674d69c435337</anchor>
      <arglist>(Buffer_t *pxBuff, void **ppvItems, UBaseType_t uxSize)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN UBaseType_t</type>
      <name>xBuffer_EndPush</name>
      <anchorfile>group__board__buffer.html</anchorfile>
      <anchor>gadad425b702e1dbe03184fc87e8b1efbf</anchor>
      <arglist>(Buffer_t *pxBuff, UBaseType_t uxSize)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN UBaseType_t</type>
      <name>xBuffer_BeginPop</name>
      <anchorfile>group__board__buffer.html</anchorfile>
      <anchor>ga477adcaae2b3733d9effa18e2f6cf4f5</anchor>
      <arglist>(Buffer_t *pxBuff, void **ppvItems, UBaseType_t uxSize)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN UBaseType_t</type>
      <name>xBuffer_EndPop</name>
      <anchorfile>group__board__buffer.html</anchorfile>
      <anchor>gae29fc3d9946804fac3412bf5bac21991</anchor>
      <arglist>(Buffer_t *pxBuff, UBaseType_t uxSize)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN INLINE BaseType_t</type>
      <name>xBuffer_IsEmpty</name>
      <anchorfile>group__board__buffer.html</anchorfile>
      <anchor>ga79e5159fce0db04230b117645c7e83f7</anchor>
      <arglist>(Buffer_t *pxBuff)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN INLINE BaseType_t</type>
      <name>xBuffer_IsFull</name>
      <anchorfile>group__board__buffer.html</anchorfile>
      <anchor>ga5ef28fcbafefab543417493c89fffaef</anchor>
      <arglist>(Buffer_t *pxBuff)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_com</name>
    <title>COM Port block driver</title>
    <filename>group__board__com.html</filename>
    <subgroup>board_com_buff</subgroup>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_COM_Init</name>
      <anchorfile>group__board__com.html</anchorfile>
      <anchor>ga7f5de867e8c0996a26680d9af16c0606</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_COM_Read</name>
      <anchorfile>group__board__com.html</anchorfile>
      <anchor>gabe924a5e41514f661aaf638d1cc1f3b9</anchor>
      <arglist>(void *pvData, BaseType_t xSize, UBaseType_t uxFlags)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_COM_Write</name>
      <anchorfile>group__board__com.html</anchorfile>
      <anchor>ga9525cde88d448ca6de0dde3a020ae026</anchor>
      <arglist>(const void *pvData, BaseType_t xSize, UBaseType_t uxFlags)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN void</type>
      <name>vBoard_COM_Deinit</name>
      <anchorfile>group__board__com.html</anchorfile>
      <anchor>ga6e66323825ce10ff1c08b01f1d0ab2c0</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="variable">
      <type>const StreamSink_t</type>
      <name>COMSink</name>
      <anchorfile>group__board__com.html</anchorfile>
      <anchor>ga67ae23574eaeb75e6465c7affd447971</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_com_buff</name>
    <title>COM Port buffering</title>
    <filename>group__board__com__buff.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>COM_RX_BUFFER_SIZE</name>
      <anchorfile>group__board__com__buff.html</anchorfile>
      <anchor>gace2f41e6c4558587a9471aa449a5d288</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COM_TX_BUFFER_SIZE</name>
      <anchorfile>group__board__com__buff.html</anchorfile>
      <anchor>ga09362ea0df227b0429db79110413b5b3</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_common</name>
    <title>Board common constants &amp; utility macros</title>
    <filename>group__board__common.html</filename>
    <subgroup>board_common_flags</subgroup>
    <member kind="define">
      <type>#define</type>
      <name>deref</name>
      <anchorfile>group__board__common.html</anchorfile>
      <anchor>gad58e1c2089097e82e5f8191e95b5caac</anchor>
      <arglist>(ptr, type)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_common_flags</name>
    <title>Board flags</title>
    <filename>group__board__common__flags.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>NONBLOCKING</name>
      <anchorfile>group__board__common__flags.html</anchorfile>
      <anchor>ga12fb0161cc76f483ba5b6fa9ad6f45f5</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_config</name>
    <title>Board configuration</title>
    <filename>group__board__config.html</filename>
  </compound>
  <compound kind="group">
    <name>board_dma</name>
    <title>DMA block driver</title>
    <filename>group__board__dma.html</filename>
    <subgroup>board_dma_hal</subgroup>
    <class kind="struct">DMA_Transfer_t</class>
    <member kind="typedef">
      <type>GPDMA_HANDLE_T</type>
      <name>DMA_Handle_t</name>
      <anchorfile>group__board__dma.html</anchorfile>
      <anchor>ga2e2740a41a6b741d75cac3d2a3b4cda6</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>GPDMA_CFG_T</type>
      <name>DMA_Configuration_t</name>
      <anchorfile>group__board__dma.html</anchorfile>
      <anchor>gad94eb317c9eb23b3e495ecadadffa406</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>GPDMA_DESC_T</type>
      <name>DMA_Descriptor_t</name>
      <anchorfile>group__board__dma.html</anchorfile>
      <anchor>ga2e715b1592d4ead2cab6410ab1efb521</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_DMA_Init</name>
      <anchorfile>group__board__dma.html</anchorfile>
      <anchor>ga293da91ab081e4ec78fde0adff77ae68</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_DMA_Transfer_Forward</name>
      <anchorfile>group__board__dma.html</anchorfile>
      <anchor>gaa6b549dacb6b99cfbd9c266acb0a29ac</anchor>
      <arglist>(DMA_Transfer_t *pxTransfer, UBaseType_t uxFlags)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_DMA_Transfer_Abort</name>
      <anchorfile>group__board__dma.html</anchorfile>
      <anchor>gad2c2ca929a0cdca8da5750187ef19854</anchor>
      <arglist>(DMA_Transfer_t *pxTransfer)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN void</type>
      <name>vBoard_DMA_Deinit</name>
      <anchorfile>group__board__dma.html</anchorfile>
      <anchor>gad5da0c402db50c477b1934b6659e6532</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_dma_hal</name>
    <title>DMA block Hardware Abstraction Layer</title>
    <filename>group__board__dma__hal.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>DMA_CONTROLLER</name>
      <anchorfile>group__board__dma__hal.html</anchorfile>
      <anchor>ga42385ba8b249b55d8a140a7c35f41c35</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_i2c_hal</name>
    <title>I2C block Hardware Abstraction Layer</title>
    <filename>group__board__i2c__hal.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>I2C_BUS_CONTROLLER</name>
      <anchorfile>group__board__i2c__hal.html</anchorfile>
      <anchor>gaeee8aed7cd65bdd87052272905679e8f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_BUS_CONTROLLER_CLOCKRATE</name>
      <anchorfile>group__board__i2c__hal.html</anchorfile>
      <anchor>ga40be0e178073473ce1ee938ec93bd19c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_BUS_CONTROLLER_SDA_PORT</name>
      <anchorfile>group__board__i2c__hal.html</anchorfile>
      <anchor>gafd06cc87589e6b812d55ac6e6b6b0c3c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_BUS_CONTROLLER_SDA_PIN</name>
      <anchorfile>group__board__i2c__hal.html</anchorfile>
      <anchor>gab3bd64f7a7a28e75ccb8733bf16324bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_BUS_CONTROLLER_SDA_MODE</name>
      <anchorfile>group__board__i2c__hal.html</anchorfile>
      <anchor>ga61a560bf29b8d876968a6f6ce845b2d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_BUS_CONTROLLER_SDA_FUNC</name>
      <anchorfile>group__board__i2c__hal.html</anchorfile>
      <anchor>gabc9f2e511cfa5c22aea3cfd0bd775c0f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_BUS_CONTROLLER_SCL_PORT</name>
      <anchorfile>group__board__i2c__hal.html</anchorfile>
      <anchor>ga662a88b4eacd644c97f75f1608610e68</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_BUS_CONTROLLER_SCL_PIN</name>
      <anchorfile>group__board__i2c__hal.html</anchorfile>
      <anchor>ga76cc650f3d94fd494257c2e14157299d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_BUS_CONTROLLER_SCL_MODE</name>
      <anchorfile>group__board__i2c__hal.html</anchorfile>
      <anchor>ga815d8cc8d130d85fa1182b73fc8286ba</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_BUS_CONTROLLER_SCL_FUNC</name>
      <anchorfile>group__board__i2c__hal.html</anchorfile>
      <anchor>ga9f48761ea04945c669b71b238583a797</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_lit_hal</name>
    <title>LIT block Hardware Abstraction Layer</title>
    <filename>group__board__lit__hal.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>RED_LIT_PORT</name>
      <anchorfile>group__board__lit__hal.html</anchorfile>
      <anchor>gaf7aaff2384814fe241b6842a853ffd4b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RED_LIT_PIN</name>
      <anchorfile>group__board__lit__hal.html</anchorfile>
      <anchor>gabacd3c65425a18a41e3476f6774e13d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>YELLOW_LIT_PORT</name>
      <anchorfile>group__board__lit__hal.html</anchorfile>
      <anchor>gaf6b4f97f39cbcd1f2219240391d6cb17</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>YELLOW_LIT_PIN</name>
      <anchorfile>group__board__lit__hal.html</anchorfile>
      <anchor>ga64768b24466b85045b6be3ece71838fc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GREEN_LIT_PORT</name>
      <anchorfile>group__board__lit__hal.html</anchorfile>
      <anchor>ga0798ee72c4b8bf6ee91d85df7a3611e0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GREEN_LIT_PIN</name>
      <anchorfile>group__board__lit__hal.html</anchorfile>
      <anchor>gaa51d0694b1a5dee98ae8dbd36d5cea07</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LIT_MODE</name>
      <anchorfile>group__board__lit__hal.html</anchorfile>
      <anchor>gaf75cc7afd0822a90c383773829a1f6c6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LIT_FUNC</name>
      <anchorfile>group__board__lit__hal.html</anchorfile>
      <anchor>ga4d8c1efcaad8f0eac419a87b0074dd5f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_lo_hal</name>
    <title>LO block Hardware Abstraction Layer</title>
    <filename>group__board__lo__hal.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_MODE</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga90207b3752a6c7d9567cb67815fa9ac1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J1_PORT</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga146728dc297e7b7085c8af8a20219d33</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J1_PIN</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga5a0d6b82407110867dcf59e9965634c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J2_PORT</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>gac55218bc370a707e552cbf3197365ff7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J2_PIN</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga1377a256e4239283ef071f8ab5be1d20</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J3_PORT</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga2204a6f8e984d831d797c9da2690063b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J3_PIN</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>gacbc2fb8422e71fc97f400ffa6b0e3a96</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J4_PORT</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>gaac786b5bcaeced253104c22bb52830e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J4_PIN</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>gae6e755c1da7c983a41afc95962e93d76</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J16_PORT</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga6e781281407494219306b8200dfef1df</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J16_PIN</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga0d341409e65f427184a219bd43fb8b3c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J15_PORT</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga392913d626044f3e3a8d5c84c3e37739</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J15_PIN</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga05b47597c5d2bb62797d3201f9e33126</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J14_PORT</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga344bf51b03066ac6e6f3800dd69e7094</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J14_PIN</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>gadd46f3f3b6afcd1edda78df1a5af5bb1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J13_PORT</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga402ab4cf4099924d6d663563ced21d62</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J13_PIN</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>gaeb031c15c357dbb71142f47e73f62a04</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J5_PORT</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>gafe72ed61237be36fb1418958ca02e971</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J5_PIN</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>gafbc15de9ac566a977267830cefc5869c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J6_PORT</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga58e7f3214f3a3cd2cd777fb90621e15b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J6_PIN</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>gac4f5432427cb03e3708b34414beebacb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J7_PORT</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga35453996113a52bdff983f4b84935c4e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J7_PIN</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga787e7916520b3f4190d4a47ed028bbd4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J8_PORT</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga8059fd6eed51d03fda1ea18e19b10101</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J8_PIN</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga6d6f5b65bea0a088d61f87b4d24cca6c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J9_PORT</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga3e71cf2046bfa5293145a9831aa331a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J9_PIN</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga58b6aebc5f50f2ae134ec380628b70e4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J10_PORT</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga1bdef6aa8d0a1289d58dee2da5a64823</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J10_PIN</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>gacb2b4f53f0b7a207a29b2dc90d596da3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J11_PORT</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga1330caab1b7c2d87e67dfb16802adc3b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J11_PIN</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>gab7e3a3976e094224465c3376cf5f5fe9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J12_PORT</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>gaba9a535b9b7e029921662e7558200183</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_J12_PIN</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga8311a822e4847bf3aeec04a18a2067e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_JAM_MODE</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga52f8332d22f40bb8d58ff9670bc430bd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_JAM_FUNC</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga4713d28c0b0a48d2c33a50ad2c4678d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_LOCK_FLAG_PORT</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga1871232bbdf20be5f1e6b39a91278e34</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_LOCK_FLAG_PIN</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>gadbf3a5d288dc115155890250421a2e88</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_LOCK_FLAG_MODE</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>gacce20a6daad71ce8ee94717038268e4b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_LOCK_FLAG_FUNC</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga4c8a20804e58db90f5188328cb80abc0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_JAM_PORT</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga3404d216fb1e60af9dbf81de1ef7cf26</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLL_DIV_JAM_PIN</name>
      <anchorfile>group__board__lo__hal.html</anchorfile>
      <anchor>ga4a52b1072e6ed9bc706887b0dcf2c96b</anchor>
      <arglist>(n)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_mem_hal</name>
    <title>24LC64 Nonvolatile Memory block  Hardware Abstraction Layer</title>
    <filename>group__board__mem__hal.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>NVMEM_SLAVE_ADDRESS</name>
      <anchorfile>group__board__mem__hal.html</anchorfile>
      <anchor>gad315844a06215ff5d0a1a8cb91d5595f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>NVMEM_WRITE_CYCLE_TICKS</name>
      <anchorfile>group__board__mem__hal.html</anchorfile>
      <anchor>ga25a3fbc9e64db393bed2467fef7aa627</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>NVMEM_WRITE_CYCLE_MS</name>
      <anchorfile>group__board__mem__hal.html</anchorfile>
      <anchor>ga67eb18faeae09d82b45409815ca0203a</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_rssi_hal</name>
    <title>SA615 Received Signal Strength Indicator block Hardware Abstraction Layer</title>
    <filename>group__board__rssi__hal.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>RSSI_ADC_PORT</name>
      <anchorfile>group__board__rssi__hal.html</anchorfile>
      <anchor>gae259494349a9f8ab2ef756a3d976a252</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RSSI_ADC_PIN</name>
      <anchorfile>group__board__rssi__hal.html</anchorfile>
      <anchor>ga5ae5af60be79a3a2b54866d293e2738d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RSSI_ADC_MODE</name>
      <anchorfile>group__board__rssi__hal.html</anchorfile>
      <anchor>ga6e4272d18e3eee8322746cf39072df14</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RSSI_ADC_FUNC</name>
      <anchorfile>group__board__rssi__hal.html</anchorfile>
      <anchor>gaf5f5898dd6608da0d66db2ac2892fd1e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RSSI_ADC_DMA_CONN</name>
      <anchorfile>group__board__rssi__hal.html</anchorfile>
      <anchor>ga46e3f04a2af23580338c56d524ff44ae</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RSSI_ADC_CHANNEL</name>
      <anchorfile>group__board__rssi__hal.html</anchorfile>
      <anchor>ga9fd089ce8edd5c7a0efd562b636bc992</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_usb_hal</name>
    <title>USB stack Hardware Abstraction Layer</title>
    <filename>group__board__usb__hal.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>USB_CONTROLLER</name>
      <anchorfile>group__board__usb__hal.html</anchorfile>
      <anchor>gae8b8ae8be46aed069622e077d64860ab</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_CONTROLLER_DPLUS_PORT</name>
      <anchorfile>group__board__usb__hal.html</anchorfile>
      <anchor>ga4dc976f60ae3267b04e28d838e5f4d64</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_CONTROLLER_DPLUS_PIN</name>
      <anchorfile>group__board__usb__hal.html</anchorfile>
      <anchor>gabc8845cef97205f6c1e6c4c96299ebd4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_CONTROLLER_DPLUS_MODE</name>
      <anchorfile>group__board__usb__hal.html</anchorfile>
      <anchor>gadef35fa58ae937cb985df029efdc88f1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_CONTROLLER_DPLUS_FUNC</name>
      <anchorfile>group__board__usb__hal.html</anchorfile>
      <anchor>ga0b1f2dbd5707cdaa9fd5557e5d15cf10</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_CONTROLLER_DMINUS_PORT</name>
      <anchorfile>group__board__usb__hal.html</anchorfile>
      <anchor>gab8934b55794cf007821347b1716e92e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_CONTROLLER_DMINUS_PIN</name>
      <anchorfile>group__board__usb__hal.html</anchorfile>
      <anchor>ga87182778a63dd923dce9b0e34d421333</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_CONTROLLER_DMINUS_MODE</name>
      <anchorfile>group__board__usb__hal.html</anchorfile>
      <anchor>ga07a61d9995e86a33f3a3b3b25616425c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_CONTROLLER_DMINUS_FUNC</name>
      <anchorfile>group__board__usb__hal.html</anchorfile>
      <anchor>ga25db611c0c818d8205fa5f9844a5e69e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_CONTROLLER_DEVICE_EN_PORT</name>
      <anchorfile>group__board__usb__hal.html</anchorfile>
      <anchor>gaf40f6c8d6bc6b061d9da5968729379c1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_CONTROLLER_DEVICE_EN_PIN</name>
      <anchorfile>group__board__usb__hal.html</anchorfile>
      <anchor>ga1b09ec565f6fa27d2cc984c014033a6f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_CONTROLLER_DEVICE_EN_MODE</name>
      <anchorfile>group__board__usb__hal.html</anchorfile>
      <anchor>ga8d3435c0c046d8b241d048a4cc9a3aa7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_CONTROLLER_DEVICE_EN_FUNC</name>
      <anchorfile>group__board__usb__hal.html</anchorfile>
      <anchor>gae788210cf4b5252283bb5e452443d917</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_vga_hal</name>
    <title>LMH6505 VGA block Hardware Abstraction Layer</title>
    <filename>group__board__vga__hal.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>LPC_DAC_PORT</name>
      <anchorfile>group__board__vga__hal.html</anchorfile>
      <anchor>ga231529b8988c975556af2d634e218dbd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LPC_DAC_PIN</name>
      <anchorfile>group__board__vga__hal.html</anchorfile>
      <anchor>gaa5e400bf0696848c79517470a922d5d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LPC_DAC_MODE</name>
      <anchorfile>group__board__vga__hal.html</anchorfile>
      <anchor>gacec16825ceb1eae3b14d11919e2d87b8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LPC_DAC_FUNC</name>
      <anchorfile>group__board__vga__hal.html</anchorfile>
      <anchor>ga91231a3bd6a3d03a5bb9a66ef5537ef5</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_het</name>
    <title>Super Heterodyne block driver</title>
    <filename>group__board__het.html</filename>
    <subgroup>board_het_params</subgroup>
    <subgroup>board_het_seek</subgroup>
    <member kind="define">
      <type>#define</type>
      <name>HET_SLOT_SIZE</name>
      <anchorfile>group__board__het.html</anchorfile>
      <anchor>ga78044a0294ef4f6df8f1821d56a34570</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_INTERMEDIATE_FREQUENCY</name>
      <anchorfile>group__board__het.html</anchorfile>
      <anchor>ga0dab466d21b3daadbc774150aa3bc03f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_HET_Init</name>
      <anchorfile>group__board__het.html</anchorfile>
      <anchor>ga1708eb20c6bd763f84e69bab41f6df93</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_HET_Read</name>
      <anchorfile>group__board__het.html</anchorfile>
      <anchor>gaeb42d661427a2ce0f9163ecb2b106937</anchor>
      <arglist>(int32_t *plData, UBaseType_t uxSize, UBaseType_t uxFlags)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_HET_Seek</name>
      <anchorfile>group__board__het.html</anchorfile>
      <anchor>gac138df27c2ce003c4219d9f142dc36fc</anchor>
      <arglist>(BaseType_t xWhence, BaseType_t xOffset, UBaseType_t uxFlags)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_HET_Ioctl</name>
      <anchorfile>group__board__het.html</anchorfile>
      <anchor>ga2aa98d129d09b15370bae540ffa0833a</anchor>
      <arglist>(BaseType_t xCmd, void *pvData)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN void</type>
      <name>vBoard_HET_Deinit</name>
      <anchorfile>group__board__het.html</anchorfile>
      <anchor>gac02d975173fc038ebf0c46467c934963</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="variable">
      <type>const StreamSource_t</type>
      <name>HETSource</name>
      <anchorfile>group__board__het.html</anchorfile>
      <anchor>gac2f08767349ddbbeb42e24ae241a26c3</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_het_params</name>
    <title>Super Heterodyne Parameters</title>
    <filename>group__board__het__params.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>HET_DETECTOR_SET</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>gaec574c796c8e95cb44f95bd557212c85</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_DETECTOR_GET</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>ga4d3a727dfcc53d35cc445512dfc9282f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_NO_DETECTOR</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>ga42c9caeb2420f6c062aa6381478c5249</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_AVG_DETECTOR</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>gab2ec817937072cc818318ee9cf9cabd5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_MAX_PK_DETECTOR</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>ga461001eda6f4193b9dab656296df9861</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_MIN_PK_DETECTOR</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>ga9fab0564005fb1c851a9f0e8beae3d3c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_DEFAULT_DETECTOR</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>gadf0b7fa28e793e04facee744ebd0c1b6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_LEVEL_SET</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>ga7e479399c53e0b5757ccc2204cad1e74</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_LEVEL_GET</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>gac63549d812e0115b0e27de0cb07b1a75</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_MAX_LEVEL</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>gac0162084ae5812ed2997a20f97d200b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_MIN_LEVEL</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>gacee8cef886d4ad567d3e8b5885af74da</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_DEFAULT_LVL</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>ga15614e9ade629f5948964d2918bddbfb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_LOW_FREQUENCY_SET</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>gaa8b12820620a8ae1cdeef399140f4d49</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_LOW_FREQUENCY_GET</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>ga3cd1d337910efc5704333633857a1455</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_MIN_FREQUENCY</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>ga1fc0643609981f2a9fcd137051e2c49a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_DEFAULT_LO_FREQ</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>gaf8fc6b53da21d36207fe753e94ae3d86</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_HIGH_FREQUENCY_SET</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>ga5d03b0c87517bbe5017aac451a641095</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_HIGH_FREQUENCY_GET</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>ga50bfa4b295445df21ec96f88217d1720</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_MAX_FREQUENCY</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>ga5b13320618f607e452edb94d15b9280d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_DEFAULT_HI_FREQ</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>gaf80dd2f4d8dbc51dbd6095c980dfe7b7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_RESOLUTION_BW_SET</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>gae025cf866c15136f0eb8e0de661c1b26</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_RESOLUTION_BW_GET</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>gaf9def289460fadd928f7eb12e6f3dc05</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_MIN_RESOLUTION</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>gab29a33560ab9eca9a5c735dad24cc9db</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_DEFAULT_RES_BW</name>
      <anchorfile>group__board__het__params.html</anchorfile>
      <anchor>gaceee2fc18e18b1d81d20d733ef08bb52</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_het_seek</name>
    <title>Seeking locations</title>
    <filename>group__board__het__seek.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>HET_SPAN_START</name>
      <anchorfile>group__board__het__seek.html</anchorfile>
      <anchor>gaf305d6372863b1d8463d21ddde045d7f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_SPAN_CURRENT</name>
      <anchorfile>group__board__het__seek.html</anchorfile>
      <anchor>gaea0f80bc2d599ba07e992db3fab8c28e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HET_SPAN_END</name>
      <anchorfile>group__board__het__seek.html</anchorfile>
      <anchor>ga29c1e43276fc0c0cee099757dfb5c9ba</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_i2c</name>
    <title>I2C Bus driver</title>
    <filename>group__board__i2c.html</filename>
    <subgroup>board_i2c_hal</subgroup>
    <member kind="typedef">
      <type>I2C_XFER_T</type>
      <name>I2C_Transfer_t</name>
      <anchorfile>group__board__i2c.html</anchorfile>
      <anchor>gaebc96f2cbe11f64fe5b6d7a121d74c94</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>I2C_BUFF_T</type>
      <name>I2C_Buffer_t</name>
      <anchorfile>group__board__i2c.html</anchorfile>
      <anchor>gabe14f37f260d49fc439caaf2c5945b8b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_I2C_Init</name>
      <anchorfile>group__board__i2c.html</anchorfile>
      <anchor>gaabf02f1b48acabf26248074d3423ea66</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_I2C_Transfer</name>
      <anchorfile>group__board__i2c.html</anchorfile>
      <anchor>gaf0f86985b675496d82e149b76429a958</anchor>
      <arglist>(I2C_Transfer_t *pxTransfer, UBaseType_t Flags)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN void</type>
      <name>vBoard_I2C_Deinit</name>
      <anchorfile>group__board__i2c.html</anchorfile>
      <anchor>ga8d46b054d4968828d5ef50b807854c5d</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_lit</name>
    <title>Light indication block driver</title>
    <filename>group__board__lit.html</filename>
    <subgroup>board_lit_hal</subgroup>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_LIT_Init</name>
      <anchorfile>group__board__lit.html</anchorfile>
      <anchor>ga53e0cec1a704b90a21e48209d5012dec</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_LIT_Glow</name>
      <anchorfile>group__board__lit.html</anchorfile>
      <anchor>ga21d438ef97ffa2b3775aa3a1ac737f26</anchor>
      <arglist>(uint8_t xRed, uint8_t xYellow, uint8_t xGreen)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LIT_NO_MSG</name>
      <anchorfile>group__board__lit.html</anchorfile>
      <anchor>ga8d763142f061d23428a84272da706c0e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LIT_SYS_FAIL</name>
      <anchorfile>group__board__lit.html</anchorfile>
      <anchor>ga5d29d536c5c83d4ad73cdd3e8952deec</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LIT_FAIL_COM</name>
      <anchorfile>group__board__lit.html</anchorfile>
      <anchor>gac6e1c472f8ff225b7b1a274d4eae551e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LIT_SYS_OK</name>
      <anchorfile>group__board__lit.html</anchorfile>
      <anchor>ga8579f0d7d0b28afde4e1ffd56ef5bd47</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_lo</name>
    <title>74HC40xx Local oscillator block driver</title>
    <filename>group__board__lo.html</filename>
    <subgroup>board_lo_hal</subgroup>
    <member kind="define">
      <type>#define</type>
      <name>LO_FREQ_RESOLUTION</name>
      <anchorfile>group__board__lo.html</anchorfile>
      <anchor>ga1b1644ca6c46a49eb11dc84ef8c22ab7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO_MIN_FREQUENCY</name>
      <anchorfile>group__board__lo.html</anchorfile>
      <anchor>ga1e134185a810afac24ccc7413c161c38</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO_MAX_FREQUENCY</name>
      <anchorfile>group__board__lo.html</anchorfile>
      <anchor>ga05e8b82a3d4e4baa2367007053e145a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO_DEFAULT_FREQUENCY</name>
      <anchorfile>group__board__lo.html</anchorfile>
      <anchor>gad5e426384d851fab9459c1a316453743</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_LO_Init</name>
      <anchorfile>group__board__lo.html</anchorfile>
      <anchor>gae4315f468e4fd8f74fa5fc71c60b722f</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_LO_SetFrequency</name>
      <anchorfile>group__board__lo.html</anchorfile>
      <anchor>ga7004e8ede97b3ac3cb2c96392f31847d</anchor>
      <arglist>(uint32_t ulFrequency)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN uint32_t</type>
      <name>ulBoard_LO_GetFrequency</name>
      <anchorfile>group__board__lo.html</anchorfile>
      <anchor>ga879c023052ab2d956d780e5f029bcfde</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_LO_IsLocked</name>
      <anchorfile>group__board__lo.html</anchorfile>
      <anchor>ga3950907dc39dea5a33701b9a2621624d</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_LO_Sweep</name>
      <anchorfile>group__board__lo.html</anchorfile>
      <anchor>ga31b2123f06524d7e0c7f10f91b9573bf</anchor>
      <arglist>(uint32_t ulFreqLow, uint32_t ulFreqHigh, uint32_t ulFreqInterval)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN void</type>
      <name>vBoard_LO_Deinit</name>
      <anchorfile>group__board__lo.html</anchorfile>
      <anchor>ga68c5d84bac75677a10816fb681cdd307</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_mem</name>
    <title>24LC64 Nonvolatile Memory block driver</title>
    <filename>group__board__mem.html</filename>
    <subgroup>board_mem_hal</subgroup>
    <member kind="define">
      <type>#define</type>
      <name>NVMEM_MIN_ADDRESS</name>
      <anchorfile>group__board__mem.html</anchorfile>
      <anchor>gadeab493fd7870ca84ad5b3c8def197c7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>NVMEM_MAX_ADDRESS</name>
      <anchorfile>group__board__mem.html</anchorfile>
      <anchor>ga6ceb65b63efed5947c42b43c3ff084f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>NVMEM_PAGE_SIZE</name>
      <anchorfile>group__board__mem.html</anchorfile>
      <anchor>gab83cf98993a546363d66b5555e6dc9e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_NVMEM_Init</name>
      <anchorfile>group__board__mem.html</anchorfile>
      <anchor>ga25c4bd5fdcee6f61a5da2e2f2228fb17</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_NVMEM_Read</name>
      <anchorfile>group__board__mem.html</anchorfile>
      <anchor>ga68556d643dad4dcfbdd52878c7eb0060</anchor>
      <arglist>(uint16_t usAddress, void *pxData, UBaseType_t Size, UBaseType_t Flags)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_NVMEM_Write</name>
      <anchorfile>group__board__mem.html</anchorfile>
      <anchor>ga1e8ddb3db71390fb4e194ae8c19d6d19</anchor>
      <arglist>(uint16_t usAddress, const void *pxData, UBaseType_t Size, UBaseType_t Flags)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN void</type>
      <name>vBoard_NVMEM_Deinit</name>
      <anchorfile>group__board__mem.html</anchorfile>
      <anchor>ga04cf6e9d20edd6dcc71c3084c31fe792</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_pmu</name>
    <title>Power management unit block driver</title>
    <filename>group__board__pmu.html</filename>
    <subgroup>board_pmu_modes</subgroup>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_PMU_Init</name>
      <anchorfile>group__board__pmu.html</anchorfile>
      <anchor>gafab9d6a19bd7ad6d850ada37198cbbe1</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_PMU_Mode</name>
      <anchorfile>group__board__pmu.html</anchorfile>
      <anchor>ga1ed96a1034390c2c252490192ba0fb8a</anchor>
      <arglist>(BaseType_t xMode)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_pmu_modes</name>
    <title>Power management modes</title>
    <filename>group__board__pmu__modes.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>PMU_POWER_ALL</name>
      <anchorfile>group__board__pmu__modes.html</anchorfile>
      <anchor>ga79bf41b20c2ef4e3c1649277b78621f5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PMU_POWER_MIN</name>
      <anchorfile>group__board__pmu__modes.html</anchorfile>
      <anchor>ga63ab8d2e78a6f15556bd3bec16fc25ec</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DEFAULT_PMU_MODE</name>
      <anchorfile>group__board__pmu__modes.html</anchorfile>
      <anchor>gae13e5227730b9a5f2f140758e811167c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_rssi</name>
    <title>SA615 Received Signal Strength Indicator block driver</title>
    <filename>group__board__rssi.html</filename>
    <subgroup>board_rssi_hal</subgroup>
    <member kind="define">
      <type>#define</type>
      <name>RSSI_ADC_SAMPLE_RATE</name>
      <anchorfile>group__board__rssi.html</anchorfile>
      <anchor>ga98ab6ca7c643ec255299b1962edaa75d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RSSI_RESOLUTION</name>
      <anchorfile>group__board__rssi.html</anchorfile>
      <anchor>ga8a3a4fd89e62712d2582a33417f5b840</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RSSI_MIN_VALUE</name>
      <anchorfile>group__board__rssi.html</anchorfile>
      <anchor>gad0fa98926e4bafbd516ed783b5d1d0ad</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RSSI_MAX_VALUE</name>
      <anchorfile>group__board__rssi.html</anchorfile>
      <anchor>ga1bcc62cf31cd0595a0c738b0e7568afd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RSSI_VALUE_SPAN</name>
      <anchorfile>group__board__rssi.html</anchorfile>
      <anchor>ga8d8c2d21d7f5fecc0bdffd81866fb682</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_RSSI_Init</name>
      <anchorfile>group__board__rssi.html</anchorfile>
      <anchor>ga3854bc08cde780e2a578f0bbb1d3c459</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_RSSI_Read</name>
      <anchorfile>group__board__rssi.html</anchorfile>
      <anchor>ga691c8dc6f2bc86848d14fdb943d5e708</anchor>
      <arglist>(int32_t *pxData, UBaseType_t uxSize, UBaseType_t uxFlags)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN void</type>
      <name>vBoard_RSSI_Deinit</name>
      <anchorfile>group__board__rssi.html</anchorfile>
      <anchor>gae228df32f4aa06047497fb2a7cdf3109</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_usb_cfg</name>
    <title>USB stack configuration</title>
    <filename>group__board__usb__cfg.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>USB_MAX_IF_NUM</name>
      <anchorfile>group__board__usb__cfg.html</anchorfile>
      <anchor>ga1e67b9866fc52d0634e1a5b835d6d734</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_MAX_EP_NUM</name>
      <anchorfile>group__board__usb__cfg.html</anchorfile>
      <anchor>ga2e163dc867c928ab7b61de44368db7ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_MAX_PACKET0</name>
      <anchorfile>group__board__usb__cfg.html</anchorfile>
      <anchor>ga4659143950f825db983fadc9a1b1df4e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_FS_MAX_BULK_PACKET</name>
      <anchorfile>group__board__usb__cfg.html</anchorfile>
      <anchor>ga8255a55256721f38ab8df831dedf5748</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_HS_MAX_BULK_PACKET</name>
      <anchorfile>group__board__usb__cfg.html</anchorfile>
      <anchor>ga8b8eb21794dc58514f46ce73a4ab7c5f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_DFU_XFER_SIZE</name>
      <anchorfile>group__board__usb__cfg.html</anchorfile>
      <anchor>gad31026fa95c1c46e6508495743acab5b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_usb_desc</name>
    <title>USB stack descriptors</title>
    <filename>group__board__usb__desc.html</filename>
    <member kind="function">
      <type>EXTERN USB_INTERFACE_DESCRIPTOR *</type>
      <name>Find_IntfDescriptor</name>
      <anchorfile>group__board__usb__desc.html</anchorfile>
      <anchor>ga2f3fb19a9fe77d993850845a222ee1f3</anchor>
      <arglist>(const uint8_t *pxDescriptor, uint32_t xIntfClass)</arglist>
    </member>
    <member kind="variable">
      <type>EXTERN CONST uint8_t</type>
      <name>USB_DeviceDescriptor</name>
      <anchorfile>group__board__usb__desc.html</anchorfile>
      <anchor>ga2427a760299fcbdbbf95584f773c8263</anchor>
      <arglist>[]</arglist>
    </member>
    <member kind="variable">
      <type>EXTERN uint8_t</type>
      <name>USB_FsConfigDescriptor</name>
      <anchorfile>group__board__usb__desc.html</anchorfile>
      <anchor>gacfc48aacb1865ff3b309c3e28c805682</anchor>
      <arglist>[]</arglist>
    </member>
    <member kind="variable">
      <type>EXTERN CONST uint8_t</type>
      <name>USB_StringDescriptor</name>
      <anchorfile>group__board__usb__desc.html</anchorfile>
      <anchor>ga2e7518555fe039d6a2783a3282bca43d</anchor>
      <arglist>[]</arglist>
    </member>
    <member kind="variable">
      <type>EXTERN CONST uint8_t</type>
      <name>USB_DeviceQualifier</name>
      <anchorfile>group__board__usb__desc.html</anchorfile>
      <anchor>gacb10f13d972f2390ac5715828750d6c5</anchor>
      <arglist>[]</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_usb</name>
    <title>USB stack setup</title>
    <filename>group__board__usb.html</filename>
    <subgroup>board_usb_hal</subgroup>
    <subgroup>board_usb_cfg</subgroup>
    <subgroup>board_usb_desc</subgroup>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_USB_Init</name>
      <anchorfile>group__board__usb.html</anchorfile>
      <anchor>gaac56d78064b22e7932e11c13fbbb7a31</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN void</type>
      <name>vBoard_USB_Deinit</name>
      <anchorfile>group__board__usb.html</anchorfile>
      <anchor>ga30a37a2d66e10d0de48ac5faa217054a</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>board_vga</name>
    <title>LMH6505 Variable gain amplifier block driver</title>
    <filename>group__board__vga.html</filename>
    <subgroup>board_vga_hal</subgroup>
    <member kind="define">
      <type>#define</type>
      <name>VGA_GAIN_RESOLUTION</name>
      <anchorfile>group__board__vga.html</anchorfile>
      <anchor>ga3eefd5b16efeeca812419c47f96a8549</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>VGA_MIN_GAIN</name>
      <anchorfile>group__board__vga.html</anchorfile>
      <anchor>gad80c1eb84c73abd590bfab01c2eb0773</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>VGA_MAX_GAIN</name>
      <anchorfile>group__board__vga.html</anchorfile>
      <anchor>gac03c6337b349d31ab0be860e672ba1ba</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_VGA_Init</name>
      <anchorfile>group__board__vga.html</anchorfile>
      <anchor>ga037d3bc40921a25998b12dceee999871</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN BaseType_t</type>
      <name>xBoard_VGA_SetGain</name>
      <anchorfile>group__board__vga.html</anchorfile>
      <anchor>gac2ac37250cefff6a10ab370f58e01cf9</anchor>
      <arglist>(int32_t lGain)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN int32_t</type>
      <name>lBoard_VGA_Gain</name>
      <anchorfile>group__board__vga.html</anchorfile>
      <anchor>ga373ac69ad358c7e86a40bfe4799579c0</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>EXTERN void</type>
      <name>vBoard_VGA_Deinit</name>
      <anchorfile>group__board__vga.html</anchorfile>
      <anchor>ga4d186a852b72ce120395faf8f93e07d7</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>Buffer_t</name>
    <filename>structBuffer__t.html</filename>
    <member kind="variable">
      <type>UBaseType_t</type>
      <name>uxItemSize</name>
      <anchorfile>structBuffer__t.html</anchorfile>
      <anchor>a8071ffca5259416feb93e6d1d312b74e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>UBaseType_t</type>
      <name>uxItemCnt</name>
      <anchorfile>structBuffer__t.html</anchorfile>
      <anchor>aacb480f84380ce3c3621d7faaa8ab300</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>UBaseType_t</type>
      <name>uxWrap</name>
      <anchorfile>structBuffer__t.html</anchorfile>
      <anchor>a1ce1d7dcdd95f06d0ddcd5aee6e899e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>UBaseType_t</type>
      <name>uxHead</name>
      <anchorfile>structBuffer__t.html</anchorfile>
      <anchor>a172e3f27d84c9903759705c08f24af35</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>UBaseType_t</type>
      <name>uxTail</name>
      <anchorfile>structBuffer__t.html</anchorfile>
      <anchor>a6bc90b0ed91a738d429229b29a2d7686</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>DMA_Transfer_t</name>
    <filename>structDMA__Transfer__t.html</filename>
    <member kind="variable">
      <type>xTaskHandle</type>
      <name>xOwner</name>
      <anchorfile>structDMA__Transfer__t.html</anchorfile>
      <anchor>a5039ae757a458374f0cbde7fe65c2858</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>DMA_Handle_t</type>
      <name>xHandle</name>
      <anchorfile>structDMA__Transfer__t.html</anchorfile>
      <anchor>a3be894761b50f7d1a3066b34fd9ed76d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>DMA_Configuration_t *</type>
      <name>pxConfig</name>
      <anchorfile>structDMA__Transfer__t.html</anchorfile>
      <anchor>a30054a20c784162effaf436d0bc8adc3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>DMA_Descriptor_t *</type>
      <name>pxDescs</name>
      <anchorfile>structDMA__Transfer__t.html</anchorfile>
      <anchor>ace273ad356414e3c91f891e570fb8ba7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>BaseType_t</type>
      <name>xStatus</name>
      <anchorfile>structDMA__Transfer__t.html</anchorfile>
      <anchor>af815606185ed140d40acc793f92d71d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>ulPriority</name>
      <anchorfile>structDMA__Transfer__t.html</anchorfile>
      <anchor>a5960e009fc9b25b32435cd8e98892faf</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>HetSetup_t</name>
    <filename>structHetSetup__t.html</filename>
  </compound>
  <compound kind="union">
    <name>JamBuffer_t</name>
    <filename>unionJamBuffer__t.html</filename>
  </compound>
  <compound kind="struct">
    <name>JamGpio_t</name>
    <filename>structJamGpio__t.html</filename>
  </compound>
  <compound kind="dir">
    <name>inc/hal</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/board/inc/hal/</path>
    <filename>dir_2ec6d3f2b06d7c732d63cb9ae829746c.html</filename>
    <file>dma.h</file>
    <file>i2c.h</file>
    <file>lit.h</file>
    <file>lo74hc40xx.h</file>
    <file>nvmem24lc64.h</file>
    <file>pmu.h</file>
    <file>rssiSA615.h</file>
    <file>usb.h</file>
    <file>vgaLMH6505.h</file>
  </compound>
  <compound kind="dir">
    <name>inc</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/board/inc/</path>
    <filename>dir_bfccd401955b95cf8c75461437045ac0.html</filename>
    <dir>inc/hal</dir>
    <file>app_usbd_cfg.h</file>
    <file>board.h</file>
    <file>buffer.h</file>
    <file>com.h</file>
    <file>common.h</file>
    <file>config.h</file>
    <file>dma.h</file>
    <file>ecpp.h</file>
    <file>het.h</file>
    <file>i2c.h</file>
    <file>lit.h</file>
    <file>lo.h</file>
    <file>lo74HC40xx.h</file>
    <file>nvmem.h</file>
    <file>nvmem24lc64.h</file>
    <file>pmu.h</file>
    <file>rssi.h</file>
    <file>rssiSA615.h</file>
    <file>usb_config.h</file>
    <file>usb_desc.h</file>
    <file>usb_setup.h</file>
    <file>vga.h</file>
    <file>vgaLMH6505.h</file>
  </compound>
  <compound kind="dir">
    <name>src</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/board/src/</path>
    <filename>dir_68267d1309a1af8e8297ef4c3efbcdba.html</filename>
    <file>board.c</file>
    <file>buffer.c</file>
    <file>com.c</file>
    <file>dma.c</file>
    <file>het.c</file>
    <file>i2c.c</file>
    <file>lit.c</file>
    <file>lo74HC40xx.c</file>
    <file>nvmem24lc64.c</file>
    <file>pmu.c</file>
    <file>rssiSA615.c</file>
    <file>usb_config.c</file>
    <file>usb_desc.c</file>
    <file>usb_setup.c</file>
    <file>vgaLMH6505.c</file>
  </compound>
  <compound kind="page">
    <name>index</name>
    <title>Board Support Package</title>
    <filename>index</filename>
    <docanchor file="index">board_pkg</docanchor>
    <docanchor file="index" title="Overview">board_overview</docanchor>
    <docanchor file="index">Dependencies</docanchor>
  </compound>
</tagfile>
