#include "board.h"
#include "chip.h" 

/* System oscillator rate and RTC oscillator rate (fucking LPCOpen) */
const uint32_t OscRateIn = 12000000;
const uint32_t RTCOscRateIn = 32768;

void HardFault_Handler(void) {
	while(1);
 // xBoard_LIT_Glow(LIT_SYS_FAIL);
}



BaseType_t xBoard_Init(void) {
	BaseType_t xOutcome;
	//Setup clock
	Chip_SetupXtalClocking();
	Chip_SYSCTL_SetFLASHAccess(FLASHTIM_120MHZ_CPU);
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock/1000);

	vBoard_LED_Init();
	vBoard_PMU_Init();

	xBoard_AFE_Init();
	vBoard_AFE_PowerDown();
	vBoard_PMU_PowerUp();
	xBoard_CCD_Init();

	vBoard_AFE_PowerUp();
	xOutcome = xBoard_COM_Init();

	return xOutcome;
}

