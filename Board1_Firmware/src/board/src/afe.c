#include "afe.h"


static struct{
	UBaseType_t xInits;
	xSemaphoreHandle xMutex;
	uint16_t reg[BOARD_AFE_REG_SIZE]/* = {0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x003, 0x001, 0x000, 0x007, 0x000, 0x000, 0x000, 0x000}*/;
}AFEData;

BaseType_t xBoard_AFE_Init(void)
{
	BaseType_t error = pdFREERTOS_ERRNO_NONE;
	if(!AFEData.xInits)
	{
		AFEData.xMutex = xSemaphoreCreateMutex();
		if(AFEData.xMutex == NULL)
		{
	      return pdFREERTOS_ERRNO_ENOMEM;
		}

		Chip_IOCON_PinMux(LPC_IOCON, AFE_CLCCD_PORT, AFE_CLCCD_PIN, AFE_CLCCD_MODE, AFE_CLCCD_FUNC);
		Chip_GPIO_SetPinDIROutput(LPC_GPIO, AFE_CLCCD_PORT, AFE_CLCCD_PIN);

		Chip_IOCON_PinMux(LPC_IOCON, AFE_OBCLP_PORT, AFE_OBCLP_PIN, AFE_OBCLP_MODE, AFE_OBCLP_FUNC);
		Chip_GPIO_SetPinDIROutput(LPC_GPIO, AFE_OBCLP_PORT, AFE_OBCLP_PIN);

	    Chip_IOCON_PinMux(LPC_IOCON, AFE_OE_PORT, AFE_OE_PIN, AFE_OE_MODE, AFE_OE_FUNC);
	    Chip_GPIO_SetPinDIROutput(LPC_GPIO, AFE_OE_PORT, AFE_OE_PIN);

	    Chip_IOCON_PinMux(LPC_IOCON, AFE_SV_PORT, AFE_SV_PIN, AFE_SV_MODE, AFE_SV_FUNC);
	    Chip_GPIO_SetPinDIROutput(LPC_GPIO, AFE_SV_PORT, AFE_SV_PIN);

	    Chip_IOCON_PinMux(LPC_IOCON, AFE_SR_PORT, AFE_SR_PIN, AFE_SR_MODE, AFE_SR_FUNC);
	    Chip_GPIO_SetPinDIROutput(LPC_GPIO, AFE_SR_PORT, AFE_SR_PIN);


	    Chip_IOCON_PinMux(LPC_IOCON, AFE_OE_PORT, AFE_OE_PIN, AFE_OE_MODE, AFE_OE_FUNC);
	    Chip_GPIO_SetPinDIROutput(LPC_GPIO, AFE_OE_PORT, AFE_OE_PIN);

	    Chip_IOCON_PinMux(LPC_IOCON, AFE_RST_PORT, AFE_RST_PIN, AFE_RST_MODE, AFE_RST_FUNC);
	    Chip_GPIO_SetPinDIROutput(LPC_GPIO, AFE_RST_PORT, AFE_RST_PIN);

	    Chip_IOCON_PinMux(LPC_IOCON, AFE_STBY_PORT, AFE_STBY_PIN, AFE_STBY_MODE, AFE_STBY_FUNC);
	    Chip_GPIO_SetPinDIROutput(LPC_GPIO, AFE_STBY_PORT, AFE_STBY_PIN);

	    Chip_IOCON_PinMux(LPC_IOCON, AFE_BLK_PORT, AFE_BLK_PIN, AFE_BLK_MODE, AFE_BLK_FUNC);
	    Chip_GPIO_SetPinDIROutput(LPC_GPIO, AFE_BLK_PORT, AFE_BLK_PIN);


	    Chip_IOCON_PinMux(LPC_IOCON, AFE_CS_PORT, AFE_CS_PIN, AFE_CS_MODE, AFE_CS_FUNC);
	    Chip_GPIO_SetPinDIROutput(LPC_GPIO, AFE_CS_PORT, AFE_CS_PIN);

	    Chip_IOCON_PinMux(LPC_IOCON, AFE_SCLK_PORT, AFE_SCLK_PIN, AFE_SCLK_MODE, AFE_SCLK_FUNC);
	    Chip_GPIO_SetPinDIROutput(LPC_GPIO, AFE_SCLK_PORT, AFE_SCLK_PIN);

	    Chip_IOCON_PinMux(LPC_IOCON, AFE_SDIN_PORT, AFE_SDIN_PIN, AFE_SDIN_MODE, AFE_SDIN_FUNC);
	    Chip_GPIO_SetPinDIROutput(LPC_GPIO, AFE_SDIN_PORT, AFE_SDIN_PIN);

	    Chip_IOCON_PinMux(LPC_IOCON, AFE_D0_PORT, AFE_D0_PIN, AFE_DATA_MODE, AFE_DATA_FUNC);
	    Chip_GPIO_SetPinDIRInput(LPC_GPIO, AFE_D0_PORT, AFE_D0_PIN);
	    Chip_IOCON_PinMux(LPC_IOCON, AFE_D1_PORT, AFE_D1_PIN, AFE_DATA_MODE, AFE_DATA_FUNC);
	    Chip_GPIO_SetPinDIRInput(LPC_GPIO, AFE_D1_PORT, AFE_D1_PIN);
	    Chip_IOCON_PinMux(LPC_IOCON, AFE_D2_PORT, AFE_D2_PIN, AFE_DATA_MODE, AFE_DATA_FUNC);
	    Chip_GPIO_SetPinDIRInput(LPC_GPIO, AFE_D2_PORT, AFE_D2_PIN);
	    Chip_IOCON_PinMux(LPC_IOCON, AFE_D3_PORT, AFE_D3_PIN, AFE_DATA_MODE, AFE_DATA_FUNC);
	    Chip_GPIO_SetPinDIRInput(LPC_GPIO, AFE_D3_PORT, AFE_D3_PIN);
	    Chip_IOCON_PinMux(LPC_IOCON, AFE_D4_PORT, AFE_D4_PIN, AFE_DATA_MODE, AFE_DATA_FUNC);
	    Chip_GPIO_SetPinDIRInput(LPC_GPIO, AFE_D4_PORT, AFE_D4_PIN);
	    Chip_IOCON_PinMux(LPC_IOCON, AFE_D5_PORT, AFE_D5_PIN, AFE_DATA_MODE, AFE_DATA_FUNC);
	    Chip_GPIO_SetPinDIRInput(LPC_GPIO, AFE_D5_PORT, AFE_D5_PIN);
	    Chip_IOCON_PinMux(LPC_IOCON, AFE_D6_PORT, AFE_D6_PIN, AFE_DATA_MODE, AFE_DATA_FUNC);
	    Chip_GPIO_SetPinDIRInput(LPC_GPIO, AFE_D6_PORT, AFE_D6_PIN);
	    Chip_IOCON_PinMux(LPC_IOCON, AFE_D7_PORT, AFE_D7_PIN, AFE_DATA_MODE, AFE_DATA_FUNC);
	    Chip_GPIO_SetPinDIRInput(LPC_GPIO, AFE_D7_PORT, AFE_D7_PIN);
	    Chip_IOCON_PinMux(LPC_IOCON, AFE_D8_PORT, AFE_D8_PIN, AFE_DATA_MODE, AFE_DATA_FUNC);
	    Chip_GPIO_SetPinDIRInput(LPC_GPIO, AFE_D8_PORT, AFE_D8_PIN);
	    Chip_IOCON_PinMux(LPC_IOCON, AFE_D9_PORT, AFE_D9_PIN, AFE_DATA_MODE, AFE_DATA_FUNC);
	    Chip_GPIO_SetPinDIRInput(LPC_GPIO, AFE_D9_PORT, AFE_D9_PIN);

	    vBoard_AFE_SSP_Init();
	}
	AFEData.xInits++;
	return error;

}

void vBoard_AFE_PowerDown(void)
{
	Chip_GPIO_SetPinOutLow(LPC_GPIO, AFE_STBY_PORT, AFE_STBY_PIN);
}

void vBoard_AFE_PowerUp(void)
{
	Chip_GPIO_SetPinOutHigh(LPC_GPIO, AFE_STBY_PORT, AFE_STBY_PIN);
	Chip_GPIO_SetPinOutHigh(LPC_GPIO, AFE_RST_PORT, AFE_RST_PIN);
}

void vBoard_AFE_Reset(void)
{
	Chip_GPIO_SetPinOutLow(LPC_GPIO, AFE_RST_PORT, AFE_RST_PIN);
	
	Chip_GPIO_SetPinOutHigh(LPC_GPIO, AFE_RST_PORT, AFE_RST_PIN);
}


void vBoard_AFE_SSP_Init(void)
{
	Chip_SSP_Init(LPC_SSP0);
	Chip_SSP_Set_Mode(LPC_SSP0, SSP_MODE_MASTER);
	/* CPOL 0 and CPHA 1 when SCKP is pulled low */
	Chip_SSP_SetFormat(LPC_SSP0, SSP_BITS_16, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_CPHA1_CPOL1);
	Chip_SSP_SetClockRate(LPC_SSP0, 200, 0);
	Chip_SSP_DMA_Disable(LPC_SSP0);
	Chip_SSP_Int_Disable(LPC_SSP0);

	Chip_SSP_Enable(LPC_SSP0);
}


BaseType_t xBoard_AFE_ConfigRegister(BOARD_AFE_REG_T pos, uint16_t data)
{	
	xSemaphoreTake(AFEData.xMutex, portMAX_DELAY);
	/* The two leading null bits are followed by four address bits for which the internal
	 register is to be updated, and then ten bits of data to be written to the register */
	Chip_SSP_SendFrame(LPC_SSP0, ((pos & 0x0F) << 10) | (data & 0x3FF));
	AFEData.reg[pos] = data;
	xSemaphoreGive(AFEData.xMutex);
	return 0;
	
}

BaseType_t vBoard_AFE_SetDAC1Gain(uint16_t val)
{
	BaseType_t error = pdFREERTOS_ERRNO_NONE;
	if(val <= BOARD_AFE_DAC_MAXGAIN)
	{
		if(AFEData.reg[BOARD_AFE_CONTROL_1] & AFE_CTRL1_DAC1_PWDN)
		{
			/* Must Turn on DAC in the AFE Control Register first */
			xBoard_AFE_ConfigRegister(BOARD_AFE_CONTROL_1, AFEData.reg[BOARD_AFE_CONTROL_1] & ~(AFE_CTRL1_DAC1_PWDN));
		}
		xBoard_AFE_ConfigRegister(BOARD_AFE_DAC_1, AFE_DAC1_VALUE(val));
	
	}

	else
		error = pdFREERTOS_ERRNO_EINVAL;

	return error;
}

BaseType_t vBoard_AFE_SetDAC2Gain(uint16_t val)
{
	BaseType_t error = pdFREERTOS_ERRNO_NONE;
	if(val <= BOARD_AFE_DAC_MAXGAIN)
	{
		if(AFEData.reg[BOARD_AFE_CONTROL_1] & AFE_CTRL1_DAC2_PWDN)
		{
			/* Must Turn on DAC in the AFE Control Register first */
			xBoard_AFE_ConfigRegister(BOARD_AFE_CONTROL_1, AFEData.reg[BOARD_AFE_CONTROL_1] & ~(AFE_CTRL1_DAC2_PWDN));
		}
		xBoard_AFE_ConfigRegister(BOARD_AFE_DAC_2, AFE_DAC2_VALUE(val));
	}
	else
		error = pdFREERTOS_ERRNO_EINVAL;

	return error;
}