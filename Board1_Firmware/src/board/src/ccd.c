#include "ccd.h"

static volatile BaseType_t shCount = 0;
static volatile BaseType_t mclkCount = 0;

static struct{
	UBaseType_t xInits;
	uint32_t intTime;
	uint32_t clkPeriod;
	xSemaphoreHandle xMutex;
}CCDData = {0};


void TIMER1_IRQHandler(void)
{
	//Chip_GPIO_SetPinToggle(LPC_GPIO, CCD_SH_PORT, CCD_SH_PIN);
	if(shCount == 0)
	{
		Chip_GPIO_WritePortBit(LPC_GPIO, CCD_ICG_PORT, CCD_ICG_PIN, false);
		Chip_GPIO_SetPinState(LPC_GPIO, CCD_SH_PORT, CCD_SH_PIN, true);
	}
	if(shCount == 1)
	{
		Chip_GPIO_SetPinState(LPC_GPIO, CCD_SH_PORT, CCD_SH_PIN, false);
	}
	shCount++;
	if(shCount == 3)
	{
		Chip_GPIO_SetPinState(LPC_GPIO, CCD_ICG_PORT, CCD_ICG_PIN, true);
		shCount = 0;
		Chip_TIMER_Disable(CCD_TIMER_ICG);
	}
	Chip_TIMER_ClearMatch(CCD_TIMER_ICG, ICG_TIM_MATCH);
}

BaseType_t xBoard_CCD_Init(void)
{
	BaseType_t error = pdFREERTOS_ERRNO_NONE;
	if(!CCDData.xInits)
	{
		CCDData.xMutex = xSemaphoreCreateMutex();
		if(CCDData.xMutex == NULL)
		{
	      return pdFREERTOS_ERRNO_ENOMEM;
		}
	    if ((error = xBoard_CCD_SetClkPeriod(1000)) != pdFREERTOS_ERRNO_NONE)
	    		return error;
	    if ((error = xBoard_CCD_SetIntegrationTime(15000)) != pdFREERTOS_ERRNO_NONE)
		    	return error;

	    Chip_IOCON_PinMux(LPC_IOCON, CCD_ICG_PORT, CCD_ICG_PIN, CCD_TIMING_MODE, CCD_TIMING_FUNC);
	    Chip_GPIO_SetPinDIROutput(LPC_GPIO, CCD_ICG_PORT, CCD_ICG_PIN);
		Chip_GPIO_WritePortBit(LPC_GPIO, CCD_ICG_PORT, CCD_ICG_PIN, true);

	    Chip_IOCON_PinMux(LPC_IOCON, CCD_MCLK_PORT, CCD_MCLK_PIN, CCD_TIMING_MODE, CCD_MCLK_FUNC);
	    Chip_GPIO_SetPinDIROutput(LPC_GPIO, CCD_MCLK_PORT, CCD_MCLK_PIN);
		//Chip_GPIO_WritePortBit(LPC_GPIO, CCD_MCLK_PORT, CCD_MCLK_PIN, false);

	    Chip_IOCON_PinMux(LPC_IOCON, CCD_SH_PORT, CCD_SH_PIN, CCD_TIMING_MODE, CCD_TIMING_FUNC);
	    Chip_GPIO_SetPinDIROutput(LPC_GPIO, CCD_SH_PORT, CCD_SH_PIN);
		Chip_GPIO_WritePortBit(LPC_GPIO, CCD_SH_PORT, CCD_SH_PIN, false);

	    /* Set match2 in timer0 toggle for master clock output */
	    Chip_TIMER_Init(CCD_TIMER_MCLK);
	    Chip_Clock_SetPCLKDiv(SYSCTL_PCLK_TIMER2 , SYSCTL_CLKDIV_1);
		NVIC_DisableIRQ(TIMER2_IRQn);
		Chip_TIMER_TIMER_SetCountClockSrc(CCD_TIMER_MCLK, TIMER_CAPSRC_RISING_PCLK, 0);
		Chip_TIMER_PrescaleSet(CCD_TIMER_MCLK, 0);
		Chip_TIMER_SetMatch(CCD_TIMER_MCLK, MCLK_TIM_MATCH, 40);
		Chip_TIMER_ResetOnMatchEnable(CCD_TIMER_MCLK, MCLK_TIM_MATCH);
		Chip_TIMER_ExtMatchControlSet(CCD_TIMER_MCLK, 0, TIMER_EXTMATCH_TOGGLE, MCLK_TIM_MATCH);
		Chip_TIMER_Enable(CCD_TIMER_MCLK);



	    /* Set external match in timer1 for integration time */

	    Chip_TIMER_Init(CCD_TIMER_ICG);
	    Chip_Clock_SetPCLKDiv(SYSCTL_PCLK_TIMER1 , SYSCTL_CLKDIV_1);
		NVIC_DisableIRQ(TIMER1_IRQn);
		Chip_TIMER_TIMER_SetCountClockSrc(CCD_TIMER_ICG, TIMER_CAPSRC_RISING_PCLK, 0);
		Chip_TIMER_PrescaleSet(CCD_TIMER_ICG, 0);
		Chip_TIMER_SetMatch(CCD_TIMER_ICG, ICG_TIM_MATCH, ((Chip_Clock_GetSystemClockRate()/1000000)*CCDData.intTime)/1000);
		Chip_TIMER_ResetOnMatchEnable(CCD_TIMER_ICG, ICG_TIM_MATCH);
		Chip_TIMER_MatchEnableInt(CCD_TIMER_ICG, ICG_TIM_MATCH);
		NVIC_ClearPendingIRQ(TIMER1_IRQn);
		NVIC_EnableIRQ(TIMER1_IRQn);
		Chip_TIMER_Disable(CCD_TIMER_ICG);

	}
	CCDData.xInits++;	
	return error;
}




void vBoard_CCD_Read(void)
{
    /* Enable interrupt for SH match timer */
//	Chip_TIMER_MatchEnableInt(CCD_TIMER_MCLK, 1);

	/* Enable icg timer match count */
	Chip_TIMER_Enable(CCD_TIMER_ICG);

	//NVIC_EnableIRQ(TIMER0_IRQn);
	//NVIC_EnableIRQ(TIMER2_IRQn);
	/* Wait readout time. Should probably add some sort of ack from AFE, though. */
	vTaskDelay(configTICK_RATE_HZ/100);
	
}


BaseType_t xBoard_CCD_SetIntegrationTime(uint32_t intTime)
{
	if (intTime <= CCD_INT_TIME_MIN)
	{
		return pdFREERTOS_ERRNO_EINVAL;
	}

	xSemaphoreTake(CCDData.xMutex, portMAX_DELAY);
	CCDData.intTime = intTime;
	if(CCDData.xInits)
		Chip_TIMER_SetMatch(CCD_TIMER_ICG, MCLK_TIM_MATCH, ((Chip_Clock_GetSystemClockRate()/1000000)*CCDData.intTime)/1000);
	xSemaphoreGive(CCDData.xMutex);
	

	return pdFREERTOS_ERRNO_NONE;
}

BaseType_t xBoard_CCD_SetClkPeriod(uint32_t clkPeriod)
{
	if ((clkPeriod <= CCD_CLK_PERIOD_MIN) || (clkPeriod >= CCD_CLK_PERIOD_MAX))
	{
		return pdFREERTOS_ERRNO_EINVAL;
	}

	xSemaphoreTake(CCDData.xMutex, portMAX_DELAY);
	CCDData.clkPeriod = clkPeriod;
	if(CCDData.xInits)
		Chip_TIMER_SetMatch(CCD_TIMER_MCLK, MCLK_TIM_MATCH,((Chip_Clock_GetSystemClockRate()/1000000)*clkPeriod)/1000);
	xSemaphoreGive(CCDData.xMutex);


	return pdFREERTOS_ERRNO_NONE;
}

