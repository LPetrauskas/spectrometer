#include "hal/dma.h" 
#include "dma.h"
#include "common.h"

STATIC struct{
  UBaseType_t       xInits;
  SemaphoreHandle_t xMutex;
} DMAData = {0};

STATIC void vBoard_DMA_OnComplete(GPDMA_STATUS_T Status, void *pxData) {  
  BaseType_t xHigherPriorityTaskWoken = pdFALSE;
  DMA_Transfer_t *pxTransfer = (DMA_Transfer_t *)pxData;
  pxTransfer->xStatus = Status;
  xTaskNotifyFromISR(pxTransfer->xOwner, 0x00, eNoAction, &xHigherPriorityTaskWoken);
  portYIELD_FROM_ISR(xHigherPriorityTaskWoken);						  
}

IRQHandler(DMA_CONTROLLER) {
  Chip_GPDMA_Interrupt(LPC_GPDMA);
}

BaseType_t xBoard_DMA_Init(void) {
  if (!DMAData.xInits) {		
    DMAData.xMutex = xSemaphoreCreateMutex();
    if (DMAData.xMutex == NULL) {
      return pdFREERTOS_ERRNO_ENOMEM;
    }
    Chip_GPDMA_Init(LPC_GPDMA);
    
    NVIC_EnableIRQ(IRQSource(DMA_CONTROLLER));
  }
  DMAData.xInits++;
  return pdFREERTOS_ERRNO_NONE;
} 

BaseType_t xBoard_DMA_Transfer_Forward(DMA_Transfer_t *pxTransfer, UBaseType_t xFlags) {		
  DMA_Descriptor_t *pxDsc;
  BaseType_t xOutcome;
  if (xSemaphoreTake(DMAData.xMutex, (xFlags & NONBLOCKING)? 0:portMAX_DELAY) == pdTRUE) {
    for (pxDsc = pxTransfer->pxDescs;pxDsc != NULL; pxDsc = (DMA_Descriptor_t *) pxDsc->LinkAddr) {
      Chip_GPDMA_PrepareDescriptor(LPC_GPDMA, pxTransfer->pxConfig, pxDsc);
    }
    pxTransfer->pxConfig->Callback = &vBoard_DMA_OnComplete;
    pxTransfer->pxConfig->Arg = pxTransfer;
    pxTransfer->xOwner = xTaskGetCurrentTaskHandle();  
    pxTransfer->xHandle = Chip_GPDMA_Transfer(LPC_GPDMA, pxTransfer->pxConfig, pxTransfer->pxDescs, pxTransfer->ulPriority);
    xSemaphoreGive(DMAData.xMutex);
    if (pxTransfer->xHandle != GPDMA_HANDLE_NULL) {
      xTaskNotifyWait(0x00, 0xFF, NULL, portMAX_DELAY);
      xOutcome = pdFREERTOS_ERRNO_NONE;
    } else xOutcome = pdFREERTOS_ERRNO_EIO;
  } else xOutcome = pdFREERTOS_ERRNO_EBUSY;
  return xOutcome;
}

BaseType_t xBoard_DMA_Transfer_Abort(DMA_Transfer_t *pxTransfer) {	
  return (Chip_GPDMA_Stop(LPC_GPDMA, pxTransfer->xHandle) == SUCCESS)? pdFREERTOS_ERRNO_NONE:pdFREERTOS_ERRNO_EBADF;
}

void vBoard_DMA_Deinit(void) {
  DMAData.xInits--;
  
  if (!DMAData.xInits) {
    NVIC_DisableIRQ(IRQSource(DMA_CONTROLLER));
    Chip_GPDMA_DeInit(LPC_GPDMA);	
    vSemaphoreDelete(DMAData.xMutex);
  }
}
