#ifndef DMA_H
#define DMA_H

#include "chip.h"
#include "os.h"

#ifdef __cplusplus
extern "C" {
#endif

/** @defgroup board_dma DMA block driver
 * @ingroup board
 * @{
 */

/**
 * @brief Chip re-definition.
 */
typedef GPDMA_HANDLE_T DMA_Handle_t;

/**
 * @brief Chip re-definition.
 */
typedef GPDMA_CFG_T DMA_Configuration_t;

/**
 * @brief Chip re-definition.
 */
typedef GPDMA_DESC_T DMA_Descriptor_t;

/**
 * @brief DMA transfer descriptor 
 */
typedef struct {
  xTaskHandle xOwner; /**< Callee task */
  DMA_Handle_t xHandle; /**< DMA handle */
  DMA_Configuration_t *pxConfig; /**< DMA configuration */
  DMA_Descriptor_t *pxDescs; /**< DMA descriptor list head */
  BaseType_t xStatus; /**< DMA status */
  uint32_t ulPriority; /**< DMA priority, being 0 the highest */
} DMA_Transfer_t;

/**
 * @brief	Initialize the DMA block
 * @return	FreeRTOS-like return value
 */
EXTERN BaseType_t xBoard_DMA_Init(void);

/**
 * @brief	Initiate transfer on the DMA block
 * @param	pxTransfer	: DMA transfer to be forwarded. 
 * @param	uxFlags     	: Flags for BSP driver operation 
 * @return	FreeRTOS-like return value
 */
EXTERN BaseType_t xBoard_DMA_Transfer_Forward(DMA_Transfer_t *pxTransfer, UBaseType_t uxFlags);

/**
 * @brief	Abort transfer on the DMA block
 * @param	pxTransfer	: DMA transfer to be aborted.  
 * @return	FreeRTOS-like return value
 */
EXTERN BaseType_t xBoard_DMA_Transfer_Abort(DMA_Transfer_t *pxTransfer);

/**
 * @brief	Deinitialize the DMA block
 * @return	None
 */
EXTERN void vBoard_DMA_Deinit(void);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* DMA_H */
