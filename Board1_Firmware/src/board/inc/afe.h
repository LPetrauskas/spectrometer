

#ifndef __AFE_H_
#define __AFE_H_


#ifdef __cplusplus
extern "C" {
#endif
/** @defgroup TLV990 Analog front end Block
 * @ingroup board
 * @{
 */
#include "hal/afe.h"
#include "chip.h"
#include "os.h"


/**
 * @brief typedef with register structure for TLV990
 */
//typedef struct 
//{
//	 uint16_t AFE_CONTROL_1; 		/* Control register 1 */
//	 uint16_t AFE_PGA_GAIN;			/* PGA gain register */
//	 uint16_t AFE_DAC_2;			/* User DAC2 register */
//	 uint16_t AFE_DAC_1;			/* User DAC1 register */
//	 uint16_t AFE_DAC_COARSE;		/* Coarse offset DAC */ 
//	 uint16_t AFE_DAC_FINE;			/* Fine offset DAC */
//	 uint16_t AFE_DIG_VB;			/* Digital Vb register (sets reference-code level at the ADC output during the optical black interval) */
//	 uint16_t AFE_OPTICAL_BLACK;	/* Optical black setup register (sets the number of black pixels per line for digital averaging) */ 
//	 uint16_t AFE_PIXEL_LIMIT;		/* Hot/cold pixel limit register (sets the limit for maximum positive deviation of optical black pixel from Vb value) */
//	 uint16_t RESERVED;				/* Reserved */
//	 uint16_t AFE_CONTROL_2;		/* Control register2 (sets the weight for digital filtering) */
//	 uint16_t AFE_BLANKING;			/* Blanking data register (The data in this register appears at digital output during blanking (BLKG is low)) */
//	 uint16_t AFE_ADCCLK_DELAY;		/* ADCCLK internal programmable delay register */
//	 uint16_t AFE_SR_SV_DELAY;		/* SR and SV internal programmable delay register */
//	 uint16_t AFE_TEST;       		/* Test Register */
//} afeRegisters_t;
/**
 * @brief typedef with register structure for TLV990
 */
typedef enum 
{
	BOARD_AFE_CONTROL_1, 		/* Control register 1 */
	BOARD_AFE_PGA_GAIN, 		/* PGA gain register */
	BOARD_AFE_DAC_1,			/* User DAC1 register */
	BOARD_AFE_DAC_2,			/* User DAC2 register */
	BOARD_AFE_DAC_COARSE,		/* Coarse offset DAC */ 
	BOARD_AFE_DAC_FINE,			/* Fine offset DAC */
	BOARD_AFE_DIG_VB,			/* Digital Vb register (sets reference-code level at the ADC output during the optical black interval) */
	BOARD_AFE_OPTICAL_BLACK,	/* Optical black setup register (sets the number of black pixels per line for digital averaging) */ 
	BOARD_AFE_PIXEL_LIMIT,		/* Hot/cold pixel limit register (sets the limit for maximum positive deviation of optical black pixel from Vb value) */
	BOARD_RESERVED,				/* Reserved */
	BOARD_AFE_CONTROL_2,		/* Control register2 (sets the weight for digital filtering) */
	BOARD_AFE_BLANKING,			/* Blanking data register (The data in this register appears at digital output during blanking (BLKG is low)) */
	BOARD_AFE_ADCCLK_DELAY,		/* ADCCLK internal programmable delay register */
	BOARD_AFE_SR_SV_DELAY,		/* SR and SV internal programmable delay register */
	BOARD_AFE_TEST       		/* Test Register */
} BOARD_AFE_REG_T;

#define BOARD_AFE_REG_SIZE		15

#define AFE_CTRL1_PWDN			(01 << 9)
#define AFE_CTRL1_ACTV			(00 << 9)
#define AFE_CTRL1_DAC1_PWDN		(01 << 8)
#define AFE_CTRL1_DAC1_ACTV		(00 << 8)
#define AFE_CTRL1_DAC2_PWDN		(01 << 7)
#define AFE_CTRL1_DAC2_ACTV		(00 << 7)
#define AFE_CTRL1_ACD_BYPS		(00 << 6)
#define AFE_CTRL1_ACD_AUTO		(01 << 6)
#define AFE_CTRL1_AFD_BYPS		(00 << 5)
#define AFE_CTRL1_AFD_AUTO		(01 << 5)
#define AFE_CTRL1_SRSV_LOW		(00 << 2)
#define AFE_CTRL1_SRSV_HIGH		(01 << 2)
#define AFE_CTRL1_BLK_RESET 	(01 << 1)
#define AFE_CTRL1_RESET 		(01 << 0)

#define	AFE_PGA_GAIN(n)			(n  << 0)

#define AFE_DAC1_VALUE(n)		(n  << 0)
#define AFE_DAC2_VALUE(n)		(n  << 0)

#define AFE_DAC_COARSE_LVL(n)	(n  << 0)
#define AFE_DAC_COARSE_OFF(n)	(n  << 0)
#define AFE_DAC_COARSE_POS		(00 << 8)
#define AFE_DAC_COARSE_NEG		(01 << 8)

#define	AFE_DAC_FINE_LVL(n)		(n  << 0)
#define AFE_DAC_FINE_OFF(n)		(n  << 0)
#define AFE_DAC_FINE_POS		(00 << 8)
#define AFE_DAC_FINE_NEG		(01 << 8)

#define	AFE_DIG_VB_LVL(n)		(n  << 0)

#define AFE_OBLK_OMUX_ADC		(00 << 8)
#define AFE_OBLK_OMUX_COARSE	(02 << 8)
#define AFE_OBLK_OMUX_FINE		(03 << 8)
#define AFE_OBLK_HYST_DAC		(00 << 7)
#define AFE_OBLK_HYST_OFF		(01 << 7)
#define AFE_OBLK_SOFW_0			(00 << 4)
#define AFE_OBLK_SOFW_1			(01 << 4)
#define AFE_OBLK_SOFW_2			(02 << 4)
#define AFE_OBLK_SOFW_3			(03 << 4)
#define AFE_OBLK_MULT_OFF		(00 << 3)
#define AFE_OBLK_MULT_ON		(01 << 3)
#define AFE_OBLK_PX_AVG(n)		(n  << 0)

#define AFE_PX_LMT_SET			(00 << 0)
#define AFE_PX_LMT_OFF			(01 << 0)

#define AFE_CTRL2_NORMAL		(00 << 9)
#define AFE_CTRL2_SOF			(01 << 9)
#define AFE_CTRL2_AUTOSOF_ON	(01 << 7)
#define AFE_CTRL2_AUTOSOF_OFF	(00 << 7)
#define AFE_CTRL2_DIGFILT_WGT(n)	(n)

#define AFE_BLKING_BDTA_SET		(01 << 5)
#define AFE_BLKING_BDTA_CLEAR	(00 << 5)

#define AFE_ADCCLK_SETDLY(n)		(n)

#define AFE_SRSV_SV_DELAY(n)	(n << 4)
#define AFE_SRSV_SR_DELAY(n)	(n << 0)
	
#define BOARD_AFE_DAC_MAXGAIN	256


/**
 * @brief	initializes CCD Analog Front end.
 * @return	FreeRTOS-like return
 */
BaseType_t xBoard_AFE_Init(void);

/**
 * @brief   Powers Down Analog front end by setting de StandBy pin
 * @return	void.
 * @note
 */
void vBoard_AFE_PowerDown(void);


/**
 * @brief   Powers UP Analog front end by setting de StandBy pin
 * @return	void.
 * @note
 */
void vBoard_AFE_PowerUp(void);


/**
 * @brief   Resets Analog front end
 * @return	void.
 * @note
 */
void vBoard_AFE_Reset(void);


/**
 * @brief   Resets Analog front end
 * @return	void.
 * @note
 */
void vBoard_AFE_SSP_Init(void);


/**
 * @brief   Sets value of single register and stores it in the AFE's struct
 * @param	reg     : register to write
 * @param	data    : value to write in	register
 * @return	freertos-like return value indicating whether the op was successful.
 * @note
 */
BaseType_t xBoard_AFE_ConfigRegister(BOARD_AFE_REG_T reg, uint16_t data);

/**
 * @brief   Set first DAC's output value
 * @param	val     : output level in counts	
 * @return	freertos-like return value indicating whether the op was successful.
 * @note
 * For any level setting, the following relation must always
 * hold: @ref BOARD_AFE_DAC_MAXGAIN >= val 
 */
BaseType_t vBoard_AFE_SetDAC1Gain(uint16_t val);


/**
 * @brief   Set second DAC's output value
 * @param	val     : output level in counts	
 * @return	freertos-like return value indicating whether the op was successful.
 * @note
 * For any level setting, the following relation must always
 * hold: @ref BOARD_AFE_DAC_MAXGAIN >= val 
 */
BaseType_t vBoard_AFE_SetDAC2Gain(uint16_t val);




/**
 * @}
 */

#ifdef __cplusplus
}
#endif


#endif /* __AFE_H_ */

