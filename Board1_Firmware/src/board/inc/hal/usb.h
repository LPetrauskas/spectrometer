#ifndef HAL_USB_H
#define HAL_USB_H

/** @defgroup board_usb_hal USB stack Hardware Abstraction Layer
 * @ingroup board_usb
 * @{
 */

/**
 * @brief USB controller to be used 
 */
#define USB_CONTROLLER USB

/**
 * @brief USB controller DPLUS gpio port
 */
#define USB_CONTROLLER_DPLUS_PORT 0

/**
 * @brief USB controller DPLUS gpio pin
 */
#define USB_CONTROLLER_DPLUS_PIN  29

/**
 * @brief USB controller DPLUS gpio mode
 */
#define USB_CONTROLLER_DPLUS_MODE IOCON_MODE_INACT

/**
 * @brief USB controller DPLUS gpio function
 */
#define USB_CONTROLLER_DPLUS_FUNC IOCON_FUNC1


/**
 * @brief USB controller DMINUS gpio port
 */
#define USB_CONTROLLER_DMINUS_PORT 0

/**
 * @brief USB controller DMINUS gpio pin
 */
#define USB_CONTROLLER_DMINUS_PIN  30

/**
 * @brief USB controller DMINUS gpio mode
 */
#define USB_CONTROLLER_DMINUS_MODE IOCON_MODE_INACT

/**
 * @brief USB controller DMIN gpio function
 */
#define USB_CONTROLLER_DMINUS_FUNC IOCON_FUNC1

/**
 * @brief USB controller EN gpio port
 */
#define USB_CONTROLLER_DEVICE_EN_PORT 2

/**
 * @brief USB controller EN gpio pin
 */
#define USB_CONTROLLER_DEVICE_EN_PIN 9

/**
 * @brief USB controller EN gpio mode
 */
#define USB_CONTROLLER_DEVICE_EN_MODE IOCON_MODE_INACT

/**
 * @brief USB controller EN gpio function
 */
#define USB_CONTROLLER_DEVICE_EN_FUNC IOCON_FUNC1


/**
 * @brief USB controller VBUS gpio port
 */
#define USB_CONTROLLER_VBUS_PORT 1

/**
 * @brief USB controller VBUS gpio pin
 */
#define USB_CONTROLLER_VBUS_PIN 30

/**
 * @brief USB controller VBUS gpio mode
 */
#define USB_CONTROLLER_VBUS_MODE IOCON_MODE_INACT

/**
 * @brief USB controller VBUS gpio function
 */
#define USB_CONTROLLER_VBUS_FUNC IOCON_FUNC1




/**
 * @}
 */

#endif /* HAL_USB_H */
