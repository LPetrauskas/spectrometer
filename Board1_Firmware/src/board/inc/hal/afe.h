#ifndef HAL_AFE_H
#define HAL_AFE_H

/** @defgroup  Hardware Abstraction Layer
 * @ingroup boar_afe
 * @{
 */

/**
 * @brief AFE CCD signal clamp control input port
 */
#define AFE_CLCCD_PORT 0

/**
 * @brief AFE CCD signal clamp control input pin
 */
#define AFE_CLCCD_PIN 9

/**
 * @brief AFE CCD signal clamp control input mode
 */
#define AFE_CLCCD_MODE IOCON_MODE_INACT
/**
 * @brief AFE CCD signal clamp control input func
 */
#define AFE_CLCCD_FUNC IOCON_FUNC0


/**
 * @brief AFE Optical black level and offset calibration control input port
 */
#define AFE_OBCLP_PORT 1

/**
 * @brief AFE Optical black level and offset calibration control input pin
 */
#define AFE_OBCLP_PIN 18

/**
 * @brief AFE Optical black level and offset calibration control input mode
 */
#define AFE_OBCLP_MODE IOCON_MODE_INACT
/**
 * @brief AFE Optical black level and offset calibration control input func
 */
#define AFE_OBCLP_FUNC IOCON_FUNC0


/**
 * @brief AFE ADC Clock Input port
 */
#define AFE_ACLK_PORT 0

/**
 * @brief AFE ADC Clock Input pin
 */
#define AFE_ACLK_PIN 20

/**
 * @brief AFE ADC Clock Input mode
 */
#define AFE_ACLK_MODE IOCON_MODE_INACT
/**
 * @brief AFE ADC Clock Input func
 */
#define AFE_ACLK_FUNC IOCON_FUNC0



/**
 * @brief AFE signal level sample clock  input port
 */
#define AFE_SV_PORT 0

/**
 * @brief AFE signal level sample clock  input pin
 */
#define AFE_SV_PIN 8

/**
 * @brief AFE signal level sample clock  input mode
 */
#define AFE_SV_MODE IOCON_MODE_PULLDOWN
/**
 * @brief AFE signal level sample clock  input func
 */
#define AFE_SV_FUNC IOCON_FUNC0


/**
 * @brief AFE reference level sample clock input port
 */
#define AFE_SR_PORT 0

/**
 * @brief AFE reference level sample clock input pin
 */
#define AFE_SR_PIN 7

/**
 * @brief AFE reference level sample clock input mode
 */
#define AFE_SR_MODE IOCON_MODE_PULLDOWN
/**
 * @brief AFE reference level sample clock input func
 */
#define AFE_SR_FUNC IOCON_FUNC0



/**
 * @brief AFE Output data enable port
 */
#define AFE_OE_PORT 0

/**
 * @brief AFE Output data enable pin
 */
#define AFE_OE_PIN 19

/**
 * @brief AFE Output data enable mode
 */
#define AFE_OE_MODE IOCON_MODE_PULLDOWN
/**
 * @brief AFE Output data enable func
 */
#define AFE_OE_FUNC IOCON_FUNC0



/**
 * @brief AFE Hardware power-down control input port
 */
#define AFE_STBY_PORT 1

/**
 * @brief AFE Hardware power-down control input pin
 */
#define AFE_STBY_PIN 19

/**
 * @brief AFE Hardware power-down control input mode
 */
#define AFE_STBY_MODE IOCON_MODE_PULLUP
/**
 * @brief AFE Hardware power-down control input func
 */
#define AFE_STBY_FUNC IOCON_FUNC0

/**
 * @brief AFE CDS operation Disabler port
 */
#define AFE_BLK_PORT 2

/**
 * @brief AFE CDS operation Disabler pin
 */
#define AFE_BLK_PIN 12

/**
 * @brief AFE CDS operation Disabler mode
 */
#define AFE_BLK_MODE IOCON_MODE_INACT
/**
 * @brief AFE CDS operation Disabler func
 */
#define AFE_BLK_FUNC IOCON_FUNC0

/**
 * @brief AFE Reset port
 */
#define AFE_RST_PORT 2

/**
 * @brief AFE Reset pin
 */
#define AFE_RST_PIN 11

/**
 * @brief AFE Reset mode
 */
#define AFE_RST_MODE IOCON_MODE_PULLUP
/**
 * @brief AFE Reset func
 */
#define AFE_RST_FUNC IOCON_FUNC0


/**
 * @brief AFE Serial Chip Select port
 */
#define AFE_CS_PORT 1

/**
 * @brief AFE Serial Chip Select pin
 */
#define AFE_CS_PIN 21

/**
 * @brief AFE Serial Chip Select mode
 */
#define AFE_CS_MODE IOCON_MODE_INACT
/**
 * @brief AFE Serial Chip Select func
 */
#define AFE_CS_FUNC IOCON_FUNC3


/**
 * @brief AFE Serial Clock port
 */
#define AFE_SCLK_PORT 1

/**
 * @brief AFE Serial Clock pin
 */
#define AFE_SCLK_PIN 20

/**
 * @brief AFE Serial Clock mode
 */
#define AFE_SCLK_MODE IOCON_MODE_INACT
/**
 * @brief AFE Serial Clock func
 */
#define AFE_SCLK_FUNC IOCON_FUNC3


/**
 * @brief AFE Serial data Input port
 */
#define AFE_SDIN_PORT 1

/**
 * @brief AFE Serial data Input pin
 */
#define AFE_SDIN_PIN 24

/**
 * @brief AFE Serial data Input mode
 */
#define AFE_SDIN_MODE IOCON_MODE_INACT
/**
 * @brief AFE Serial data Input func
 */
#define AFE_SDIN_FUNC IOCON_FUNC3


/**
 * @brief AFE Data Input 0 port
 */
#define AFE_D0_PORT 2

/**
 * @brief AFE  Data Input 0 pin
 */
#define AFE_D0_PIN 3
/**
 * @brief AFE Data Input 1 port
 */
#define AFE_D1_PORT 2

/**
 * @brief AFE  Data Input 1 pin
 */
#define AFE_D1_PIN 4
/**
 * @brief AFE Data Input 2 port
 */
#define AFE_D2_PORT 2
/**
 * @brief AFE  Data Input 2 pin
 */
#define AFE_D2_PIN 5
/**
 * @brief AFE Data Input 3 port
 */
#define AFE_D3_PORT 2

/**
 * @brief AFE  Data Input 3 pin
 */
#define AFE_D3_PIN 6
/**
 * @brief AFE Data Input 4 port
 */
#define AFE_D4_PORT 2

/**
 * @brief AFE  Data Input 4 pin
 */
#define AFE_D4_PIN 7
/**
 * @brief AFE Data Input 5 port
 */
#define AFE_D5_PORT 2

/**
 * @brief AFE  Data Input 5 pin
 */
#define AFE_D5_PIN 8
/**
 * @brief AFE Data Input 6 port
 */
#define AFE_D6_PORT 0

/**
 * @brief AFE  Data Input 6 pin
 */
#define AFE_D6_PIN 16
/**
 * @brief AFE Data Input 7 port
 */
#define AFE_D7_PORT 0

/**
 * @brief AFE  Data Input 7 pin
 */
#define AFE_D7_PIN 15
/**
 * @brief AFE Data Input 8 port
 */
#define AFE_D8_PORT 0

/**
 * @brief AFE  Data Input 8 pin
 */
#define AFE_D8_PIN 17
/**
 * @brief AFE Data Input 9 port
 */
#define AFE_D9_PORT 0

/**
 * @brief AFE  Data Input 9 pin
 */
#define AFE_D9_PIN 18

/**
 * @brief AFE  Data mode
 */
#define AFE_DATA_MODE IOCON_MODE_INACT

/**
 * @brief AFE  Data func
 */
#define AFE_DATA_FUNC IOCON_FUNC0

/** 
 * @}
 */

#endif /* HAL_AFE_H  */
