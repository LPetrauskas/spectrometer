
#ifndef __HAL_CCD_H_
#define __HAL_CCD_H_


/** @defgroup Hardware abstraction layer for TCD1304 block driver
 * @ingroup board
 * @{
 */

/**
 * @brief CCD shutter port
 */

#define CCD_SH_PORT 0

/**
 * @brief CCD shutter pin
 */

#define CCD_SH_PIN 4

/**
 * @brief CCD integration clear gate port
 */

#define CCD_ICG_PORT 0

/**
 * @brief CCD integration clear gate pin
 */

#define CCD_ICG_PIN 5


/**
 * @brief CCD Master clock port
 */

#define CCD_MCLK_PORT 0

/**
 * @brief CCD Master clock pin
 */

#define CCD_MCLK_PIN 6

/**
 * @brief CCD ports mode
 */

#define CCD_TIMING_MODE IOCON_MODE_INACT

/**
 * @brief CCD ports function
 */

#define CCD_TIMING_FUNC IOCON_FUNC0

/**
 * @brief GPIO Function for external match
 */
#define CCD_MCLK_FUNC 3




/** 
 * @}
 */

#endif /* HAL_CCD_H  */

