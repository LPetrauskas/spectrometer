#ifndef HAL_LED_H
#define HAL_LED_H

/** @defgroup  Hardware Abstraction Layer
 * @ingroup boar_led
 * @{
 */


/**
 * @brief AFE CCD signal clamp control input port
 */
#define LED_GREEN_PORT 1

/**
 * @brief AFE CCD signal clamp control input pin
 */
#define LED_GREEN_PIN 8


/**
 * @brief AFE CCD signal clamp control input port
 */
#define LED_RED_PORT 1

/**
 * @brief AFE CCD signal clamp control input pin
 */
#define LED_RED_PIN 4

#endif /* __HAL_LED_H */