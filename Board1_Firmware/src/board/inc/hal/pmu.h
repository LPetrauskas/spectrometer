#ifndef __HAL_PMU_H_
#define __HAL_PMU_H_



/** @defgroup Hardware Abstraction Layer for Power Management Unit Block
 * @ingroup board_pmu
 * @{
 */

/**
 * @brief PMU Enable port
 */
#define PMU_ENABLE_PORT 1
/**
 * @brief PMU Enable pin
 */
#define PMU_ENABLE_PIN 	28
/**
 * @brief PMU Enable mode
 */
#define PMU_ENABLE_MODE IOCON_MODE_INACT
/**
 * @brief PMU Enable function
 */
#define PMU_ENABLE_FUNC IOCON_FUNC0


/** 
 * @}
 */

#endif /* HAL_PMU_H  */
