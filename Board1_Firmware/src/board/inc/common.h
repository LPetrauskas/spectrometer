#ifndef COMMON_H
#define COMMON_H

#ifdef __cplusplus
extern "C" {
#endif

/** @defgroup board_common Board common constants & utility macros
 * @ingroup board
 * @{
 */

/** @defgroup board_common_flags Board flags
 * @ingroup board_common
 * @{
 */

/** 
 * @brief Nonblocking board operation
 */
#define NONBLOCKING (1 << 0)

/**
 * @}
 */

/** 
 * @brief Safe deref 
 * @param ptr : Opaque pointer
 * @param type : Type 
 */
#define deref(ptr, type) *((type *)ptr)

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* COMMON_H */
