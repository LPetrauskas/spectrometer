#ifndef BUFFER_H
#define BUFFER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "os.h"

/** @defgroup board_buffer Two-phase ring buffer
 * @ingroup board
 * @{
 */

/**
 * @brief Ring buffer
 * @details This buffer allows for two-phase push/pop operations, useful  
 *          when the amount of data to be pushed/popped is unknown, but bounded.                     
 */
typedef struct {
  void *pvItems;   /**!< Pool of items. */
  UBaseType_t uxItemSize; /**!< Item size, in bytes. */
  UBaseType_t uxItemCnt;  /**!< Item count in the pool. */
  UBaseType_t uxWrap;     /**!< Wrap index. */
  UBaseType_t uxHead;     /**!< Head index. */
  UBaseType_t uxTail;     /**!< Tail index. */
} Buffer_t;

/**
 * @brief	Initialize the buffer
 * @param       pxBuff  : Buffer to be initialized.
 * @param       pvItems : Item array to be managed.
 * @param       uxItemSize : Size of each item.
 * @param       uxItemCnt  : Amount of items.
 * @return	Always pdTRUE
 */
EXTERN BaseType_t xBuffer_Init(Buffer_t *pxBuff, void *pvItems, UBaseType_t uxItemSize, UBaseType_t uxItemCnt);

/**
 * @brief	Begin push into buffer
 * @param       pxBuff  : Buffer to be pushed data into
 * @param       ppvItems : Pointer to item array for pushing 
 * @param       uxSize : Amount of items to be pushed at most.
 * @return	Amount of items available for pushing
 */
EXTERN UBaseType_t xBuffer_BeginPush(Buffer_t *pxBuff, void **ppvItems, UBaseType_t uxSize);

/**
 * @brief	End push into buffer
 * @param       pxBuff  : Buffer to be pushed data into
 * @param       uxSize : Amount of items actually pushed
 * @return	Amount of items actually pushed
 */
EXTERN UBaseType_t xBuffer_EndPush(Buffer_t *pxBuff, UBaseType_t uxSize);

/**
 * @brief	Begin pop into buffer
 * @param       pxBuff  : Buffer to be pushed data into
 * @param       ppvItems : Pointer to item array for poping 
 * @param       uxSize : Amount of items to be poped at most
 * @return	Amount of items available for poping
 */
EXTERN UBaseType_t xBuffer_BeginPop(Buffer_t *pxBuff, void **ppvItems, UBaseType_t uxSize);

/**
 * @brief	End pop from buffer
 * @param       pxBuff  : Buffer to be poped data from
 * @param       uxSize : Amount of items actually poped
 * @return	Amount of items actually poped
 */
EXTERN UBaseType_t xBuffer_EndPop(Buffer_t *pxBuff, UBaseType_t uxSize);

/**
 * @brief	Check if buffer is empty
 * @param	pxBuff	: Buffer to be checked
 * @return	pdTRUE if empty, pdFALSE otherwise
 */
EXTERN INLINE BaseType_t xBuffer_IsEmpty(Buffer_t *pxBuff); 

/**
 * @brief	Check if buffer is full
 * @param	pxBuff	: Buffer to be checked
 * @return	pdTRUE if full, pdFALSE otherwise
 */
EXTERN INLINE BaseType_t xBuffer_IsFull(Buffer_t *pxBuff);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* BUFFER_H */
