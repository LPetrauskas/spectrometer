#ifndef USB_DESC_H
#define USB_DESC_H

#include "usbd/usbd_rom_api.h"
#include "usb_config.h"
#include "chip.h"

#ifdef __cplusplus
extern "C" {
#endif

/** @defgroup board_usb_desc USB stack descriptors
 * @ingroup board_usb
 * @{
 */

/**
 * @brief Device descriptors
 */
EXTERN CONST uint8_t USB_DeviceDescriptor[];

/**
 * @brief Full speed configuration descriptors
 */
EXTERN       uint8_t USB_FsConfigDescriptor[];

/**
 * @brief String descriptors
 */
EXTERN CONST uint8_t USB_StringDescriptor[];

/**
 * @brief Device qualifier
 */
EXTERN CONST uint8_t USB_DeviceQualifier[];

/**
 * @brief	Find the address of interface descriptor for given class type.
 * @param	pxDescriptor	: Pointer to configuration descriptor in which the desired class
 *			          interface descriptor is to be found.
 * @param	xIntfClass	: Interface class type to be searched.
 * @return	If found returns the address of requested interface else returns NULL.
 */
EXTERN USB_INTERFACE_DESCRIPTOR *Find_IntfDescriptor(const uint8_t *pxDescriptor, uint32_t xIntfClass);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* USB_DESC_H */
