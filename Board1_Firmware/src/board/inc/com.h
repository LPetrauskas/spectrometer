#ifndef COM_H
#define COM_H

#include "os.h"

#ifdef __cplusplus
extern "C" {
#endif

/** @defgroup board_com COM Port block driver
 * @ingroup board
 * @{
 */

/** @defgroup board_com_buff COM Port buffering
 * @ingroup board_com
 * @{
 */

/**
 * @brief COM port rx buffer size
 */
#define COM_RX_BUFFER_SIZE 256

/**
 * @brief COM port tx buffer size
 */
#define COM_TX_BUFFER_SIZE 256

/**
 * @}
 */

/**
 * @brief COM port streamed-sink 
 */
extern const StreamSink_t COMSink;

/**
 * @brief	Initialize COM port.
 * @return	FreeRTOS-like return value.
 */
EXTERN BaseType_t xBoard_COM_Init(void);

/**
 * @brief	Read from the COM port
 * @param	pvData	: Pointer to buffer for data to be read
 * @param	xSize    : Amount of data to be read
 * @param       uxFlags   : Flags for  BSP driver operation
 * @return	Number of bytes actually read, or FreeRTOS-like error
 */
EXTERN BaseType_t xBoard_COM_Read(void *pvData, BaseType_t xSize, UBaseType_t uxFlags);

/**
 * @brief	Write to the COM port
 * @param	pvData	: Pointer to data to be written
 * @param	xSize	: Amount of data to be written
 * @param       uxFlags   : Flags for  BSP driver operation
 * @return	Number of bytes actually written, or FreeRTOS-like error
 */
EXTERN BaseType_t xBoard_COM_Write(const void *pvData, BaseType_t xSize, UBaseType_t uxFlags);

/**
 * @brief	Deinitialize COM port
 * @return	Nothing
 */
EXTERN void vBoard_COM_Deinit(void);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* COM_H */
