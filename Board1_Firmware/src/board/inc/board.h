#ifndef BOARD_H
#define BOARD_H


#include "chip.h"
#include "ccd.h"
#include "pmu.h"
#include "afe.h"
#include "led.h"
#include "com.h"


#ifdef __cplusplus
extern "C" {
#endif

/** @defgroup board Board Drivers
 * @{
 */


/**
 * @brief	Initialize hw for complete board functionality.
 * @return	FreeRTOS-like return value
 */
EXTERN BaseType_t xBoard_Init(void);


/**
 * @}
 */

#ifdef __cplusplus
}
#endif


#endif /* BOARD_H */
