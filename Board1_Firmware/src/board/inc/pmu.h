#ifndef __PMU_H_
#define __PMU_H_


#ifdef __cplusplus
extern "C" {
#endif
/** @defgroup Power Management Unit Block
 * @ingroup board
 * @{
 */
#include "hal/pmu.h"
#include "chip.h"
#include "os.h"


/**
 * @brief   Configures power supply control circuit
 * @return	void.
 * @note
 */
void vBoard_PMU_Init(void);

/**
 * @brief   Turns on power supply
 * @return	void.
 * @note
 */
void vBoard_PMU_PowerUp(void);

/**
 * @brief   Turns off power supply
 * @return	void.
 * @note
 */
void vBoard_PMU_PowerDown(void);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif


#endif /* _PMU_H_ */

