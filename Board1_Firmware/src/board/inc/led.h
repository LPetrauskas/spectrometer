

#ifndef __LED_H_
#define __LED_H_


#ifdef __cplusplus
extern "C" {
#endif

/** @defgroup Leds in board front end Block
 * @ingroup board
 * @{
 */
#include "hal/led.h"
#include "chip.h"



/**
 * @brief   Initialized GPIO pins connected to the LEDs
 * @return	void.
 * @note
 */
void vBoard_LED_Init(void);


/**
 * @brief   Toggles green led
 * @return	void.
 * @note
 */
void vBoard_LED_ToggleGreen(void);


/**
 * @brief   Toggles red led
 * @return	void.
 * @note
 */
void vBoard_LED_ToggleRed(void);


/**
 * @}
 */

#ifdef __cplusplus
}
#endif


#endif /* __LED_H_ */

