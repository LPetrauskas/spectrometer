<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>core_cm3.h</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/chip/inc/</path>
    <filename>core__cm3_8h</filename>
    <includes id="core__cmInstr_8h" name="core_cmInstr.h" local="no" imported="no">core_cmInstr.h</includes>
    <includes id="core__cmFunc_8h" name="core_cmFunc.h" local="no" imported="no">core_cmFunc.h</includes>
    <class kind="union">APSR_Type</class>
    <class kind="union">IPSR_Type</class>
    <class kind="union">xPSR_Type</class>
    <class kind="union">CONTROL_Type</class>
    <class kind="struct">NVIC_Type</class>
    <class kind="struct">SCB_Type</class>
    <class kind="struct">SCnSCB_Type</class>
    <class kind="struct">SysTick_Type</class>
    <class kind="struct">ITM_Type</class>
    <class kind="struct">DWT_Type</class>
    <class kind="struct">TPI_Type</class>
    <class kind="struct">CoreDebug_Type</class>
    <member kind="define">
      <type>#define</type>
      <name>NVIC_STIR_INTID_Pos</name>
      <anchorfile>group__CMSIS__NVIC.html</anchorfile>
      <anchor>ga9eebe495e2e48d302211108837a2b3e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>NVIC_STIR_INTID_Msk</name>
      <anchorfile>group__CMSIS__NVIC.html</anchorfile>
      <anchor>gae4060c4dfcebb08871ca4244176ce752</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CPUID_IMPLEMENTER_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga58686b88f94f789d4e6f429fe1ff58cf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CPUID_IMPLEMENTER_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga0932b31faafd47656a03ced75a31d99b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CPUID_VARIANT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga104462bd0815391b4044a70bd15d3a71</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CPUID_VARIANT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gad358dfbd04300afc1824329d128b99e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CPUID_ARCHITECTURE_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaf8b3236b08fb8e840efb682645fb0e98</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CPUID_ARCHITECTURE_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gafae4a1f27a927338ae9dc51a0e146213</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CPUID_PARTNO_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga705f68eaa9afb042ca2407dc4e4629ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CPUID_PARTNO_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga98e581423ca016680c238c469aba546d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CPUID_REVISION_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga3c3d9071e574de11fb27ba57034838b1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CPUID_REVISION_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga2ec0448b6483f77e7f5d08b4b81d85df</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_NMIPENDSET_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga750d4b52624a46d71356db4ea769573b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_NMIPENDSET_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga340e3f79e9c3607dee9f2c048b6b22e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_PENDSVSET_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gab5ded23d2ab1d5ff7cc7ce746205e9fe</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_PENDSVSET_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga1e40d93efb402763c8c00ddcc56724ff</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_PENDSVCLR_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gae218d9022288f89faf57187c4d542ecd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_PENDSVCLR_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga4a901ace381d3c1c74ac82b22fae2e1e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_PENDSTSET_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga9dbb3358c6167c9c3f85661b90fb2794</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_PENDSTSET_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga7325b61ea0ec323ef2d5c893b112e546</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_PENDSTCLR_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gadbe25e4b333ece1341beb1a740168fdc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_PENDSTCLR_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gab241827d2a793269d8cd99b9b28c2157</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_ISRPREEMPT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga11cb5b1f9ce167b81f31787a77e575df</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_ISRPREEMPT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaa966600396290808d596fe96e92ca2b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_ISRPENDING_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga10749d92b9b744094b845c2eb46d4319</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_ISRPENDING_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga056d74fd538e5d36d3be1f28d399c877</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_VECTPENDING_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gada60c92bf88d6fd21a8f49efa4a127b8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_VECTPENDING_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gacb6992e7c7ddc27a370f62878a21ef72</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_RETTOBASE_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga403d154200242629e6d2764bfc12a7ec</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_RETTOBASE_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaca6fc3f79bb550f64fd7df782ed4a5f6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_VECTACTIVE_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gae4f602c7c5c895d5fb687b71b0979fc3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_VECTACTIVE_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga5533791a4ecf1b9301c883047b3e8396</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_VTOR_TBLBASE_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gad9720a44320c053883d03b883b955751</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_VTOR_TBLBASE_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga778dd0ba178466b2a8877a6b8aa345ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_VTOR_TBLOFF_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gac6a55451ddd38bffcff5a211d29cea78</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_VTOR_TBLOFF_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga75e395ed74042923e8c93edf50f0996c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_VECTKEY_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaaa27c0ba600bf82c3da08c748845b640</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_VECTKEY_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga90c7cf0c490e7ae55f9503a7fda1dd22</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_VECTKEYSTAT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaec404750ff5ca07f499a3c06b62051ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_VECTKEYSTAT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gabacedaefeefc73d666bbe59ece904493</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_ENDIANESS_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gad31dec98fbc0d33ace63cb1f1a927923</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_ENDIANESS_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga2f571f93d3d4a6eac9a3040756d3d951</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_PRIGROUP_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaca155deccdeca0f2c76b8100d24196c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_PRIGROUP_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga8be60fff03f48d0d345868060dc6dae7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_SYSRESETREQ_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaffb2737eca1eac0fc1c282a76a40953c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_SYSRESETREQ_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaae1181119559a5bd36e62afa373fa720</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_VECTCLRACTIVE_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaa30a12e892bb696e61626d71359a9029</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_VECTCLRACTIVE_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga212c5ab1c1c82c807d30d2307aa8d218</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_VECTRESET_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga0d483d9569cd9d1b46ec0d171b1f18d8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_VECTRESET_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga3006e31968bb9725e7b4ee0784d99f7f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SCR_SEVONPEND_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga3bddcec40aeaf3d3a998446100fa0e44</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SCR_SEVONPEND_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gafb98656644a14342e467505f69a997c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SCR_SLEEPDEEP_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gab304f6258ec03bd9a6e7a360515c3cfe</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SCR_SLEEPDEEP_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga77c06a69c63f4b3f6ec1032e911e18e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SCR_SLEEPONEXIT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga3680a15114d7fdc1e25043b881308fe9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SCR_SLEEPONEXIT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga50a243e317b9a70781b02758d45b05ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_STKALIGN_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gac2d20a250960a432cc74da59d20e2f86</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_STKALIGN_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga33cf22d3d46af158a03aad25ddea1bcb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_BFHFNMIGN_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga4010a4f9e2a745af1b58abe1f791ebbf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_BFHFNMIGN_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga89a28cc31cfc7d52d9d7a8fcc69c7eac</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_DIV_0_TRP_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gac8d512998bb8cd9333fb7627ddf59bba</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_DIV_0_TRP_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gabb9aeac71b3abd8586d0297070f61dcb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_UNALIGN_TRP_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gac4e4928b864ea10fc24dbbc57d976229</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_UNALIGN_TRP_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga68c96ad594af70c007923979085c99e0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_USERSETMPEND_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga789e41f45f59a8cd455fd59fa7652e5e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_USERSETMPEND_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga4cf59b6343ca962c80e1885710da90aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_NONBASETHRDENA_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gab4615f7deb07386350365b10240a3c83</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_NONBASETHRDENA_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gafe0f6be81b35d72d0736a0a1e3b4fbb3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_USGFAULTENA_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gae71949507636fda388ec11d5c2d30b52</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_USGFAULTENA_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga056fb6be590857bbc029bed48b21dd79</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_BUSFAULTENA_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga3d32edbe4a5c0335f808cfc19ec7e844</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_BUSFAULTENA_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga43e8cbe619c9980e0d1aacc85d9b9e47</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_MEMFAULTENA_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga685b4564a8760b4506f14ec4307b7251</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_MEMFAULTENA_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaf084424fa1f69bea36a1c44899d83d17</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_SVCALLPENDED_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga2f93ec9b243f94cdd3e94b8f0bf43641</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_SVCALLPENDED_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga6095a7acfbad66f52822b1392be88652</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_BUSFAULTPENDED_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaa22551e24a72b65f1e817f7ab462203b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_BUSFAULTPENDED_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga677c23749c4d348f30fb471d1223e783</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_MEMFAULTPENDED_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaceb60fe2d8a8cb17fcd1c1f6b5aa924f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_MEMFAULTPENDED_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga9abc6c2e395f9e5af4ce05fc420fb04c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_USGFAULTPENDED_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga3cf03acf1fdc2edc3b047ddd47ebbf87</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_USGFAULTPENDED_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga122b4f732732010895e438803a29d3cc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_SYSTICKACT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaec9ca3b1213c49e2442373445e1697de</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_SYSTICKACT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gafef530088dc6d6bfc9f1893d52853684</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_PENDSVACT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga9b9fa69ce4c5ce7fe0861dbccfb15939</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_PENDSVACT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gae0e837241a515d4cbadaaae1faa8e039</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_MONITORACT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga8b71cf4c61803752a41c96deb00d26af</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_MONITORACT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaad09b4bc36e9bccccc2e110d20b16e1a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_SVCALLACT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga977f5176be2bc8b123873861b38bc02f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_SVCALLACT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga634c0f69a233475289023ae5cb158fdf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_USGFAULTACT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gae06f54f5081f01ed3f6824e451ad3656</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_USGFAULTACT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gab3166103b5a5f7931d0df90949c47dfe</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_BUSFAULTACT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaf272760f2df9ecdd8a5fbbd65c0b767a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_BUSFAULTACT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga9d7a8b1054b655ad08d85c3c535d4f73</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_MEMFAULTACT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga7c856f79a75dcc1d1517b19a67691803</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_MEMFAULTACT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga9147fd4e1b12394ae26eadf900a023a3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CFSR_USGFAULTSR_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gac8e4197b295c8560e68e2d71285c7879</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CFSR_USGFAULTSR_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga565807b1a3f31891f1f967d0fa30d03f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CFSR_BUSFAULTSR_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga555a24f4f57d199f91d1d1ab7c8c3c8a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CFSR_BUSFAULTSR_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga26dc1ddfdc37a6b92597a6f7e498c1d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CFSR_MEMFAULTSR_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga91f41491cec5b5acca3fbc94efbd799e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CFSR_MEMFAULTSR_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gad46716159a3808c9e7da22067d6bec98</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_HFSR_DEBUGEVT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga300c90cfb7b35c82b4d44ad16c757ffb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_HFSR_DEBUGEVT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gababd60e94756bb33929d5e6f25d8dba3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_HFSR_FORCED_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gab361e54183a378474cb419ae2a55d6f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_HFSR_FORCED_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga6560d97ed043bc01152a7247bafa3157</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_HFSR_VECTTBL_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga77993da8de35adea7bda6a4475f036ab</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_HFSR_VECTTBL_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaac5e289211d0a63fe879a9691cb9e1a9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_DFSR_EXTERNAL_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga13f502fb5ac673df9c287488c40b0c1d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_DFSR_EXTERNAL_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga3cba2ec1f588ce0b10b191d6b0d23399</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_DFSR_VCATCH_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gad02d3eaf062ac184c18a7889c9b6de57</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_DFSR_VCATCH_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gacbb931575c07b324ec793775b7c44d05</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_DFSR_DWTTRAP_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaccf82364c6d0ed7206f1084277b7cc61</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_DFSR_DWTTRAP_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga3f7384b8a761704655fd45396a305663</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_DFSR_BKPT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaf28fdce48655f0dcefb383aebf26b050</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_DFSR_BKPT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga609edf8f50bc49adb51ae28bcecefe1f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_DFSR_HALTED_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaef4ec28427f9f88ac70a13ae4e541378</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_DFSR_HALTED_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga200bcf918d57443b5e29e8ce552e4bdf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCnSCB_ICTR_INTLINESNUM_Pos</name>
      <anchorfile>group__CMSIS__SCnSCB.html</anchorfile>
      <anchor>ga0777ddf379af50f9ca41d40573bfffc5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCnSCB_ICTR_INTLINESNUM_Msk</name>
      <anchorfile>group__CMSIS__SCnSCB.html</anchorfile>
      <anchor>ga3efa0f5210051464e1034b19fc7b33c7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCnSCB_ACTLR_DISFOLD_Pos</name>
      <anchorfile>group__CMSIS__SCnSCB.html</anchorfile>
      <anchor>gaab395870643a0bee78906bb15ca5bd02</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCnSCB_ACTLR_DISFOLD_Msk</name>
      <anchorfile>group__CMSIS__SCnSCB.html</anchorfile>
      <anchor>gaa9dd2d4a2350499188f438d0aa9fd982</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCnSCB_ACTLR_DISDEFWBUF_Pos</name>
      <anchorfile>group__CMSIS__SCnSCB.html</anchorfile>
      <anchor>gafa2eb37493c0f8dae77cde81ecf80f77</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCnSCB_ACTLR_DISDEFWBUF_Msk</name>
      <anchorfile>group__CMSIS__SCnSCB.html</anchorfile>
      <anchor>ga6cda7b7219232a008ec52cc8e89d5d08</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCnSCB_ACTLR_DISMCYCINT_Pos</name>
      <anchorfile>group__CMSIS__SCnSCB.html</anchorfile>
      <anchor>gaaa3e79f5ead4a32c0ea742b2a9ffc0cd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCnSCB_ACTLR_DISMCYCINT_Msk</name>
      <anchorfile>group__CMSIS__SCnSCB.html</anchorfile>
      <anchor>ga2a2818f0489ad10b6ea2964e899d4cbc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CTRL_COUNTFLAG_Pos</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>gadbb65d4a815759649db41df216ed4d60</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CTRL_COUNTFLAG_Msk</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>ga1bf3033ecccf200f59baefe15dbb367c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CTRL_CLKSOURCE_Pos</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>ga24fbc69a5f0b78d67fda2300257baff1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CTRL_CLKSOURCE_Msk</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>gaa41d06039797423a46596bd313d57373</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CTRL_TICKINT_Pos</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>ga88f45bbb89ce8df3cd2b2613c7b48214</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CTRL_TICKINT_Msk</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>ga95bb984266ca764024836a870238a027</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CTRL_ENABLE_Pos</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>ga0b48cc1e36d92a92e4bf632890314810</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CTRL_ENABLE_Msk</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>ga16c9fee0ed0235524bdeb38af328fd1f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_LOAD_RELOAD_Pos</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>gaf44d10df359dc5bf5752b0894ae3bad2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_LOAD_RELOAD_Msk</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>ga265912a7962f0e1abd170336e579b1b1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_VAL_CURRENT_Pos</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>ga3208104c3b019b5de35ae8c21d5c34dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_VAL_CURRENT_Msk</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>gafc77b56d568930b49a2474debc75ab45</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CALIB_NOREF_Pos</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>ga534dbe414e7a46a6ce4c1eca1fbff409</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CALIB_NOREF_Msk</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>ga3af0d891fdd99bcc8d8912d37830edb6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CALIB_SKEW_Pos</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>gadd0c9cd6641b9f6a0c618e7982954860</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CALIB_SKEW_Msk</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>ga8a6a85a87334776f33d77fd147587431</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CALIB_TENMS_Pos</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>gacae558f6e75a0bed5d826f606d8e695e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CALIB_TENMS_Msk</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>gaf1e68865c5aece2ad58971225bd3e95e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TPR_PRIVMASK_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga7abe5e590d1611599df87a1884a352e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TPR_PRIVMASK_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga168e089d882df325a387aab3a802a46b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_BUSY_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga9174ad4a36052c377cef4e6aba2ed484</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_BUSY_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga43ad7cf33de12f2ef3a412d4f354c60f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_TraceBusID_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>gaca0281de867f33114aac0636f7ce65d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_TraceBusID_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga60c20bd9649d1da5a2be8e656ba19a60</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_GTSFREQ_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga96c7c7cbc0d98426c408090b41f583f1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_GTSFREQ_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>gade862cf009827f7f6748fc44c541b067</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_TSPrescale_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>gad7bc9ee1732032c6e0de035f0978e473</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_TSPrescale_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga7a723f71bfb0204c264d8dbe8cc7ae52</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_SWOENA_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga7a380f0c8078f6560051406583ecd6a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_SWOENA_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga97476cb65bab16a328b35f81fd02010a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_DWTENA_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga30e83ebb33aa766070fe3d1f27ae820e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_DWTENA_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga98ea1c596d43d3633a202f9ee746cf70</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_SYNCENA_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>gaa93a1147a39fc63980d299231252a30e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_SYNCENA_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>gac89b74a78701c25b442105d7fe2bbefb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_TSENA_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga5aa381845f810114ab519b90753922a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_TSENA_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga436b2e8fa24328f48f2da31c00fc9e65</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_ITMENA_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga3286b86004bce7ffe17ee269f87f8d9d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_ITMENA_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga7dd53e3bff24ac09d94e61cb595cb2d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_IWR_ATVALIDM_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga04d3f842ad48f6a9127b4cecc963e1d7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_IWR_ATVALIDM_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga67b969f8f04ed15886727788f0e2ffd7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_IRR_ATREADYM_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga259edfd1d2e877a62e06d7a240df97f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_IRR_ATREADYM_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga3dbc3e15f5bde2669cd8121a1fe419b9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_IMCR_INTEGRATION_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga08de02bf32caf48aaa29f7c68ff5d755</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_IMCR_INTEGRATION_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga8838bd3dd04c1a6be97cd946364a3fd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_LSR_ByteAcc_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>gabfae3e570edc8759597311ed6dfb478e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_LSR_ByteAcc_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga91f492b2891bb8b7eac5b58de7b220f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_LSR_Access_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga144a49e12b83ad9809fdd2769094fdc0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_LSR_Access_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>gac8ae69f11c0311da226c0c8ec40b3d37</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_LSR_Present_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>gaf5740689cf14564d3f3fd91299b6c88d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_LSR_Present_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>gaa5bc2a7f5f1d69ff819531f5508bb017</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_NUMCOMP_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaac44b9b7d5391a7ffef129b7f6c84cd7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_NUMCOMP_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaa3d37d68c2ba73f2026265584c2815e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_NOTRCPKT_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaa82840323a2628e7f4a2b09b74fa73fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_NOTRCPKT_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga04d8bb0a065ca38e2e5f13a97e1f7073</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_NOEXTTRIG_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gad997b9026715d5609b5a3b144eca42d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_NOEXTTRIG_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gacc7d15edf7a27147c422099ab475953e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_NOCYCCNT_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga337f6167d960f57f12aa382ffecce522</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_NOCYCCNT_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaf40c8d7a4fd978034c137e90f714c143</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_NOPRFCNT_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gad52a0e5be84363ab166cc17beca0d048</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_NOPRFCNT_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gafd8448d7db4bc51f27f202e6e1f27823</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_CYCEVTENA_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga0cb0640aaeb18a626d7823570d5c3cb6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_CYCEVTENA_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga40554bd81460e39abf08810f45fac1a2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_FOLDEVTENA_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga5602b0707f446ce78d88ff2a3a82bfff</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_FOLDEVTENA_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga717e679d775562ae09185a3776b1582f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_LSUEVTENA_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaea5d1ee72188dc1d57b54c60a9f5233e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_LSUEVTENA_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gac47427f455fbc29d4b6f8a479169f2b2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_SLEEPEVTENA_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga9c6d62d121164013a8e3ee372f17f3e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_SLEEPEVTENA_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga2f431b3734fb840daf5b361034856da9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_EXCEVTENA_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaf4e73f548ae3e945ef8b1d9ff1281544</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_EXCEVTENA_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gab7ee0def33423b5859ca4030dff63b58</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_CPIEVTENA_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga9fff0b71fb0be1499f5180c6bce1fc8f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_CPIEVTENA_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga189089c30aade60b983df17ad2412f6f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_EXCTRCENA_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga05f13b547a9a1e63e003ee0bc6446d0d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_EXCTRCENA_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaf4fbb509ab3cbb768f16484c660a24c3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_PCSAMPLENA_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga1e14afc7790fcb424fcf619e192554c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_PCSAMPLENA_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gafdcf1c86f43fbeaf2780ce797c9ef3d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_SYNCTAP_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga678ef08786edcbef964479217efb9284</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_SYNCTAP_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaf1e6c3729d56ecadeb6eeff4d225968c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_CYCTAP_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaf70b80936c7db60bf84fb6dadb8a3559</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_CYCTAP_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga6c12e2868b8989a69445646698b8c331</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_POSTINIT_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga2868c0b28eb13be930afb819f55f6f25</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_POSTINIT_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gab8cbbee1e1d94d09f9a1f86379a08ee8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_POSTPRESET_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga129bc152febfddd67a0c20c6814cba69</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_POSTPRESET_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga11d9e1e2a758fdd2657aa68ce61b9c9d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_CYCCNTENA_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaa4509f5f8514a7200be61691f0e01f10</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_CYCCNTENA_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga4a9d209dc2a81ea6bfa0ea21331769d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CPICNT_CPICNT_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga80e9ad8f6a9e2344af8a3cf989bebe3d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CPICNT_CPICNT_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga76f39e7bca3fa86a4dbf7b8f6adb7217</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_EXCCNT_EXCCNT_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga031c693654030d4cba398b45d2925b1d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_EXCCNT_EXCCNT_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga057fa604a107b58a198bbbadb47e69c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_SLEEPCNT_SLEEPCNT_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga0371a84a7996dc5852c56afb2676ba1c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_SLEEPCNT_SLEEPCNT_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga1e340751d71413fef400a0a1d76cc828</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_LSUCNT_LSUCNT_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gab9394c7911b0b4312a096dad91d53a3d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_LSUCNT_LSUCNT_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga2186d7fc9317e20bad61336ee2925615</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FOLDCNT_FOLDCNT_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga7f8af5ac12d178ba31a516f6ed141455</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FOLDCNT_FOLDCNT_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga9cb73d0342d38b14e41027d3c5c02647</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_MASK_MASK_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaf798ae34e2b9280ea64f4d9920cd2e7d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_MASK_MASK_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gadd798deb0f1312feab4fb05dcddc229b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_MATCHED_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga22c5787493f74a6bacf6ffb103a190ba</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_MATCHED_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gac8b1a655947490280709037808eec8ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_DATAVADDR1_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga8b75e8ab3ffd5ea2fa762d028dc30e8c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_DATAVADDR1_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gafdbf5a8c367befe8661a4f6945c83445</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_DATAVADDR0_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga9854cd8bf16f7dce0fb196a8029b018e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_DATAVADDR0_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gafc5efbe8f9b51e04aecd00c8a4eb50fb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_DATAVSIZE_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga0517a186d4d448aa6416440f40fe7a4d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_DATAVSIZE_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaab42cbc1e6084c44d5de70971613ea76</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_LNK1ENA_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga89d7c48858b4d4de96cdadfac91856a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_LNK1ENA_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga64bd419260c3337cacf93607d1ad27ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_DATAVMATCH_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga106f3672cd4be7c7c846e20497ebe5a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_DATAVMATCH_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga32af1f1c0fcd2d8d9afd1ad79cd9970e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_CYCMATCH_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga4b65d79ca37ae8010b4a726312413efd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_CYCMATCH_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga8e2ed09bdd33a8f7f7ce0444f5f3bb25</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_EMITRANGE_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga41d5b332216baa8d29561260a1b85659</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_EMITRANGE_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gad46dd5aba29f2e28d4d3f50b1d291f41</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_FUNCTION_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga5797b556edde2bbaa4d33dcdb1a891bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_FUNCTION_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga3b2cda708755ecf5f921d08b25d774d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_ACPR_PRESCALER_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga5a82d274eb2df8b0c92dd4ed63535928</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_ACPR_PRESCALER_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga4fcacd27208419929921aec8457a8c13</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_SPPR_TXMODE_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga0f302797b94bb2da24052082ab630858</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_SPPR_TXMODE_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaca085c8a954393d70dbd7240bb02cc1f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFSR_FtNonStop_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga9537b8a660cc8803f57cbbee320b2fc8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFSR_FtNonStop_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaaa313f980974a8cfc7dac68c4d805ab1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFSR_TCPresent_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gad30fde0c058da2ffb2b0a213be7a1b5c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFSR_TCPresent_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga0d6bfd263ff2fdec72d6ec9415fb1135</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFSR_FtStopped_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaedf31fd453a878021b542b644e2869d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFSR_FtStopped_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga1ab6c3abe1cf6311ee07e7c479ce5f78</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFSR_FlInProg_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga542ca74a081588273e6d5275ba5da6bf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFSR_FlInProg_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga63dfb09259893958962914fc3a9e3824</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFCR_TrigIn_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaa7ea11ba6ea75b541cd82e185c725b5b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFCR_TrigIn_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga360b413bc5da61f751546a7133c3e4dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFCR_EnFCont_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga99e58a0960b275a773b245e2b69b9a64</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFCR_EnFCont_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga27d1ecf2e0ff496df03457a2a97cb2c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_TRIGGER_TRIGGER_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga5517fa2ced64efbbd413720329c50b99</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_TRIGGER_TRIGGER_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga814227af2b2665a0687bb49345e21110</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ITM_ATVALID_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaa7e050e9eb6528241ebc6835783b6bae</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ITM_ATVALID_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga94cb2493ed35d2dab7bd4092b88a05bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ITM_bytecount_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gac2b6f7f13a2fa0be4aa7645a47dcac52</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ITM_bytecount_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga07bafa971b8daf0d63b3f92b9ae7fa16</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ETM_ATVALID_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga7fdeb3e465ca4aa9e3b2f424ab3bbd1d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ETM_ATVALID_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga4f0005dc420b28f2369179a935b9a9d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ETM_bytecount_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga2f738e45386ebf58c4d406f578e7ddaf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ETM_bytecount_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gad2536b3a935361c68453cd068640af92</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ETM2_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga5f0037cc80c65e86d9e94e5005077a48</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ETM2_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaa82a7b9b99c990fb12eafb3c84b68254</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ETM1_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gac5a2ef4b7f811d1f3d81ec919d794413</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ETM1_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaad9c1a6ed34a70905005a0cc14d5f01b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ETM0_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga48783ce3c695d8c06b1352a526110a87</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ETM0_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaf924f7d1662f3f6c1da12052390cb118</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_ITATBCTR2_ATREADY_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga6959f73d7db4a87ae9ad9cfc99844526</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_ITATBCTR2_ATREADY_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga1859502749709a2e5ead9a2599d998db</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ITM_ATVALID_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga08edfc862b2c8c415854cc4ae2067dfb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ITM_ATVALID_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gabc1f6a3b6cac0099d7c01ca949b4dd08</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ITM_bytecount_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaa22ebf7c86e4f4b2c98cfd0b5981375a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ITM_bytecount_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gacba2edfc0499828019550141356b0dcb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ETM_ATVALID_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga3177b8d815cf4a707a2d3d3d5499315d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ETM_ATVALID_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga0e8f29a1e9378d1ceb0708035edbb86d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ETM_bytecount_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaab31238152b5691af633a7475eaf1f06</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ETM_bytecount_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gab554305459953b80554fdb1908b73291</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ITM2_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga1828c228f3940005f48fb8dd88ada35b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ITM2_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gae54512f926ebc00f2e056232aa21d335</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ITM1_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaece86ab513bc3d0e0a9dbd82258af49f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ITM1_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga3347f42828920dfe56e3130ad319a9e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ITM0_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga2188671488417a52abb075bcd4d73440</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ITM0_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga8ae09f544fc1a428797e2a150f14a4c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_ITATBCTR0_ATREADY_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gab1eb6866c65f02fa9c83696b49b0f346</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_ITATBCTR0_ATREADY_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaee320b3c60f9575aa96a8742c4ff9356</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_ITCTRL_Mode_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaa847adb71a1bc811d2e3190528f495f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_ITCTRL_Mode_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gad6f87550b468ad0920d5f405bfd3f017</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_NRZVALID_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga9f46cf1a1708575f56d6b827766277f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_NRZVALID_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gacecc8710a8f6a23a7d1d4f5674daf02a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_MANCVALID_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga675534579d9e25477bb38970e3ef973c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_MANCVALID_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga4c3ee4b1a34ad1960a6b2d6e7e0ff942</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_PTINVALID_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga974cccf4c958b4a45cb71c7b5de39b7b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_PTINVALID_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga1ca84d62243e475836bba02516ba6b97</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_MinBufSz_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga3f7da5de2a34be41a092e5eddd22ac4d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_MinBufSz_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga939e068ff3f1a65b35187ab34a342cd8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_AsynClkIn_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gab382b1296b5efd057be606eb8f768df8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_AsynClkIn_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gab67830557d2d10be882284275025a2d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_NrTraceInput_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga80ecae7fec479e80e583f545996868ed</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_NrTraceInput_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gabed454418d2140043cd65ec899abd97f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVTYPE_SubType_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga0c799ff892af5eb3162d152abc00af7a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVTYPE_SubType_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga5b2fd7dddaf5f64855d9c0696acd65c1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVTYPE_MajorType_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga69c4892d332755a9f64c1680497cebdd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVTYPE_MajorType_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaecbceed6d08ec586403b37ad47b38c88</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_DBGKEY_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gac91280edd0ce932665cf75a23d11d842</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_DBGKEY_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga1ce997cee15edaafe4aed77751816ffc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_RESET_ST_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga6f934c5427ea057394268e541fa97753</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_RESET_ST_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gac474394bcceb31a8e09566c90b3f8922</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_RETIRE_ST_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga2328118f8b3574c871a53605eb17e730</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_RETIRE_ST_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga89dceb5325f6bcb36a0473d65fbfcfa6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_LOCKUP_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga2900dd56a988a4ed27ad664d5642807e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_LOCKUP_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga7b67e4506d7f464ef5dafd6219739756</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_SLEEP_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga349ccea33accc705595624c2d334fbcb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_SLEEP_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga98d51538e645c2c1a422279cd85a0a25</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_HALT_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga760a9a0d7f39951dc3f07d01f1f64772</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_HALT_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga9f881ade3151a73bc5b02b73fe6473ca</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_REGRDY_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga20a71871ca8768019c51168c70c3f41d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_REGRDY_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gac4cd6f3178de48f473d8903e8c847c07</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_C_SNAPSTALL_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga85747214e2656df6b05ec72e4d22bd6d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_C_SNAPSTALL_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga53aa99b2e39a67622f3b9973e079c2b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_C_MASKINTS_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga0d2907400eb948a4ea3886ca083ec8e3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_C_MASKINTS_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga77fe1ef3c4a729c1c82fb62a94a51c31</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_C_STEP_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gae1fc39e80de54c0339cbb1b298a9f0f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_C_STEP_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gae6bda72fbd32cc5734ff3542170dc00d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_C_HALT_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gaddf1d43f8857e4efc3dc4e6b15509692</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_C_HALT_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga1d905a3aa594eb2e8bb78bcc4da05b3f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_C_DEBUGEN_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gab557abb5b172b74d2cf44efb9d824e4e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_C_DEBUGEN_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gab815c741a4fc2a61988cd2fb7594210b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DCRSR_REGWnR_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga51e75942fc0614bc9bb2c0e96fcdda9a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DCRSR_REGWnR_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga1eef4992d8f84bc6c0dffed1c87f90a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DCRSR_REGSEL_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga52182c8a9f63a52470244c0bc2064f7b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DCRSR_REGSEL_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga17cafbd72b55030219ce5609baa7c01d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_TRCENA_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga6ff2102b98f86540224819a1b767ba39</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_TRCENA_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga5e99652c1df93b441257389f49407834</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_MON_REQ_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga341020a3b7450416d72544eaf8e57a64</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_MON_REQ_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gae6384cbe8045051186d13ef9cdeace95</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_MON_STEP_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga9ae10710684e14a1a534e785ef390e1b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_MON_STEP_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga2ded814556de96fc369de7ae9a7ceb98</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_MON_PEND_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga1e2f706a59e0d8131279af1c7e152f8d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_MON_PEND_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga68ec55930269fab78e733dcfa32392f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_MON_EN_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga802829678f6871863ae9ecf60a10425c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_MON_EN_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gac2b46b9b65bf8d23027f255fc9641977</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_HARDERR_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gaed9f42053031a9a30cd8054623304c0a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_HARDERR_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga803fc98c5bb85f10f0347b23794847d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_INTERR_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga22079a6e436f23b90308be97e19cf07e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_INTERR_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gad6815d8e3df302d2f0ff2c2c734ed29a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_BUSERR_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gab8e3d8f0f9590a51bbf10f6da3ad6933</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_BUSERR_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga9d29546aefe3ca8662a7fe48dd4a5b2b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_STATERR_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga16f0d3d2ce1e1e8cd762d938ac56c4ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_STATERR_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gaa38b947d77672c48bba1280c0a642e19</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_CHKERR_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga10fc7c53bca904c128bc8e1a03072d50</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_CHKERR_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga2f98b461d19746ab2febfddebb73da6f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_NOCPERR_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gac9d13eb2add61f610d5ced1f7ad2adf8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_NOCPERR_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga03ee58b1b02fdbf21612809034562f1c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_MMERR_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga444454f7c7748e76cd76c3809c887c41</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_MMERR_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gad420a9b60620584faaca6289e83d3a87</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_CORERESET_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga9fcf09666f7063a7303117aa32a85d5a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_CORERESET_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga906476e53c1e1487c30f3a1181df9e30</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCS_BASE</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>ga3c14ed93192c8d9143322bbf77ebf770</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_BASE</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>gadd76251e412a195ec0a8f47227a8359e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_BASE</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>gafdab534f961bf8935eb456cb7700dcd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_BASE</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>ga2b1eeff850a7e418844ca847145a1a68</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_BASE</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>ga680604dbcda9e9b31a1639fcffe5230b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_BASE</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>ga58effaac0b93006b756d33209e814646</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>NVIC_BASE</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>gaa0288691785a5f868238e0468b39523d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_BASE</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>gad55a7ddb8d4b2398b0c1cfec76c0d9fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCnSCB</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>ga9fe0cd2eef83a8adad94490d9ecca63f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>gaaaf6477c2bde2f00f99e3c2fd1060b01</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>gacd96c53beeaff8f603fcda425eb295de</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>NVIC</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>gac8e97e8ce56ae9f57da1363a937f8a17</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>gabae7cdf882def602cb787bb039ff6a43</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>gabbe5a060185e1d5afa3f85b14e10a6ce</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>ga8b4dd00016aed25a0ea54e9a9acd1239</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>gab6e30a2b802d9021619dbb0be7f5d63d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_RXBUFFER_EMPTY</name>
      <anchorfile>group__CMSIS__core__DebugFunctions.html</anchorfile>
      <anchor>gaa822cb398ee022b59e9e6c5d7bbb228a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>__CM3_CMSIS_VERSION_MAIN</name>
      <anchorfile>core__cm3_8h.html</anchorfile>
      <anchor>ac1c1120e9fe082fac8225c60143ac79a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>__CM3_CMSIS_VERSION_SUB</name>
      <anchorfile>core__cm3_8h.html</anchorfile>
      <anchor>a9ff7a998d4b8b3c87bfaca6e78607950</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>__CM3_CMSIS_VERSION</name>
      <anchorfile>core__cm3_8h.html</anchorfile>
      <anchor>af888c651cd8c93fd25364f9e74306a1c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>__CORTEX_M</name>
      <anchorfile>core__cm3_8h.html</anchorfile>
      <anchor>a63ea62503c88acab19fcf3d5743009e3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>__FPU_USED</name>
      <anchorfile>core__cm3_8h.html</anchorfile>
      <anchor>aa167d0f532a7c2b2e3a6395db2fa0776</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>__I</name>
      <anchorfile>core__cm3_8h.html</anchorfile>
      <anchor>af63697ed9952cc71e1225efe205f6cd3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>__O</name>
      <anchorfile>core__cm3_8h.html</anchorfile>
      <anchor>a7e25d9380f9ef903923964322e71f2f6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>__IO</name>
      <anchorfile>core__cm3_8h.html</anchorfile>
      <anchor>aec43007d9998a0a0e01faede4133d6be</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE void</type>
      <name>NVIC_SetPriorityGrouping</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>ga77cfbb35a9d8027e392034321bed6904</anchor>
      <arglist>(uint32_t PriorityGroup)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE uint32_t</type>
      <name>NVIC_GetPriorityGrouping</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>ga394f7ce2ca826c0da26284d17ac6524d</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE void</type>
      <name>NVIC_EnableIRQ</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>ga3349f2e3580d7ce22d6530b7294e5921</anchor>
      <arglist>(IRQn_Type IRQn)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE void</type>
      <name>NVIC_DisableIRQ</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>ga260fba04ac8346855c57f091d4ee1e71</anchor>
      <arglist>(IRQn_Type IRQn)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE uint32_t</type>
      <name>NVIC_GetPendingIRQ</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>gafec8042db64c0f8ed432b6c8386a05d8</anchor>
      <arglist>(IRQn_Type IRQn)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE void</type>
      <name>NVIC_SetPendingIRQ</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>ga3ecf446519da33e1690deffbf5be505f</anchor>
      <arglist>(IRQn_Type IRQn)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE void</type>
      <name>NVIC_ClearPendingIRQ</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>ga332e10ef9605dc6eb10b9e14511930f8</anchor>
      <arglist>(IRQn_Type IRQn)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE uint32_t</type>
      <name>NVIC_GetActive</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>ga47a0f52794068d076c9147aa3cb8d8a6</anchor>
      <arglist>(IRQn_Type IRQn)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE void</type>
      <name>NVIC_SetPriority</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>ga2305cbd44aaad792e3a4e538bdaf14f9</anchor>
      <arglist>(IRQn_Type IRQn, uint32_t priority)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE uint32_t</type>
      <name>NVIC_GetPriority</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>ga1cbaf8e6abd4aa4885828e7f24fcfeb4</anchor>
      <arglist>(IRQn_Type IRQn)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE uint32_t</type>
      <name>NVIC_EncodePriority</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>gadb94ac5d892b376e4f3555ae0418ebac</anchor>
      <arglist>(uint32_t PriorityGroup, uint32_t PreemptPriority, uint32_t SubPriority)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE void</type>
      <name>NVIC_DecodePriority</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>ga4f23ef94633f75d3c97670a53949003c</anchor>
      <arglist>(uint32_t Priority, uint32_t PriorityGroup, uint32_t *pPreemptPriority, uint32_t *pSubPriority)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE void</type>
      <name>NVIC_SystemReset</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>ga1143dec48d60a3d6f238c4798a87759c</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE uint32_t</type>
      <name>SysTick_Config</name>
      <anchorfile>group__CMSIS__Core__SysTickFunctions.html</anchorfile>
      <anchor>gae4e8f0238527c69f522029b93c8e5b78</anchor>
      <arglist>(uint32_t ticks)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE uint32_t</type>
      <name>ITM_SendChar</name>
      <anchorfile>group__CMSIS__core__DebugFunctions.html</anchorfile>
      <anchor>gac90a497bd64286b84552c2c553d3419e</anchor>
      <arglist>(uint32_t ch)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE int32_t</type>
      <name>ITM_ReceiveChar</name>
      <anchorfile>group__CMSIS__core__DebugFunctions.html</anchorfile>
      <anchor>gac3ee2c30a1ac4ed34c8a866a17decd53</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE int32_t</type>
      <name>ITM_CheckChar</name>
      <anchorfile>group__CMSIS__core__DebugFunctions.html</anchorfile>
      <anchor>gae61ce9ca5917735325cd93b0fb21dd29</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="variable">
      <type>volatile int32_t</type>
      <name>ITM_RxBuffer</name>
      <anchorfile>group__CMSIS__core__DebugFunctions.html</anchorfile>
      <anchor>ga12e68e55a7badc271b948d6c7230b2a8</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>core_cmFunc.h</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/chip/inc/</path>
    <filename>core__cmFunc_8h</filename>
  </compound>
  <compound kind="file">
    <name>core_cmInstr.h</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/chip/inc/</path>
    <filename>core__cmInstr_8h</filename>
  </compound>
  <compound kind="file">
    <name>usbd.h</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/chip/inc/usbd/</path>
    <filename>usbd_8h</filename>
    <class kind="struct">_WB_T</class>
    <class kind="union">__WORD_BYTE</class>
    <class kind="struct">_BM_T</class>
    <class kind="union">_REQUEST_TYPE</class>
    <class kind="struct">_USB_SETUP_PACKET</class>
    <class kind="struct">_USB_DEVICE_DESCRIPTOR</class>
    <class kind="struct">_USB_DEVICE_QUALIFIER_DESCRIPTOR</class>
    <class kind="struct">_USB_CONFIGURATION_DESCRIPTOR</class>
    <class kind="struct">_USB_IAD_DESCRIPTOR</class>
    <class kind="struct">_USB_INTERFACE_DESCRIPTOR</class>
    <class kind="struct">_USB_ENDPOINT_DESCRIPTOR</class>
    <class kind="struct">_USB_STRING_DESCRIPTOR</class>
    <class kind="struct">_USB_COMMON_DESCRIPTOR</class>
    <class kind="struct">_USB_OTHER_SPEED_CONFIGURATION</class>
    <member kind="define">
      <type>#define</type>
      <name>USB_CONFIG_POWER_MA</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gadcf08dc7fdb6117608624203612b3104</anchor>
      <arglist>(mA)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_0_HS_MAXP</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gac8df859211824ddcf32db5c80b06290a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_0_LS_MAXP</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga2b0c6c2be1d518967da6c7b32458c894</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_BULK_HS_MAXP</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gad4839d37e7a31a4d9e8ddf242b16608f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>REQUEST_HOST_TO_DEVICE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga8d90abb0515c0c9cd7c80480f1ebe7d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>REQUEST_DEVICE_TO_HOST</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gaf8f8eae5ae324d7d3e1344e97e23b20f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>REQUEST_STANDARD</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga1dd6ef57678e7af95d835049d5b355b9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>REQUEST_CLASS</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gafd24e03239e2df9e6d9c6cbcd53c1337</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>REQUEST_VENDOR</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga40131e4daec200a7d931c2b26e43d27f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>REQUEST_RESERVED</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga63cb1192a7cd205455bddd302f67c915</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>REQUEST_TO_DEVICE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gaf89871a0b4aa9411e09a9961de76e76f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>REQUEST_TO_INTERFACE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga0e5e473281fc9c1ee1ea446823c02623</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>REQUEST_TO_ENDPOINT</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga4c4571ca0bf6b462bd8954a95456561c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>REQUEST_TO_OTHER</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gad1f005d9ae04da7324d465a86e489af5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_REQUEST_GET_STATUS</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga062f3147a1bae954b642b915827b3f3f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_REQUEST_CLEAR_FEATURE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gab9fe860caa1f21c9410984a24bf57c7c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_REQUEST_SET_FEATURE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gaa2b4d305dcd185cc1242e94ee78eea25</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_REQUEST_SET_ADDRESS</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gac87659819bfd6491120c819fae2d46bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_REQUEST_GET_DESCRIPTOR</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga3572226ee2137c7fbf7487a02b0f560f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_REQUEST_SET_DESCRIPTOR</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga3c80fdedd69586ccf2894f5bd4638252</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_REQUEST_GET_CONFIGURATION</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga341ed4aff1f0d5a1a4a4f17cb606d08d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_REQUEST_SET_CONFIGURATION</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gaadef005acc40340955d34d9767ef297c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_REQUEST_GET_INTERFACE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga25550f44898d9c9a3bf0a7814af7fe96</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_REQUEST_SET_INTERFACE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gaf8d2ea632916bfc2a7628f63d3f643aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_REQUEST_SYNC_FRAME</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga916e1a4b8f8a5cb369d896ef35c4db34</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_GETSTATUS_SELF_POWERED</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga1e801ee37e93522f0f38eff9bc7aac02</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_GETSTATUS_REMOTE_WAKEUP</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gaad5790ebb5c8f5a25ce60140c60c2ec5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_GETSTATUS_ENDPOINT_STALL</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga4ebb10046221004827e4b2a72cdeefa5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_FEATURE_ENDPOINT_STALL</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga3ff0518fb298c9c9b556f59c0468ec0d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_FEATURE_REMOTE_WAKEUP</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga2185a0541f4155470596c9c739c1656b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_FEATURE_TEST_MODE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga90a1a76a81c04849dc897dacf6c3a213</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_DEVICE_DESCRIPTOR_TYPE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga722f2d6ee2892228b6708e8b9904c645</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_CONFIGURATION_DESCRIPTOR_TYPE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga45e81401e4b530280fdd539da1b06c26</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_STRING_DESCRIPTOR_TYPE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gab01c9c74f2eb266b20aecd48bab4b35c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_INTERFACE_DESCRIPTOR_TYPE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gad1699884fa580bba35246a566264c978</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_DESCRIPTOR_TYPE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga4e8d6d81a224f8b511edc92b6cb4e085</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_DEVICE_QUALIFIER_DESCRIPTOR_TYPE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga7158946184590ea6cfcb406d7f3c4ce5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_OTHER_SPEED_CONFIG_DESCRIPTOR_TYPE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga0d886bcc28cf10d730106d7be9b661fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_INTERFACE_POWER_DESCRIPTOR_TYPE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga3356db66a2c62f7018f23f9189ba34c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_OTG_DESCRIPTOR_TYPE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga0e105187070c79d5843dde5503bb78f2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_DEBUG_DESCRIPTOR_TYPE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga9d5ac46d5f303464ac827d63f9da984e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_INTERFACE_ASSOCIATION_DESCRIPTOR_TYPE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga2ab1841a48da627b702526b01eb31268</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_DEVICE_CLASS_RESERVED</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gaca7ece737f3f37b7a6f458fc24864776</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_DEVICE_CLASS_AUDIO</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gade360faee3b601af707b65122a46d446</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_DEVICE_CLASS_COMMUNICATIONS</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gab6fd7e18adf5c748a6b56391c1c2b819</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_DEVICE_CLASS_HUMAN_INTERFACE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga3d9e0560a81b4dfd30bbf829960dbfab</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_DEVICE_CLASS_MONITOR</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga23d001aa49fada2019ff3a4886a9f2a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_DEVICE_CLASS_PHYSICAL_INTERFACE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga6053794e5de119b37615ec98f7eceedb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_DEVICE_CLASS_POWER</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga88a8b2d45b2950ffd5654e4c3b6741ff</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_DEVICE_CLASS_PRINTER</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gae6181c6bdb18c0e6491dd189983b6bd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_DEVICE_CLASS_STORAGE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga053d2a9c3c9856ee01589ebb64d2e09b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_DEVICE_CLASS_HUB</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gab8167347cb586d3f82a7ad95fa25bc22</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_DEVICE_CLASS_MISCELLANEOUS</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gabd79da65d7f8f58b37e402fa1442831a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_DEVICE_CLASS_APP</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga9b5f0021003a2fc948daf8d0f4768736</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_DEVICE_CLASS_VENDOR_SPECIFIC</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga89b94b8bc3c5fa2ef7151b5d282f22bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_CONFIG_POWERED_MASK</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gafdd4f846b5497985c182a1a090d99729</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_CONFIG_BUS_POWERED</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga63c0a08ce70650c3c677ab523a45a0bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_CONFIG_SELF_POWERED</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga8c322771b797c09e3066a388b46e9ef9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_CONFIG_REMOTE_WAKEUP</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga7b28a766a8a916fae421d52fbedd2cba</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_DIRECTION_MASK</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gab7544c5b8cf975839422c1ce29d4c5cb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_OUT</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga81ac2a8c9688d35940a054c4194c278d</anchor>
      <arglist>(addr)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_IN</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gabe258bf57244c49874da6d53679e6df8</anchor>
      <arglist>(addr)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_IN_EP_IDX</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga0e14f9a574a34520401d550a5fea5d69</anchor>
      <arglist>(addr)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_OUT_EP_IDX</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga4472e9d475bd876594ddff5f8d8f9053</anchor>
      <arglist>(addr)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_TYPE_MASK</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gacd114fdf068aaa69407358ee5ccfd170</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_TYPE_CONTROL</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga6a51ed22b06e5ca0f76a35b6bac8d73a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_TYPE_ISOCHRONOUS</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gab05aae4a5d079b8dfb6a9cb1325ea6e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_TYPE_BULK</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gaf4bac84d4576dc8f74f39dc75749e3dc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_TYPE_INTERRUPT</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga2cb5aa69a03df20aab217b808ad692a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_SYNC_MASK</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga91301868eb0b52506fc9bc02b131f783</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_SYNC_NO_SYNCHRONIZATION</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga7733e58f8c4fbcef1533eb02f375aeb0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_SYNC_ASYNCHRONOUS</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gad1bb5cc537ea61634cb61c8c364c9a6f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_SYNC_ADAPTIVE</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gaa7808914116fe45223462e68d6f8aa5d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_SYNC_SYNCHRONOUS</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga0309c68623fb48fdc621f15dabc579f2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_USAGE_MASK</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga834006343743238a5eb1d273ef3226ae</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_USAGE_DATA</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gac852b5e62e4e577b1b43239ae190908a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_USAGE_FEEDBACK</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gabe8c89e5b624ac6bb5b9cd2919fc06be</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_USAGE_IMPLICIT_FEEDBACK</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gabfdb9f73f34907249b8fd1128c25ae1a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_USAGE_RESERVED</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga0ee1319e7242bb61298af850ea64f08a</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>void *</type>
      <name>USBD_HANDLE_T</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gafdbb2204d929cb9d75736bd2b42342ac</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>usbd_cdcuser.h</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/chip/inc/usbd/</path>
    <filename>usbd__cdcuser_8h</filename>
    <includes id="usbd_8h" name="usbd.h" local="yes" imported="no">usbd.h</includes>
    <class kind="struct">USBD_CDC_INIT_PARAM_T</class>
    <class kind="struct">USBD_CDC_API_T</class>
  </compound>
  <compound kind="file">
    <name>usbd_core.h</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/chip/inc/usbd/</path>
    <filename>usbd__core_8h</filename>
    <includes id="usbd_8h" name="usbd.h" local="yes" imported="no">usbd.h</includes>
    <class kind="struct">USB_CORE_DESCS_T</class>
    <class kind="struct">USBD_API_INIT_PARAM_T</class>
    <class kind="struct">USBD_CORE_API_T</class>
    <member kind="typedef">
      <type>ErrorCode_t(*</type>
      <name>USB_CB_T</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga0404ce046312aa5c798cc4a05c417e46</anchor>
      <arglist>)(USBD_HANDLE_T hUsb)</arglist>
    </member>
    <member kind="typedef">
      <type>ErrorCode_t(*</type>
      <name>USB_PARAM_CB_T</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga7df622c61ebb152b83dd5972ac789b28</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, uint32_t param1)</arglist>
    </member>
    <member kind="typedef">
      <type>ErrorCode_t(*</type>
      <name>USB_EP_HANDLER_T</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gaa578d29a85226108ef62c6d5c325b742</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, void *data, uint32_t event)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>usbd_dfu.h</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/chip/inc/usbd/</path>
    <filename>usbd__dfu_8h</filename>
    <includes id="usbd_8h" name="usbd.h" local="yes" imported="no">usbd.h</includes>
    <class kind="struct">_USB_DFU_FUNC_DESCRIPTOR</class>
    <class kind="struct">_DFU_STATUS</class>
    <member kind="define">
      <type>#define</type>
      <name>USB_DFU_IF_NUM</name>
      <anchorfile>usbd__dfu_8h.html</anchorfile>
      <anchor>a9c6096c21b3d4525edce753d8b8e84b6</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>usbd_dfuuser.h</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/chip/inc/usbd/</path>
    <filename>usbd__dfuuser_8h</filename>
    <includes id="usbd_8h" name="usbd.h" local="yes" imported="no">usbd.h</includes>
    <includes id="usbd__dfu_8h" name="usbd_dfu.h" local="yes" imported="no">usbd_dfu.h</includes>
    <includes id="usbd__core_8h" name="usbd_core.h" local="yes" imported="no">usbd_core.h</includes>
    <class kind="struct">USBD_DFU_INIT_PARAM_T</class>
    <class kind="struct">USBD_DFU_API_T</class>
  </compound>
  <compound kind="file">
    <name>usbd_hid.h</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/chip/inc/usbd/</path>
    <filename>usbd__hid_8h</filename>
    <includes id="usbd_8h" name="usbd.h" local="yes" imported="no">usbd.h</includes>
    <class kind="struct">_HID_DESCRIPTOR</class>
    <class kind="struct">_HID_DESCRIPTOR::_HID_DESCRIPTOR_LIST</class>
    <member kind="define">
      <type>#define</type>
      <name>HID_SUBCLASS_NONE</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>ga1f7d3e3accc103eee60cb9a9162a7c49</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_SUBCLASS_BOOT</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>ga7f90b6b8acd0bce821f59ca15625da3a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_PROTOCOL_NONE</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>ga050926404254a7fa1d6b5253a79abc59</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_PROTOCOL_KEYBOARD</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>ga059227938fbd1b45ce693d91d2856ca9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_PROTOCOL_MOUSE</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>gaeabcfc288ffa25ac6896d19e0642b104</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_HID_DESCRIPTOR_TYPE</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>ga07873aac8f5caf1ddad4748721a48951</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_REPORT_DESCRIPTOR_TYPE</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>ga8c0707cabc1c319ea980c84332958f1a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_PHYSICAL_DESCRIPTOR_TYPE</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>gaa54747908c648f185f40eef198ce9382</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_REQUEST_GET_REPORT</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>ga73dbc093cd8c096054332fde410cd9ed</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_REPORT_INPUT</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>gae73239f50524749753c8b6e1f11253b9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_USAGE_PAGE_UNDEFINED</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>ga73a32173a51a8d1e21dd14de726593d7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_USAGE_GENERIC_POINTER</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>ga641222beb63bba22954b45dd26c3f948</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_USAGE_SIMULATION_RUDDER</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>gab530d25dfff496c8f0ad9326f831ee44</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_USAGE_KEYBOARD_NOEVENT</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>ga85b93a68c76952d39c3bc1daf17df5e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_USAGE_KEYBOARD_aA</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>ga59f598363095effd3b6a9ece083ff78b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_USAGE_KEYBOARD_ONE</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>ga8170c3b8a8c895fc203a185eaa1bdccb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_USAGE_KEYBOARD_F1</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>ga9ea2479fe9a71562a02bc3788b0df998</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_USAGE_KEYBOARD_LCTRL</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>gae3b79c5b31060eabe7779b54cb164f6d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_USAGE_LED_NUM_LOCK</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>ga36ce120868adf30bbd161bb7dc158381</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_USAGE_TELEPHONY_PHONE</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>ga226152f0f3a8d5b62e6f11c079e39ba4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_USAGE_CONSUMER_CONTROL</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>ga37a3000d35ee451d1e02800487eda996</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_Input</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>ga272fc60a8ac573e1d6ed3e98d82c01ba</anchor>
      <arglist>(x)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_Data</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>ga6fdf3aabdbd36deecb675ec299911862</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_Physical</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>ga306baa2119b8f91dbd0cf307615458a2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_UsagePage</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>gad7c0e2c28b4a3d728bfa2fab4eb864db</anchor>
      <arglist>(x)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HID_Usage</name>
      <anchorfile>group__USBD__HID.html</anchorfile>
      <anchor>gae78646c72db02459eabce39de60ab5e7</anchor>
      <arglist>(x)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>usbd_hiduser.h</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/chip/inc/usbd/</path>
    <filename>usbd__hiduser_8h</filename>
    <includes id="usbd_8h" name="usbd.h" local="yes" imported="no">usbd.h</includes>
    <includes id="usbd__hid_8h" name="usbd_hid.h" local="yes" imported="no">usbd_hid.h</includes>
    <includes id="usbd__core_8h" name="usbd_core.h" local="yes" imported="no">usbd_core.h</includes>
    <class kind="struct">USB_HID_REPORT_T</class>
    <class kind="struct">USBD_HID_INIT_PARAM_T</class>
    <class kind="struct">USBD_HID_API_T</class>
  </compound>
  <compound kind="file">
    <name>usbd_hw.h</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/chip/inc/usbd/</path>
    <filename>usbd__hw_8h</filename>
    <includes id="usbd_8h" name="usbd.h" local="yes" imported="no">usbd.h</includes>
    <includes id="usbd__core_8h" name="usbd_core.h" local="yes" imported="no">usbd_core.h</includes>
    <class kind="struct">USBD_HW_API_T</class>
    <member kind="enumeration">
      <type></type>
      <name>USBD_EVENT_T</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>ga61dde6aa35d2912927ef1b185eedaa13</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_SETUP</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13a8d26afb2e56f4b1b788321a0d1633b33</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_OUT</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13ad720a106a796d05d1dd52fb0b021be67</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_IN</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13aa284007e2d06d2f9241618b55c1bcd3a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_OUT_NAK</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13a8966a8816143f41ac6aafd7215151f88</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_IN_NAK</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13af973a80c8f31a5c9aa320b15cd230434</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_OUT_STALL</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13a3e17b2dcf495ff4befaeba2718affae4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_IN_STALL</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13a1ba247a8c3cdc166098e817ec2808777</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_OUT_DMA_EOT</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13a3a3b0dc5766c4d17bd6d2daea5812fbc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_IN_DMA_EOT</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13aaddab8476ea17ac9fb4e10796bc7c633</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_OUT_DMA_NDR</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13a744297c96c9296f311a28da97df03001</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_IN_DMA_NDR</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13a6c66edac1510b9e9741a8f5019142214</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_OUT_DMA_ERR</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13ac469035908127847ebb32fd51a0a33bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_IN_DMA_ERR</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13a2e9c73676bebd4b0c4ebb412c2a53af4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_RESET</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13ac9da611df8ba920792bb1388c70fbc53</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_SOF</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13a327107e15682a2403deec540ca209670</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_DEV_STATE</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13ae30ace519ceb787d84de55ebf5e4901e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_DEV_ERROR</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13a7e46c0177f969c8be4e4a40784c7c2f4</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>usbd_msc.h</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/chip/inc/usbd/</path>
    <filename>usbd__msc_8h</filename>
    <includes id="usbd_8h" name="usbd.h" local="yes" imported="no">usbd.h</includes>
    <class kind="struct">_MSC_CBW</class>
    <class kind="struct">_MSC_CSW</class>
  </compound>
  <compound kind="file">
    <name>usbd_mscuser.h</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/chip/inc/usbd/</path>
    <filename>usbd__mscuser_8h</filename>
    <includes id="usbd_8h" name="usbd.h" local="yes" imported="no">usbd.h</includes>
    <includes id="usbd__msc_8h" name="usbd_msc.h" local="yes" imported="no">usbd_msc.h</includes>
    <includes id="usbd__core_8h" name="usbd_core.h" local="yes" imported="no">usbd_core.h</includes>
    <class kind="struct">USBD_MSC_INIT_PARAM_T</class>
    <class kind="struct">USBD_MSC_API_T</class>
  </compound>
  <compound kind="file">
    <name>usbd_rom_api.h</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/chip/inc/usbd/</path>
    <filename>usbd__rom__api_8h</filename>
    <includes id="usbd_8h" name="usbd.h" local="yes" imported="no">usbd.h</includes>
    <includes id="usbd__hw_8h" name="usbd_hw.h" local="yes" imported="no">usbd_hw.h</includes>
    <includes id="usbd__core_8h" name="usbd_core.h" local="yes" imported="no">usbd_core.h</includes>
    <includes id="usbd__mscuser_8h" name="usbd_mscuser.h" local="yes" imported="no">usbd_mscuser.h</includes>
    <includes id="usbd__dfuuser_8h" name="usbd_dfuuser.h" local="yes" imported="no">usbd_dfuuser.h</includes>
    <includes id="usbd__hiduser_8h" name="usbd_hiduser.h" local="yes" imported="no">usbd_hiduser.h</includes>
    <includes id="usbd__cdcuser_8h" name="usbd_cdcuser.h" local="yes" imported="no">usbd_cdcuser.h</includes>
    <class kind="struct">USBD_API_T</class>
  </compound>
  <compound kind="page">
    <name>CMSIS_MISRA_Exceptions</name>
    <title>MISRA-C:2004 Compliance Exceptions</title>
    <filename>CMSIS_MISRA_Exceptions</filename>
  </compound>
  <compound kind="group">
    <name>ADC_17XX_40XX</name>
    <title>CHIP:  LPC17xx/40xx A/D conversion driver</title>
    <filename>group__ADC__17XX__40XX.html</filename>
    <class kind="struct">LPC_ADC_T</class>
    <class kind="struct">ADC_CLOCK_SETUP_T</class>
    <member kind="define">
      <type>#define</type>
      <name>ADC_DR_RESULT</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>ga6e5fc80635b66a16e87c6f0eea02bb9e</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ADC_DR_DONE</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>ga43dae5912e092ae5cd2455b69b6f4b00</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ADC_DR_OVERRUN</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>ga48010d662d45810f9a240b29bbca5700</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ADC_CR_CH_SEL</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>ga3a6629b2190324c0b6abafc4b720df2e</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ADC_CR_CLKDIV</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gad2327ec652bedf37c800e077cc46d904</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ADC_CR_BURST</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gac4274c705620f3ddd5ba7f73249e6248</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ADC_CR_PDN</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>ga7474e4ab5695434acbfe8a5fcad35ef0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ADC_CR_START_MASK</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gadb696eab756362a8e2dbc5502f8bdeaf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ADC_CR_START_MODE_SEL</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>ga1af437f22df0b2b07bef704b23733874</anchor>
      <arglist>(SEL)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ADC_CR_START_NOW</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gad9f225e8d5f50609888edab56cd0da5c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ADC_CR_START_CTOUT15</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gab4000daab3c792e48cf8d3de3e5b0a36</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ADC_CR_START_CTOUT8</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>ga6be3ddfec0f4d7d9b8f56363b100ecef</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ADC_CR_START_ADCTRIG0</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gad37a3aadfc28d617b673194170831de6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ADC_CR_START_ADCTRIG1</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>ga900f9645ad799143f6db3568947bdf44</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ADC_CR_START_MCOA2</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gae86c1cc94b370b0313ea4dbc09514d2f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ADC_CR_EDGE</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>ga1d05d43234dd2d1489841dff0012225e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>ADC_STATUS_T</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gac71c41adcac9b5bf8390f57d9041e1a8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>ADC_DR_DONE_STAT</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>ggac71c41adcac9b5bf8390f57d9041e1a8a44ef9495010c41d5ee491dda81173b66</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>ADC_DR_OVERRUN_STAT</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>ggac71c41adcac9b5bf8390f57d9041e1a8ac316a82a964f0763ad2274824287dccf</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>ADC_DR_ADINT_STAT</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>ggac71c41adcac9b5bf8390f57d9041e1a8ad1bc63f19c8eaf39b4310bf308d80282</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>ADC_CHANNEL_T</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>ga120cc3135ffbba747bcdbb10f502a208</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>ADC_CH0</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gga120cc3135ffbba747bcdbb10f502a208a0db887b05fe2d35518140c53e4d3cd37</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>ADC_CH1</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gga120cc3135ffbba747bcdbb10f502a208a6642852ba79ec550f9db5aee024eafc9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>ADC_CH2</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gga120cc3135ffbba747bcdbb10f502a208ac4dcc5d123e548febb592bad090fa13a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>ADC_CH3</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gga120cc3135ffbba747bcdbb10f502a208a3955d3413cc4000e0121db7f11296c21</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>ADC_CH4</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gga120cc3135ffbba747bcdbb10f502a208adc9c466c876b5d2226155bf1ebc6e2ec</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>ADC_CH5</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gga120cc3135ffbba747bcdbb10f502a208a57679984f78b2a16619436593843da64</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>ADC_CH6</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gga120cc3135ffbba747bcdbb10f502a208aebc31ace36f08bbe5cc3ef39de3b70ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>ADC_CH7</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gga120cc3135ffbba747bcdbb10f502a208acf0c880171a962668da324da08aadcfe</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>ADC_EDGE_CFG_T</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>ga6f7bad9162b752cf270605e1cbe28c76</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>ADC_TRIGGERMODE_RISING</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gga6f7bad9162b752cf270605e1cbe28c76a394e8daefd5da77c7210bafad6c768ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>ADC_TRIGGERMODE_FALLING</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gga6f7bad9162b752cf270605e1cbe28c76ae643a0b7527bd40b08afc82bbeb233a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>ADC_START_MODE_T</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>ga08333dcd1e469efd6212d8f0e30d48f7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>ADC_START_NOW</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gga08333dcd1e469efd6212d8f0e30d48f7a0a0386df411db0ce22aeebf763bdaa21</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>ADC_START_ON_CTOUT15</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gga08333dcd1e469efd6212d8f0e30d48f7a7e106cd156664397eeaf9af0fcd9b014</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>ADC_START_ON_CTOUT8</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gga08333dcd1e469efd6212d8f0e30d48f7a20c0134132b97d003d09745bfa96bff3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>ADC_START_ON_ADCTRIG0</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gga08333dcd1e469efd6212d8f0e30d48f7a421835864f96ee615115802a08681880</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>ADC_START_ON_ADCTRIG1</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gga08333dcd1e469efd6212d8f0e30d48f7ac1792ece65ee0108a400412184fca507</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>ADC_START_ON_MCOA2</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gga08333dcd1e469efd6212d8f0e30d48f7a390c797ca8e170b65a45eb8f9669dbc9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_ADC_Init</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>ga459b17621657a66281f2bc9baae3626c</anchor>
      <arglist>(LPC_ADC_T *pADC, ADC_CLOCK_SETUP_T *ADCSetup)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_ADC_DeInit</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>ga749cff51066bd49acaca0497d78f3332</anchor>
      <arglist>(LPC_ADC_T *pADC)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_ADC_ReadValue</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gab6374a3aa75b052970c472ee2e9f600e</anchor>
      <arglist>(LPC_ADC_T *pADC, uint8_t channel, uint16_t *data)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_ADC_ReadByte</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>ga5dc774072fa55b145e57a25c1a146535</anchor>
      <arglist>(LPC_ADC_T *pADC, ADC_CHANNEL_T channel, uint8_t *data)</arglist>
    </member>
    <member kind="function">
      <type>FlagStatus</type>
      <name>Chip_ADC_ReadStatus</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>ga182ae98a23007564b3eaeb61a31ac553</anchor>
      <arglist>(LPC_ADC_T *pADC, uint8_t channel, uint32_t StatusType)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_ADC_Int_SetChannelCmd</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gac0bf9a8d016bcd88866d4ae59b1ca78c</anchor>
      <arglist>(LPC_ADC_T *pADC, uint8_t channel, FunctionalState NewState)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_ADC_Int_SetGlobalCmd</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gafa58ed3d91229dfcc78a5fc05dd4221b</anchor>
      <arglist>(LPC_ADC_T *pADC, FunctionalState NewState)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_ADC_SetStartMode</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>ga951b5b680e4d3be64c83fc6e1caf644d</anchor>
      <arglist>(LPC_ADC_T *pADC, ADC_START_MODE_T mode, ADC_EDGE_CFG_T EdgeOption)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_ADC_SetSampleRate</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gae1629ea72c61a224e69e55f7699b7810</anchor>
      <arglist>(LPC_ADC_T *pADC, ADC_CLOCK_SETUP_T *ADCSetup, uint32_t rate)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_ADC_EnableChannel</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gaf2fd510be97c2747ed3c53c132a45e15</anchor>
      <arglist>(LPC_ADC_T *pADC, ADC_CHANNEL_T channel, FunctionalState NewState)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_ADC_SetBurstCmd</name>
      <anchorfile>group__ADC__17XX__40XX.html</anchorfile>
      <anchor>gaa9890ccee17bea824b2af26e5bb2f1d6</anchor>
      <arglist>(LPC_ADC_T *pADC, FunctionalState NewState)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>CAN_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx CAN driver</title>
    <filename>group__CAN__17XX__40XX.html</filename>
    <class kind="struct">LPC_CANAF_RAM_T</class>
    <class kind="struct">LPC_CANAF_T</class>
    <class kind="struct">IP_CAN_001_CR_T</class>
    <class kind="struct">LPC_CAN_TX_T</class>
    <class kind="struct">IP_CAN_001_RX_T</class>
    <class kind="struct">LPC_CAN_T</class>
    <class kind="struct">CAN_MSG_T</class>
    <class kind="struct">IP_CAN_BUS_TIMING_T</class>
    <class kind="struct">CAN_STD_ID_ENTRY_T</class>
    <class kind="struct">CAN_STD_ID_RANGE_ENTRY_T</class>
    <class kind="struct">CAN_EXT_ID_ENTRY_T</class>
    <class kind="struct">CAN_EXT_ID_RANGE_ENTRY_T</class>
    <class kind="struct">CANAF_LUT_T</class>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_RAM_ENTRY_NUM</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga79546dad61d5645789e339371e9f491f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_MOD_BITMASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gadde236ce652b01b5854592cd4b40808e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_MOD_OPERATION</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gac47f17c3134b87bbabc990411f21049d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_MOD_RM</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gafd10049f6c1d6a63bfb481578d97de16</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_MOD_LOM</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga4242a50d3797d3ea05304d81eb8fd891</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_MOD_STM</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga0131118db0e68b5635a10777cbd794e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_MOD_TPM</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gad8795757b9206a4daee0f5a40517385f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_MOD_SM</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gab0cd0939676d3c6ce846e47cc8cc2b15</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_MOD_RPM</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaec973a15597675e749f4033835dd66f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_MOD_TM</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga0fc0c3414f4f0e4166e90e271631d89b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_CMR_BITMASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga5e4f3e4201311f127d98fff1b8eb2d3f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_CMR_TR</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga4dc9265838852815dbea8a977cfa5331</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_CMR_AT</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga2c62ebf606fecb5f3a891642f5bc25d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_CMR_RRB</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaca5b99941b4a44bf4fc0233cd740db2c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_CMR_CDO</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga867ee74878ca3d86ec6d0cd2e25e15ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_CMR_SRR</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gadf85238951b64fd26cc0efcb0e8508ec</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_CMR_STB</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga68fe7e21ef84fbef9e996e3df8c4cc9d</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_GSR_BITMASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga35466902944ebf9952a1c4fc53a96561</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_GSR_RBS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gacde523e4e3bdc91292d6fca05f3cf744</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_GSR_DOS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga2812bef760e324f711bdf9f5ec6080db</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_GSR_TBS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga06334090a05005aac523bb3f7273de6f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_GSR_TCS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gac8f1a706b4a31638bf92c8cfa6b191a2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_GSR_RS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga5baf53192002d55ec53fd5c679222d93</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_GSR_TS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga204f003cdb1c178fa4822fd690bd75b8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_GSR_ES</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga2e57210b60bc08effc08aa5362978cdc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_GSR_BS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaa304ce39b130166e51304ded48d0c969</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_GSR_RXERR</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga6c1dfb3c4e775f46effbff095b230673</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_GSR_TXERR</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaf6a01e0337eac8147f58b709c09ad347</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_BITMASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga116eeeca0b00f6afde8667f5095e0c73</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_RI</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga7d64f1d188b6121983be38f7d688c6d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_TI1</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga4a568e35c08d16197f943bcdaf48b4de</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_EI</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gab04d96ef59661fd0d47ff05ddc3397d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_DOI</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga9fae105cbd02053ab9bd369c473b859c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_WUI</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaf0e24df45c2cc58b8b05c8475c0dbc0f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_EPI</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga81258499c237327be7203edaa80adcd0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ALI</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga1807f526e7aed02db75fd75435cd3d44</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_BEI</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga667b60ec168d7c17a932e562f45f6a76</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_IDI</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gab57b8c6d33827a856266f8340052ce84</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_TI2</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gad43c387e2fbc22cc95c3b3803e5e890f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_TI3</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga82d809099125ff45ae2f404eea9113a0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ERRBIT_VAL</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga2688506c7491fa966711cce9ab57c430</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ERR_SOF</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga43d081dee709043e9b2db1830b244efe</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ERR_ID28_ID21</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gabdfc90bd57b293b818a3ede8d682a69c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ERR_ID20_ID18</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga53bcd653d1338f6c146920d1d149d840</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ERR_SRTR</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga19d41d01d80224aac902c69f5331c733</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ERR_IDE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga2267eddf693f953aa6069d8c47b16325</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ERR_ID17_ID13</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga9e0f491e1db70190987c57c521011267</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ERR_ID12_ID5</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gac6abf275f88e7fbe796e35614280537d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ERR_ID4_ID0</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga0f52fa1bf8a31f19e4085bc35b5164cd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ERR_RTR</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gabe25510882df75fa40045dd344b864e9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ERR_ReservedBit_1</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga6d82f14b8693f6322835ed728975a95c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ERR_ReservedBit_0</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga01ab782d97f7c4efcad005c2508dae78</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ERR_DLC</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga1b69439e74c370c75838c407b277e5c5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ERR_DATA_FIELD</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga6e4f045223b2f154cb449a95b5c19d2d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ERR_CRC_SEQ</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga796e6b5aaf64f404e0d6960ce0dc2945</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ERR_CRC_DELIMITER</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaa6defe8882ac6fb2e0048afa6bec7205</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ERR_ACK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gadbec8cb293c961db6ba27da0fdd6a30c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ERR_ACK_DELIMITER</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaa073aa014b3274b624a476395bb3865a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ERR_EOF</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga800e0bc49882ff1cfa4043087a8ff76c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ERR_INTERMISSION</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gafd3bd849f9878685f2493810f74828ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ERRDIR_RECEIVE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga8c2502b4b9de05c032f1f9e30dbce585</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ERRC_VAL</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaf1e12de60b5088129364cf16e293e62d</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_ICR_ALCBIT_VAL</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga573d99b47e23224ccaa36e473439018c</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_IER_BITMASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga97b59b8ddd85cfd43b01fca0c9ec5a9a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_IER_RIE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga25db0b070c10a960394a51c1374c5850</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_IER_TIE1</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga8acb357ce8234e896c4f4cd550beb2c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_IER_EIE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga7c5a45cabe99423ecbbd05df116dad20</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_IER_DOIE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga0040248415efe4811e416b4265873a70</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_IER_WUIE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gad9eff97f356d550aa52f0545e3c459d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_IER_EPIE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gae01dedfd23e22e9fb22466c4a8ef7b9d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_IER_ALIE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga71c94a127f0774689febf52d4e08f974</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_IER_BEIE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga9f2bbfc38e14781668ae928252b3639d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_IER_IDIE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gac81c8d3f2c4417b30af2236ae66c4926</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_IER_TIE2</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga8168f7849c90eae9dae59835aac5fef8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_IER_TIE3</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga4d555856f1c80fe21c7fbd0858ec2118</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_BTR_BITMASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga4e724f70048e25e40f190539157751e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_BTR_BRP</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga5155a1064a96ca3936fc54714aa6278d</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_BTR_SJW</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaa4865b8598369cf16361cce7036f9a4f</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_BTR_TESG1</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaae8fdfac247c2cf1eb9e37916aa82958</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_BTR_TESG2</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga222446d7f4fb6f0e57ce58c0830033cc</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_BTR_SAM</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga91d6a3f10153591c0e9b0f75b0020db7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_EWL_BITMASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga1af81af9f590526d0f245373424d60d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_EWL_VAL</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga22b02cbd9a63e5074b38f32446aa0f98</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_SR_BITMASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga747cd5c630eed3f8fa7732ad24c958c5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_SR_RBS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaf881e3b6ba93db020ad79d3018a81b83</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_SR_DOS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga5c73dbfb4b2929e575834bab962f11f3</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_SR_TBS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga19e3372539198200b71d6bbcb7e6deb2</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_SR_TCS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gae3bc0c43b2adf2ffc5d960f90a1f1dbd</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_SR_RS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gab00fb9a32de5b423d0734159509a4ce5</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_SR_TS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gad6568a71936b23a42148b146fa109540</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_SR_ES</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga59159553852897005328a5e48a3c6b4c</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_SR_BS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gab9f8ecb4eefb0b33ba518bcaa911c611</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_RFS_BITMASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaae5a748ec50af6827ba0536db45e48e4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_RFS_ID_INDEX</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gac392aec22ad3737b7750bb77e173fb72</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_RFS_BP</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga3ce764619664805bd640def59318426b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_RFS_DLC</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga972eeafea01ef8b32a2209a526f07f27</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_RFS_RTR</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gac276a6ce0a89fe58afd91907438022ca</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_RFS_FF</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga02abf35a3c80ac7be66fa94945d48f56</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_RID_ID_11</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaaabfc7706456825e5f99e9a14765b86d</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_RID_ID_29</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gab542e5da2c707aecd09253a08fde6f99</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_TFI_BITMASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga88986a2ea8f1ed5ff9abdfb2ae6a1f72</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_TFI_PRIO</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gab8d03e1a3f1a9a58ed40cfa8e7f50bed</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_TFI_DLC</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga19e3a3cc002c6ede36b35751fde91b37</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_TFI_RTR</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga40db542a8b3172164d0b7a31d00d1381</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_TFI_FF</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga892da7cfaf3607c61b5df2463919e26d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_TID_ID11</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga6cf98efc2f108576def65afb23845531</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_TID_ID29</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gac0578426f86e4cf94d1e25200dff29c0</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_TSR_BITMASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga2865a694212165a2907851f844316b57</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_TSR_TS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaa0a76b36b4ff5bec2c9c6fc318ce8ffb</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_TSR_TBS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga803043142caf6420aacd8feb177e138e</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_TSR_TCS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaab4aed263f15a694f95886927201519d</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_RSR_BITMASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga0c56e3e5f89db118cd719437f74e085e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_RSR_RS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gab0a7682692ff7af9765aa19d494c431c</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_RSR_RBS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga993319214e1f4374346aaee97ae9eb8c</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_RSR_DOS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga734b769abd20988bff348ddf03214fbe</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_MSR_BITMASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga6f49f63724cbd8724eb783eb7b6d2074</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_MSR_E</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga132c1afad5e0dae9c6bef033b677af4d</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_MSR_BS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gae40d344745e67d3277ba1f2f0219707a</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_AFMR_OPERATION</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga76c064827ea8236b6a195525ffeff007</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_AFMR_ACCOFF</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga1746504019bacce82b5018f18dc7e710</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_AFMR_ACCBP</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga55f1fb64b69f8341d479e5a8afe6ce58</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_AFMR_EFCAN</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga88269b6aec8e17003920d83d5454578d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_ENDADDR</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga57532f0dce6ac43f32e65222b9ae309b</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_LUTERRAD</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga2a94ef7b565935f3f23238eb12d7ffc3</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_LUTERR</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaed3dccb13b13c3f28089fcdd6a002869</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_FCANIE_BITMASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gacdb9df29363290a810f83bac6a4c449b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_FCANIE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaa88dea898eb0dc46abbe7257d73c3d55</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_FULLCAN_MSG_FF_POS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga3fc8e9c8da63067c48ef7c7789980f45</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_FULLCAN_MSG_RTR_POS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga99e21de5676bd992bdcd022656ad01cb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_FULLCAN_MSG_LOST_POS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaf748e11cd9a7e573528d510b3b1f794f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_FULLCAN_MSG_SEM_POS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gad3f88ccba76ce1843fa9f20af3650c4a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_FULLCAN_MSG_SEM_BITMASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga60ab049509af08d3715fb5c3a11f1b59</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_FULLCAN_MSG_DLC_POS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gac926e34803050930adc9784f6f6432c3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_FULLCAN_MSG_DLC_BITMASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaf1d8388c9f6b6a15e1d338eb1dd03114</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_FULLCAN_MSG_SCC_POS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga19bc4e1bae73c2b3db1144c083a3fe67</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_FULLCAN_MSG_SCC_BITMASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaae4ad1be2ac77398fe5202385f6706a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_FULLCAN_MSG_ID11_POS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga6e699ec924f45f2df91b521e21405c46</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_FULLCAN_MSG_ID11_BITMASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga3b7e29cacfe97518e0947fa70cd2073e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_FULCAN_MSG_AF_UPDATING</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga7faed980dda1c2c2425f2c135a13ded0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_FULCAN_MSG_AF_FINISHED</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga0703ff937d013317ff346514af620c01</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_FULCAN_MSG_CPU_READING</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gabd6b90de26c04aa196f9fa61da8b5fd0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CANAF_FCAN_IC_INTPND</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga5a6c170c29326278a807f70d94e48bcb</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_STD_ENTRY_CTRL_NO_POS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga61ae528adf71eb852cbd646df9dfa702</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_STD_ENTRY_CTRL_NO_MASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga349506f245dce5ac65e33b622053916c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_STD_ENTRY_DISABLE_POS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga222c235d9fc30777d2380f5fcc843c8a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_STD_ENTRY_DISABLE_MASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga8c813fa1c0a9066aeacd15a7d28c4049</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_STD_ENTRY_IE_POS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga3f2146c4a801f8ec202db8670a808ca3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_STD_ENTRY_IE_MASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gac71896bdcee29b8bd5558220ba13088c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_STD_ENTRY_ID_POS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga97b02bf9ac5d417eb24c951c8838257c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_STD_ENTRY_ID_MASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga29ad79d718c09251fa620d0eb979dd83</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_EXT_ENTRY_CTRL_NO_POS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaf77918623507089bfa66ef2ab9d54c1b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_EXT_ENTRY_CTRL_NO_MASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gae9329bb3412435b68b922622466a5f87</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_EXT_ENTRY_ID_POS</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga31e5396a3ff068f0c090852f176109f7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_EXT_ENTRY_ID_MASK</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaffc8d5b181f437f429a86430ea0a9942</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_REMOTE_MSG</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga1b77aaba2a11dc258857a633277b7b9d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_EXTEND_ID_USAGE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga51d10a0931e925cff94c86d2e2237ddc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CAN_MSG_MAX_DATA_LEN</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gafef54df2cd501083697e84c64614ffca</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>CANAF_RAM_SECTION_T</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaf0ae33e71a7acfc561972811aa1207e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CANAF_RAM_FULLCAN_SEC</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ggaf0ae33e71a7acfc561972811aa1207e8abe9837dc35bd06778ce14557fb60c0d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CANAF_RAM_SFF_SEC</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ggaf0ae33e71a7acfc561972811aa1207e8a3fbfc3b49a65a73278b5fe4a392c38f7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CANAF_RAM_SFF_GRP_SEC</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ggaf0ae33e71a7acfc561972811aa1207e8a6f0abb002e71440f460a50f5d9a67af7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CANAF_RAM_EFF_SEC</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ggaf0ae33e71a7acfc561972811aa1207e8a1d1b4c4406c8913e08d9edba297f9d34</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CANAF_RAM_EFF_GRP_SEC</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ggaf0ae33e71a7acfc561972811aa1207e8a3a869c6db7177da7b55a903b051775c1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>CAN_BUFFER_ID_T</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga073d4792bdf9dd0d8ceda6b8c3752dd9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CAN_BUFFER_1</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gga073d4792bdf9dd0d8ceda6b8c3752dd9a4676dbb752485410e03d6b995983faf9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CAN_BUFFER_2</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gga073d4792bdf9dd0d8ceda6b8c3752dd9a23a79343fe82a5c1b6b58d6c5cd5a656</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CAN_BUFFER_3</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gga073d4792bdf9dd0d8ceda6b8c3752dd9a88d946b80f354f8d7dc216c017b9d1ce</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CAN_BUFFER_LAST</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gga073d4792bdf9dd0d8ceda6b8c3752dd9a636fb6eb431d35079351e9ac16b3b8a8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>CAN_MODE_T</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga57c03b9f9b4ebf86dccc13baf8c2889c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CAN_RESET_MODE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gga57c03b9f9b4ebf86dccc13baf8c2889ca398cd9777634ece5fcbab4df3cf3a526</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CAN_SELFTEST_MODE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gga57c03b9f9b4ebf86dccc13baf8c2889cabdca353874fc8859b6d8d84fc8650b51</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CAN_TEST_MODE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gga57c03b9f9b4ebf86dccc13baf8c2889ca7a621122f70de0ed611cee6827ad05c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CAN_LISTEN_ONLY_MODE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gga57c03b9f9b4ebf86dccc13baf8c2889ca26235b6e55aa176156126830508fcf77</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CAN_SLEEP_MODE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gga57c03b9f9b4ebf86dccc13baf8c2889cad4b949fba84b06206011287e25a69480</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CAN_OPERATION_MODE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gga57c03b9f9b4ebf86dccc13baf8c2889ca48f5ffda42c1bd17d9fcb26900b1ced6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CAN_TRANSMIT_PRIORITY_MODE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gga57c03b9f9b4ebf86dccc13baf8c2889cab20dd6c22d6383a3957083cd10d9b80d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CAN_RECEIVE_PRIORITY_MODE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gga57c03b9f9b4ebf86dccc13baf8c2889ca962f843f0fbf8d8d8a79b12727742e08</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>CAN_AF_MODE_T</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaf9abd01ef9ba38a2113a240871e9b48a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CAN_AF_NORMAL_MODE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ggaf9abd01ef9ba38a2113a240871e9b48aa62b523726ff5237efa0e7fad638a5f04</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CAN_AF_OFF_MODE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ggaf9abd01ef9ba38a2113a240871e9b48aaecb73e4da7841519d8e59677d44e47cd</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CAN_AF_BYBASS_MODE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ggaf9abd01ef9ba38a2113a240871e9b48aa388c6b86ae9a41adbcdfb801401a9854</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CAN_AF_FULL_MODE</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ggaf9abd01ef9ba38a2113a240871e9b48aaaf64e87c0a985f362b46284932e44b45</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_CAN_SetCmd</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga204f7aa9af769ea809d9dc54ef4408a9</anchor>
      <arglist>(LPC_CAN_T *pCAN, uint32_t command)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_CAN_SetEWL</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga70f13e4a2f4cd74d56e14b0c6343c8df</anchor>
      <arglist>(LPC_CAN_T *pCAN, uint32_t ewl)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint8_t</type>
      <name>Chip_CAN_GetEWL</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga7a4f26336007dc7ebba7163ed449e384</anchor>
      <arglist>(LPC_CAN_T *pCAN)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_CAN_GetGlobalStatus</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga524163e227eb47b1ddae20b2abc6d588</anchor>
      <arglist>(LPC_CAN_T *pCAN)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_CAN_GetStatus</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaa7142c33df0796bd194ba44629923b6d</anchor>
      <arglist>(LPC_CAN_T *pCAN)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_CAN_EnableInt</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga9b5dfccc60fec9c7da50ea7ee3515a63</anchor>
      <arglist>(LPC_CAN_T *pCAN, uint32_t IntMask)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_CAN_DisableInt</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga5dcf82fd6e7df4b699e32249eb3021dd</anchor>
      <arglist>(LPC_CAN_T *pCAN, uint32_t IntMask)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_CAN_GetIntStatus</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga40be5e20a794a039df4465b164d50f74</anchor>
      <arglist>(LPC_CAN_T *pCAN)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_CAN_ConfigFullCANInt</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga90ebd27acbebb24951b5707abca13220</anchor>
      <arglist>(LPC_CANAF_T *pCANAF, FunctionalState NewState)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_CAN_GetFullCANIntStatus</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaf955e0441bf4ca24b8c72f59aabbd9e8</anchor>
      <arglist>(LPC_CANAF_T *pCANAF, uint8_t ObjID)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_CAN_SetMode</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gad935e9c3c93278bb18b99df67ba52516</anchor>
      <arglist>(LPC_CAN_T *pCAN, CAN_MODE_T Mode, FunctionalState NewState)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE CAN_MODE_T</type>
      <name>Chip_CAN_GetMode</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaee6c1afaca19ff9c9ad2ee1bab77a713</anchor>
      <arglist>(LPC_CAN_T *pCAN)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_CAN_SetAFMode</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaea7126f0eb3c4195137d5158415410a2</anchor>
      <arglist>(LPC_CANAF_T *pCANAF, CAN_AF_MODE_T AfMode)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE CAN_AF_MODE_T</type>
      <name>Chip_CAN_GetAFMode</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga7f33c4f2edcfc126ad4d55ac86b2ad23</anchor>
      <arglist>(LPC_CANAF_T *pCanAF)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_CAN_SetAFLUT</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaa58803ce4de8c8520925ebd1cbfd2019</anchor>
      <arglist>(LPC_CANAF_T *pCANAF, LPC_CANAF_RAM_T *pCANAFRam, CANAF_LUT_T *pAFSections)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_CAN_InsertFullCANEntry</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga3666d1f79e2dcbf0f041205f27494bdf</anchor>
      <arglist>(LPC_CANAF_T *pCANAF, LPC_CANAF_RAM_T *pCANAFRam, CAN_STD_ID_ENTRY_T *pEntry)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_CAN_InsertSTDEntry</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga164daa2297bee23837a93bbea3793531</anchor>
      <arglist>(LPC_CANAF_T *pCANAF, LPC_CANAF_RAM_T *pCANAFRam, CAN_STD_ID_ENTRY_T *pEntry)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_CAN_InsertGroupSTDEntry</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gaf418fa9333574ccb4e710982f90b650c</anchor>
      <arglist>(LPC_CANAF_T *pCANAF, LPC_CANAF_RAM_T *pCANAFRam, CAN_STD_ID_RANGE_ENTRY_T *pEntry)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_CAN_InsertEXTEntry</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gad6ce5875d9ca861b291a1fc2d91ba026</anchor>
      <arglist>(LPC_CANAF_T *pCANAF, LPC_CANAF_RAM_T *pCANAFRam, CAN_EXT_ID_ENTRY_T *pEntry)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_CAN_InsertGroupEXTEntry</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga6bb7c9685df871a1d6dae14908c70397</anchor>
      <arglist>(LPC_CANAF_T *pCANAF, LPC_CANAF_RAM_T *pCANAFRam, CAN_EXT_ID_RANGE_ENTRY_T *pEntry)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_CAN_RemoveFullCANEntry</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga8d00f1ed0727ec6812ce72d4a194519f</anchor>
      <arglist>(LPC_CANAF_T *pCANAF, LPC_CANAF_RAM_T *pCANAFRam, int16_t Position)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_CAN_RemoveSTDEntry</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gac991bc7a590c7069e4149a0b8ccfcb55</anchor>
      <arglist>(LPC_CANAF_T *pCANAF, LPC_CANAF_RAM_T *pCANAFRam, int16_t Position)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_CAN_RemoveGroupSTDEntry</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga090e4a29bbaebc8de77bc929438dcfb7</anchor>
      <arglist>(LPC_CANAF_T *pCANAF, LPC_CANAF_RAM_T *pCANAFRam, int16_t Position)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_CAN_RemoveEXTEntry</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga01e3a9942d50effd9da2a773d3af943f</anchor>
      <arglist>(LPC_CANAF_T *pCANAF, LPC_CANAF_RAM_T *pCANAFRam, int16_t Position)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_CAN_RemoveGroupEXTEntry</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gafa0a59577a1c1d38c4c6a339d018c35d</anchor>
      <arglist>(LPC_CANAF_T *pCANAF, LPC_CANAF_RAM_T *pCANAFRam, int16_t Position)</arglist>
    </member>
    <member kind="function">
      <type>uint16_t</type>
      <name>Chip_CAN_GetEntriesNum</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gabf7932e8e5dfd4b8efcd17b1900c53b6</anchor>
      <arglist>(LPC_CANAF_T *pCANAF, LPC_CANAF_RAM_T *pCANAFRam, CANAF_RAM_SECTION_T SectionID)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_CAN_ReadFullCANEntry</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gac54b6ced8b370416a071912bd08a0091</anchor>
      <arglist>(LPC_CANAF_T *pCANAF, LPC_CANAF_RAM_T *pCANAFRam, uint16_t Position, CAN_STD_ID_ENTRY_T *pEntry)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_CAN_ReadSTDEntry</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga0594319bfc5d033b7500a4ca706dc11a</anchor>
      <arglist>(LPC_CANAF_T *pCANAF, LPC_CANAF_RAM_T *pCANAFRam, uint16_t Position, CAN_STD_ID_ENTRY_T *pEntry)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_CAN_ReadGroupSTDEntry</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga1d8da3b3270e092f42dbe9e61f025b06</anchor>
      <arglist>(LPC_CANAF_T *pCANAF, LPC_CANAF_RAM_T *pCANAFRam, uint16_t Position, CAN_STD_ID_RANGE_ENTRY_T *pEntry)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_CAN_ReadEXTEntry</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gad8bbd206134607402721450d085e3ccf</anchor>
      <arglist>(LPC_CANAF_T *pCANAF, LPC_CANAF_RAM_T *pCANAFRam, uint16_t Position, CAN_EXT_ID_ENTRY_T *pEntry)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_CAN_ReadGroupEXTEntry</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga230f718177c73eab776ae534906b5c9a</anchor>
      <arglist>(LPC_CANAF_T *pCANAF, LPC_CANAF_RAM_T *pCANAFRam, uint16_t Position, CAN_EXT_ID_RANGE_ENTRY_T *pEntry)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_CAN_Init</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga4995b9302885e0e577d4566d23c2ce37</anchor>
      <arglist>(LPC_CAN_T *pCAN, LPC_CANAF_T *pCANAF, LPC_CANAF_RAM_T *pCANAFRam)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_CAN_DeInit</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga7f490bb9d12b30243264cd06feb795db</anchor>
      <arglist>(LPC_CAN_T *pCAN)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_CAN_SetBitRate</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga269c61b9e26af6f207632993274b04e5</anchor>
      <arglist>(LPC_CAN_T *pCAN, uint32_t BitRate)</arglist>
    </member>
    <member kind="function">
      <type>CAN_BUFFER_ID_T</type>
      <name>Chip_CAN_GetFreeTxBuf</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gadb33450a707ebe03be084c2783017272</anchor>
      <arglist>(LPC_CAN_T *pCAN)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_CAN_Send</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga27767c0aa2e0c5da0e680e7e432bbc69</anchor>
      <arglist>(LPC_CAN_T *pCAN, CAN_BUFFER_ID_T TxBufID, CAN_MSG_T *pMsg)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_CAN_Receive</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>gadbb9fadb00c42f7c33f0ed06876a8a03</anchor>
      <arglist>(LPC_CAN_T *pCAN, CAN_MSG_T *pMsg)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_CAN_FullCANReceive</name>
      <anchorfile>group__CAN__17XX__40XX.html</anchorfile>
      <anchor>ga4029c0d9ce2f898bf911e0e49b570f5b</anchor>
      <arglist>(LPC_CANAF_T *pCANAF, LPC_CANAF_RAM_T *pCANAFRam, uint8_t ObjID, CAN_MSG_T *pMsg, uint8_t *pSCC)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>SUPPORT_17XX_40XX_FUNC</name>
    <title>CHIP: LPC17xx/40xx support functions</title>
    <filename>group__SUPPORT__17XX__40XX__FUNC.html</filename>
    <member kind="function">
      <type>void</type>
      <name>SystemCoreClockUpdate</name>
      <anchorfile>group__SUPPORT__17XX__40XX__FUNC.html</anchorfile>
      <anchor>gae0c36a9591fe6e9c45ecb21a794f0f0f</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_SystemInit</name>
      <anchorfile>group__SUPPORT__17XX__40XX__FUNC.html</anchorfile>
      <anchor>ga3450fa020f6b569cc2deb69c11e11b7c</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_USB_Init</name>
      <anchorfile>group__SUPPORT__17XX__40XX__FUNC.html</anchorfile>
      <anchor>gaa098023402e8e6034f34663ce39d9ccd</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_SetupXtalClocking</name>
      <anchorfile>group__SUPPORT__17XX__40XX__FUNC.html</anchorfile>
      <anchor>ga18737e4a022570724c77c5cdea9c0258</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_SetupIrcClocking</name>
      <anchorfile>group__SUPPORT__17XX__40XX__FUNC.html</anchorfile>
      <anchor>ga6eff97a8da15798119eada6c5f000404</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>SystemCoreClock</name>
      <anchorfile>group__SUPPORT__17XX__40XX__FUNC.html</anchorfile>
      <anchor>gaa3cd3e43291e81e795d642b79b6088e6</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>PERIPH_175X_6X_BASE</name>
    <title>CHIP: LPC175x/6x Peripheral addresses and register set declarations</title>
    <filename>group__PERIPH__175X__6X__BASE.html</filename>
  </compound>
  <compound kind="group">
    <name>PERIPH_177X_8X_BASE</name>
    <title>CHIP: LPC177x/8x Peripheral addresses and register set declarations</title>
    <filename>group__PERIPH__177X__8X__BASE.html</filename>
  </compound>
  <compound kind="group">
    <name>PERIPH_407X_8X_BASE</name>
    <title>CHIP: LPC407x/8x Peripheral addresses and register set declarations</title>
    <filename>group__PERIPH__407X__8X__BASE.html</filename>
  </compound>
  <compound kind="group">
    <name>CLOCK_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx Clock Driver</title>
    <filename>group__CLOCK__17XX__40XX.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>SYSCTL_OSCRANGE_15_25</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gaf04369ab801acde194d9ffa65437364b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SYSCTL_OSCEC</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga590cc793271161c9b49e27345956b527</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SYSCTL_OSCSTAT</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gae0252af1bba39952df3137fc471ae395</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SYSCTL_PLL_ENABLE</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga78f6b62e70d59854ada7a72d6a01893d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SYSCTL_PLLSTS_ENABLED</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga9cbb95ab3333cf966d3adcd375fb2759</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SYSCTL_PLLSTS_LOCKED</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga83355c269f77d2fbc095a9f7be29b128</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>CHIP_SYSCTL_CLOCK_T</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga9b5dbe058e6fe8dc20c806e1067e902f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_LCD</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fa674049386c6b9e2abaec3be16d269edc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_TIMER0</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fa56ba4c9be10a5ad04802118bcfc93462</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_TIMER1</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fa8c317a092452670b72d96b7541054481</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_UART0</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fadbdc5e66061c50a150c2e83f34937d35</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_UART1</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fac74cbe5e9b1b6843d4d2ccf1683c823b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_PWM0</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fab7a975f0650a55512d609c6105ddb5a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_PWM1</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fad2c05edf717b8e25d5818ef40e6f6b7e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_I2C0</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fad2b351e81e7d26de1380a45fd54fba34</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_UART4</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fac71d21170ee435409de6b2db27e8a9c7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_RTC</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fa584cca9c1a5b27c2644d5c35e45968e9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_SSP1</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fa43b7f1867997b2ac597faf58b498709b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_EMC</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902faed88ac3ef7c5427178494034a8b51866</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_ADC</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fac4fd0cef82c82f7fc5983d88ae5b1634</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_CAN1</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fa3b2121b4604dda5ba85332c6f855192c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_CAN2</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fa87fcb2dcc94fa2a6532820e016d19d72</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_GPIO</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fa38b4384a5ad94c76d25aa81dd97e2d52</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_SPIFI</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902faa8abb0f86c39bf3c59b7fc09387374f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_MCPWM</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fa24c6fa5a8ddc63ef563ade2ad3ffdbab</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_QEI</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fac08c68359b2ad88c0e76b7044e75166a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_I2C1</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fa049f8753b4621c998b51e56d55d4e5c4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_SSP2</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902faf43b8d96372c74b0b80e96e42add4b49</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_SSP0</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902faa89c9c84ce5480d449a1c02c9fe3e970</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_TIMER2</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fa90c809bf197e7ebb605891a01a541141</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_TIMER3</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fabfb3c4bdb298080f7a1a054d37d44404</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_UART2</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fa4cc9e0a3d0bc512079f7b78778afc3d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_UART3</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902faafa2fc465c99af3c97c72989094b39ce</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_I2C2</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902faf818e9967f8246970e2bcadef157da38</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_I2S</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fa885559e474e10e471f8af9ce731dd295</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_SDC</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fa927e0c9c2e98efe3dcfda56209c8560a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_GPDMA</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fa195ba2cfb4c74845e67c57ff9aa7731e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_ENET</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fabfe552c78fb92a868d403e54989720e4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLOCK_USB</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga9b5dbe058e6fe8dc20c806e1067e902fa8f762d4d116ca94819a7b12a93902766</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>CHIP_SYSCTL_EMC_DIV_T</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gab43c51bf1fc2f88126d1ebb31bdb2719</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>CHIP_SYSCTL_CCLKSRC_T</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gaa78b6a06b5caef787228aedf2751568f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CCLKSRC_SYSCLK</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ggaa78b6a06b5caef787228aedf2751568faac9d326b25dd6dadba459ffdcfe0679a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CCLKSRC_MAINPLL</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ggaa78b6a06b5caef787228aedf2751568fac78f32749b26834cfa6eca8b321eb5c4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>CHIP_SYSCTL_USBCLKSRC_T</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga6116ed8cf14d985f3fa79829a7505026</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_USBCLKSRC_SYSCLK</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga6116ed8cf14d985f3fa79829a7505026a3ebbbf99b1deb65f6b624149d14c1beb</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_USBCLKSRC_MAINPLL</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga6116ed8cf14d985f3fa79829a7505026aeab3df3138cbe305eb987768d360f45b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_USBCLKSRC_USBPLL</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga6116ed8cf14d985f3fa79829a7505026a4bbad6784c46e0e17846393115601319</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>CHIP_SYSCTL_PLLCLKSRC_T</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gad62d0d49b3bbe1a99c92a4edf29e5c30</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_PLLCLKSRC_IRC</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ggad62d0d49b3bbe1a99c92a4edf29e5c30a37d972735f72955a65f80ab847e79b01</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_PLLCLKSRC_MAINOSC</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ggad62d0d49b3bbe1a99c92a4edf29e5c30af9e797188ff50eb46eff50659e4dbe5b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>CHIP_SYSCTL_SPIFICLKSRC_T</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gad500579caf3496d23c87cc434db1cef4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_SPIFICLKSRC_SYSCLK</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ggad500579caf3496d23c87cc434db1cef4a155f5aa32ff58a7077d767af8c5b33da</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_SPIFICLKSRC_MAINPLL</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ggad500579caf3496d23c87cc434db1cef4a2dc694ceea01067f81967f3bc0f11826</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_SPIFICLKSRC_USBPLL</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ggad500579caf3496d23c87cc434db1cef4a3a39bb75fbecf8fb81dc696c247511fa</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>CHIP_SYSCTL_CLKOUTSRC_T</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga157c4adb8f619a3aaf58dacca66a9292</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLKOUTSRC_CPU</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga157c4adb8f619a3aaf58dacca66a9292a32bae97d5425bfbadf2a694c17ee58e3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLKOUTSRC_MAINOSC</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga157c4adb8f619a3aaf58dacca66a9292a03ace6cf39b89cd24da3b4e5b6dc98ec</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLKOUTSRC_IRC</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga157c4adb8f619a3aaf58dacca66a9292aab6063885c9c1ed6bba5e124c41bd632</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLKOUTSRC_USB</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga157c4adb8f619a3aaf58dacca66a9292a6a2451b3a2bf139576123b5c739c710b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLKOUTSRC_RTC</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga157c4adb8f619a3aaf58dacca66a9292a11372deae97553a10a9304034bb987cc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLKOUTSRC_SPIFI</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga157c4adb8f619a3aaf58dacca66a9292ae5bd27ebb2ad906e08227184842503e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_CLKOUTSRC_WATCHDOGOSC</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gga157c4adb8f619a3aaf58dacca66a9292aea495475cf6b307768e012e9483b5de7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_Clock_EnablePLL</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga2f3b19dee294433937973fb4fedfe3f6</anchor>
      <arglist>(CHIP_SYSCTL_PLL_T PLLNum, uint32_t flags)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_Clock_DisablePLL</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga3b9589b6a14d39653503be3693990f8f</anchor>
      <arglist>(CHIP_SYSCTL_PLL_T PLLNum, uint32_t flags)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_Clock_SetupPLL</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gaf4a2dd4c6aae5d6b830f7cf529ec8773</anchor>
      <arglist>(CHIP_SYSCTL_PLL_T PLLNum, uint32_t msel, uint32_t psel)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_Clock_GetPLLStatus</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga30a134e114a19fdf625326f8258e4652</anchor>
      <arglist>(CHIP_SYSCTL_PLL_T PLLNum)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE bool</type>
      <name>Chip_Clock_IsMainPLLEnabled</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga63e9e5918d1a8b0d1c5811c3f0189846</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE bool</type>
      <name>Chip_Clock_IsUSBPLLEnabled</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga0915951815b5bd5e32b7220784a96032</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE bool</type>
      <name>Chip_Clock_IsMainPLLLocked</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gaaa21bfa511cd25dc4f4394709dc58ced</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE bool</type>
      <name>Chip_Clock_IsUSBPLLLocked</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gae0a614530296b96ef560bd2986277c3e</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_Clock_EnableCrystal</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga970e95c74fe056b7d846004c90a58012</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE bool</type>
      <name>Chip_Clock_IsCrystalEnabled</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gaa6cb29b84e27240e44c6ab89775bac97</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_Clock_SetCrystalRangeHi</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga6887346857de2ee7ec4d0cbc0b8d8396</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_Clock_SetCrystalRangeLo</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga9d93c4a5c3330839f8813d7d5bb55fd8</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_Clock_FeedPLL</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga4d4cc965838cf2c5cddafd07aa66a790</anchor>
      <arglist>(CHIP_SYSCTL_PLL_T PLLNum)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_Clock_EnablePeriphClock</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gac63024a1f928ba359c4f4cac7e48fe39</anchor>
      <arglist>(CHIP_SYSCTL_CLOCK_T clk)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_Clock_DisablePeriphClock</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga1bcb3f29f3cfbe896517e7bb6ebeaf03</anchor>
      <arglist>(CHIP_SYSCTL_CLOCK_T clk)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>Chip_Clock_IsPeripheralClockEnabled</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga0d7f762be4d4293d10d379fcc0b2388b</anchor>
      <arglist>(CHIP_SYSCTL_CLOCK_T clk)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_Clock_SetEMCClockDiv</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga0bcc4845f3e483266e97a414d514e51c</anchor>
      <arglist>(CHIP_SYSCTL_EMC_DIV_T emcDiv)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_Clock_GetEMCClockDiv</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga83d188982840ca3fe4c37eaea4341d74</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_Clock_SetCPUClockSource</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gaefeafe3f6ad6d2690c252e6cfcc826dd</anchor>
      <arglist>(CHIP_SYSCTL_CCLKSRC_T src)</arglist>
    </member>
    <member kind="function">
      <type>CHIP_SYSCTL_CCLKSRC_T</type>
      <name>Chip_Clock_GetCPUClockSource</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga84a5acda2829b05c81326606630b7238</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_Clock_SetCPUClockDiv</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gaf88a9722800b98c3ea3cccb572c54230</anchor>
      <arglist>(uint32_t div)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_Clock_GetCPUClockDiv</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga3e1ef8ac1f9c19b33016c914b01fd9a4</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_Clock_SetUSBClockSource</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga4836fb1246978d20cbbf54d7ca1bff5a</anchor>
      <arglist>(CHIP_SYSCTL_USBCLKSRC_T src)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE CHIP_SYSCTL_USBCLKSRC_T</type>
      <name>Chip_Clock_GetUSBClockSource</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga2e7aa1ac705f25b3b1d41cec33a3893b</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_Clock_SetUSBClockDiv</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gae63a884704ec0b314373e34165f62963</anchor>
      <arglist>(uint32_t div)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_Clock_GetUSBClockDiv</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga5852bdb5470a03988aaae8c68e3fb5fd</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_Clock_SetMainPLLSource</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gadeba4ec298c7ad2cf1627f9add199d02</anchor>
      <arglist>(CHIP_SYSCTL_PLLCLKSRC_T src)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE CHIP_SYSCTL_PLLCLKSRC_T</type>
      <name>Chip_Clock_GetMainPLLSource</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gaf678411ef6cde49c95c603030554fa9d</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_Clock_SetPCLKDiv</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga847b9fe292e8d5461c02750a21d34885</anchor>
      <arglist>(uint32_t div)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_Clock_GetPCLKDiv</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga2fb0ad885ca820d555cf9ff4268cc252</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_Clock_SetSPIFIClockSource</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga5f9e0080824fdcbca2d70f277302da12</anchor>
      <arglist>(CHIP_SYSCTL_SPIFICLKSRC_T src)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE CHIP_SYSCTL_SPIFICLKSRC_T</type>
      <name>Chip_Clock_GetSPIFIClockSource</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gad02349cdbc8ced26704cccf06cc57dc6</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_Clock_SetSPIFIClockDiv</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga2cde27bd6930a102a0b2b1b7f0561200</anchor>
      <arglist>(uint32_t div)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_Clock_GetSPIFIClockDiv</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gae658b84de2ea58b38fe7b881a16999a1</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_Clock_SetLCDClockDiv</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga1ce42fc25d5a91dadf0d7c6c9b3c3f34</anchor>
      <arglist>(uint32_t div)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_Clock_GetLCDClockDiv</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gaa861b046872f8dff6f623ef7ba63fa80</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_Clock_SetCLKOUTSource</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga0e8ed739d1ffa5480fc028b08a751d28</anchor>
      <arglist>(CHIP_SYSCTL_CLKOUTSRC_T src, uint32_t div)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_Clock_EnableCLKOUT</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga6b9661b89b2284e6f09af0e77303f9d2</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_Clock_DisableCLKOUT</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga87c79d2ff63948102a6addd4222c4879</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE bool</type>
      <name>Chip_Clock_IsCLKOUTEnabled</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga01120c20f4485599176f9b46a34b70fe</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_Clock_GetMainOscRate</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga32ea9f95eed11d2bfa470b473232456e</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_Clock_GetIntOscRate</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gaa8db0ad49f51bf5d6870181e77249c2b</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_Clock_GetRTCOscRate</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga64b01351fd2019749b1f1d18dfd263f0</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_Clock_GetSYSCLKRate</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gaec133465745ce56e49b184185f8252e1</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_Clock_GetMainPLLInClockRate</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gade97c5e68f4609663e247043b48949d9</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_Clock_GetMainPLLOutClockRate</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gad1a38c10a143b8e21d2a8085ec0cb13e</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_Clock_GetUSBPLLInClockRate</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gaa97e3b970f577ea06e5d4f76097576c7</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_Clock_GetUSBPLLOutClockRate</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga89cab6cddba486f9c820b06e0a28bade</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_Clock_GetMainClockRate</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gaf5319079ca1531102c01860d05a69960</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_Clock_GetSystemClockRate</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gaf3dd97239f9db511dbc71c531132cc08</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_Clock_GetUSBClockRate</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gac8679aba3cc005f859604a09ceceb4a4</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_Clock_GetSPIFIClockRate</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga09e6d1ff0c53ebffd5f6fd407ea01ddb</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_Clock_GetEMCClockRate</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>gae2649650e17f30a5fc753c11cfa3efb4</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_Clock_GetPeripheralClockRate</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga9c2bc86c857119426aa6a724c12a6f42</anchor>
      <arglist>(CHIP_SYSCTL_PCLK_T clk)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_Clock_GetRTCClockRate</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga7c09db016cc48f17aca47d96b0d68814</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_Clock_GetENETClockRate</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga04af0fa6fa72517538fc3d3918fcc0d9</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_Clock_GetGPDMAClockRate</name>
      <anchorfile>group__CLOCK__17XX__40XX.html</anchorfile>
      <anchor>ga3141accbf546f4de8cfb005e31ec53a8</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>CMP_17XX_40XX</name>
    <title>CHIP: LPC40xx Comparator driver</title>
    <filename>group__CMP__17XX__40XX.html</filename>
  </compound>
  <compound kind="group">
    <name>CMSIS_175X_6X</name>
    <title>CHIP: LPC175x/6x CMSIS include file</title>
    <filename>group__CMSIS__175X__6X.html</filename>
    <subgroup>CMSIS_175X_6X_IRQ</subgroup>
    <subgroup>CMSIS_175X_6X_COMMON</subgroup>
  </compound>
  <compound kind="group">
    <name>CMSIS_175X_6X_IRQ</name>
    <title>CHIP_175X_6X: LPC175x/6x peripheral interrupt numbers</title>
    <filename>group__CMSIS__175X__6X__IRQ.html</filename>
    <member kind="enumeration">
      <type></type>
      <name>LPC175X_6X_IRQn_Type</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>gaaaeafe7bd8401a46d55e8431b6326116</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>Reset_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a50ad21f2fd0d54d04b390d5a9145889a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>NonMaskableInt_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116ade177d9c70c89e084093024b932a4e30</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>HardFault_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116ab1a222a34a32f0ef5ac65e714efc1f85</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>MemoryManagement_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a33ff1cf7098de65d61b6354fee6cd5aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>BusFault_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a8693500eff174f16119e96234fee73af</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>UsageFault_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a6895237c9443601ac832efa635dd8bbf</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SVCall_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a4ce820b3cc6cf3a796b41aadc0cf1237</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>DebugMonitor_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a8e033fcef7aed98a31c60a7de206722c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>PendSV_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a03c3cc89984928816d81793fc7bce4a2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SysTick_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a6dbff8f8543325f3474cbae2446776e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WDT_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a78573b84a4133ef5812b33ce10dcba12</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>TIMER0_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a2336220ce1e39507eb592958064a2b87</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>TIMER1_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a0b751a83bbf254701e0d5a1d863010d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>TIMER2_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116ad97a34027a8b1017d42d1d6320381e48</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>TIMER3_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a7d15b5c33e51350d11303db7d4bc3381</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>UART0_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116ae9122b85b58f7c24033a8515615a7b74</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>UART_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a9d9be6e918c912367e393dae3480eabb</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>UART1_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a9650ab92e46bc16f333d4c63ad0459b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>UART2_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116ab051fac6b15b88454713bb36d96e5dd5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>UART3_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a11614a227a56dc01859bf803013e6358</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>PWM1_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a0cd5b403aa037bacf964fd1a60c3f08f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C0_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a0f1945c7372a6de732306ea3801c8e2a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116adf8c31fe1c7ade2eb05f1414710dbce7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C1_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116af651b1769e03e4653b1a4a7c88132398</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C2_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a0e9ff46d0a6311ca3cc43a71702d638d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SPI_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a8f5f99956cf9765f17bcba6865b93ce2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP0_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a6c8d6262fd7ecedc57b2fe9209be9765</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a91a7a38ad2f048e2813b98dc68f88adc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP1_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116adcc0cfa46f0d13c2de0f3e826c10a789</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>PLL0_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a7726163ff47c899c26ce68f99eb937a9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>RTC_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116adcc0f2770f7f57f75fac3d8bcac0e858</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>EINT0_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a0a3db3233549f012f8ecb88f0510adcf</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>EINT1_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116ad855ae101e21a04054a9844066900d7c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>EINT2_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a40ab356422a691418668d6bbfd9f17b9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>EINT3_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a14098dd2e0d0331c1e5f1f80dde14371</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>ADC_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a4d69175258ae261dd545001e810421b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>BOD_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116ac2ee5960aed41ff349aa7a86c37e9ab2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a5078f46ddc47f29eae4aa40bd57d1692</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CAN_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a20d0bdfa1654b723493895e3ea617cdb</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>DMA_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a4968eb85558b7cd25e0f0fa1b839f881</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2S_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a133fe4eabad3ed7b1959daddaace94b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>ETHERNET_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a7601b42a319f3767dde4e08ff6e613c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>RITIMER_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a6c4ccf16e191963e48badc499a56cf3a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>MCPWM_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a66a11398b7cc13d7c525945b0a86a2f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>QEI_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a82471ba65527ad3f3da8af38acb953bf</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>PLL1_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a0f0e46a33c20be148ef16fe7ed4dcd4b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USBActivity_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116aa3c495fabd97a50818dc748f738c71e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CANActivity_IRQn</name>
      <anchorfile>group__CMSIS__175X__6X__IRQ.html</anchorfile>
      <anchor>ggaaaeafe7bd8401a46d55e8431b6326116a25ef61f3d4a0d5f2bcf0525702b872b7</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>CMSIS_175X_6X_COMMON</name>
    <title>CHIP: LPC175x/6x Cortex CMSIS definitions</title>
    <filename>group__CMSIS__175X__6X__COMMON.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>__MPU_PRESENT</name>
      <anchorfile>group__CMSIS__175X__6X__COMMON.html</anchorfile>
      <anchor>ga4127d1b31aaf336fab3d7329d117f448</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>__NVIC_PRIO_BITS</name>
      <anchorfile>group__CMSIS__175X__6X__COMMON.html</anchorfile>
      <anchor>gae3fe3587d5100c787e02102ce3944460</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>__Vendor_SysTickConfig</name>
      <anchorfile>group__CMSIS__175X__6X__COMMON.html</anchorfile>
      <anchor>gab58771b4ec03f9bdddc84770f7c95c68</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>__FPU_PRESENT</name>
      <anchorfile>group__CMSIS__175X__6X__COMMON.html</anchorfile>
      <anchor>gac1ba8a48ca926bddc88be9bfd7d42641</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>CMSIS_glob_defs</name>
    <title>CMSIS Global Defines</title>
    <filename>group__CMSIS__glob__defs.html</filename>
  </compound>
  <compound kind="group">
    <name>CMSIS_core_register</name>
    <title>Defines and Type Definitions</title>
    <filename>group__CMSIS__core__register.html</filename>
    <subgroup>CMSIS_CORE</subgroup>
    <subgroup>CMSIS_NVIC</subgroup>
    <subgroup>CMSIS_SCB</subgroup>
    <subgroup>CMSIS_SCnSCB</subgroup>
    <subgroup>CMSIS_SysTick</subgroup>
    <subgroup>CMSIS_ITM</subgroup>
    <subgroup>CMSIS_DWT</subgroup>
    <subgroup>CMSIS_TPI</subgroup>
    <subgroup>CMSIS_CoreDebug</subgroup>
    <subgroup>CMSIS_core_base</subgroup>
  </compound>
  <compound kind="group">
    <name>CMSIS_CORE</name>
    <title>Status and Control Registers</title>
    <filename>group__CMSIS__CORE.html</filename>
    <class kind="union">APSR_Type</class>
    <class kind="union">IPSR_Type</class>
    <class kind="union">xPSR_Type</class>
    <class kind="union">CONTROL_Type</class>
  </compound>
  <compound kind="group">
    <name>CMSIS_NVIC</name>
    <title>Nested Vectored Interrupt Controller (NVIC)</title>
    <filename>group__CMSIS__NVIC.html</filename>
    <class kind="struct">NVIC_Type</class>
    <member kind="define">
      <type>#define</type>
      <name>NVIC_STIR_INTID_Pos</name>
      <anchorfile>group__CMSIS__NVIC.html</anchorfile>
      <anchor>ga9eebe495e2e48d302211108837a2b3e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>NVIC_STIR_INTID_Msk</name>
      <anchorfile>group__CMSIS__NVIC.html</anchorfile>
      <anchor>gae4060c4dfcebb08871ca4244176ce752</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>CMSIS_SCB</name>
    <title>System Control Block (SCB)</title>
    <filename>group__CMSIS__SCB.html</filename>
    <class kind="struct">SCB_Type</class>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CPUID_IMPLEMENTER_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga58686b88f94f789d4e6f429fe1ff58cf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CPUID_IMPLEMENTER_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga0932b31faafd47656a03ced75a31d99b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CPUID_VARIANT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga104462bd0815391b4044a70bd15d3a71</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CPUID_VARIANT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gad358dfbd04300afc1824329d128b99e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CPUID_ARCHITECTURE_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaf8b3236b08fb8e840efb682645fb0e98</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CPUID_ARCHITECTURE_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gafae4a1f27a927338ae9dc51a0e146213</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CPUID_PARTNO_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga705f68eaa9afb042ca2407dc4e4629ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CPUID_PARTNO_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga98e581423ca016680c238c469aba546d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CPUID_REVISION_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga3c3d9071e574de11fb27ba57034838b1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CPUID_REVISION_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga2ec0448b6483f77e7f5d08b4b81d85df</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_NMIPENDSET_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga750d4b52624a46d71356db4ea769573b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_NMIPENDSET_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga340e3f79e9c3607dee9f2c048b6b22e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_PENDSVSET_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gab5ded23d2ab1d5ff7cc7ce746205e9fe</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_PENDSVSET_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga1e40d93efb402763c8c00ddcc56724ff</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_PENDSVCLR_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gae218d9022288f89faf57187c4d542ecd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_PENDSVCLR_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga4a901ace381d3c1c74ac82b22fae2e1e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_PENDSTSET_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga9dbb3358c6167c9c3f85661b90fb2794</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_PENDSTSET_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga7325b61ea0ec323ef2d5c893b112e546</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_PENDSTCLR_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gadbe25e4b333ece1341beb1a740168fdc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_PENDSTCLR_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gab241827d2a793269d8cd99b9b28c2157</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_ISRPREEMPT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga11cb5b1f9ce167b81f31787a77e575df</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_ISRPREEMPT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaa966600396290808d596fe96e92ca2b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_ISRPENDING_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga10749d92b9b744094b845c2eb46d4319</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_ISRPENDING_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga056d74fd538e5d36d3be1f28d399c877</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_VECTPENDING_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gada60c92bf88d6fd21a8f49efa4a127b8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_VECTPENDING_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gacb6992e7c7ddc27a370f62878a21ef72</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_RETTOBASE_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga403d154200242629e6d2764bfc12a7ec</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_RETTOBASE_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaca6fc3f79bb550f64fd7df782ed4a5f6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_VECTACTIVE_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gae4f602c7c5c895d5fb687b71b0979fc3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_ICSR_VECTACTIVE_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga5533791a4ecf1b9301c883047b3e8396</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_VTOR_TBLBASE_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gad9720a44320c053883d03b883b955751</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_VTOR_TBLBASE_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga778dd0ba178466b2a8877a6b8aa345ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_VTOR_TBLOFF_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gac6a55451ddd38bffcff5a211d29cea78</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_VTOR_TBLOFF_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga75e395ed74042923e8c93edf50f0996c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_VECTKEY_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaaa27c0ba600bf82c3da08c748845b640</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_VECTKEY_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga90c7cf0c490e7ae55f9503a7fda1dd22</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_VECTKEYSTAT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaec404750ff5ca07f499a3c06b62051ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_VECTKEYSTAT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gabacedaefeefc73d666bbe59ece904493</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_ENDIANESS_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gad31dec98fbc0d33ace63cb1f1a927923</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_ENDIANESS_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga2f571f93d3d4a6eac9a3040756d3d951</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_PRIGROUP_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaca155deccdeca0f2c76b8100d24196c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_PRIGROUP_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga8be60fff03f48d0d345868060dc6dae7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_SYSRESETREQ_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaffb2737eca1eac0fc1c282a76a40953c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_SYSRESETREQ_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaae1181119559a5bd36e62afa373fa720</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_VECTCLRACTIVE_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaa30a12e892bb696e61626d71359a9029</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_VECTCLRACTIVE_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga212c5ab1c1c82c807d30d2307aa8d218</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_VECTRESET_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga0d483d9569cd9d1b46ec0d171b1f18d8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_AIRCR_VECTRESET_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga3006e31968bb9725e7b4ee0784d99f7f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SCR_SEVONPEND_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga3bddcec40aeaf3d3a998446100fa0e44</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SCR_SEVONPEND_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gafb98656644a14342e467505f69a997c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SCR_SLEEPDEEP_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gab304f6258ec03bd9a6e7a360515c3cfe</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SCR_SLEEPDEEP_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga77c06a69c63f4b3f6ec1032e911e18e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SCR_SLEEPONEXIT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga3680a15114d7fdc1e25043b881308fe9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SCR_SLEEPONEXIT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga50a243e317b9a70781b02758d45b05ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_STKALIGN_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gac2d20a250960a432cc74da59d20e2f86</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_STKALIGN_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga33cf22d3d46af158a03aad25ddea1bcb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_BFHFNMIGN_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga4010a4f9e2a745af1b58abe1f791ebbf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_BFHFNMIGN_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga89a28cc31cfc7d52d9d7a8fcc69c7eac</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_DIV_0_TRP_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gac8d512998bb8cd9333fb7627ddf59bba</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_DIV_0_TRP_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gabb9aeac71b3abd8586d0297070f61dcb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_UNALIGN_TRP_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gac4e4928b864ea10fc24dbbc57d976229</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_UNALIGN_TRP_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga68c96ad594af70c007923979085c99e0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_USERSETMPEND_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga789e41f45f59a8cd455fd59fa7652e5e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_USERSETMPEND_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga4cf59b6343ca962c80e1885710da90aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_NONBASETHRDENA_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gab4615f7deb07386350365b10240a3c83</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CCR_NONBASETHRDENA_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gafe0f6be81b35d72d0736a0a1e3b4fbb3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_USGFAULTENA_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gae71949507636fda388ec11d5c2d30b52</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_USGFAULTENA_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga056fb6be590857bbc029bed48b21dd79</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_BUSFAULTENA_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga3d32edbe4a5c0335f808cfc19ec7e844</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_BUSFAULTENA_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga43e8cbe619c9980e0d1aacc85d9b9e47</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_MEMFAULTENA_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga685b4564a8760b4506f14ec4307b7251</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_MEMFAULTENA_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaf084424fa1f69bea36a1c44899d83d17</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_SVCALLPENDED_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga2f93ec9b243f94cdd3e94b8f0bf43641</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_SVCALLPENDED_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga6095a7acfbad66f52822b1392be88652</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_BUSFAULTPENDED_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaa22551e24a72b65f1e817f7ab462203b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_BUSFAULTPENDED_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga677c23749c4d348f30fb471d1223e783</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_MEMFAULTPENDED_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaceb60fe2d8a8cb17fcd1c1f6b5aa924f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_MEMFAULTPENDED_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga9abc6c2e395f9e5af4ce05fc420fb04c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_USGFAULTPENDED_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga3cf03acf1fdc2edc3b047ddd47ebbf87</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_USGFAULTPENDED_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga122b4f732732010895e438803a29d3cc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_SYSTICKACT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaec9ca3b1213c49e2442373445e1697de</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_SYSTICKACT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gafef530088dc6d6bfc9f1893d52853684</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_PENDSVACT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga9b9fa69ce4c5ce7fe0861dbccfb15939</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_PENDSVACT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gae0e837241a515d4cbadaaae1faa8e039</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_MONITORACT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga8b71cf4c61803752a41c96deb00d26af</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_MONITORACT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaad09b4bc36e9bccccc2e110d20b16e1a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_SVCALLACT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga977f5176be2bc8b123873861b38bc02f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_SVCALLACT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga634c0f69a233475289023ae5cb158fdf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_USGFAULTACT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gae06f54f5081f01ed3f6824e451ad3656</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_USGFAULTACT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gab3166103b5a5f7931d0df90949c47dfe</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_BUSFAULTACT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaf272760f2df9ecdd8a5fbbd65c0b767a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_BUSFAULTACT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga9d7a8b1054b655ad08d85c3c535d4f73</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_MEMFAULTACT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga7c856f79a75dcc1d1517b19a67691803</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_SHCSR_MEMFAULTACT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga9147fd4e1b12394ae26eadf900a023a3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CFSR_USGFAULTSR_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gac8e4197b295c8560e68e2d71285c7879</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CFSR_USGFAULTSR_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga565807b1a3f31891f1f967d0fa30d03f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CFSR_BUSFAULTSR_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga555a24f4f57d199f91d1d1ab7c8c3c8a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CFSR_BUSFAULTSR_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga26dc1ddfdc37a6b92597a6f7e498c1d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CFSR_MEMFAULTSR_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga91f41491cec5b5acca3fbc94efbd799e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_CFSR_MEMFAULTSR_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gad46716159a3808c9e7da22067d6bec98</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_HFSR_DEBUGEVT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga300c90cfb7b35c82b4d44ad16c757ffb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_HFSR_DEBUGEVT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gababd60e94756bb33929d5e6f25d8dba3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_HFSR_FORCED_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gab361e54183a378474cb419ae2a55d6f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_HFSR_FORCED_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga6560d97ed043bc01152a7247bafa3157</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_HFSR_VECTTBL_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga77993da8de35adea7bda6a4475f036ab</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_HFSR_VECTTBL_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaac5e289211d0a63fe879a9691cb9e1a9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_DFSR_EXTERNAL_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga13f502fb5ac673df9c287488c40b0c1d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_DFSR_EXTERNAL_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga3cba2ec1f588ce0b10b191d6b0d23399</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_DFSR_VCATCH_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gad02d3eaf062ac184c18a7889c9b6de57</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_DFSR_VCATCH_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gacbb931575c07b324ec793775b7c44d05</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_DFSR_DWTTRAP_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaccf82364c6d0ed7206f1084277b7cc61</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_DFSR_DWTTRAP_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga3f7384b8a761704655fd45396a305663</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_DFSR_BKPT_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaf28fdce48655f0dcefb383aebf26b050</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_DFSR_BKPT_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga609edf8f50bc49adb51ae28bcecefe1f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_DFSR_HALTED_Pos</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>gaef4ec28427f9f88ac70a13ae4e541378</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_DFSR_HALTED_Msk</name>
      <anchorfile>group__CMSIS__SCB.html</anchorfile>
      <anchor>ga200bcf918d57443b5e29e8ce552e4bdf</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>CMSIS_SCnSCB</name>
    <title>System Controls not in SCB (SCnSCB)</title>
    <filename>group__CMSIS__SCnSCB.html</filename>
    <class kind="struct">SCnSCB_Type</class>
    <member kind="define">
      <type>#define</type>
      <name>SCnSCB_ICTR_INTLINESNUM_Pos</name>
      <anchorfile>group__CMSIS__SCnSCB.html</anchorfile>
      <anchor>ga0777ddf379af50f9ca41d40573bfffc5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCnSCB_ICTR_INTLINESNUM_Msk</name>
      <anchorfile>group__CMSIS__SCnSCB.html</anchorfile>
      <anchor>ga3efa0f5210051464e1034b19fc7b33c7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCnSCB_ACTLR_DISFOLD_Pos</name>
      <anchorfile>group__CMSIS__SCnSCB.html</anchorfile>
      <anchor>gaab395870643a0bee78906bb15ca5bd02</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCnSCB_ACTLR_DISFOLD_Msk</name>
      <anchorfile>group__CMSIS__SCnSCB.html</anchorfile>
      <anchor>gaa9dd2d4a2350499188f438d0aa9fd982</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCnSCB_ACTLR_DISDEFWBUF_Pos</name>
      <anchorfile>group__CMSIS__SCnSCB.html</anchorfile>
      <anchor>gafa2eb37493c0f8dae77cde81ecf80f77</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCnSCB_ACTLR_DISDEFWBUF_Msk</name>
      <anchorfile>group__CMSIS__SCnSCB.html</anchorfile>
      <anchor>ga6cda7b7219232a008ec52cc8e89d5d08</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCnSCB_ACTLR_DISMCYCINT_Pos</name>
      <anchorfile>group__CMSIS__SCnSCB.html</anchorfile>
      <anchor>gaaa3e79f5ead4a32c0ea742b2a9ffc0cd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCnSCB_ACTLR_DISMCYCINT_Msk</name>
      <anchorfile>group__CMSIS__SCnSCB.html</anchorfile>
      <anchor>ga2a2818f0489ad10b6ea2964e899d4cbc</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>CMSIS_SysTick</name>
    <title>System Tick Timer (SysTick)</title>
    <filename>group__CMSIS__SysTick.html</filename>
    <class kind="struct">SysTick_Type</class>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CTRL_COUNTFLAG_Pos</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>gadbb65d4a815759649db41df216ed4d60</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CTRL_COUNTFLAG_Msk</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>ga1bf3033ecccf200f59baefe15dbb367c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CTRL_CLKSOURCE_Pos</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>ga24fbc69a5f0b78d67fda2300257baff1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CTRL_CLKSOURCE_Msk</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>gaa41d06039797423a46596bd313d57373</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CTRL_TICKINT_Pos</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>ga88f45bbb89ce8df3cd2b2613c7b48214</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CTRL_TICKINT_Msk</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>ga95bb984266ca764024836a870238a027</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CTRL_ENABLE_Pos</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>ga0b48cc1e36d92a92e4bf632890314810</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CTRL_ENABLE_Msk</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>ga16c9fee0ed0235524bdeb38af328fd1f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_LOAD_RELOAD_Pos</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>gaf44d10df359dc5bf5752b0894ae3bad2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_LOAD_RELOAD_Msk</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>ga265912a7962f0e1abd170336e579b1b1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_VAL_CURRENT_Pos</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>ga3208104c3b019b5de35ae8c21d5c34dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_VAL_CURRENT_Msk</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>gafc77b56d568930b49a2474debc75ab45</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CALIB_NOREF_Pos</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>ga534dbe414e7a46a6ce4c1eca1fbff409</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CALIB_NOREF_Msk</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>ga3af0d891fdd99bcc8d8912d37830edb6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CALIB_SKEW_Pos</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>gadd0c9cd6641b9f6a0c618e7982954860</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CALIB_SKEW_Msk</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>ga8a6a85a87334776f33d77fd147587431</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CALIB_TENMS_Pos</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>gacae558f6e75a0bed5d826f606d8e695e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_CALIB_TENMS_Msk</name>
      <anchorfile>group__CMSIS__SysTick.html</anchorfile>
      <anchor>gaf1e68865c5aece2ad58971225bd3e95e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>CMSIS_ITM</name>
    <title>Instrumentation Trace Macrocell (ITM)</title>
    <filename>group__CMSIS__ITM.html</filename>
    <class kind="struct">ITM_Type</class>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TPR_PRIVMASK_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga7abe5e590d1611599df87a1884a352e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TPR_PRIVMASK_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga168e089d882df325a387aab3a802a46b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_BUSY_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga9174ad4a36052c377cef4e6aba2ed484</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_BUSY_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga43ad7cf33de12f2ef3a412d4f354c60f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_TraceBusID_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>gaca0281de867f33114aac0636f7ce65d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_TraceBusID_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga60c20bd9649d1da5a2be8e656ba19a60</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_GTSFREQ_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga96c7c7cbc0d98426c408090b41f583f1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_GTSFREQ_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>gade862cf009827f7f6748fc44c541b067</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_TSPrescale_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>gad7bc9ee1732032c6e0de035f0978e473</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_TSPrescale_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga7a723f71bfb0204c264d8dbe8cc7ae52</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_SWOENA_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga7a380f0c8078f6560051406583ecd6a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_SWOENA_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga97476cb65bab16a328b35f81fd02010a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_DWTENA_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga30e83ebb33aa766070fe3d1f27ae820e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_DWTENA_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga98ea1c596d43d3633a202f9ee746cf70</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_SYNCENA_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>gaa93a1147a39fc63980d299231252a30e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_SYNCENA_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>gac89b74a78701c25b442105d7fe2bbefb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_TSENA_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga5aa381845f810114ab519b90753922a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_TSENA_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga436b2e8fa24328f48f2da31c00fc9e65</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_ITMENA_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga3286b86004bce7ffe17ee269f87f8d9d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_TCR_ITMENA_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga7dd53e3bff24ac09d94e61cb595cb2d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_IWR_ATVALIDM_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga04d3f842ad48f6a9127b4cecc963e1d7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_IWR_ATVALIDM_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga67b969f8f04ed15886727788f0e2ffd7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_IRR_ATREADYM_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga259edfd1d2e877a62e06d7a240df97f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_IRR_ATREADYM_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga3dbc3e15f5bde2669cd8121a1fe419b9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_IMCR_INTEGRATION_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga08de02bf32caf48aaa29f7c68ff5d755</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_IMCR_INTEGRATION_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga8838bd3dd04c1a6be97cd946364a3fd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_LSR_ByteAcc_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>gabfae3e570edc8759597311ed6dfb478e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_LSR_ByteAcc_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga91f492b2891bb8b7eac5b58de7b220f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_LSR_Access_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>ga144a49e12b83ad9809fdd2769094fdc0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_LSR_Access_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>gac8ae69f11c0311da226c0c8ec40b3d37</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_LSR_Present_Pos</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>gaf5740689cf14564d3f3fd91299b6c88d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_LSR_Present_Msk</name>
      <anchorfile>group__CMSIS__ITM.html</anchorfile>
      <anchor>gaa5bc2a7f5f1d69ff819531f5508bb017</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>CMSIS_DWT</name>
    <title>Data Watchpoint and Trace (DWT)</title>
    <filename>group__CMSIS__DWT.html</filename>
    <class kind="struct">DWT_Type</class>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_NUMCOMP_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaac44b9b7d5391a7ffef129b7f6c84cd7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_NUMCOMP_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaa3d37d68c2ba73f2026265584c2815e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_NOTRCPKT_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaa82840323a2628e7f4a2b09b74fa73fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_NOTRCPKT_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga04d8bb0a065ca38e2e5f13a97e1f7073</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_NOEXTTRIG_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gad997b9026715d5609b5a3b144eca42d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_NOEXTTRIG_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gacc7d15edf7a27147c422099ab475953e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_NOCYCCNT_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga337f6167d960f57f12aa382ffecce522</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_NOCYCCNT_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaf40c8d7a4fd978034c137e90f714c143</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_NOPRFCNT_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gad52a0e5be84363ab166cc17beca0d048</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_NOPRFCNT_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gafd8448d7db4bc51f27f202e6e1f27823</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_CYCEVTENA_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga0cb0640aaeb18a626d7823570d5c3cb6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_CYCEVTENA_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga40554bd81460e39abf08810f45fac1a2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_FOLDEVTENA_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga5602b0707f446ce78d88ff2a3a82bfff</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_FOLDEVTENA_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga717e679d775562ae09185a3776b1582f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_LSUEVTENA_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaea5d1ee72188dc1d57b54c60a9f5233e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_LSUEVTENA_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gac47427f455fbc29d4b6f8a479169f2b2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_SLEEPEVTENA_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga9c6d62d121164013a8e3ee372f17f3e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_SLEEPEVTENA_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga2f431b3734fb840daf5b361034856da9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_EXCEVTENA_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaf4e73f548ae3e945ef8b1d9ff1281544</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_EXCEVTENA_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gab7ee0def33423b5859ca4030dff63b58</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_CPIEVTENA_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga9fff0b71fb0be1499f5180c6bce1fc8f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_CPIEVTENA_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga189089c30aade60b983df17ad2412f6f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_EXCTRCENA_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga05f13b547a9a1e63e003ee0bc6446d0d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_EXCTRCENA_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaf4fbb509ab3cbb768f16484c660a24c3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_PCSAMPLENA_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga1e14afc7790fcb424fcf619e192554c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_PCSAMPLENA_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gafdcf1c86f43fbeaf2780ce797c9ef3d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_SYNCTAP_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga678ef08786edcbef964479217efb9284</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_SYNCTAP_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaf1e6c3729d56ecadeb6eeff4d225968c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_CYCTAP_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaf70b80936c7db60bf84fb6dadb8a3559</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_CYCTAP_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga6c12e2868b8989a69445646698b8c331</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_POSTINIT_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga2868c0b28eb13be930afb819f55f6f25</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_POSTINIT_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gab8cbbee1e1d94d09f9a1f86379a08ee8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_POSTPRESET_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga129bc152febfddd67a0c20c6814cba69</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_POSTPRESET_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga11d9e1e2a758fdd2657aa68ce61b9c9d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_CYCCNTENA_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaa4509f5f8514a7200be61691f0e01f10</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CTRL_CYCCNTENA_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga4a9d209dc2a81ea6bfa0ea21331769d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CPICNT_CPICNT_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga80e9ad8f6a9e2344af8a3cf989bebe3d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_CPICNT_CPICNT_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga76f39e7bca3fa86a4dbf7b8f6adb7217</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_EXCCNT_EXCCNT_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga031c693654030d4cba398b45d2925b1d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_EXCCNT_EXCCNT_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga057fa604a107b58a198bbbadb47e69c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_SLEEPCNT_SLEEPCNT_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga0371a84a7996dc5852c56afb2676ba1c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_SLEEPCNT_SLEEPCNT_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga1e340751d71413fef400a0a1d76cc828</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_LSUCNT_LSUCNT_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gab9394c7911b0b4312a096dad91d53a3d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_LSUCNT_LSUCNT_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga2186d7fc9317e20bad61336ee2925615</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FOLDCNT_FOLDCNT_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga7f8af5ac12d178ba31a516f6ed141455</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FOLDCNT_FOLDCNT_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga9cb73d0342d38b14e41027d3c5c02647</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_MASK_MASK_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaf798ae34e2b9280ea64f4d9920cd2e7d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_MASK_MASK_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gadd798deb0f1312feab4fb05dcddc229b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_MATCHED_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga22c5787493f74a6bacf6ffb103a190ba</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_MATCHED_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gac8b1a655947490280709037808eec8ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_DATAVADDR1_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga8b75e8ab3ffd5ea2fa762d028dc30e8c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_DATAVADDR1_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gafdbf5a8c367befe8661a4f6945c83445</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_DATAVADDR0_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga9854cd8bf16f7dce0fb196a8029b018e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_DATAVADDR0_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gafc5efbe8f9b51e04aecd00c8a4eb50fb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_DATAVSIZE_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga0517a186d4d448aa6416440f40fe7a4d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_DATAVSIZE_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gaab42cbc1e6084c44d5de70971613ea76</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_LNK1ENA_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga89d7c48858b4d4de96cdadfac91856a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_LNK1ENA_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga64bd419260c3337cacf93607d1ad27ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_DATAVMATCH_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga106f3672cd4be7c7c846e20497ebe5a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_DATAVMATCH_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga32af1f1c0fcd2d8d9afd1ad79cd9970e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_CYCMATCH_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga4b65d79ca37ae8010b4a726312413efd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_CYCMATCH_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga8e2ed09bdd33a8f7f7ce0444f5f3bb25</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_EMITRANGE_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga41d5b332216baa8d29561260a1b85659</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_EMITRANGE_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>gad46dd5aba29f2e28d4d3f50b1d291f41</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_FUNCTION_Pos</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga5797b556edde2bbaa4d33dcdb1a891bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_FUNCTION_FUNCTION_Msk</name>
      <anchorfile>group__CMSIS__DWT.html</anchorfile>
      <anchor>ga3b2cda708755ecf5f921d08b25d774d1</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>CMSIS_TPI</name>
    <title>Trace Port Interface (TPI)</title>
    <filename>group__CMSIS__TPI.html</filename>
    <class kind="struct">TPI_Type</class>
    <member kind="define">
      <type>#define</type>
      <name>TPI_ACPR_PRESCALER_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga5a82d274eb2df8b0c92dd4ed63535928</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_ACPR_PRESCALER_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga4fcacd27208419929921aec8457a8c13</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_SPPR_TXMODE_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga0f302797b94bb2da24052082ab630858</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_SPPR_TXMODE_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaca085c8a954393d70dbd7240bb02cc1f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFSR_FtNonStop_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga9537b8a660cc8803f57cbbee320b2fc8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFSR_FtNonStop_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaaa313f980974a8cfc7dac68c4d805ab1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFSR_TCPresent_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gad30fde0c058da2ffb2b0a213be7a1b5c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFSR_TCPresent_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga0d6bfd263ff2fdec72d6ec9415fb1135</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFSR_FtStopped_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaedf31fd453a878021b542b644e2869d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFSR_FtStopped_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga1ab6c3abe1cf6311ee07e7c479ce5f78</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFSR_FlInProg_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga542ca74a081588273e6d5275ba5da6bf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFSR_FlInProg_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga63dfb09259893958962914fc3a9e3824</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFCR_TrigIn_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaa7ea11ba6ea75b541cd82e185c725b5b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFCR_TrigIn_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga360b413bc5da61f751546a7133c3e4dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFCR_EnFCont_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga99e58a0960b275a773b245e2b69b9a64</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FFCR_EnFCont_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga27d1ecf2e0ff496df03457a2a97cb2c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_TRIGGER_TRIGGER_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga5517fa2ced64efbbd413720329c50b99</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_TRIGGER_TRIGGER_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga814227af2b2665a0687bb49345e21110</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ITM_ATVALID_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaa7e050e9eb6528241ebc6835783b6bae</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ITM_ATVALID_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga94cb2493ed35d2dab7bd4092b88a05bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ITM_bytecount_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gac2b6f7f13a2fa0be4aa7645a47dcac52</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ITM_bytecount_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga07bafa971b8daf0d63b3f92b9ae7fa16</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ETM_ATVALID_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga7fdeb3e465ca4aa9e3b2f424ab3bbd1d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ETM_ATVALID_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga4f0005dc420b28f2369179a935b9a9d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ETM_bytecount_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga2f738e45386ebf58c4d406f578e7ddaf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ETM_bytecount_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gad2536b3a935361c68453cd068640af92</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ETM2_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga5f0037cc80c65e86d9e94e5005077a48</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ETM2_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaa82a7b9b99c990fb12eafb3c84b68254</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ETM1_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gac5a2ef4b7f811d1f3d81ec919d794413</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ETM1_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaad9c1a6ed34a70905005a0cc14d5f01b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ETM0_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga48783ce3c695d8c06b1352a526110a87</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO0_ETM0_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaf924f7d1662f3f6c1da12052390cb118</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_ITATBCTR2_ATREADY_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga6959f73d7db4a87ae9ad9cfc99844526</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_ITATBCTR2_ATREADY_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga1859502749709a2e5ead9a2599d998db</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ITM_ATVALID_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga08edfc862b2c8c415854cc4ae2067dfb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ITM_ATVALID_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gabc1f6a3b6cac0099d7c01ca949b4dd08</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ITM_bytecount_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaa22ebf7c86e4f4b2c98cfd0b5981375a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ITM_bytecount_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gacba2edfc0499828019550141356b0dcb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ETM_ATVALID_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga3177b8d815cf4a707a2d3d3d5499315d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ETM_ATVALID_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga0e8f29a1e9378d1ceb0708035edbb86d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ETM_bytecount_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaab31238152b5691af633a7475eaf1f06</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ETM_bytecount_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gab554305459953b80554fdb1908b73291</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ITM2_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga1828c228f3940005f48fb8dd88ada35b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ITM2_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gae54512f926ebc00f2e056232aa21d335</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ITM1_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaece86ab513bc3d0e0a9dbd82258af49f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ITM1_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga3347f42828920dfe56e3130ad319a9e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ITM0_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga2188671488417a52abb075bcd4d73440</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_FIFO1_ITM0_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga8ae09f544fc1a428797e2a150f14a4c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_ITATBCTR0_ATREADY_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gab1eb6866c65f02fa9c83696b49b0f346</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_ITATBCTR0_ATREADY_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaee320b3c60f9575aa96a8742c4ff9356</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_ITCTRL_Mode_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaa847adb71a1bc811d2e3190528f495f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_ITCTRL_Mode_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gad6f87550b468ad0920d5f405bfd3f017</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_NRZVALID_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga9f46cf1a1708575f56d6b827766277f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_NRZVALID_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gacecc8710a8f6a23a7d1d4f5674daf02a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_MANCVALID_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga675534579d9e25477bb38970e3ef973c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_MANCVALID_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga4c3ee4b1a34ad1960a6b2d6e7e0ff942</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_PTINVALID_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga974cccf4c958b4a45cb71c7b5de39b7b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_PTINVALID_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga1ca84d62243e475836bba02516ba6b97</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_MinBufSz_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga3f7da5de2a34be41a092e5eddd22ac4d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_MinBufSz_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga939e068ff3f1a65b35187ab34a342cd8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_AsynClkIn_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gab382b1296b5efd057be606eb8f768df8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_AsynClkIn_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gab67830557d2d10be882284275025a2d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_NrTraceInput_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga80ecae7fec479e80e583f545996868ed</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVID_NrTraceInput_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gabed454418d2140043cd65ec899abd97f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVTYPE_SubType_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga0c799ff892af5eb3162d152abc00af7a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVTYPE_SubType_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga5b2fd7dddaf5f64855d9c0696acd65c1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVTYPE_MajorType_Pos</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>ga69c4892d332755a9f64c1680497cebdd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_DEVTYPE_MajorType_Msk</name>
      <anchorfile>group__CMSIS__TPI.html</anchorfile>
      <anchor>gaecbceed6d08ec586403b37ad47b38c88</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>CMSIS_CoreDebug</name>
    <title>Core Debug Registers (CoreDebug)</title>
    <filename>group__CMSIS__CoreDebug.html</filename>
    <class kind="struct">CoreDebug_Type</class>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_DBGKEY_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gac91280edd0ce932665cf75a23d11d842</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_DBGKEY_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga1ce997cee15edaafe4aed77751816ffc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_RESET_ST_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga6f934c5427ea057394268e541fa97753</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_RESET_ST_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gac474394bcceb31a8e09566c90b3f8922</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_RETIRE_ST_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga2328118f8b3574c871a53605eb17e730</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_RETIRE_ST_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga89dceb5325f6bcb36a0473d65fbfcfa6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_LOCKUP_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga2900dd56a988a4ed27ad664d5642807e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_LOCKUP_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga7b67e4506d7f464ef5dafd6219739756</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_SLEEP_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga349ccea33accc705595624c2d334fbcb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_SLEEP_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga98d51538e645c2c1a422279cd85a0a25</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_HALT_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga760a9a0d7f39951dc3f07d01f1f64772</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_HALT_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga9f881ade3151a73bc5b02b73fe6473ca</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_REGRDY_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga20a71871ca8768019c51168c70c3f41d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_S_REGRDY_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gac4cd6f3178de48f473d8903e8c847c07</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_C_SNAPSTALL_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga85747214e2656df6b05ec72e4d22bd6d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_C_SNAPSTALL_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga53aa99b2e39a67622f3b9973e079c2b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_C_MASKINTS_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga0d2907400eb948a4ea3886ca083ec8e3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_C_MASKINTS_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga77fe1ef3c4a729c1c82fb62a94a51c31</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_C_STEP_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gae1fc39e80de54c0339cbb1b298a9f0f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_C_STEP_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gae6bda72fbd32cc5734ff3542170dc00d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_C_HALT_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gaddf1d43f8857e4efc3dc4e6b15509692</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_C_HALT_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga1d905a3aa594eb2e8bb78bcc4da05b3f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_C_DEBUGEN_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gab557abb5b172b74d2cf44efb9d824e4e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DHCSR_C_DEBUGEN_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gab815c741a4fc2a61988cd2fb7594210b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DCRSR_REGWnR_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga51e75942fc0614bc9bb2c0e96fcdda9a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DCRSR_REGWnR_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga1eef4992d8f84bc6c0dffed1c87f90a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DCRSR_REGSEL_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga52182c8a9f63a52470244c0bc2064f7b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DCRSR_REGSEL_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga17cafbd72b55030219ce5609baa7c01d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_TRCENA_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga6ff2102b98f86540224819a1b767ba39</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_TRCENA_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga5e99652c1df93b441257389f49407834</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_MON_REQ_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga341020a3b7450416d72544eaf8e57a64</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_MON_REQ_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gae6384cbe8045051186d13ef9cdeace95</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_MON_STEP_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga9ae10710684e14a1a534e785ef390e1b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_MON_STEP_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga2ded814556de96fc369de7ae9a7ceb98</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_MON_PEND_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga1e2f706a59e0d8131279af1c7e152f8d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_MON_PEND_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga68ec55930269fab78e733dcfa32392f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_MON_EN_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga802829678f6871863ae9ecf60a10425c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_MON_EN_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gac2b46b9b65bf8d23027f255fc9641977</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_HARDERR_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gaed9f42053031a9a30cd8054623304c0a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_HARDERR_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga803fc98c5bb85f10f0347b23794847d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_INTERR_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga22079a6e436f23b90308be97e19cf07e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_INTERR_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gad6815d8e3df302d2f0ff2c2c734ed29a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_BUSERR_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gab8e3d8f0f9590a51bbf10f6da3ad6933</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_BUSERR_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga9d29546aefe3ca8662a7fe48dd4a5b2b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_STATERR_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga16f0d3d2ce1e1e8cd762d938ac56c4ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_STATERR_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gaa38b947d77672c48bba1280c0a642e19</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_CHKERR_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga10fc7c53bca904c128bc8e1a03072d50</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_CHKERR_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga2f98b461d19746ab2febfddebb73da6f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_NOCPERR_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gac9d13eb2add61f610d5ced1f7ad2adf8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_NOCPERR_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga03ee58b1b02fdbf21612809034562f1c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_MMERR_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga444454f7c7748e76cd76c3809c887c41</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_MMERR_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>gad420a9b60620584faaca6289e83d3a87</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_CORERESET_Pos</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga9fcf09666f7063a7303117aa32a85d5a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_DEMCR_VC_CORERESET_Msk</name>
      <anchorfile>group__CMSIS__CoreDebug.html</anchorfile>
      <anchor>ga906476e53c1e1487c30f3a1181df9e30</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>CMSIS_core_base</name>
    <title>Core Definitions</title>
    <filename>group__CMSIS__core__base.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>SCS_BASE</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>ga3c14ed93192c8d9143322bbf77ebf770</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM_BASE</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>gadd76251e412a195ec0a8f47227a8359e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT_BASE</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>gafdab534f961bf8935eb456cb7700dcd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI_BASE</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>ga2b1eeff850a7e418844ca847145a1a68</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug_BASE</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>ga680604dbcda9e9b31a1639fcffe5230b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick_BASE</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>ga58effaac0b93006b756d33209e814646</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>NVIC_BASE</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>gaa0288691785a5f868238e0468b39523d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB_BASE</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>gad55a7ddb8d4b2398b0c1cfec76c0d9fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCnSCB</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>ga9fe0cd2eef83a8adad94490d9ecca63f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCB</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>gaaaf6477c2bde2f00f99e3c2fd1060b01</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SysTick</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>gacd96c53beeaff8f603fcda425eb295de</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>NVIC</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>gac8e97e8ce56ae9f57da1363a937f8a17</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ITM</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>gabae7cdf882def602cb787bb039ff6a43</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DWT</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>gabbe5a060185e1d5afa3f85b14e10a6ce</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TPI</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>ga8b4dd00016aed25a0ea54e9a9acd1239</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoreDebug</name>
      <anchorfile>group__CMSIS__core__base.html</anchorfile>
      <anchor>gab6e30a2b802d9021619dbb0be7f5d63d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>CMSIS_Core_FunctionInterface</name>
    <title>Functions and Instructions Reference</title>
    <filename>group__CMSIS__Core__FunctionInterface.html</filename>
    <subgroup>CMSIS_Core_NVICFunctions</subgroup>
    <subgroup>CMSIS_Core_SysTickFunctions</subgroup>
    <subgroup>CMSIS_core_DebugFunctions</subgroup>
    <subgroup>CMSIS_Core_RegAccFunctions</subgroup>
  </compound>
  <compound kind="group">
    <name>CMSIS_Core_NVICFunctions</name>
    <title>NVIC Functions</title>
    <filename>group__CMSIS__Core__NVICFunctions.html</filename>
    <member kind="function">
      <type>__STATIC_INLINE void</type>
      <name>NVIC_SetPriorityGrouping</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>ga77cfbb35a9d8027e392034321bed6904</anchor>
      <arglist>(uint32_t PriorityGroup)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE uint32_t</type>
      <name>NVIC_GetPriorityGrouping</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>ga394f7ce2ca826c0da26284d17ac6524d</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE void</type>
      <name>NVIC_EnableIRQ</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>ga3349f2e3580d7ce22d6530b7294e5921</anchor>
      <arglist>(IRQn_Type IRQn)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE void</type>
      <name>NVIC_DisableIRQ</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>ga260fba04ac8346855c57f091d4ee1e71</anchor>
      <arglist>(IRQn_Type IRQn)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE uint32_t</type>
      <name>NVIC_GetPendingIRQ</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>gafec8042db64c0f8ed432b6c8386a05d8</anchor>
      <arglist>(IRQn_Type IRQn)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE void</type>
      <name>NVIC_SetPendingIRQ</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>ga3ecf446519da33e1690deffbf5be505f</anchor>
      <arglist>(IRQn_Type IRQn)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE void</type>
      <name>NVIC_ClearPendingIRQ</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>ga332e10ef9605dc6eb10b9e14511930f8</anchor>
      <arglist>(IRQn_Type IRQn)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE uint32_t</type>
      <name>NVIC_GetActive</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>ga47a0f52794068d076c9147aa3cb8d8a6</anchor>
      <arglist>(IRQn_Type IRQn)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE void</type>
      <name>NVIC_SetPriority</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>ga2305cbd44aaad792e3a4e538bdaf14f9</anchor>
      <arglist>(IRQn_Type IRQn, uint32_t priority)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE uint32_t</type>
      <name>NVIC_GetPriority</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>ga1cbaf8e6abd4aa4885828e7f24fcfeb4</anchor>
      <arglist>(IRQn_Type IRQn)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE uint32_t</type>
      <name>NVIC_EncodePriority</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>gadb94ac5d892b376e4f3555ae0418ebac</anchor>
      <arglist>(uint32_t PriorityGroup, uint32_t PreemptPriority, uint32_t SubPriority)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE void</type>
      <name>NVIC_DecodePriority</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>ga4f23ef94633f75d3c97670a53949003c</anchor>
      <arglist>(uint32_t Priority, uint32_t PriorityGroup, uint32_t *pPreemptPriority, uint32_t *pSubPriority)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE void</type>
      <name>NVIC_SystemReset</name>
      <anchorfile>group__CMSIS__Core__NVICFunctions.html</anchorfile>
      <anchor>ga1143dec48d60a3d6f238c4798a87759c</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>CMSIS_Core_SysTickFunctions</name>
    <title>SysTick Functions</title>
    <filename>group__CMSIS__Core__SysTickFunctions.html</filename>
    <member kind="function">
      <type>__STATIC_INLINE uint32_t</type>
      <name>SysTick_Config</name>
      <anchorfile>group__CMSIS__Core__SysTickFunctions.html</anchorfile>
      <anchor>gae4e8f0238527c69f522029b93c8e5b78</anchor>
      <arglist>(uint32_t ticks)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>CMSIS_core_DebugFunctions</name>
    <title>ITM Functions</title>
    <filename>group__CMSIS__core__DebugFunctions.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>ITM_RXBUFFER_EMPTY</name>
      <anchorfile>group__CMSIS__core__DebugFunctions.html</anchorfile>
      <anchor>gaa822cb398ee022b59e9e6c5d7bbb228a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE uint32_t</type>
      <name>ITM_SendChar</name>
      <anchorfile>group__CMSIS__core__DebugFunctions.html</anchorfile>
      <anchor>gac90a497bd64286b84552c2c553d3419e</anchor>
      <arglist>(uint32_t ch)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE int32_t</type>
      <name>ITM_ReceiveChar</name>
      <anchorfile>group__CMSIS__core__DebugFunctions.html</anchorfile>
      <anchor>gac3ee2c30a1ac4ed34c8a866a17decd53</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>__STATIC_INLINE int32_t</type>
      <name>ITM_CheckChar</name>
      <anchorfile>group__CMSIS__core__DebugFunctions.html</anchorfile>
      <anchor>gae61ce9ca5917735325cd93b0fb21dd29</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="variable">
      <type>volatile int32_t</type>
      <name>ITM_RxBuffer</name>
      <anchorfile>group__CMSIS__core__DebugFunctions.html</anchorfile>
      <anchor>ga12e68e55a7badc271b948d6c7230b2a8</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>CMSIS_Core_RegAccFunctions</name>
    <title>CMSIS Core Register Access Functions</title>
    <filename>group__CMSIS__Core__RegAccFunctions.html</filename>
  </compound>
  <compound kind="group">
    <name>CMSIS_Core_InstructionInterface</name>
    <title>CMSIS Core Instruction Interface</title>
    <filename>group__CMSIS__Core__InstructionInterface.html</filename>
  </compound>
  <compound kind="group">
    <name>CRC_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx Cyclic Redundancy Check Engine driver</title>
    <filename>group__CRC__17XX__40XX.html</filename>
  </compound>
  <compound kind="group">
    <name>DAC_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx D/A conversion driver</title>
    <filename>group__DAC__17XX__40XX.html</filename>
    <class kind="struct">LPC_DAC_T</class>
    <member kind="define">
      <type>#define</type>
      <name>DAC_VALUE</name>
      <anchorfile>group__DAC__17XX__40XX.html</anchorfile>
      <anchor>gac8d652cef1a35726a91ef3e5b4bad59c</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DAC_BIAS_EN</name>
      <anchorfile>group__DAC__17XX__40XX.html</anchorfile>
      <anchor>gabdfe0f0d61a206418a2ffdba26653873</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DAC_CCNT_VALUE</name>
      <anchorfile>group__DAC__17XX__40XX.html</anchorfile>
      <anchor>ga23fecd9fa274c0f8387aad48330a0432</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DAC_DBLBUF_ENA</name>
      <anchorfile>group__DAC__17XX__40XX.html</anchorfile>
      <anchor>gaa52ab08dc967f09afb7fcbb15ef1b1c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DAC_CNT_ENA</name>
      <anchorfile>group__DAC__17XX__40XX.html</anchorfile>
      <anchor>gae7c63d487a239e6abaf2bfdced3c67e4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DAC_DMA_ENA</name>
      <anchorfile>group__DAC__17XX__40XX.html</anchorfile>
      <anchor>gab00cf8cef7eee4ff812d53ae52e4b38d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DAC_DACCTRL_MASK</name>
      <anchorfile>group__DAC__17XX__40XX.html</anchorfile>
      <anchor>ga3d9ce4bc003bffdea6fb98da402d2318</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>DAC_CURRENT_OPT_T</name>
      <anchorfile>group__DAC__17XX__40XX.html</anchorfile>
      <anchor>ga44037a99810eb4654e18602c0b4a1567</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>DAC_MAX_UPDATE_RATE_1MHz</name>
      <anchorfile>group__DAC__17XX__40XX.html</anchorfile>
      <anchor>gga44037a99810eb4654e18602c0b4a1567a6cbe5615f534716fd5228b011d36a405</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>DAC_MAX_UPDATE_RATE_400kHz</name>
      <anchorfile>group__DAC__17XX__40XX.html</anchorfile>
      <anchor>gga44037a99810eb4654e18602c0b4a1567a306308068cf75cb045ed9ce3ec9bbfc7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_DAC_Init</name>
      <anchorfile>group__DAC__17XX__40XX.html</anchorfile>
      <anchor>ga677c6f03e4ea92656c4cb3497fbb4a1b</anchor>
      <arglist>(LPC_DAC_T *pDAC)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_DAC_DeInit</name>
      <anchorfile>group__DAC__17XX__40XX.html</anchorfile>
      <anchor>gabc7ee03f31b9085f6e77137e7ed1158d</anchor>
      <arglist>(LPC_DAC_T *pDAC)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_DAC_UpdateValue</name>
      <anchorfile>group__DAC__17XX__40XX.html</anchorfile>
      <anchor>ga8fa415039ac2f63388ff12cf43cc45bf</anchor>
      <arglist>(LPC_DAC_T *pDAC, uint32_t dac_value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_DAC_SetBias</name>
      <anchorfile>group__DAC__17XX__40XX.html</anchorfile>
      <anchor>ga01352564a69b1286d45a65cf06a246f2</anchor>
      <arglist>(LPC_DAC_T *pDAC, uint32_t bias)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_DAC_ConfigDAConverterControl</name>
      <anchorfile>group__DAC__17XX__40XX.html</anchorfile>
      <anchor>gadd2150c87e3fc28f301417f34a5eb287</anchor>
      <arglist>(LPC_DAC_T *pDAC, uint32_t dacFlags)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_DAC_SetDMATimeOut</name>
      <anchorfile>group__DAC__17XX__40XX.html</anchorfile>
      <anchor>ga25d6e27e6b906dc2062257175697b438</anchor>
      <arglist>(LPC_DAC_T *pDAC, uint32_t time_out)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE IntStatus</type>
      <name>Chip_DAC_GetIntStatus</name>
      <anchorfile>group__DAC__17XX__40XX.html</anchorfile>
      <anchor>ga22cbef1d579da4780f4fb330107eb7cb</anchor>
      <arglist>(LPC_DAC_T *pDAC)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>EEPROM_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx EEPROM driver</title>
    <filename>group__EEPROM__17XX__40XX.html</filename>
  </compound>
  <compound kind="group">
    <name>EMC_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx External Memory Controller driver</title>
    <filename>group__EMC__17XX__40XX.html</filename>
  </compound>
  <compound kind="group">
    <name>ENET_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx Ethernet driver (2)</title>
    <filename>group__ENET__17XX__40XX.html</filename>
    <class kind="struct">ENET_MAC_T</class>
    <class kind="struct">ENET_TRANSFER_INFO_T</class>
    <class kind="struct">ENET_CONTROL_T</class>
    <class kind="struct">ENET_RXFILTER_T</class>
    <class kind="struct">ENET_MODULE_CTRL_T</class>
    <class kind="struct">LPC_ENET_T</class>
    <class kind="struct">ENET_RXDESC_T</class>
    <class kind="struct">ENET_RXSTAT_T</class>
    <class kind="struct">ENET_TXDESC_T</class>
    <class kind="struct">ENET_TXSTAT_T</class>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC1_MASK</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gadc9dc4a912f946aef0f397e01c745e23</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC1_RXENABLE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga1c0b1a53b97bbb37a634729dbb1f7c64</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC1_PARF</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga4c1b1afa07ca17af9a536e55b5214651</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC1_RXFLOWCTRL</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga715c51b94b6651715bec8a58d63ede58</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC1_TXFLOWCTRL</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga9621f7d3f1df5610f251f74a90d4bfd9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC1_LOOPBACK</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga5d10c874131056c389ecb1357be5e7a2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC1_RESETTX</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaf1ab84b82270514182f96bf19db18b38</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC1_RESETMCSTX</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga929295618f7227006918b172c37274ba</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC1_RESETRX</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga08220afd93ff8aaee439369cbda83d55</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC1_RESETMCSRX</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaededc9960450035cfa31578c67d9765a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC1_SIMRESET</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gad1496577c2254ab94911ee01420744ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC1_SOFTRESET</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga7b0dd5ccbaca44db89764a33f221b225</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC2_MASK</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga090fe0a3c430ad527911dd8948323e56</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC2_FULLDUPLEX</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gada771902490e3391858094a565832f75</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC2_FLC</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga572da08adf7e20dd2e9689c4a1bc6ee8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC2_HFEN</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga8937f821f7f1aa8aee29db2ade25085f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC2_DELAYEDCRC</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga87f7319213d7ad9c90e61ca1330ab531</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC2_CRCEN</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gad21fa9aa92b57dff934b097738297a38</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC2_PADCRCEN</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga2e96a6046b10faf1b2e490ca0c44fe5e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC2_VLANPADEN</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaff48f524d6da4e540a30879dabebd6c4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC2_AUTODETPADEN</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga7d73ffbca6a6db5a5262bb59e33627ed</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC2_PPENF</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gabd9c8010102840735375d5b1bfd3ca73</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC2_LPENF</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaa1c082cfc7d20fb9683a0a17c355fccf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC2_NOBACKOFF</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga914e8c639aaf94a0b451153485a2c1dc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC2_BP_NOBACKOFF</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga43f27812a197029734f14e6d43671564</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAC2_EXCESSDEFER</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaadf555a1169009b0dbb728dbc5046b14</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_IPGT_BTOBINTEGAP</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga7cdae0530c55e9f236d7d9d5bde3391a</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_IPGT_FULLDUPLEX</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gabcaea91a823fd58281d82d9dcb754c2f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_IPGT_HALFDUPLEX</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga4c98e396a39809243d38a68b27eeec02</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_IPGR_NBTOBINTEGAP2</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gab665d33e25749c26b32f51b2a9e14b79</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_IPGR_P2_DEF</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gacb2c77e9648c3945d156149617c6d620</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_IPGR_NBTOBINTEGAP1</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga346ffe9920e1abd20950e001e920d1fb</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_IPGR_P1_DEF</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gabb7222a12e8e058581bca37fd98cbbe5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_CLRT_RETRANSMAX</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga2e261dd603d1a1909d1b00e11634a958</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_CLRT_COLLWIN</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga541a85ac1f97834806ea7bd907ca7ef5</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_CLRT_DEF</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga3840d22e13373045c63bc14bb36800a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MAXF_MAXFLEN</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gafa842b5b6e26b8e4890fcedf64d52a9a</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_SUPP_100Mbps_SPEED</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga66870c22b0f10f275b7b2c1e6d0709a0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TEST_SCPQ</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gafa057fdcf20418b56df28532c048f038</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TEST_TESTPAUSE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gafa31bcfcbdb16b2dbfdc132568826153</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TEST_TESTBP</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga89c6649c9c320cd57d5bc82b1ecf42ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MCFG_SCANINC</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gade8846eaaed89450478cf7e8dd07beb2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MCFG_SUPPPREAMBLE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga9f39657f690648231761d2aea5aee7cb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MCFG_CLOCKSEL</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga00253a49de1cfbbb4ad88f7563ce5954</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MCFG_RES_MII</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaf31c596c785314febc778a974cefa64e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MCFG_RESETMIIMGMT</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gafd634c8258b41a78b89aff1fa4558996</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MCMD_READ</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga07dfdda78003dd5af6254fc863ae019e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MCMD_SCAN</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga77d7e45ecfbd3f0bace6900cc69b8beb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MII_WR_TOUT</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga41de953453bd483f8c9d5805c54010b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MII_RD_TOUT</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga68db523c6f21026af56d79f5af27b9d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MADR_REGADDR</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga564fa0d5d9bd63bd38c696647b9377cd</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MADR_PHYADDR</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga57a3e26d33732b010970cf90d735484c</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MWTD_DATA</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gae7bcd90e71467290d7bd0d5fe5c8f614</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MRDD_DATA</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga0ea5fc7eb540696086df0392804f7b86</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MIND_BUSY</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga9431ce0c183ce96c4fe9956d850f1f5e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MIND_SCANNING</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga748c92254d0cf1e612418e1d48d6793f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MIND_NOTVALID</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gab961efeb9758ffffe627a821cb525f06</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_MIND_MIILINKFAIL</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gad05320f311da8c5386dfbbe950eca62b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_COMMAND_RXENABLE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga9cbb8fee3c8f649b53585358bd4b78a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_COMMAND_TXENABLE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaad9ec2211ed810f3b3c37156c152a307</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_COMMAND_REGRESET</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga6745bb544166e94ec0ec4635ce85c2a2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_COMMAND_TXRESET</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gab9c38db14b3c0edbe8f9a9062d653612</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_COMMAND_RXRESET</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gabcbc994bca6d1eb4b677e8f8c58e3ef9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_COMMAND_PASSRUNTFRAME</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga22c8103c39e17d184acb4956428022c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_COMMAND_PASSRXFILTER</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gad15a361a9ea2dae055b13d1416dde2df</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_COMMAND_TXFLOWCONTROL</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga720037c58537b876089d9d3f4df0130f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_COMMAND_RMII</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga933d30adf6557d69ddcd1bd0ff3dd3b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_COMMAND_FULLDUPLEX</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gac5380c0bb8853772b22db6b37b2c812e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_STATUS_RXSTATUS</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gad0face01c298deb04c2a6df44839db44</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_STATUS_TXSTATUS</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gae74d3a5997ed8bf6618a4896b12403e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TSV0_CRCERR</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gadb4cb33d047889ef5584d58ee0110140</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TSV0_LCE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaca1a7e04bad3d3a31621f81ee11e7833</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TSV0_LOR</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gadf4b40d301d071e9f0a7819ad45dc534</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TSV0_DONE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga93ca8c0d6c7224b4a8c41a077bbdf40d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TSV0_MULTICAST</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga35811b54e0e0ff6e878184c9bc69c279</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TSV0_BROADCAST</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga8c81671c6a9577c6bd4cbfca14d0d699</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TSV0_PACKETDEFER</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gac2d842291755f2488ce8b4ad9d189f6f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TSV0_EXDF</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga7cb26ea3f926fb8f7950bbbc98eb2d61</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TSV0_EXCOL</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga70a4c2d8bcefc36d60b2a006ff3f7496</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TSV0_LCOL</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga51155a3f13ea78c0a858b402212b8b67</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TSV0_GIANT</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga2cc9638b9a72a6aca275ce4aed3701a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TSV0_UNDERRUN</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga62faeddfcb2c47a79c79eafd0d53ae70</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TSV0_TOTALBYTES</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaced2c895c68a65e25f18810332a9e60c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TSV0_CONTROLFRAME</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga2ab4cf2c962a57d2b59cdac5324caf08</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TSV0_PAUSE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga389586db52739a1a88da7059b813cf80</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TSV0_BACKPRESSURE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga0a02af6e44148eb10ad3872f558a7d75</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TSV0_VLAN</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gacc8396f1c837bf51215f1f29a062bba4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TSV1_TBC</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga85659c24bcb857d841bc18e037ee3a78</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TSV1_TCC</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga9a2d8f8be313560a8d1858f623d5d599</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RSV_RBC</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gabebe49780c8611118ce1596cf3d77436</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RSV_PPI</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaa45464496eefb8f66d8281ceefe3b198</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RSV_RXDVSEEN</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga96fe41c39224b433c1fe074b76161ec7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RSV_CESEEN</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga32813050731261f0abb8a938a49db29d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RSV_RCV</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gac21ffe7a15a96229eb770a7f1dcdb7dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RSV_CRCERR</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga7481775906d508f6a529b46520e58a97</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RSV_LCERR</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gafadac62e2e18ad75a244e4227509931d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RSV_LOR</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaae8c1aa30582b7a66657c78b54bcf43a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RSV_ROK</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga32032cd510a057a50e4e08acf50ddcde</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RSV_MULTICAST</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaf0bd8c3446555f296b06d4865c8cb6cd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RSV_BROADCAST</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga87f8e1929545347ff2ddd8ee658ef54b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RSV_DRIBBLENIBBLE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga5c2ebce59acc2a9ca4e4569bcc6290ca</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RSV_CONTROLFRAME</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga4ad18351df747e65f1b3fff139c8d552</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RSV_PAUSE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga7ad487168e3201b01eab8504e623d79b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RSV_UO</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gae2f33f7ad025f84d58c97ccb9fecf89a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RSV_VLAN</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gabe88542e8c0a6fb6ea649cba94d2028d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_FLOWCONTROLCOUNTER_MC</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga6792fccacadbec16632406f00afbb202</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_FLOWCONTROLCOUNTER_PT</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga08d75c6c9a79476c9473d49efea2eb4b</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_FLOWCONTROLSTATUS_MCC</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaa8ef1915c4211c216269ae6f5c7bad26</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RXFILTERCTRL_AUE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga61d70abfbcb89e08b14d7aa571520db3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RXFILTERCTRL_ABE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaa63bf71c0439dc5176b4af167f4c7f75</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RXFILTERCTRL_AME</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga868a51913649ab45dce2ed495d09e401</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RXFILTERCTRL_AUHE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga14e7ab247ade314679d3546278102373</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RXFILTERCTRL_AMHE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga9d6abb88eb71199e6b8206c71ecc1148</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RXFILTERCTRL_APE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga86aaaa49bc70fe343262a4373c056c87</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RXFILTERCTRL_MPEW</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gad2cef615764124959531e6c8fae552b7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RXFILTERCTRL_RFEW</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga90e5fb9b8c5350d668d5fac5a40f297c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RXFILTERWOLSTATUS_AUW</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga23b957a106202e22a74ade7e44f2136c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RXFILTERWOLSTATUS_ABW</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gab827c42b0f15cd643b878e3949c86205</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RXFILTERWOLSTATUS_AMW</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gad27e9fe84adddb132645b353d479e4f1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RXFILTERWOLSTATUS_AUHW</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gad81a6e913cfad896e79c391229a68bd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RXFILTERWOLSTATUS_AMHW</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga0b050f561283fcfe8d2f76a6c1b000f2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RXFILTERWOLSTATUS_APW</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gafe115b309fb518d3fb178e41acecf62d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RXFILTERWOLSTATUS_RFW</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaeda9888b5a4d439b3e89635ee7044fe4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RXFILTERWOLSTATUS_MPW</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga46ecfc247fbae6521566f024e6baa0c5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RXFILTERWOLSTATUS_BITMASK</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga244047f695bb766a84b85a2667a71e0e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_INT_RXOVERRUN</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga037e3a6511ff23e15616f883f73e79d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_INT_RXERROR</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gad798142f39d48af8fc32d00b7c43bf03</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_INT_RXFINISHED</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga77e2f10bb242ca14d8b0fb555ba85482</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_INT_RXDONE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga781ad1e525dde056be7d72df24ed0ee2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_INT_TXUNDERRUN</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gae41a8df58ad75321ba10799774141411</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_INT_TXERROR</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga6930081cf2294320579ddea844355ae5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_INT_TXFINISHED</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gae86d066808374ed933169271a2fd6932</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_INT_TXDONE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga4aa84a9aeae31c299a172a2393ad0e41</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_INT_SOFT</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gabf841ab6a0cb5f2a0f1db26d83423029</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_INT_WAKEUP</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga1741a903d6aad6718911587aa842ce8d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_POWERDOWN_PD</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga6acb31f9219726651b229e11719950e9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RCTRL_SIZE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaeae1e7c66c6db99741e78b73aed044fa</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RCTRL_INT</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga12ee900d60382c12ef287bc722a2047a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RHASH_SA</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gae4a45f7daf153627307963edb349072b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RHASH_DA</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga28fbef246f1b3c1145c244c5d2b3768a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RINFO_SIZE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gafaad5a1eff605c6503d88c256d815ac6</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RINFO_CTRL_FRAME</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gabc12c8acaedae0c90e8c10777a1efc35</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RINFO_VLAN</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga04fdec12cd4f4055863a1bca40a7afe1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RINFO_FAIL_FILT</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaf3bb0d91c0511c58691d7189b551fe79</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RINFO_MCAST</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gafd8d58d02c5805332909dd4a7213be4a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RINFO_BCAST</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaa602c5e08609912f99b95fb4342b3036</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RINFO_CRC_ERR</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gad7ea1586c49309069d1d72a4ebe5aa89</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RINFO_SYM_ERR</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga6c8d6c27397230acdab0d86aae333adb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RINFO_LEN_ERR</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga9a8b65ee2091950cb6a66c2cf2f8be79</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RINFO_RANGE_ERR</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga2aca15d552258eb4116193c9ea803e69</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RINFO_ALIGN_ERR</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga4e4fcb457d4d1a16900c13f7a5a2db46</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RINFO_OVERRUN</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gac8b350c71c904bca07eab6e4338303cf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RINFO_NO_DESCR</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaf961528973a113b3917e4c493d50d2e4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RINFO_LAST_FLAG</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gac84556562920c84d6c161c03c6ed4a52</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RINFO_ERR</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga15c3af4e6532d6de88860eb58046373a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_RINFO_ERR_MASK</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga07c87e5b7136302124084fee594d2d4a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TCTRL_SIZE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga2acf2f4610df8309e83344164ccbc107</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TCTRL_OVERRIDE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga64b15e381cbd1fa942764060e167327f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TCTRL_HUGE</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gae22a34de314676fcebc0497288fc5392</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TCTRL_PAD</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga1e82cc8733b002c96104d063ac42d79a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TCTRL_CRC</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gac92f044f648d5e928f609e9b606b92b1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TCTRL_LAST</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga77c27155fbe90a33eccb8a4331f1c2b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TCTRL_INT</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga38618011f4bed353ba352c639b511268</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TINFO_COL_CNT</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gab483256508551c09e411706f45c81c70</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TINFO_DEFER</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gace4cfc3d6e1db023f33281b1593a97ae</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TINFO_EXCESS_DEF</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga6572529bff7597c6951184bd325195da</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TINFO_EXCESS_COL</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga5c85ac99ceb6444c5beb2afbcafee2ec</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TINFO_LATE_COL</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga28ff3c0b58337f73dcf19af5c77b1813</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TINFO_UNDERRUN</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga1c695274991bb04562d674a2869bd2c7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TINFO_NO_DESCR</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga217a2ea7ea34a8ce88c5252d838943ba</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_TINFO_ERR</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaeb11bfbbf43a3c82fb88e56a5e68e7bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENET_ETH_MAX_FLEN</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gacba3b6d78224fc1ee628088edbff5609</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>ENET_BUFF_STATUS_T</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gab29d71439401a8d4f68643acfd10959b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_ENET_Reset</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gae6567d8fb1d427ecf30ce65f48a074b4</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_ENET_SetADDR</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga6d915db1de0ba98ac0cdb1df8c59515b</anchor>
      <arglist>(LPC_ENET_T *pENET, const uint8_t *macAddr)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_ENET_SetupMII</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga9694421dbd0f331895fcf514fd18c938</anchor>
      <arglist>(LPC_ENET_T *pENET, uint32_t div, uint8_t addr)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_ENET_StartMIIWrite</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gade9f31bbc04119bc06638fd8ce874f73</anchor>
      <arglist>(LPC_ENET_T *pENET, uint8_t reg, uint16_t data)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_ENET_StartMIIRead</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaca2166605d385fd5150f173cd33a3ac2</anchor>
      <arglist>(LPC_ENET_T *pENET, uint8_t reg)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE bool</type>
      <name>Chip_ENET_IsMIIBusy</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga2ae5389a2b99d6006980083d02667e07</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>uint16_t</type>
      <name>Chip_ENET_ReadMIIData</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gab6d3dfc1cc671503532d00947f236e1d</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_ENET_TXEnable</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gacc7e455d4b168d2405b4ef9dda242488</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_ENET_TXDisable</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaaf0d9ef785afa5a847431e86520430c9</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_ENET_RXEnable</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaccc601f3393807fe6255771fb1c47a0b</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_ENET_RXDisable</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaef60a4790dcce43254081ea7fe5a22d4</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_ENET_ResetTXLogic</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga23cc17a06ee9e42b4f1327cd792a2a8e</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_ENET_ResetRXLogic</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaddd915f4c5b177bef0cd29d882ed3e6f</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_ENET_EnableRXFilter</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga87335faab88d9a16579596f4a0a6ce14</anchor>
      <arglist>(LPC_ENET_T *pENET, uint32_t mask)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_ENET_DisableRXFilter</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga591bdc31e1ca992c3ac642c8d64d3ac9</anchor>
      <arglist>(LPC_ENET_T *pENET, uint32_t mask)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_ENET_SetFullDuplex</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga0881ac9e0e98620c0e65f9a6d7a43240</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_ENET_SetHalfDuplex</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga6d7031584471e30e89ded1fff5f3340d</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_ENET_Set100Mbps</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gad78db17fecab956142cb20d9a00e39e0</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_ENET_Set10Mbps</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga996dae6b7276fc708594ce239f40785f</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_ENET_InitTxDescriptors</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga1e57967b888b1a7bfd4313fd9e70c696</anchor>
      <arglist>(LPC_ENET_T *pENET, ENET_TXDESC_T *pDescs, ENET_TXSTAT_T *pStatus, uint32_t descNum)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_ENET_InitRxDescriptors</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga04559be4fcfb24203ce9e372750383c1</anchor>
      <arglist>(LPC_ENET_T *pENET, ENET_RXDESC_T *pDescs, ENET_RXSTAT_T *pStatus, uint32_t descNum)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint16_t</type>
      <name>Chip_ENET_GetTXProduceIndex</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga276d359d20f70fdb005740c3287dd2d7</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint16_t</type>
      <name>Chip_ENET_GetTXConsumeIndex</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga6ed1ce4f6bde22c64966d2d513cc7a6d</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint16_t</type>
      <name>Chip_ENET_GetRXProduceIndex</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gaa70d279d6bb31b783656b31fe3a1cf8f</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint16_t</type>
      <name>Chip_ENET_GetRXConsumeIndex</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gae79ff7cdffb9e43c95fb309ca7c16aa2</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>ENET_BUFF_STATUS_T</type>
      <name>Chip_ENET_GetBufferStatus</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gafed5419d7a6be4580641e557429a63ad</anchor>
      <arglist>(LPC_ENET_T *pENET, uint16_t produceIndex, uint16_t consumeIndex, uint16_t buffSize)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_ENET_GetFillDescNum</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gac5f44f20f55847c4c9b9906f051aeca3</anchor>
      <arglist>(LPC_ENET_T *pENET, uint16_t produceIndex, uint16_t consumeIndex, uint16_t buffSize)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_ENET_GetFreeDescNum</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga4d0afde7abd542590c1bb53fba4d90b1</anchor>
      <arglist>(LPC_ENET_T *pENET, uint16_t produceIndex, uint16_t consumeIndex, uint16_t buffSize)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE bool</type>
      <name>Chip_ENET_IsTxFull</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga593f01cc1a9e665c8c3293c0599b254c</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE bool</type>
      <name>Chip_ENET_IsRxEmpty</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga19412cfa444b976f9ec13554da17d210</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>uint16_t</type>
      <name>Chip_ENET_IncTXProduceIndex</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga9a1d6a9086bfbca3c856ad2b3b90546c</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>uint16_t</type>
      <name>Chip_ENET_IncRXConsumeIndex</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga4246666fbc4e9be17c45e6837a0e5dca</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_ENET_EnableInt</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gac0afbb3c822a9e581416fd500b922313</anchor>
      <arglist>(LPC_ENET_T *pENET, uint32_t mask)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_ENET_DisableInt</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga708d5e95e8c437779028713075a55117</anchor>
      <arglist>(LPC_ENET_T *pENET, uint32_t mask)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_ENET_GetIntStatus</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga5931404e2db29a1b4cdf357edf79504d</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_ENET_ClearIntStatus</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga0a574e2cc8946c200d71152722ac30d7</anchor>
      <arglist>(LPC_ENET_T *pENET, uint32_t mask)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_ENET_Init</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga24a13ad31f70570906a4b885754f8953</anchor>
      <arglist>(LPC_ENET_T *pENET, bool useRMII)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_ENET_DeInit</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>ga94eb7a70f4023c83ca18e4e675ad0b32</anchor>
      <arglist>(LPC_ENET_T *pENET)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_ENET_FindMIIDiv</name>
      <anchorfile>group__ENET__17XX__40XX.html</anchorfile>
      <anchor>gad107338dec5ee221957fafe5e711e42e</anchor>
      <arglist>(LPC_ENET_T *pENET, uint32_t clockRate)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>FMC_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx FLASH Memory Controller driver</title>
    <filename>group__FMC__17XX__40XX.html</filename>
    <class kind="struct">LPC_FMC_T</class>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_FMC_ComputeSignature</name>
      <anchorfile>group__FMC__17XX__40XX.html</anchorfile>
      <anchor>ga12cde229d831a9e32208b038651ca228</anchor>
      <arglist>(uint32_t start, uint32_t stop)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_FMC_ComputeSignatureBlocks</name>
      <anchorfile>group__FMC__17XX__40XX.html</anchorfile>
      <anchor>ga1341098e386d4291bf7ad3fb43b6d3bc</anchor>
      <arglist>(uint32_t start, uint32_t blocks)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_FMC_ClearSignatureBusy</name>
      <anchorfile>group__FMC__17XX__40XX.html</anchorfile>
      <anchor>gab7ec5bd928d355795e7cd4f56371a9ec</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE bool</type>
      <name>Chip_FMC_IsSignatureBusy</name>
      <anchorfile>group__FMC__17XX__40XX.html</anchorfile>
      <anchor>gaf2d1ec53b9f1e3e79cd6e9389597220d</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_FMC_GetSignature</name>
      <anchorfile>group__FMC__17XX__40XX.html</anchorfile>
      <anchor>ga800317f87d55b213bdbd9aebe7c2a6b6</anchor>
      <arglist>(int index)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>GPDMA_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx General Purpose DMA driver</title>
    <filename>group__GPDMA__17XX__40XX.html</filename>
    <class kind="struct">GPDMA_CH_T</class>
    <class kind="struct">LPC_GPDMA_T</class>
    <class kind="struct">GPDMA_CFG_T</class>
    <class kind="struct">GPDMA_DESC_T</class>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_NUMBER_CHANNELS</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gaf7c43b3d13c91c30ddf67e479966d5cd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACHx_BitMask</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gae45636a0c2eff96d4ebf701077c27def</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACCxControl_TransferSize</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga0e3ee35f724f4ef0cc8e91dfaec761e4</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACCxControl_SBSize</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga1f5c9d534965c6a89ea22ca3fa48d859</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACCxControl_DBSize</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga18c40b7931f0b6cfe8b81f9a982c0641</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACCxControl_SWidth</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga2ee63289c5e248a07ea901e233e1dd00</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACCxControl_DWidth</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga67bb6ed286afe6d4091c0fcd8799b451</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACCxControl_SI</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gaa9b006e86536835dfe6f7034ee25d12a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACCxControl_DI</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gaddcec41c911bbd0911391e6195c1c040</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACCxControl_Prot1</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga883ee41e16f8df248437075cebab7993</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACCxControl_Prot2</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gabe38faff26ee3951122c23ff1425d70a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACCxControl_Prot3</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gaf6a439bbf5a4b082fa7f36effca23f15</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACCxControl_I</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga6ef0b54b0190c139796679d6db1e6e19</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACConfig_E</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga253822f2712564a42379a76d9447cde4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACConfig_M</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga0f2a7e8c8704f5a897f911ac3a8617e3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACCxConfig_E</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga1c7608bb37d512277e42672ee4e785a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACCxConfig_SrcPeripheral</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gaccb73c66c5349ec79a3f332d77554f0c</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACCxConfig_DestPeripheral</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga3b65fa1394d8c93789dd249c4e8a7568</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACCxConfig_TransferType</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga63bace54233ed2446a1b442b46243833</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACCxConfig_IE</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga0fb4c3e9768c0a757b6ff25f77b75a26</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACCxConfig_ITC</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gac7c379cbf11a214f436620e4f7a7ee2a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACCxConfig_L</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga37a55c8ebde3d56defbf6281534237fe</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACCxConfig_A</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gae28eb1e74ecafc5e6170e3f7a8688335</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_DMACCxConfig_H</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga987a4bb2d26cf2ef476483f347ad49ff</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_CONN_ARR</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga55cd089ac7a2f89454b5a11119a8b720</anchor>
      <arglist>(base, index)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_BSIZE_1</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gafd44c148b998d28bc156b947794ad011</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_BSIZE_4</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga768f1f0d1cf1a2611573362ed7b6a18d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_BSIZE_8</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga21239562985215b67c024871f804f0bd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_BSIZE_16</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga6a56379136d5416a0799642fa2217fe2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_BSIZE_32</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga2ebaf7a771f5bf603ecfed0503a66c5c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_BSIZE_64</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gababd7d98382dd69ae09a9a4b447a0977</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_BSIZE_128</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga808a32404cd29d656eb9ee31dacacdbb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_BSIZE_256</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gad11cb83bebe2dc426f62cded244ea391</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_WIDTH_BYTE</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gacc6deb5ab0e06eded3cdd151754db8f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_WIDTH_HALFWORD</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gab28fc48561886a87a1c87eeb7078ef8b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_WIDTH_WORD</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gad611897f330a6ec01b0699b244b132bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_CONTROLLER</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gab824afb0509655911c41fdf19b81b4ec</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SRC_PER_CONTROLLER</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga850a296f501c6a30228f8df251b73767</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DST_PER_CONTROLLER</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga09bdacfb969a90a403f99d379c34638c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPDMA_HANDLE_NULL</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga6f8b9841938ab70acd17c4687caacf0d</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>void(*</type>
      <name>GPDMA_CBK_T</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga3b8aae8b70f6dcf92164496ee7f645dd</anchor>
      <arglist>)(GPDMA_STATUS_T, void *)</arglist>
    </member>
    <member kind="typedef">
      <type>uint32_t</type>
      <name>GPDMA_HANDLE_T</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga1075c74b4796b5396e2a4bf9525147ec</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>GPDMA_STATECLEAR_T</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gabbb281ef4b818f2e60167cf766f94fdb</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>GPDMA_STATCLR_INTTC</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ggabbb281ef4b818f2e60167cf766f94fdba9a0c8256eb0f656d56f065914219c96c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>GPDMA_STATCLR_INTERR</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ggabbb281ef4b818f2e60167cf766f94fdbaf6e975b9dfa7d659d18a377c7873b92a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>GPDMA_STATUS_T</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga2f4aa97bd0ffa5046c8e2b17028d99cc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>GPDMA_STAT_TC</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gga2f4aa97bd0ffa5046c8e2b17028d99cca8a00ea025aacdcf50773f8894649cbd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>GPDMA_STAT_ERR</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gga2f4aa97bd0ffa5046c8e2b17028d99ccaed205ac5b7eb37d642521bbca8331dd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>GPDMA_FLOW_CONTROL_T</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga2cb59b641cd840f22780c44be1208133</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>GPDMA_TRANSFERTYPE_M2M_CONTROLLER_DMA</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gga2cb59b641cd840f22780c44be1208133ae5ff87adb4451f2a695c9d21c3c52c59</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gga2cb59b641cd840f22780c44be1208133a331474560497f41cfe921b0e55ce8722</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gga2cb59b641cd840f22780c44be1208133aa2ae587fb924cb679f51250470927e34</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>GPDMA_TRANSFERTYPE_P2P_CONTROLLER_DMA</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gga2cb59b641cd840f22780c44be1208133a29ec59e967f3a1841002ef740552c1d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>GPDMA_TRANSFERTYPE_P2P_CONTROLLER_DestPERIPHERAL</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gga2cb59b641cd840f22780c44be1208133a176e307292918213de220bdae957ad6d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>GPDMA_TRANSFERTYPE_M2P_CONTROLLER_PERIPHERAL</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gga2cb59b641cd840f22780c44be1208133a640177df7a3c696a9ccab9a09dcdbc0c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>GPDMA_TRANSFERTYPE_P2M_CONTROLLER_PERIPHERAL</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gga2cb59b641cd840f22780c44be1208133aaeaf72b20cee326722ee7650405e2e43</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>GPDMA_TRANSFERTYPE_P2P_CONTROLLER_SrcPERIPHERAL</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gga2cb59b641cd840f22780c44be1208133a4615bdb6a415ddc02f8eab20a700a17d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_GPDMA_Init</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga21c99277e4579af4be9d9311a03b5542</anchor>
      <arglist>(LPC_GPDMA_T *pGPDMA)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_GPDMA_DeInit</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga673cc6cab2ad87185f5f5d0ff8424075</anchor>
      <arglist>(LPC_GPDMA_T *pGPDMA)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_GPDMA_Interrupt</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga3d8ea5b37e569c27be94bce44d0b3b48</anchor>
      <arglist>(LPC_GPDMA_T *pGPDMA)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_GPDMA_PrepareDescriptor</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gabc5ce801769950900276709be4a9e093</anchor>
      <arglist>(LPC_GPDMA_T *pGPDMA, GPDMA_CFG_T *pCFG, GPDMA_DESC_T *pDESC)</arglist>
    </member>
    <member kind="function">
      <type>GPDMA_HANDLE_T</type>
      <name>Chip_GPDMA_Transfer</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>gaa0e1dd7a537cefb7348901c8e95a8ce7</anchor>
      <arglist>(LPC_GPDMA_T *pGPDMA, GPDMA_CFG_T *pCFG, GPDMA_DESC_T *pDESC, uint32_t minPriority)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_GPDMA_Stop</name>
      <anchorfile>group__GPDMA__17XX__40XX.html</anchorfile>
      <anchor>ga68f499ed04fe103368b0ed6959d6d6e1</anchor>
      <arglist>(LPC_GPDMA_T *pGPDMA, GPDMA_HANDLE_T Handle)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>GPIO_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx GPIO driver</title>
    <filename>group__GPIO__17XX__40XX.html</filename>
    <class kind="struct">LPC_GPIO_T</class>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIO_Init</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>gaeaca39372c8ff9f288243a20dd2259ce</anchor>
      <arglist>(LPC_GPIO_T *pGPIO)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIO_DeInit</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga260a25e615613c663745ed72203dd7a6</anchor>
      <arglist>(LPC_GPIO_T *pGPIO)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIO_SetPinState</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga2d8db9f0a52f061d64e8cada713ae03e</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t port, uint8_t pin, bool setting)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIO_WritePortBit</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga4ce4cce2499df5cee49a591ee5be6a48</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint32_t port, uint8_t pin, bool setting)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE bool</type>
      <name>Chip_GPIO_GetPinState</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga9f0e35190f01c706564a88f1f88cf716</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t port, uint8_t pin)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE bool</type>
      <name>Chip_GPIO_ReadPortBit</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga74ca55b747c3a51c1ae3e47645da0c75</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint32_t port, uint8_t pin)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIO_SetPinDIROutput</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>gadd0450341df62f7e13ee57cd249fe2a2</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t port, uint8_t pin)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIO_SetPinDIRInput</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga0c20af0c6b9cee61714643bef6614485</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t port, uint8_t pin)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIO_SetPinDIR</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>gac384ba462a45291528e95945c3273772</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t port, uint8_t pin, bool output)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIO_WriteDirBit</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga4b726d64407e21f40d4fc23f16da04ab</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t port, uint8_t bit, bool setting)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE bool</type>
      <name>Chip_GPIO_GetPinDIR</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga5f36fe1a2c2b2eb958133c27cb65bee5</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t port, uint8_t pin)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE bool</type>
      <name>Chip_GPIO_ReadDirBit</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga8f436d49d7737db583aa4e71bc21937b</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint32_t port, uint8_t bit)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIO_SetDir</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>gacc2acb3d50b47954b25ef0ac439993d8</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t portNum, uint32_t bitValue, uint8_t out)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIO_SetPortDIROutput</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>gaeeb23db039b2bf56ed96a9d6112fab69</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t port, uint32_t pinMask)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIO_SetPortDIRInput</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga09e433572db2ec8a3e30e508ee5bcbd0</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t port, uint32_t pinMask)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIO_SetPortDIR</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga6ceffe51a34be90a077b22657b1f90f0</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t port, uint32_t pinMask, bool outSet)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_GPIO_GetPortDIR</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga06f22d0dfeb6a06b8280df9bead0fc4b</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t port)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIO_SetPortMask</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>gaee2115e847a281cf3da40209ce6336c8</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t port, uint32_t mask)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_GPIO_GetPortMask</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga6b8d692c29a4d64326130bd237826a4b</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t port)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIO_SetMaskedPortValue</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga45e5422fd37f991f4ff46cdc0090efb4</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t port, uint32_t value)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_GPIO_GetMaskedPortValue</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>gaa5b183d37e81118b37b30a71279c032b</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t port)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIO_SetPortValue</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga095eb3a5396553fa88997b4cfd21e644</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t port, uint32_t value)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_GPIO_GetPortValue</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga14c7161208fed3f7ac4e62953353ab9b</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t port)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIO_SetPortOutHigh</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga8a9b2dd9c70e835b718c2edc5b9701af</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t port, uint32_t pins)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIO_SetValue</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga88bed30fb124192d45c7bed021636643</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t portNum, uint32_t bitValue)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIO_SetPinOutHigh</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga1447549f6e88a29b5589326f177d4a96</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t port, uint8_t pin)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIO_SetPortOutLow</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga2f85c3b0c7a48d68a508a0f94f6e691e</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t port, uint32_t pins)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIO_ClearValue</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga94367ee41db6fa49cfba6605324e8d07</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t portNum, uint32_t bitValue)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIO_SetPinOutLow</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga0e06760450b8e7a2c71920b06bcd6286</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t port, uint8_t pin)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIO_SetPinToggle</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>ga5437e5a8ae2ce7662b605f9961aad8f5</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t port, uint8_t pin)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_GPIO_ReadValue</name>
      <anchorfile>group__GPIO__17XX__40XX.html</anchorfile>
      <anchor>gaadfa7274313165ae6dec004a125a1bcf</anchor>
      <arglist>(LPC_GPIO_T *pGPIO, uint8_t portNum)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>GPIOINT_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx GPIO Interrupt driver</title>
    <filename>group__GPIOINT__17XX__40XX.html</filename>
    <class kind="struct">GPIOINT_PORT_T</class>
    <class kind="struct">LPC_GPIOINT_T</class>
    <member kind="enumeration">
      <type></type>
      <name>LPC_GPIOINT_PORT_T</name>
      <anchorfile>group__GPIOINT__17XX__40XX.html</anchorfile>
      <anchor>ga41631ac5e33fde341c0afe680ded9fee</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>GPIOINT_PORT0</name>
      <anchorfile>group__GPIOINT__17XX__40XX.html</anchorfile>
      <anchor>gga41631ac5e33fde341c0afe680ded9feea976b0fb82055c52332950506a6621bba</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>GPIOINT_PORT2</name>
      <anchorfile>group__GPIOINT__17XX__40XX.html</anchorfile>
      <anchor>gga41631ac5e33fde341c0afe680ded9feeafd8afad46d3e74afccbdd0c806bc3aae</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIOINT_Init</name>
      <anchorfile>group__GPIOINT__17XX__40XX.html</anchorfile>
      <anchor>gaa35c955336ff3dc2f850f7e6cf9feee0</anchor>
      <arglist>(LPC_GPIOINT_T *pGPIOINT)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIOINT_DeInit</name>
      <anchorfile>group__GPIOINT__17XX__40XX.html</anchorfile>
      <anchor>ga469286ac9a7cf6065584e9f29dda2293</anchor>
      <arglist>(LPC_GPIOINT_T *pGPIOINT)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIOINT_SetIntFalling</name>
      <anchorfile>group__GPIOINT__17XX__40XX.html</anchorfile>
      <anchor>ga59d69f2c748bc4816268a7b4714f8058</anchor>
      <arglist>(LPC_GPIOINT_T *pGPIOINT, LPC_GPIOINT_PORT_T port, uint32_t pins)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIOINT_SetIntRising</name>
      <anchorfile>group__GPIOINT__17XX__40XX.html</anchorfile>
      <anchor>ga50cbadb361acd7190dc8edf5ffb9208c</anchor>
      <arglist>(LPC_GPIOINT_T *pGPIOINT, LPC_GPIOINT_PORT_T port, uint32_t pins)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_GPIOINT_GetIntFalling</name>
      <anchorfile>group__GPIOINT__17XX__40XX.html</anchorfile>
      <anchor>ga151aaa1239b3f8b23be4c7c4238c7370</anchor>
      <arglist>(LPC_GPIOINT_T *pGPIOINT, LPC_GPIOINT_PORT_T port)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_GPIOINT_GetIntRising</name>
      <anchorfile>group__GPIOINT__17XX__40XX.html</anchorfile>
      <anchor>ga087f70143b825e771bf9acb2d21794bf</anchor>
      <arglist>(LPC_GPIOINT_T *pGPIOINT, LPC_GPIOINT_PORT_T port)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_GPIOINT_GetStatusFalling</name>
      <anchorfile>group__GPIOINT__17XX__40XX.html</anchorfile>
      <anchor>gaa70fc03d17d6dd929249e435dcfb777b</anchor>
      <arglist>(LPC_GPIOINT_T *pGPIOINT, LPC_GPIOINT_PORT_T port)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_GPIOINT_GetStatusRising</name>
      <anchorfile>group__GPIOINT__17XX__40XX.html</anchorfile>
      <anchor>gad12bd25757ab3f58843afdb178548ef6</anchor>
      <arglist>(LPC_GPIOINT_T *pGPIOINT, LPC_GPIOINT_PORT_T port)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_GPIOINT_ClearIntStatus</name>
      <anchorfile>group__GPIOINT__17XX__40XX.html</anchorfile>
      <anchor>ga900d1ac0c053f80c19fc01ae7fc5d981</anchor>
      <arglist>(LPC_GPIOINT_T *pGPIOINT, LPC_GPIOINT_PORT_T port, uint32_t pins)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE bool</type>
      <name>Chip_GPIOINT_IsIntPending</name>
      <anchorfile>group__GPIOINT__17XX__40XX.html</anchorfile>
      <anchor>gafd07acaafc8dda672add325845939fa3</anchor>
      <arglist>(LPC_GPIOINT_T *pGPIOINT, LPC_GPIOINT_PORT_T port)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>I2C_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx I2C driver</title>
    <filename>group__I2C__17XX__40XX.html</filename>
    <class kind="struct">LPC_I2C_T</class>
    <class kind="struct">I2C_BUFF_T</class>
    <class kind="struct">I2C_XFER_T</class>
    <member kind="define">
      <type>#define</type>
      <name>RET_SLAVE_TX</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga49da135773ab760dbf75fcbcdb383623</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RET_SLAVE_RX</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga16246f0e987c65b12170dd4f127e99f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RET_SLAVE_IDLE</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gac3305a3a03ff55a1ee6ba304a09e0187</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RET_SLAVE_BUSY</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gae5dd077e6273af4e15c3b656b56f9afd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_STA_STO_RECV</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga6ed81fd3a1ae05f1fbf96525f1520cd3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_STA_STO_RECV</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga6ed81fd3a1ae05f1fbf96525f1520cd3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2CONSET_AA</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga784c4b2fe7f3299e338655d2ddbf283c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2CONSET_SI</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gaa447cd2686805ef8009fc599144ee3dc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2CONSET_STO</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gaae292803a059b84eac20ab8777d113af</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2CONSET_STA</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gabaad3370eb35644c135d40f06adbbba0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2CONSET_I2EN</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gab836acc31e0572bb0d0db614f0641f15</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2CONCLR_AAC</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gaa13b19babb8442aa9047f8ecb92a908d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2CONCLR_SIC</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga36753112210a8c33d566b572b63b753b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2CONCLR_STOC</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga449d7af35550484c4dfefcddd01095b8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2CONCLR_STAC</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gab6148bf41d7fc32bd259d2f6a7d7667d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2CONCLR_I2ENC</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga92718ac11d46f6e32d526749f09d01b2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_CON_AA</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gafd39e9ced8b71fd55deb05d7a23752b9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_CON_SI</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gad53ba19314d57093aaa5076897604a50</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_CON_STO</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga9704c03008de747eb42bde530a67350b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_CON_STA</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga75e0835be79d812d1e6df8b0a5150365</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_CON_I2EN</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga5a696207d04d3694700e427a068409b1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_STAT_CODE_BITMASK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gafeb8b5f682a81a2cc32f6c4b720a5e1f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_STAT_CODE_ERROR</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gacfbf48ab1ed4f43314f63898ac827925</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_NO_INF</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga3e2ecdeb466041bb3cf435ccf36564d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_BUS_ERROR</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga7c1e7f0f166cdc547263229c5ce07cb0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_M_TX_START</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga5c44c05176052f6040cd40d680bc2a91</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_M_TX_RESTART</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gad41d0d1392b7d905b22b9c5015f2e48f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_M_TX_SLAW_ACK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gab91c158252efeb1a3a139faec3622ae0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_M_TX_SLAW_NACK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga15729d9436fbf4c23a22e7cdbc913552</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_M_TX_DAT_ACK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gad8c325e3c58aa8e7810797b9a6761596</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_M_TX_DAT_NACK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga234f86990a6fd63f1d2064cbca4709a3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_M_TX_ARB_LOST</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga79230136402441c7e233c63fd2f71bdf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_M_RX_START</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gab1f79137c62e306269633b005ad936da</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_M_RX_RESTART</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gacb1c9145acd64a77464017f1dd9279c5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_M_RX_ARB_LOST</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gaed32dea40cf84a1efb09e0c389579163</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_M_RX_SLAR_ACK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gacd29e4a94f8a533477dbbf026c0d525a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_M_RX_SLAR_NACK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gafe156febe3313a9f3423776f97d0f24f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_M_RX_DAT_ACK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga2f07d3cfad351ba3c976d629db20cea2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_M_RX_DAT_NACK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga9cc5203175775bfa5ae6df5d6d98b014</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_S_RX_SLAW_ACK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga7055c4b55feda352df8016cbca0c270b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_S_RX_ARB_LOST_M_SLA</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gaf13df6ed7d10be0e063bcbc04d329749</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_S_RX_GENCALL_ACK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gadcab34e87ad115c011e23a5bd4fb8aa3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_S_RX_ARB_LOST_M_GENCALL</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga1605c253472e071e7f77e51bda156ba7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_S_RX_PRE_SLA_DAT_ACK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga4342227592c3e2e345517bd3e6cd8089</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_S_RX_PRE_SLA_DAT_NACK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gace56a90d55426cc6ab2a4ca540c63d02</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_S_RX_PRE_GENCALL_DAT_ACK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga75dd46ddb054c4d47a531b8e90f7f446</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_S_RX_PRE_GENCALL_DAT_NACK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga20c65f8a82c18211127e5c335026bd76</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_S_RX_STA_STO_SLVREC_SLVTRX</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga7afe9f0e54aeb9ce8428c0adeb3c3274</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_S_TX_SLAR_ACK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gae6dcbdd1f01f7ebf3595722770e3b7b6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_S_TX_ARB_LOST_M_SLA</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gac71ac23a80e1bdb700a2c37170031107</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_S_TX_DAT_ACK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gaeb92fe85cc3f6c813cb91685234ac08c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_S_TX_DAT_NACK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gad9cdcd8c06924252a2ad9baced97d838</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2STAT_S_TX_LAST_DAT_ACK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga3e4522387958da1da6017a6e48a0daad</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_SLAVE_TIME_OUT</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga33116b352c72ef28879812c66387f17a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2DAT_BITMASK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gac66b7c81c93cc59f69d204b4eb7d639b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2DAT_IDLE_CHAR</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga5b04af302e0e4007df123dff0328ac5e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2MMCTRL_MM_ENA</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga8dc8fa566a5113c3e1b35c0580d90d9f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2MMCTRL_ENA_SCL</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gabf11567c27c48d795ba0f92cf636dab6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2MMCTRL_MATCH_ALL</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gac7dd5efba503dd79c6b42f3575f70307</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2MMCTRL_BITMASK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gadca0b6beb066a8f6689960c687334ac2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2DATA_BUFFER_BITMASK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga95d605cbb817fa36d9c83545dd23fb5f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2ADR_GC</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga2509515124601141b72080b5daf45009</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2ADR_BITMASK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga8d5195514172b58efd29f09642566d37</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2MASK_MASK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga697da3f892d87fc247d6e1afafac5a34</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2SCLH_BITMASK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gad6a9a202cf4d30607475338f7b59968a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_I2SCLL_BITMASK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gadf54076e458f10fc00be5ed9504bc930</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_SETUP_STATUS_ARBF</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gae90c077796281121b65b80b661a23062</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_SETUP_STATUS_NOACKF</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga90a37b470118d82b9682cfc2e0b4de84</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_SETUP_STATUS_DONE</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gaefbb25ca99e199765e7dcdd6ef343590</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_MONITOR_CFG_SCL_OUTPUT</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga5f79529ae357b8aa86e6ead24d95167e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2C_MONITOR_CFG_MATCHALL</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gadd2b30f5b29839a83c893d88b8d09dd7</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>void(*</type>
      <name>I2C_EVENTHANDLER_T</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga932bfc2f55180a71b93427e88b6223e6</anchor>
      <arglist>)(I2C_ID_T, I2C_EVENT_T)</arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>I2C_SLAVE_ID</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga5fb1ba338fb3822bb6ca012adc4194bf</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C_SLAVE_GENERAL</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gga5fb1ba338fb3822bb6ca012adc4194bfa7d2284af5f49b5b9df2582a31fff6370</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C_SLAVE_0</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gga5fb1ba338fb3822bb6ca012adc4194bfad17f6d6b2d217b84307c502108a6fed6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C_SLAVE_1</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gga5fb1ba338fb3822bb6ca012adc4194bfa8a4d6db2156bad4cf8c21226be564fe2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C_SLAVE_2</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gga5fb1ba338fb3822bb6ca012adc4194bfab6d7a585cfa1106eb290fa9f302c5783</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C_SLAVE_3</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gga5fb1ba338fb3822bb6ca012adc4194bfaf5cff98e9db3da7cc50edd21d3873efd</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C_SLAVE_NUM_INTERFACE</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gga5fb1ba338fb3822bb6ca012adc4194bfabdb0d442b75f2dbdb63aed5a307de077</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>I2C_STATUS_T</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga21aa839302786105dcf6a96be0e6e8bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C_STATUS_DONE</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gga21aa839302786105dcf6a96be0e6e8bca9f5024a35710b260041d351d794afa9a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C_STATUS_NAK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gga21aa839302786105dcf6a96be0e6e8bcac41be4d0689494327d4dae20504c854d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C_STATUS_ARBLOST</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gga21aa839302786105dcf6a96be0e6e8bca674732e92811621ed2ca34b9e40ca724</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C_STATUS_BUSERR</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gga21aa839302786105dcf6a96be0e6e8bca803d8563e2b11514ba4549763ad441f5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C_STATUS_BUSY</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gga21aa839302786105dcf6a96be0e6e8bcab3dd2c4a10e92ca1a5cb580501bad817</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>I2C_ID_T</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga1c03245782af1bec5eaaf1f54207cfc2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C0</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gga1c03245782af1bec5eaaf1f54207cfc2a8b606b29817406621b85b78b1ee9e653</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C1</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gga1c03245782af1bec5eaaf1f54207cfc2aeb0f494c71f34592c5a4500ee98ab4a2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C2</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gga1c03245782af1bec5eaaf1f54207cfc2ae2cc6f0481e798aedb5fd5c6a2bfc01b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C_NUM_INTERFACE</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gga1c03245782af1bec5eaaf1f54207cfc2a84f3a2e3885474dda12c3d223eef5304</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>I2C_EVENT_T</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gacb2cd4e03ea48339d327e4f387441bf3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C_EVENT_WAIT</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ggacb2cd4e03ea48339d327e4f387441bf3a565d2f9e44285e1965deb390497f62ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C_EVENT_DONE</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ggacb2cd4e03ea48339d327e4f387441bf3a43d00f7d92100d4af6df5514e4ccf1d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C_EVENT_LOCK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ggacb2cd4e03ea48339d327e4f387441bf3acec197aede70fe12008a68435bf5f339</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C_EVENT_UNLOCK</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ggacb2cd4e03ea48339d327e4f387441bf3aae6af45644ff5abf7016b5a8e8f41393</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C_EVENT_SLAVE_RX</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ggacb2cd4e03ea48339d327e4f387441bf3a290fcc1bb657102af26daa1b84472848</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2C_EVENT_SLAVE_TX</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ggacb2cd4e03ea48339d327e4f387441bf3a3911d9b6505f77f0bed3f21b2710ca58</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_I2C_Init</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gab79263d278814945df2cd44c5db7b514</anchor>
      <arglist>(I2C_ID_T id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_I2C_DeInit</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga334c2c12edda443a7e949a1ea4a6a646</anchor>
      <arglist>(I2C_ID_T id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_I2C_SetClockRate</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga17fac5d72058db8eed11d247e78b74ed</anchor>
      <arglist>(I2C_ID_T id, uint32_t clockrate)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_I2C_GetClockRate</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga6b13511432337d21b8cd325651cc5b63</anchor>
      <arglist>(I2C_ID_T id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>Chip_I2C_MasterTransfer</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga5f89391d66048894f4365d3b2b7df267</anchor>
      <arglist>(I2C_ID_T id, I2C_XFER_T *xfer)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>Chip_I2C_MasterSend</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga9ff549bdb526786d313c141b11cab43e</anchor>
      <arglist>(I2C_ID_T id, uint8_t slaveAddr, const uint8_t *buff, uint8_t len)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>Chip_I2C_MasterCmdRead</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga4a875b456dfe68acbe8ce1fc74d88bd3</anchor>
      <arglist>(I2C_ID_T id, uint8_t slaveAddr, uint8_t cmd, uint8_t *buff, int len)</arglist>
    </member>
    <member kind="function">
      <type>I2C_EVENTHANDLER_T</type>
      <name>Chip_I2C_GetMasterEventHandler</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gafad03b4f6c0ecb3f59f014ab63bfda5c</anchor>
      <arglist>(I2C_ID_T id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>Chip_I2C_SetMasterEventHandler</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga1fc3fc0946344e9551d9eef0bbf610b9</anchor>
      <arglist>(I2C_ID_T id, I2C_EVENTHANDLER_T event)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>Chip_I2C_MasterRead</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gae816049843eb162c803b5058ebd9a25c</anchor>
      <arglist>(I2C_ID_T id, uint8_t slaveAddr, uint8_t *buff, int len)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_I2C_EventHandlerPolling</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gaaa89a66d658a41325b3c5e56bc059401</anchor>
      <arglist>(I2C_ID_T id, I2C_EVENT_T event)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_I2C_EventHandler</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga06b84fe3fad7ffd4ccb93f2683781936</anchor>
      <arglist>(I2C_ID_T id, I2C_EVENT_T event)</arglist>
    </member>
    <member kind="function">
      <type>I2C_STATUS_T</type>
      <name>Chip_I2C_MasterStateHandler</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga95319060d7b4b99cefc2a7d675a047da</anchor>
      <arglist>(I2C_ID_T id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_I2C_Disable</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga68f1d630224edab35ba796373c5867c0</anchor>
      <arglist>(I2C_ID_T id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>Chip_I2C_IsMasterActive</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga5fdf29aff7847c93373cf02da41285e1</anchor>
      <arglist>(I2C_ID_T id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_I2C_SlaveSetup</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>gaf6ea40668dde26e406f76ff3ddeda527</anchor>
      <arglist>(I2C_ID_T id, I2C_SLAVE_ID sid, I2C_XFER_T *xfer, I2C_EVENTHANDLER_T event, uint8_t addrMask)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_I2C_SlaveStateHandler</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga650618a5f4717c46ae3ea304142ddc03</anchor>
      <arglist>(I2C_ID_T id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>Chip_I2C_IsStateChanged</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga4240d03d5dda43ddc8afd527b3172318</anchor>
      <arglist>(I2C_ID_T id)</arglist>
    </member>
    <member kind="function">
      <type>I2C_STATUS_T</type>
      <name>Chip_I2C_MasterStatus</name>
      <anchorfile>group__I2C__17XX__40XX.html</anchorfile>
      <anchor>ga67403e4489cfbe3429648800705038ed</anchor>
      <arglist>(I2C_ID_T id)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>I2S_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx I2S driver</title>
    <filename>group__I2S__17XX__40XX.html</filename>
    <class kind="struct">LPC_I2S_T</class>
    <class kind="struct">I2S_AUDIO_FORMAT_T</class>
    <member kind="define">
      <type>#define</type>
      <name>I2S_WORDWIDTH_8</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga17cb1a91d5be4e7afad486ead6d2980d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_WORDWIDTH_16</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga4d016baad0fba07da20a6410470c71d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_WORDWIDTH_32</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga683d81436e91c1631bdd5947ee78489b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_STEREO</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga07e34ebc83183644aa54cc124b769a43</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_MONO</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga3c732d1467300d87c582a1497e8dbadf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_MASTER_MODE</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga67cd00ecbec35d0dd723909916fb1014</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_SLAVE_MODE</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga37137132251bc15250edcea1585d3e58</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_STOP_ENABLE</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gabe1e11f6f4ce8542408e466b3316ca3c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_STOP_DISABLE</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gaf442a366f6467626a2f72fdabf9b6d89</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_RESET_ENABLE</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gae090c11fade3a6f9bf9791dba1920556</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_RESET_DISABLE</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gaf008515f40666122bb1936e591b7ed99</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_MUTE_ENABLE</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga3f418f8bc5e6f401481d3b9e241758bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_MUTE_DISABLE</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga7d6c53914dea99eecc1d59eefbdbf5aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DAO_WORDWIDTH_8</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga9e390ef13bf52d00c311ca64d46c5ac2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DAO_WORDWIDTH_16</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gaf48ba0298cdb6828567de0df29abcb4d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DAO_WORDWIDTH_32</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gaa6666d92294cc244c41fd510fb7de31d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DAO_MONO</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gab32b25d6de6c9c964421ff08c1402e03</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DAO_STOP</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gaa8e56c2dab6e3206093dd6575332253d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DAO_RESET</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga7ec4a63719a45df4d30f42e03af0d9e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DAO_SLAVE</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga58ea023dda649de724684288e703ef86</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DAO_WS_HALFPERIOD</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga3bfda0af914736d2765822daee28a489</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DAO_WS_HALFPERIOD_MASK</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gaf32d949b14558ee1aad72a765726731c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DAO_MUTE</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga78bd42c0693e2a68dcba79a4cc05b8d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DAI_WORDWIDTH_8</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga8bd50ada05c4cd981f07be7a3b1002dc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DAI_WORDWIDTH_16</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga26e60492415a246afde8d4ede5aa292e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DAI_WORDWIDTH_32</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga576561248aa7e6f27c5ef6c51def80ed</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DAI_WORDWIDTH_MASK</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga1177f9594c24141162839aafe829fcb9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DAI_MONO</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga352feb095d028efc44d17f72beacfae5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DAI_STOP</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga2ed2d2a9f59339ad3cf1f28a78bfcad6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DAI_RESET</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gae57a0b564a399a726a9a6737f5733beb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DAI_SLAVE</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga664f6b8ac38d7b3a23aae4e7a2fa1c6f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DAI_WS_HALFPERIOD</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gab175fcd8e7e80e59833b596a90b16f79</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DAI_WS_HALFPERIOD_MASK</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gaa960b95856c5f78a0eaed42d383dd124</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_STATE_IRQ</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gaa412bd295235fcd4d7f0ad284d9386de</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_STATE_DMA1</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga0e9a701addd279db953ca34d56487dbd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_STATE_DMA2</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga84dfa37fbee79986ec79f4d856b8df24</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_STATE_RX_LEVEL</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gab51548f7e41cb523a77beb7c88c5d33a</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_STATE_TX_LEVEL</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga2421a91f0ddda94150de2f80c4192039</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DMA1_RX_ENABLE</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga7b643833c4b80116d3026c7933449001</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DMA1_TX_ENABLE</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga0fb809d583bb8b68d34f347d19933575</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DMA1_RX_DEPTH</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gaee0ebb1d1bc7444e2201d4473c41f182</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DMA1_TX_DEPTH</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga4eedce5a4fc3c03413a4d7cfdd0f60de</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DMA2_RX_ENABLE</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga2e4e8b4d132b01bb74d57dd34ca7eaef</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DMA2_TX_ENABLE</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga2ceda1798034a70380c9a9de567bb6b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DMA2_RX_DEPTH</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gaa988d9e73e80df6ce687e148ee9530ae</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_DMA2_TX_DEPTH</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gaff02275ceaf3866c99b464b11ed0a270</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_IRQ_RX_ENABLE</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga362154dbf216f1384f535c940d930cd9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_IRQ_TX_ENABLE</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga7811db5a292d68de3dce209883bab6c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_IRQ_RX_DEPTH</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga8719ea9f2ad9a505b0d563387e8deb48</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_IRQ_TX_DEPTH</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga115bec3e7413054e340baceb5a066fa7</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_TXRATE_Y_DIVIDER</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga310a84df35c22ce4bed1f3785145670c</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_TXRATE_X_DIVIDER</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gab4441c879ba15275e44ffe3f075fecab</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_RXRATE_Y_DIVIDER</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga7487e9a82c9bb4a58118038ca6520dbe</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_RXRATE_X_DIVIDER</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gac6b39adbbfb5ff023160af667debeef6</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_TXMODE_CLKSEL</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga34a82caff9de404af31e19e86ebaa193</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_TXMODE_4PIN_ENABLE</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga3e268d07b159ec36998eb32a9e36af69</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_TXMODE_MCENA</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gabd67f37d51f2557f49fa600abb48225b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_RXMODE_CLKSEL</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga45351ee42b08814f1860c0d2a73379f0</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_RXMODE_4PIN_ENABLE</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga2734ec0ee02ee7865c36b6ec3ffff081</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>I2S_RXMODE_MCENA</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga64eb87a1f090271b07c7110f974f8300</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>I2S_DMA_CHANNEL_T</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gac04c1583101ddd661886d9677683421b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2S_DMA_REQUEST_CHANNEL_1</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ggac04c1583101ddd661886d9677683421ba549b2d28b5da72f6185d736f5fed51c1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2S_DMA_REQUEST_CHANNEL_2</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ggac04c1583101ddd661886d9677683421ba65eabb50f670f1dcae7b3c790f088e16</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>I2S_DMA_REQUEST_CHANNEL_NUM</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ggac04c1583101ddd661886d9677683421bab9921186ab93fee889b2074d508b88ce</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_I2S_Init</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga006752f0240a956ae02fba521e63c053</anchor>
      <arglist>(LPC_I2S_T *pI2S)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_I2S_DeInit</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga2fbdb14520735214e155253f05a88a0a</anchor>
      <arglist>(LPC_I2S_T *pI2S)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_I2S_Send</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga7927122545d8ec1879743fb8caffb723</anchor>
      <arglist>(LPC_I2S_T *pI2S, uint32_t data)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_I2S_Receive</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga74005245ee6b79220df6a563db92a04f</anchor>
      <arglist>(LPC_I2S_T *pI2S)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_I2S_TxStart</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga84f5b666dcacef8140c1355ed308a782</anchor>
      <arglist>(LPC_I2S_T *pI2S)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_I2S_RxStart</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga42eb351f08a2dea90fde3ba14d1698e3</anchor>
      <arglist>(LPC_I2S_T *pI2S)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_I2S_TxPause</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga0487f27c97c88ea9d2bf9adc6ec4e469</anchor>
      <arglist>(LPC_I2S_T *pI2S)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_I2S_RxPause</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga0f7d40cc029ebd29091c0f3699bd1fe9</anchor>
      <arglist>(LPC_I2S_T *pI2S)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_I2S_EnableMute</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga3977adf6afb42ea9da7c764ef36efa6e</anchor>
      <arglist>(LPC_I2S_T *pI2S)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_I2S_DisableMute</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga0c6731695f685fa21678ea3635b458f3</anchor>
      <arglist>(LPC_I2S_T *pI2S)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_I2S_TxStop</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gac7d296f84ba3283e63861515d134edfe</anchor>
      <arglist>(LPC_I2S_T *pI2S)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_I2S_RxStop</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gaa78f387af78de456819cec21bbf21ada</anchor>
      <arglist>(LPC_I2S_T *pI2S)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_I2S_TxModeConfig</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga0c7a808e84e2c0a3f7ffbba089c7d380</anchor>
      <arglist>(LPC_I2S_T *pI2S, uint32_t clksel, uint32_t fpin, uint32_t mcena)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_I2S_RxModeConfig</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gae4e5e39c6ea22b2be42c91bdaa0f27e8</anchor>
      <arglist>(LPC_I2S_T *pI2S, uint32_t clksel, uint32_t fpin, uint32_t mcena)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint8_t</type>
      <name>Chip_I2S_GetTxLevel</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga125559c7f810a2564f5b6059ead42256</anchor>
      <arglist>(LPC_I2S_T *pI2S)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint8_t</type>
      <name>Chip_I2S_GetRxLevel</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga5880b341bea64ea33e734af422b43dc3</anchor>
      <arglist>(LPC_I2S_T *pI2S)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_I2S_SetTxBitRate</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga4ab6d7b26edf92c1b7bcb7f6f9326888</anchor>
      <arglist>(LPC_I2S_T *pI2S, uint32_t div)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_I2S_SetRxBitRate</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gaf18685ebc3f86581fd7075fafe4c72e7</anchor>
      <arglist>(LPC_I2S_T *pI2S, uint32_t div)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_I2S_SetTxXYDivider</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gaab4e4a66774d56ba378cce2dbc34e9b0</anchor>
      <arglist>(LPC_I2S_T *pI2S, uint8_t xDiv, uint8_t yDiv)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_I2S_SetRxXYDivider</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gafd2bc55fb29ea5f082d5ae3b5794f605</anchor>
      <arglist>(LPC_I2S_T *pI2S, uint8_t xDiv, uint8_t yDiv)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_I2S_TxConfig</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga09acfe336e9aee86f1d029146937326a</anchor>
      <arglist>(LPC_I2S_T *pI2S, I2S_AUDIO_FORMAT_T *format)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_I2S_RxConfig</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gab443fc12cad5f6cfb13dc5ab19a003ae</anchor>
      <arglist>(LPC_I2S_T *pI2S, I2S_AUDIO_FORMAT_T *format)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_I2S_Int_TxCmd</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gaf5ea5a92372f64937b2a967f8aa284f3</anchor>
      <arglist>(LPC_I2S_T *pI2S, FunctionalState newState, uint8_t depth)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_I2S_Int_RxCmd</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga36b0f5c62d5c703c1374bdcc0d84d07f</anchor>
      <arglist>(LPC_I2S_T *pI2S, FunctionalState newState, uint8_t depth)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_I2S_DMA_TxCmd</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>gac08890ba38fd8e5df3a3a603e7a4fa42</anchor>
      <arglist>(LPC_I2S_T *pI2S, I2S_DMA_CHANNEL_T dmaNum, FunctionalState newState, uint8_t depth)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_I2S_DMA_RxCmd</name>
      <anchorfile>group__I2S__17XX__40XX.html</anchorfile>
      <anchor>ga9c7067a9ee62d240aa6cd9426deefb13</anchor>
      <arglist>(LPC_I2S_T *pI2S, I2S_DMA_CHANNEL_T dmaNum, FunctionalState newState, uint8_t depth)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>COMMON_IAP</name>
    <title>CHIP: Common Chip ISP/IAP commands and return codes</title>
    <filename>group__COMMON__IAP.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>IAP_PREWRRITE_CMD</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga540234bb0f525a06770175699d01063b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_WRISECTOR_CMD</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga9eb15375e6dd4de560a7d43b4483a293</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_ERSSECTOR_CMD</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>gad212cc38d91507366f07f9393e42eaec</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_BLANK_CHECK_SECTOR_CMD</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga2117bbba83cf2110a3fde4cafe189784</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_REPID_CMD</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>gae385db2542e6b2c8b8417cf1b929fcb1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_READ_BOOT_CODE_CMD</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga8d0f5e06feea423e15ee0238534f80ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_COMPARE_CMD</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>gaa63a66d010441cd1b5837742455cf075</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_REINVOKE_ISP_CMD</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>gad1f3638abcb5134b6fb6aceb67a4bb2c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_READ_UID_CMD</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>gad2458645f1cf815f56f205bd7f7fd1af</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_ERASE_PAGE_CMD</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga24604c568e67c76aee924f9439426111</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_EEPROM_WRITE</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga76ebaaa7cd2a015246d1a44e3a0cc369</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_EEPROM_READ</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga34d721b0a3ff4caf7b847c9d85fe73a0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_CMD_SUCCESS</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga6855b3bb97689d746eff1f27dde58fc5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_INVALID_COMMAND</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga78fcc8d4b3835576a272431915e39eb7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_SRC_ADDR_ERROR</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>gaad19f900fbc28ee51d3e0138a88a446b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_DST_ADDR_ERROR</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga72a13c4c7132aa3da476fc0907cb2a08</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_SRC_ADDR_NOT_MAPPED</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga8d15cbc501933748afbe786c587e74f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_DST_ADDR_NOT_MAPPED</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>gacd5a7fb6c783f5d435e594170757d1d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_COUNT_ERROR</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga719488ff2abf2f325b73a10b20403813</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_INVALID_SECTOR</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>gab3af925b968fb59823d08d77261aebf7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_SECTOR_NOT_BLANK</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga19b046ec9c03aa0276a9cbe409eb3f04</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_SECTOR_NOT_PREPARED</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga8600f9e930f1dee5d67a2bc11efcde63</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_COMPARE_ERROR</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>gabeb57ce3d4009fdee1847f5494376c8d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_BUSY</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>gaa4e308bc310bb68aa8409a6f830aee04</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_PARAM_ERROR</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga6bf1bec9cbb419f8006447171e9750b7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_ADDR_ERROR</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>gaaff51c256373e4a20f8dab1adc1300f3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_ADDR_NOT_MAPPED</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga4cfd7e0c133c450664f0a60bfbbd9eaa</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_CMD_LOCKED</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>gafdceac1acd5c460094011136c08574a4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_INVALID_CODE</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga456a8363a47d21d7198056da4b1e9f61</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_INVALID_BAUD_RATE</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga6913ca0660dc1a2cccd8c6b09bcdbc75</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_INVALID_STOP_BIT</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga6c61111082b04ba6780151c83f8c3644</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IAP_CRP_ENABLED</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>gaaccc191ce402e19b0c0ae399f614efe8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>Chip_IAP_PreSectorForReadWrite</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>gac45b60745e55670c7fa806e2865625c4</anchor>
      <arglist>(uint32_t strSector, uint32_t endSector)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>Chip_IAP_CopyRamToFlash</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga71cb8cde86dc344b05219cdf1ecee638</anchor>
      <arglist>(uint32_t dstAdd, uint32_t *srcAdd, uint32_t byteswrt)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>Chip_IAP_EraseSector</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga02dbb90a49e24dc449d45a66f03a4a2d</anchor>
      <arglist>(uint32_t strSector, uint32_t endSector)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>Chip_IAP_BlankCheckSector</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga98c7e657a618cf004a8967112dae1a30</anchor>
      <arglist>(uint32_t strSector, uint32_t endSector)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_IAP_ReadPID</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>gaa4b90551649db0627dc195acfc834cbe</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>Chip_IAP_ReadBootCode</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga478acfa6d97211d4f43edfa8717dc066</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>Chip_IAP_Compare</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga0f3983319210f99d1931f1e9fa762d9e</anchor>
      <arglist>(uint32_t dstAdd, uint32_t srcAdd, uint32_t bytescmp)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>Chip_IAP_ReinvokeISP</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga91a6ef5cac3a052f637cf0b5d7d31d53</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_IAP_ReadUID</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga2835e129423f1ce0628004a8adf00773</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>Chip_IAP_ErasePage</name>
      <anchorfile>group__COMMON__IAP.html</anchorfile>
      <anchor>ga95ca28fb39884184657054e134671f95</anchor>
      <arglist>(uint32_t strPage, uint32_t endPage)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>IOCON_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx I/O configuration driver</title>
    <filename>group__IOCON__17XX__40XX.html</filename>
    <class kind="struct">PINMUX_GRP_T</class>
    <class kind="struct">LPC_IOCON_T</class>
    <member kind="define">
      <type>#define</type>
      <name>IOCON_FUNC0</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>gab08f72eae45dd5656aeca24c346c8626</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IOCON_FUNC1</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>gab6449233bd56957c684b9ad694606e3a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IOCON_FUNC2</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga43ebb7abf055ce2491fbba5ef72f6fc7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IOCON_FUNC3</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>gace4708e511b3aa9c352600604331bec0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IOCON_FUNC4</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>gac621b96a2bd1ee57a83ff1af98287a8d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IOCON_FUNC5</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga0f9630599f40a3f96a5887c7e65508a9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IOCON_FUNC6</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga6284e27c6e5df6bc9f37c482e736a1d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IOCON_FUNC7</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga38efe875290e90d2f4185cb0ffe33e03</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IOCON_MODE_INACT</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>gafc593aafe607c88c52864ecb7586ffe9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IOCON_MODE_PULLDOWN</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga270261df49234519bfd09e076dfcec6c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IOCON_MODE_PULLUP</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>gad8fe947d8e7076d6cec01a5b30261141</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IOCON_MODE_REPEATER</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga53d705841cc362c6f43ffc1370d71726</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IOCON_HYS_EN</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>gafb7c408ac1f52b7b7e46fde3061fe0b7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IOCON_INV_EN</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>gae1fd1faa316f6c3108b499928f8c9922</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IOCON_ADMODE_EN</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga9a3d4f3e281c382f795b9a769073d4e4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IOCON_DIGMODE_EN</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga516e9d9bc7b1f3aaefd9d09c6ece74d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IOCON_FILT_DIS</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>gabebfb8b8646d151bfee9615c75724119</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IOCON_HS_DIS</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>gab2c8963063faa2ee583ad54750b842fa</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IOCON_HIDRIVE_EN</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>gadaa6e6eca0d7d555f4f0816aa83dc8f3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IOCON_FASTSLEW_EN</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga478aa0f55e07c89035e36378b2c3780b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IOCON_OPENDRAIN_EN</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga226b54f672e61a7a2d2893d9ef8cbc9c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IOCON_DAC_EN</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>gabb788549592ca772def19f2a8060aa3d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>FUNC0</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>gaa3ba064ba85ae0da9c55e7cdb8ea09b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MD_HYS_ENA</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>gab6a440afbad506dec9b0227f3c82fd73</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MD_HYS_DIS</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga7c49147e8696b77ebf4bea2d90474a7f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MD_IINV_ENA</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>gaeecb9c3d257aaea0fff788e1cdd538bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MD_IINV_DIS</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga3f1345e7ffe3eed8bb0c4b9e456b0df9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MD_OD_ENA</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga83e1ce8c76aac6481562dd26b059ae61</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MD_OD_DIS</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga1b2a7926f85c2e7fddf784ed9a692d33</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MD_HS_ENA</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga95de7405f0be07e5a3c1eac75663b7e4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MD_HS_DIS</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>gaa9af2984b24150d8c005e2b7c254adbc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MD_ANA_ENA</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga981e8fc7f82693378a8856bea0b435b7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MD_ANA_DIS</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga12c40f516d771be23c8521a59e8f37dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MD_FILT_ENA</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga0bbfc0ef84fc75d356d7fd36cd23527e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MD_FILT_DIS</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>gad67d4495f8a8568bdedbaa13aceb2d3d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MD_DAC_ENA</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga6b38cbeca37aeb7bcbf7fc66ba4ec65d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MD_DAC_DIS</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>gaec6eb0c4c1101d5c5dc64ef56a5a5588</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MD_STD_SLEW_RATE</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga190b92c2bbf82f91768ad526aaadc441</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MD_FAST_SLEW_RATE</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga26dcdaed2ef64f84991cc59ae9370c80</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MD_HD_ENA</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga4fb2c193dc83d6b18a8afad131b2e139</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MD_HD_DIS</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga324ed23a726e0788749899278b0ce61a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_IOCON_Init</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga40283d81f5ad91ba8b47647059721c9d</anchor>
      <arglist>(LPC_IOCON_T *pIOCON)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_IOCON_PinMuxSet</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>ga5db68254cabb0d4cd4558d81557b77e4</anchor>
      <arglist>(LPC_IOCON_T *pIOCON, uint8_t port, uint8_t pin, uint32_t modefunc)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_IOCON_PinMux</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>gaa2f90b2873cda51e67b3a67d6cc92617</anchor>
      <arglist>(LPC_IOCON_T *pIOCON, uint8_t port, uint8_t pin, uint32_t mode, uint8_t func)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_IOCON_SetPinMuxing</name>
      <anchorfile>group__IOCON__17XX__40XX.html</anchorfile>
      <anchor>gad97c96e401016cf296e6d20454f1c522</anchor>
      <arglist>(LPC_IOCON_T *pIOCON, const PINMUX_GRP_T *pinArray, uint32_t arrayLength)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>LCD_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx LCD driver</title>
    <filename>group__LCD__17XX__40XX.html</filename>
  </compound>
  <compound kind="group">
    <name>LPC_Types</name>
    <title>CHIP: LPC Common Types</title>
    <filename>group__LPC__Types.html</filename>
    <subgroup>LPC_Types_Public_Types</subgroup>
    <subgroup>LPC_Types_Public_Macros</subgroup>
  </compound>
  <compound kind="group">
    <name>LPC_Types_Public_Types</name>
    <title>LPC Public Types</title>
    <filename>group__LPC__Types__Public__Types.html</filename>
    <member kind="typedef">
      <type>void(*</type>
      <name>PFV</name>
      <anchorfile>group__LPC__Types__Public__Types.html</anchorfile>
      <anchor>ga2d6ebcfe3babcb204d017c685825a4d8</anchor>
      <arglist>)()</arglist>
    </member>
    <member kind="typedef">
      <type>int32_t(*</type>
      <name>PFI</name>
      <anchorfile>group__LPC__Types__Public__Types.html</anchorfile>
      <anchor>ga39628beaa6435daa2ae29ba204c920da</anchor>
      <arglist>)()</arglist>
    </member>
    <member kind="typedef">
      <type>char</type>
      <name>CHAR</name>
      <anchorfile>group__LPC__Types__Public__Types.html</anchorfile>
      <anchor>gaebb9e13210d88d43e32e735ada43a425</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>uint8_t</type>
      <name>UNS_8</name>
      <anchorfile>group__LPC__Types__Public__Types.html</anchorfile>
      <anchor>ga7353117656180c64d2216c874998b98b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int8_t</type>
      <name>INT_8</name>
      <anchorfile>group__LPC__Types__Public__Types.html</anchorfile>
      <anchor>gac172005ce53b001f50a677cc10bd17b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>uint16_t</type>
      <name>UNS_16</name>
      <anchorfile>group__LPC__Types__Public__Types.html</anchorfile>
      <anchor>gafce87a7f2271b2cf38d7532f157f8a50</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int16_t</type>
      <name>INT_16</name>
      <anchorfile>group__LPC__Types__Public__Types.html</anchorfile>
      <anchor>gaae6e34a91bf60db05de64de7720df9a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>uint32_t</type>
      <name>UNS_32</name>
      <anchorfile>group__LPC__Types__Public__Types.html</anchorfile>
      <anchor>ga28adf5c6b1811ca447826319598d8aba</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int32_t</type>
      <name>INT_32</name>
      <anchorfile>group__LPC__Types__Public__Types.html</anchorfile>
      <anchor>ga3a17614f3a1b67eaf20781d8ec16a652</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int64_t</type>
      <name>INT_64</name>
      <anchorfile>group__LPC__Types__Public__Types.html</anchorfile>
      <anchor>ga1a0aab29eee6b306564084e005fa5750</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>uint64_t</type>
      <name>UNS_64</name>
      <anchorfile>group__LPC__Types__Public__Types.html</anchorfile>
      <anchor>ga2299199b92f0535ad8c2e2d8c7c7f09b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>bool</type>
      <name>BOOL_32</name>
      <anchorfile>group__LPC__Types__Public__Types.html</anchorfile>
      <anchor>gab02ba567b91b6b3d3c0c0209b2f577a0</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>bool</type>
      <name>BOOL_16</name>
      <anchorfile>group__LPC__Types__Public__Types.html</anchorfile>
      <anchor>ga586a49bf86982ab05295515d1e4e35fe</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>bool</type>
      <name>BOOL_8</name>
      <anchorfile>group__LPC__Types__Public__Types.html</anchorfile>
      <anchor>ga7cc7a8cc54a0a73fbfcc1eb0b792148d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>Bool</name>
      <anchorfile>group__LPC__Types__Public__Types.html</anchorfile>
      <anchor>ga39db6982619d623273fad8a383489309</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>FlagStatus</name>
      <anchorfile>group__LPC__Types__Public__Types.html</anchorfile>
      <anchor>ga89136caac2e14c55151f527ac02daaff</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>FunctionalState</name>
      <anchorfile>group__LPC__Types__Public__Types.html</anchorfile>
      <anchor>gac9a7e9a35d2513ec15c3b537aaa4fba1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>Status</name>
      <anchorfile>group__LPC__Types__Public__Types.html</anchorfile>
      <anchor>ga67a0db04d321a74b7e7fcfd3f1a3f70b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>TRANSFER_BLOCK_T</name>
      <anchorfile>group__LPC__Types__Public__Types.html</anchorfile>
      <anchor>gaf5297347bed33665c55dd0e9c7840403</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>NONE_BLOCKING</name>
      <anchorfile>group__LPC__Types__Public__Types.html</anchorfile>
      <anchor>ggaf5297347bed33665c55dd0e9c7840403ae00130e64382c35d172d226b79aa9acb</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>BLOCKING</name>
      <anchorfile>group__LPC__Types__Public__Types.html</anchorfile>
      <anchor>ggaf5297347bed33665c55dd0e9c7840403a854a1cd6e3a98db9e290dddea29725e7</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>LPC_Types_Public_Macros</name>
    <title>LPC Public Macros</title>
    <filename>group__LPC__Types__Public__Macros.html</filename>
  </compound>
  <compound kind="group">
    <name>MCPWM_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx Motor Control PWM driver</title>
    <filename>group__MCPWM__17XX__40XX.html</filename>
    <class kind="struct">LPC_MCPWM_T</class>
  </compound>
  <compound kind="group">
    <name>PMU_17XX_40XX</name>
    <title>CHIP: LPC17xx_40xx PMU driver</title>
    <filename>group__PMU__17XX__40XX.html</filename>
    <class kind="struct">LPC_PMU_T</class>
    <member kind="define">
      <type>#define</type>
      <name>PMU_PCON_PM0_FLAG</name>
      <anchorfile>group__PMU__17XX__40XX.html</anchorfile>
      <anchor>ga68db97d4dfb92acf82b041ddbbfa692d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PMU_PCON_SMFLAG</name>
      <anchorfile>group__PMU__17XX__40XX.html</anchorfile>
      <anchor>gabcad2ba661df07512a8facf72ad718a3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PMU_PCON_DSFLAG</name>
      <anchorfile>group__PMU__17XX__40XX.html</anchorfile>
      <anchor>ga35db160b23010e0f598641f1f40ebd15</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PMU_PCON_PDFLAG</name>
      <anchorfile>group__PMU__17XX__40XX.html</anchorfile>
      <anchor>gabad3cddc54f9c1cdd8128ec65fc50691</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PMU_PCON_DPDFLAG</name>
      <anchorfile>group__PMU__17XX__40XX.html</anchorfile>
      <anchor>ga81cd6baf090cf4649f51538b59e2e4af</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>CHIP_PMU_MCUPOWER_T</name>
      <anchorfile>group__PMU__17XX__40XX.html</anchorfile>
      <anchor>ga2f88b5672cfb96b0b89843b1815741c3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>PMU_MCU_SLEEP</name>
      <anchorfile>group__PMU__17XX__40XX.html</anchorfile>
      <anchor>gga2f88b5672cfb96b0b89843b1815741c3a642a6452710738e476df4c9873f8d53c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>PMU_MCU_DEEP_SLEEP</name>
      <anchorfile>group__PMU__17XX__40XX.html</anchorfile>
      <anchor>gga2f88b5672cfb96b0b89843b1815741c3a520bf4cb3d4ae81e7fc2b6da77117089</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>PMU_MCU_POWER_DOWN</name>
      <anchorfile>group__PMU__17XX__40XX.html</anchorfile>
      <anchor>gga2f88b5672cfb96b0b89843b1815741c3ae57e035fd3a4f9fa956fbc8668030f8d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>PMU_MCU_DEEP_PWRDOWN</name>
      <anchorfile>group__PMU__17XX__40XX.html</anchorfile>
      <anchor>gga2f88b5672cfb96b0b89843b1815741c3a23fed3c82386a96e38fcf19bc6ec20a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_PMU_SleepState</name>
      <anchorfile>group__PMU__17XX__40XX.html</anchorfile>
      <anchor>ga8b2198dfa41da8da19c47a3b7d51283a</anchor>
      <arglist>(LPC_PMU_T *pPMU)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_PMU_DeepSleepState</name>
      <anchorfile>group__PMU__17XX__40XX.html</anchorfile>
      <anchor>gaf45cdc5409ccf675212a077341e80506</anchor>
      <arglist>(LPC_PMU_T *pPMU)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_PMU_PowerDownState</name>
      <anchorfile>group__PMU__17XX__40XX.html</anchorfile>
      <anchor>gaa99405714fbc2707643ea8dd36895a26</anchor>
      <arglist>(LPC_PMU_T *pPMU)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_PMU_DeepPowerDownState</name>
      <anchorfile>group__PMU__17XX__40XX.html</anchorfile>
      <anchor>gad145abba9dc403d8db57c6b328f8d0c4</anchor>
      <arglist>(LPC_PMU_T *pPMU)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_PMU_Sleep</name>
      <anchorfile>group__PMU__17XX__40XX.html</anchorfile>
      <anchor>gaca883204092fcbc6e4705a3461029220</anchor>
      <arglist>(LPC_PMU_T *pPMU, CHIP_PMU_MCUPOWER_T SleepMode)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>QEI_17XX_40XX</name>
    <title>CHIP: LPc17xx/40xx Quadrature Encoder Interface driver</title>
    <filename>group__QEI__17XX__40XX.html</filename>
    <class kind="struct">LPC_QEI_T</class>
  </compound>
  <compound kind="group">
    <name>Ring_Buffer</name>
    <title>CHIP: Simple ring buffer implementation</title>
    <filename>group__Ring__Buffer.html</filename>
    <class kind="struct">RINGBUFF_T</class>
    <member kind="define">
      <type>#define</type>
      <name>RB_VHEAD</name>
      <anchorfile>group__Ring__Buffer.html</anchorfile>
      <anchor>gab0e914e769172c3291a9cc36962bc4de</anchor>
      <arglist>(rb)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RB_VTAIL</name>
      <anchorfile>group__Ring__Buffer.html</anchorfile>
      <anchor>ga9f7c4d3e3029e8318582a147a5c37674</anchor>
      <arglist>(rb)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>RingBuffer_Init</name>
      <anchorfile>group__Ring__Buffer.html</anchorfile>
      <anchor>gaaf3bb51f2228ea1bea603e19c7eba5bb</anchor>
      <arglist>(RINGBUFF_T *RingBuff, void *buffer, int itemSize, int count)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>RingBuffer_Flush</name>
      <anchorfile>group__Ring__Buffer.html</anchorfile>
      <anchor>ga5f66a5dd980ef03877cf8e0c96ad4ebb</anchor>
      <arglist>(RINGBUFF_T *RingBuff)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE int</type>
      <name>RingBuffer_GetSize</name>
      <anchorfile>group__Ring__Buffer.html</anchorfile>
      <anchor>ga2fc4b40b03afb19c8ea942da3cf3faf1</anchor>
      <arglist>(RINGBUFF_T *RingBuff)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE int</type>
      <name>RingBuffer_GetCount</name>
      <anchorfile>group__Ring__Buffer.html</anchorfile>
      <anchor>ga7b69777c35694637acaf39e6bfcc1822</anchor>
      <arglist>(RINGBUFF_T *RingBuff)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE int</type>
      <name>RingBuffer_GetFree</name>
      <anchorfile>group__Ring__Buffer.html</anchorfile>
      <anchor>ga75424687def8979742338366d39c8559</anchor>
      <arglist>(RINGBUFF_T *RingBuff)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE int</type>
      <name>RingBuffer_IsFull</name>
      <anchorfile>group__Ring__Buffer.html</anchorfile>
      <anchor>ga760da012435262add1d8d7aa79e873a0</anchor>
      <arglist>(RINGBUFF_T *RingBuff)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE int</type>
      <name>RingBuffer_IsEmpty</name>
      <anchorfile>group__Ring__Buffer.html</anchorfile>
      <anchor>ga6f03e04a69262864bde4f35fc6f3dfb5</anchor>
      <arglist>(RINGBUFF_T *RingBuff)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>RingBuffer_Insert</name>
      <anchorfile>group__Ring__Buffer.html</anchorfile>
      <anchor>gaafdee54f2525b2c7a983d1a631b42226</anchor>
      <arglist>(RINGBUFF_T *RingBuff, const void *data)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>RingBuffer_InsertMult</name>
      <anchorfile>group__Ring__Buffer.html</anchorfile>
      <anchor>gafeafb521d4e03052ab2c893fd0e388d5</anchor>
      <arglist>(RINGBUFF_T *RingBuff, const void *data, int num)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>RingBuffer_Peek</name>
      <anchorfile>group__Ring__Buffer.html</anchorfile>
      <anchor>ga357409718b1b61ac08751bd79c1f3c1a</anchor>
      <arglist>(RINGBUFF_T *RingBuff, void *data)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>RingBuffer_PeekMult</name>
      <anchorfile>group__Ring__Buffer.html</anchorfile>
      <anchor>ga6bf562d5d0e25d1aced3d767cd54b5ab</anchor>
      <arglist>(RINGBUFF_T *RingBuff, void *data, int num)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>RingBuffer_Pop</name>
      <anchorfile>group__Ring__Buffer.html</anchorfile>
      <anchor>gaf3ce7f43677c2b4c6eedb3cc4962b80d</anchor>
      <arglist>(RINGBUFF_T *RingBuff, void *data)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>RingBuffer_PopMult</name>
      <anchorfile>group__Ring__Buffer.html</anchorfile>
      <anchor>gae0ef7bb96d1fe84ae1441b7c214b1e56</anchor>
      <arglist>(RINGBUFF_T *RingBuff, void *data, int num)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>RIT_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx Repetitive Interrupt Timer driver</title>
    <filename>group__RIT__17XX__40XX.html</filename>
  </compound>
  <compound kind="group">
    <name>ROMAPI_407X_8X</name>
    <title>CHIP: LPC17XX/40XX ROM API declarations and functions</title>
    <filename>group__ROMAPI__407X__8X.html</filename>
    <class kind="struct">LPC_ROM_API_T</class>
  </compound>
  <compound kind="group">
    <name>RTC_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx Real Time Clock driver</title>
    <filename>group__RTC__17XX__40XX.html</filename>
    <class kind="struct">LPC_RTC_T</class>
    <class kind="struct">LPC_REGFILE_T</class>
    <class kind="struct">RTC_TIME_T</class>
    <member kind="define">
      <type>#define</type>
      <name>RTC_ILR_BITMASK</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga9c00baff0fca4f8e747fadfe9ee12775</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_IRL_RTCCIF</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gaf706fff68e830a082d476467fe71f297</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_IRL_RTCALF</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga237351d2c7f08b447254eff9578eb11e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_CCR_BITMASK</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gadc461b6f544d66841e09499b2b9734c7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_CCR_CLKEN</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gad08af035635f5acd7931cb982f95e771</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_CCR_CTCRST</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga70900054432c82dad7d63d4598502923</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_CCR_CCALEN</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gaeb77b2340d896fae77ea670bb70e972e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_AMR_CIIR_IMSEC</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga30c785b6017020d4c0b61b22aff30aac</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_AMR_CIIR_IMMIN</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga3d67e7c72ec7bcd0831628841496cbc6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_AMR_CIIR_IMHOUR</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga4e20983ef05abf10773186d3193270f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_AMR_CIIR_IMDOM</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gacd23cffe1367b6bb612fb37ebdf2e279</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_AMR_CIIR_IMDOW</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga6494d5878d87398a4519e90a367bffe5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_AMR_CIIR_IMDOY</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gaec7d51e7a503514c1d71bf9428c6f3e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_AMR_CIIR_IMMON</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga841dfecc952d8b6d275e799cb9e89c02</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_AMR_CIIR_IMYEAR</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga56b312d9e291685d843f6dae171f4441</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_AMR_CIIR_BITMASK</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gafcc754fba01521c5aa4f1775b889e894</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_AUX_RTC_OSCF</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gaafb1215dfd0f9bbe198274689a1f1584</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_AUXEN_RTC_OSCFEN</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga12b8af6f1d4757d19c1b09d85d3fc497</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_SEC_MASK</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gabc478af38e7fa3018d8449f22c51d10e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_MIN_MASK</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga9f3224adad8ed73109ee0309c3580998</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_HOUR_MASK</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga3c6fd786e3cb5f56b71474fc2d26a8a4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_DOM_MASK</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga7c4ea655a8ccf43870c672c1058a133e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_DOW_MASK</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga6564edf565a643214a54ffac364c69ca</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_DOY_MASK</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga7c8c3332f827ea092523b75ef953bd8b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_MONTH_MASK</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga491df15fba29dd3237f7bd59a9338050</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_YEAR_MASK</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga35a455a48ccdb557b824e87701449c76</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_SECOND_MAX</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga60bbeee3abbb647a0daa7c2130965646</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_MINUTE_MAX</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gab55b557eeb5c66e94a2fda3638e3b7ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_HOUR_MAX</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga35630c42240ce2e8f74c38e1731d7e68</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_MONTH_MIN</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga5002ab062a5871b7ed73c7ff0cb00e6b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_MONTH_MAX</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gacad40d1a94fd9b6884ec6160abb07674</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_DAYOFMONTH_MIN</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gafbcf1009356ffdad8f11920167908213</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_DAYOFMONTH_MAX</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gaddd515ae325f802e35e8202fef5f2957</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_DAYOFWEEK_MAX</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga83c5c53e8b56ca8d48d9c46d7e8c50fa</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_DAYOFYEAR_MIN</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga132dcf308a59affe54749f4f11cd22a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_DAYOFYEAR_MAX</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga2621bb8d69ee212101605c2e3739db4d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_YEAR_MAX</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga72b68660aec4a924c12a124e2ec5f852</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_CALIBRATION_CALVAL_MASK</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gafe263620e5d0709ddf51abfdf408c770</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_CALIBRATION_LIBDIR</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gae818a2f408ec0a1025ab3f02947d4b84</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_CALIBRATION_MAX</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gaeb077cfbe5f74f3b15ff72fed96abfe6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RTC_CALIB_DIR_FORWARD</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga4f53042ce8b730a4b2c6eb40b856e4e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>RTC_TIMEINDEX_T</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga8144898fe628404d396db06dc8aac0e0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>RTC_TIMETYPE_SECOND</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gga8144898fe628404d396db06dc8aac0e0ac0bdc1ff011be37cfeecb77c241e7fb8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>RTC_TIMETYPE_MINUTE</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gga8144898fe628404d396db06dc8aac0e0a11974e5996bfe6fbf0381d7ef3836964</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>RTC_TIMETYPE_HOUR</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gga8144898fe628404d396db06dc8aac0e0a4e88c263358395fecc19306556addacc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>RTC_TIMETYPE_DAYOFMONTH</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gga8144898fe628404d396db06dc8aac0e0a3cc8b55755f86e8a6a1a870a79122324</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>RTC_TIMETYPE_DAYOFWEEK</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gga8144898fe628404d396db06dc8aac0e0a88f328753d58927fafd45b35e0815e80</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>RTC_TIMETYPE_DAYOFYEAR</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gga8144898fe628404d396db06dc8aac0e0ad05ce02297b482d4fa5b6a491ff04aff</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>RTC_TIMETYPE_MONTH</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gga8144898fe628404d396db06dc8aac0e0a45d2078908fb25a714cbd01766f55fae</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>RTC_TIMETYPE_YEAR</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gga8144898fe628404d396db06dc8aac0e0a780e93b1c505ed02ed139894566fcfe0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>RTC_INT_OPT_T</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gacbb4f3e21ac0f90878c95afe11f49161</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>RTC_INT_COUNTER_INCREASE</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ggacbb4f3e21ac0f90878c95afe11f49161a1545f38fd2671cb7521a77e7da3bde4d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>RTC_INT_ALARM</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ggacbb4f3e21ac0f90878c95afe11f49161adec1af649886bf5a419d636276ca54a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_RTC_ResetClockTickCounter</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga1d569e8d5d570f6c79d2d1f803bb5f7c</anchor>
      <arglist>(LPC_RTC_T *pRTC)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_RTC_Enable</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gaad05032c6d6c4bc5ea9e02311cdc9a18</anchor>
      <arglist>(LPC_RTC_T *pRTC, FunctionalState NewState)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_RTC_CntIncrIntConfig</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gaddaf7559a23df0704358128234fcb214</anchor>
      <arglist>(LPC_RTC_T *pRTC, uint32_t cntrMask, FunctionalState NewState)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_RTC_AlarmIntConfig</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga2ecd6a555d1a1977a80a30ca21645ca4</anchor>
      <arglist>(LPC_RTC_T *pRTC, uint32_t alarmMask, FunctionalState NewState)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_RTC_SetTime</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga8104b580aeb00a3d5a9e325b3162b1bb</anchor>
      <arglist>(LPC_RTC_T *pRTC, RTC_TIMEINDEX_T Timetype, uint32_t TimeValue)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_RTC_GetTime</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga661c73c8fce7243b30a207ad7cbee59b</anchor>
      <arglist>(LPC_RTC_T *pRTC, RTC_TIMEINDEX_T Timetype)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_RTC_SetFullTime</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga501471295a030ca2dc2872577367073e</anchor>
      <arglist>(LPC_RTC_T *pRTC, RTC_TIME_T *pFullTime)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_RTC_GetFullTime</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gab03d971e0b77ba907f414cdc00acce3b</anchor>
      <arglist>(LPC_RTC_T *pRTC, RTC_TIME_T *pFullTime)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_RTC_SetAlarmTime</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga7ac6a9a813f710e8e93c4b598a14a795</anchor>
      <arglist>(LPC_RTC_T *pRTC, RTC_TIMEINDEX_T Timetype, uint32_t ALValue)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_RTC_GetAlarmTime</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gaf094ef7c4ff7295c74be8ab77e39a967</anchor>
      <arglist>(LPC_RTC_T *pRTC, RTC_TIMEINDEX_T Timetype)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_RTC_SetFullAlarmTime</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga7ad6b4d1d2aaaf093ddde0cef9023ba0</anchor>
      <arglist>(LPC_RTC_T *pRTC, RTC_TIME_T *pFullTime)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_RTC_GetFullAlarmTime</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga21b9a7c640870482b47deda15ff0a01b</anchor>
      <arglist>(LPC_RTC_T *pRTC, RTC_TIME_T *pFullTime)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_REGFILE_Write</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga8607152173e77715f7cc42be74799b65</anchor>
      <arglist>(LPC_REGFILE_T *pRegFile, uint8_t index, uint32_t value)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_REGFILE_Read</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gafdad7b4e551aa7e6d52b49a7a072f4ae</anchor>
      <arglist>(LPC_REGFILE_T *pRegFile, uint8_t index)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_RTC_CalibCounterCmd</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga987c9ab06dc2bb157388ddf9159de813</anchor>
      <arglist>(LPC_RTC_T *pRTC, FunctionalState NewState)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_RTC_CalibConfig</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga4f0a93e9a72f793b5891e2d691a1d35d</anchor>
      <arglist>(LPC_RTC_T *pRTC, uint32_t CalibValue, uint8_t CalibDir)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_RTC_ClearIntPending</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gaa43865e87cf61d579a4ee52a307b3e30</anchor>
      <arglist>(LPC_RTC_T *pRTC, uint32_t IntType)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE IntStatus</type>
      <name>Chip_RTC_GetIntPending</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gaab21524984ac344d4f508d2dfd6c5098</anchor>
      <arglist>(LPC_RTC_T *pRTC, uint32_t IntType)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_RTC_Init</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>gac37fe41fed088f1336797e05674125ff</anchor>
      <arglist>(LPC_RTC_T *pRTC)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_RTC_DeInit</name>
      <anchorfile>group__RTC__17XX__40XX.html</anchorfile>
      <anchor>ga63cc16f1c4b72523e0e67a6c651f0026</anchor>
      <arglist>(LPC_RTC_T *pRTC)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>SDC_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx SD Card Interafce driver</title>
    <filename>group__SDC__17XX__40XX.html</filename>
  </compound>
  <compound kind="group">
    <name>SDMMC_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx SDMMC card driver</title>
    <filename>group__SDMMC__17XX__40XX.html</filename>
  </compound>
  <compound kind="group">
    <name>SPI_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx SPI driver</title>
    <filename>group__SPI__17XX__40XX.html</filename>
  </compound>
  <compound kind="group">
    <name>SPIFI_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx SPIFI Driver</title>
    <filename>group__SPIFI__17XX__40XX.html</filename>
  </compound>
  <compound kind="group">
    <name>SSP_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx SSP driver</title>
    <filename>group__SSP__17XX__40XX.html</filename>
    <class kind="struct">LPC_SSP_T</class>
    <class kind="struct">SSP_ConfigFormat</class>
    <class kind="struct">SPI_Address_t</class>
    <class kind="struct">Chip_SSP_DATA_SETUP_T</class>
    <member kind="define">
      <type>#define</type>
      <name>SSP_CR0_DSS</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga691ba9dbc6a0722a81ed4734c7f7ac8f</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SSP_CR0_FRF_SPI</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga4f0f58a8f4b87af0f18e84b981c31a74</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SSP_CR0_FRF_TI</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga54c718a1a75a1e5e06417b9f8267ee27</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SSP_CR0_FRF_MICROWIRE</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga7ca858fcf0f529a38e1e1bf0a69d4486</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SSP_CR0_CPOL_LO</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gab4353fed07ef845a3796e154397f7e76</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SSP_CR0_CPHA_FIRST</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gaee3465bdb33add1970f6ce7f7bc638c4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SSP_CR0_SCR</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga10f56047b6024ff848675f9463f1b989</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SSP_CR0_SCR</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga10f56047b6024ff848675f9463f1b989</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SSP_CR0_BITMASK</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga90be93bebdbdfee011d90ea6e054260a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SSP_CR0_BITMASK</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga90be93bebdbdfee011d90ea6e054260a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SSP_CR1_LBM_EN</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gac0e5bef37b94df5ad96bf270aa802dcd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SSP_CR1_SSP_EN</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gaad500ed8cec6c1734a12a7f55ff6ec26</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SSP_CR1_SLAVE_EN</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga483d570ffc25bc917c99b3e8ece75649</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SSP_CR1_SO_DISABLE</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gaf8cd75ca0bf07a236b992cca4769b4dc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SSP_CR1_BITMASK</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gad90a9c1c97a5c4e19e048e9686a4d8fa</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SSP_CPSR_BITMASK</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gad90cbeb91495d457ae2dd8bda909a2a9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SSP_DR_BITMASK</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gaca7cab2d530cc7dbaa9e9d3e9dc61b53</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SSP_SR_BITMASK</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga0fe66130dd87296b6e16cd9fbcf7daf1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SSP_ICR_BITMASK</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga5ce108586bfd5b77c849aa9969c8973c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SSP_CPHA_FIRST</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga6333b5eaf9d5301431fc0399c0d417d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SSP_CPOL_HI</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gaf64aec37a92ca6c14c23af6fc0052ccb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SSP_SLAVE_MODE</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gac6bc4b92810caa934b2d7116390098c6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>SSP_STATUS_T</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga6b6d188b60d297514c6c2acb25846543</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_STAT_TFE</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga6b6d188b60d297514c6c2acb25846543af2476dad0360373ccb5896f6f4283569</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_STAT_TNF</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga6b6d188b60d297514c6c2acb25846543a895067b502eeae3c476190e309abde1a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_STAT_RNE</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga6b6d188b60d297514c6c2acb25846543a82f15e0720cc2b0dc53e90b7546fb96b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_STAT_RFF</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga6b6d188b60d297514c6c2acb25846543ad10671a086364beb55efad01f2c3688f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_STAT_BSY</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga6b6d188b60d297514c6c2acb25846543a5129659a9c16fdda33b925f07cf8ef52</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>SSP_INTMASK_T</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga1f0581ef381f19b1e2ec388aad98924e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_RORIM</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga1f0581ef381f19b1e2ec388aad98924ea891bf6d7622b4f6f010396f08a51adbf</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_RTIM</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga1f0581ef381f19b1e2ec388aad98924eaabb6398ce8ef5db95fc47fc2bd525f13</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_RXIM</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga1f0581ef381f19b1e2ec388aad98924ea2fff5a42799aae53c7a4bf3e3a900448</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_TXIM</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga1f0581ef381f19b1e2ec388aad98924ea34e61a5c1d77d3763e4f8e1bfacbdcc4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>SSP_MASKINTSTATUS_T</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga1a2c0b533e7a6d9261b4d6d98b14803f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_RORMIS</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga1a2c0b533e7a6d9261b4d6d98b14803fa05e1bf0ddb0306501fcb234c6e885082</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_RTMIS</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga1a2c0b533e7a6d9261b4d6d98b14803fa86603f74ba22232e10e325e2f0242308</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_RXMIS</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga1a2c0b533e7a6d9261b4d6d98b14803fa07742f0e285a6bf09099e3298fac1184</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_TXMIS</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga1a2c0b533e7a6d9261b4d6d98b14803fa8ad9ea51c1cde0cd518cdea049710093</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>SSP_RAWINTSTATUS_T</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga85eced3ab876414dfbdd09825f88a9ce</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_RORRIS</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga85eced3ab876414dfbdd09825f88a9cead112fd53fe6238566db3b86badca3643</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_RTRIS</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga85eced3ab876414dfbdd09825f88a9ceacadb0b951bbe2b20da961f4a55b775c2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_RXRIS</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga85eced3ab876414dfbdd09825f88a9ceaf284137785b0311fdf9ffd06dd91022f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_TXRIS</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga85eced3ab876414dfbdd09825f88a9cea0f19a960fdb1f4e1e41c2d3f391ab2a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>SSP_DMA_T</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga2516864746b851ab74be142f1704682e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_DMA_RX</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga2516864746b851ab74be142f1704682eae51f79e5713056c61aadb8d5ddfc335a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_DMA_TX</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga2516864746b851ab74be142f1704682ea5925a5061635b0742605b6bb4b434ce4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>CHIP_SSP_CLOCK_MODE_T</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga8efae4a5aae79c90592fe135c01486ca</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_CLOCK_CPHA0_CPOL0</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga8efae4a5aae79c90592fe135c01486caaa5340f759c78820f18de9290b9c061c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_CLOCK_CPHA0_CPOL1</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga8efae4a5aae79c90592fe135c01486caabf7efd605c96ca17f8d3e142583573c7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_CLOCK_CPHA1_CPOL0</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga8efae4a5aae79c90592fe135c01486caae85026628900cd0603aea8741196e37c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_CLOCK_CPHA1_CPOL1</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga8efae4a5aae79c90592fe135c01486caab45a1654a40d69da708a2fd3717b3b3f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_CLOCK_MODE0</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga8efae4a5aae79c90592fe135c01486caa356a1e930e648bee4fdcf58bbf2be871</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_CLOCK_MODE1</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga8efae4a5aae79c90592fe135c01486caa9c0ef0c2b54bd1f7cd915c072bb90c4f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_CLOCK_MODE2</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga8efae4a5aae79c90592fe135c01486caa0ead0c9201c4f3ed09b8d8395b15e38d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_CLOCK_MODE3</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga8efae4a5aae79c90592fe135c01486caa509d039d8fa85d04307fa845b5f96ef0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>CHIP_SSP_FRAME_FORMAT_T</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga63017a49bd68c82747f2900f9decb394</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_FRAMEFORMAT_SPI</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga63017a49bd68c82747f2900f9decb394a04830722a3c3c26165b6e86944f80799</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CHIP_SSP_FRAME_FORMAT_TI</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga63017a49bd68c82747f2900f9decb394a958fc5c359dea48d02188bebb93f3fd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_FRAMEFORMAT_MICROWIRE</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga63017a49bd68c82747f2900f9decb394a44c2cf7ad92d503bb0e32fa644b847d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>CHIP_SSP_BITS_T</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga9f7d343aff0392b45fc2d9db69dbe816</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_BITS_4</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga9f7d343aff0392b45fc2d9db69dbe816aa03e3d7c54b1b7e0a1159819be0fe2f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_BITS_5</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga9f7d343aff0392b45fc2d9db69dbe816ad68718bef531b459a981f3fe333885af</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_BITS_6</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga9f7d343aff0392b45fc2d9db69dbe816a983bf05fec7f08931bf8397460736fad</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_BITS_7</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga9f7d343aff0392b45fc2d9db69dbe816af427f7d2bdddeddab9780dde3e7112cd</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_BITS_8</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga9f7d343aff0392b45fc2d9db69dbe816afed74a949c86fb130814553eeeaa28fc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_BITS_9</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga9f7d343aff0392b45fc2d9db69dbe816a86c78df806e6a2c6105e4009c87a9a30</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_BITS_10</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga9f7d343aff0392b45fc2d9db69dbe816aa5af8a64fabb893ce6cca58a6603b887</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_BITS_11</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga9f7d343aff0392b45fc2d9db69dbe816a6eb61ba3d5420049cc5c6b5dc0ce180f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_BITS_12</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga9f7d343aff0392b45fc2d9db69dbe816ac07c29c55be04e5bdf6c450f8fb978f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_BITS_13</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga9f7d343aff0392b45fc2d9db69dbe816ad3b1fe6ae4d631d30f668a62a499ad04</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_BITS_14</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga9f7d343aff0392b45fc2d9db69dbe816ae1d25567553d78628b76e6d3576708fb</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_BITS_15</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga9f7d343aff0392b45fc2d9db69dbe816a0cb36a4e3580bffe6583e05ecc7560aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_BITS_16</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga9f7d343aff0392b45fc2d9db69dbe816a3d5cc36200365dc046d283f48711d885</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>CHIP_SSP_MODE_T</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga709280d66047a8015d296e272d1e8b54</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_MODE_MASTER</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga709280d66047a8015d296e272d1e8b54a5395d2342a5a2564d683efe73700d604</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SSP_MODE_SLAVE</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gga709280d66047a8015d296e272d1e8b54a8e80727639bca1e64508d92d65346f3e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SSP_Enable</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gaf49b9a4689c9ae39bbd8c1ac20d31073</anchor>
      <arglist>(LPC_SSP_T *pSSP)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SSP_Disable</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga3033c296868595a01dd74ecccaed6090</anchor>
      <arglist>(LPC_SSP_T *pSSP)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SSP_EnableLoopBack</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga8683ccce6ba5578103efcb791f39cff8</anchor>
      <arglist>(LPC_SSP_T *pSSP)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SSP_DisableLoopBack</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gaa733ed4b0773cda022ad87ff41304c40</anchor>
      <arglist>(LPC_SSP_T *pSSP)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE FlagStatus</type>
      <name>Chip_SSP_GetStatus</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga82dd278bcdbd80eaacc43abc211a970f</anchor>
      <arglist>(LPC_SSP_T *pSSP, SSP_STATUS_T Stat)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_SSP_GetIntStatus</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga207244e33021333a66bb04f2bd2f1102</anchor>
      <arglist>(LPC_SSP_T *pSSP)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE IntStatus</type>
      <name>Chip_SSP_GetRawIntStatus</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga0cc48f6c5bea491f2965b5b6fd0dcf69</anchor>
      <arglist>(LPC_SSP_T *pSSP, SSP_RAWINTSTATUS_T RawInt)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint8_t</type>
      <name>Chip_SSP_GetDataSize</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga421d39f6094d0f335e5acbc3dd5f0b09</anchor>
      <arglist>(LPC_SSP_T *pSSP)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SSP_ClearIntPending</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga5fe8047a36b3055251cff755d339ca4a</anchor>
      <arglist>(LPC_SSP_T *pSSP, SSP_INTCLEAR_T IntClear)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SSP_Int_Enable</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga98eb3a788e313aeb5a4feb2516b11e8f</anchor>
      <arglist>(LPC_SSP_T *pSSP)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SSP_Int_Disable</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga20c7c516c84ba924973318bd64c113a3</anchor>
      <arglist>(LPC_SSP_T *pSSP)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint16_t</type>
      <name>Chip_SSP_ReceiveFrame</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga7da053acf90aff24ca59bdf673207aac</anchor>
      <arglist>(LPC_SSP_T *pSSP)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SSP_SendFrame</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gab01849b80cad7f46924a04346560006c</anchor>
      <arglist>(LPC_SSP_T *pSSP, uint16_t tx_data)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_SSP_SetClockRate</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga49832a18e0618a82afd66caa6f868445</anchor>
      <arglist>(LPC_SSP_T *pSSP, uint32_t clk_rate, uint32_t prescale)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SSP_SetFormat</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga381ba3a6b470b2c84468b88deed8ac18</anchor>
      <arglist>(LPC_SSP_T *pSSP, uint32_t bits, uint32_t frameFormat, uint32_t clockMode)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SSP_Set_Mode</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga9b5a23b5030facdb75c3ed06d5e86172</anchor>
      <arglist>(LPC_SSP_T *pSSP, uint32_t mode)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SSP_DMA_Enable</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gae8bf34541c093c052e5f9baf41fcda8b</anchor>
      <arglist>(LPC_SSP_T *pSSP)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SSP_DMA_Disable</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gaa8c7ce019dfcc4ab5731615f66c30e19</anchor>
      <arglist>(LPC_SSP_T *pSSP)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_SSP_Int_FlushData</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gabf29dfba7478866abe7511d32638e57e</anchor>
      <arglist>(LPC_SSP_T *pSSP)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_SSP_Int_RWFrames8Bits</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga23d901d1757b6d95efc20c4d76721fb3</anchor>
      <arglist>(LPC_SSP_T *pSSP, Chip_SSP_DATA_SETUP_T *xf_setup)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>Chip_SSP_Int_RWFrames16Bits</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gaf97dd891912b8312a1e0989d7a542b7b</anchor>
      <arglist>(LPC_SSP_T *pSSP, Chip_SSP_DATA_SETUP_T *xf_setup)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_SSP_RWFrames_Blocking</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga302a381ad4d291164144ad2720399078</anchor>
      <arglist>(LPC_SSP_T *pSSP, Chip_SSP_DATA_SETUP_T *xf_setup)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_SSP_WriteFrames_Blocking</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>gae64bd476b75c03d0b952f08ea42a09be</anchor>
      <arglist>(LPC_SSP_T *pSSP, uint8_t *buffer, uint32_t buffer_len)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_SSP_ReadFrames_Blocking</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga8332233bb63af754bd9cc369f2a1e2d6</anchor>
      <arglist>(LPC_SSP_T *pSSP, uint8_t *buffer, uint32_t buffer_len)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_SSP_Init</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga66e20405561e8d3dacba65cbfe41d556</anchor>
      <arglist>(LPC_SSP_T *pSSP)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_SSP_DeInit</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga48f87506f2fddc1043606eae292b6f16</anchor>
      <arglist>(LPC_SSP_T *pSSP)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_SSP_SetMaster</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga60e601329b0aa6afe5f355dc6e8f84bd</anchor>
      <arglist>(LPC_SSP_T *pSSP, bool master)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_SSP_SetBitRate</name>
      <anchorfile>group__SSP__17XX__40XX.html</anchorfile>
      <anchor>ga373660d8ad7b28fb71209539b1e72717</anchor>
      <arglist>(LPC_SSP_T *pSSP, uint32_t bitRate)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>Stop_Watch</name>
    <title>CHIP: Stopwatch primitives.</title>
    <filename>group__Stop__Watch.html</filename>
    <member kind="function">
      <type>void</type>
      <name>StopWatch_Init</name>
      <anchorfile>group__Stop__Watch.html</anchorfile>
      <anchor>gaa0f8ea277b700d20b4322a15d9ddf32c</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>StopWatch_Start</name>
      <anchorfile>group__Stop__Watch.html</anchorfile>
      <anchor>ga0dbab611d6cbdd4faad5018131aca140</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>StopWatch_Elapsed</name>
      <anchorfile>group__Stop__Watch.html</anchorfile>
      <anchor>ga0a0b6b9a4391ae5f4fcccb22a2a35f73</anchor>
      <arglist>(uint32_t startTime)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>StopWatch_TicksPerSecond</name>
      <anchorfile>group__Stop__Watch.html</anchorfile>
      <anchor>gae780d65d75ed3ca6dbd33389479eed9f</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>StopWatch_TicksToMs</name>
      <anchorfile>group__Stop__Watch.html</anchorfile>
      <anchor>gab62ee68f7b01b5c14b0d19c3c391ca02</anchor>
      <arglist>(uint32_t ticks)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>StopWatch_TicksToUs</name>
      <anchorfile>group__Stop__Watch.html</anchorfile>
      <anchor>ga76fc3c7b473615677932efe3e5d0e142</anchor>
      <arglist>(uint32_t ticks)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>StopWatch_MsToTicks</name>
      <anchorfile>group__Stop__Watch.html</anchorfile>
      <anchor>ga00f0c8411acd07344c7dd3cac8ec6d3e</anchor>
      <arglist>(uint32_t mS)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>StopWatch_UsToTicks</name>
      <anchorfile>group__Stop__Watch.html</anchorfile>
      <anchor>ga65ab3801fdb76aab8879a0698fc4df45</anchor>
      <arglist>(uint32_t uS)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>StopWatch_DelayTicks</name>
      <anchorfile>group__Stop__Watch.html</anchorfile>
      <anchor>ga744f358982209fe277eb21843e88a2d9</anchor>
      <arglist>(uint32_t ticks)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>StopWatch_DelayMs</name>
      <anchorfile>group__Stop__Watch.html</anchorfile>
      <anchor>gaa3bfba465962b310e4a5bd18292e8f87</anchor>
      <arglist>(uint32_t mS)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>StopWatch_DelayUs</name>
      <anchorfile>group__Stop__Watch.html</anchorfile>
      <anchor>ga3bc7706d1a9cb32888f82b2874c3655c</anchor>
      <arglist>(uint32_t uS)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>SYSCTL_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx System Control block driver</title>
    <filename>group__SYSCTL__17XX__40XX.html</filename>
    <class kind="struct">SYSCTL_PLL_REGS_T</class>
    <class kind="struct">LPC_SYSCTL_T</class>
    <member kind="define">
      <type>#define</type>
      <name>SYSCTL_RST_POR</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>gaeb159c3928c725d8e218dbb5426f99b8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SYSCTL_RST_EXTRST</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ga7e481a088f2ff36fc65b77dbb25a749a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SYSCTL_RST_WDT</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ga84b4a0f567758052c1f4097d51aaa1ae</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SYSCTL_RST_BOD</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>gab1441ee02763d5fa7073bcf026036784</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SYSCTL_PD_SMFLAG</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>gab8cdb571c0a7cd9485523882eb033f79</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SYSCTL_PD_DSFLAG</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ga25e12d7c7721dd11b00c5a7a8ec51457</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SYSCTL_PD_PDFLAG</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ga64388699b0fcd5317d80927e6c8a1db9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SYSCTL_PD_DPDFLAG</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ga6b2596afa60ba5439b3bb53e88df3e62</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>CHIP_SYSCTL_PLL_T</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ga5f5478a201b021ed04a0724bff524c4b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_MAIN_PLL</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>gga5f5478a201b021ed04a0724bff524c4bad3626cb96bb7a84a9bce324812044d21</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_USB_PLL</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>gga5f5478a201b021ed04a0724bff524c4ba5ee38509163556b883664d7259c43dfd</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>FMC_FLASHTIM_T</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ga0779e088e3fa7b3a18e66fa0949da8a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FLASHTIM_20MHZ_CPU</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>gga0779e088e3fa7b3a18e66fa0949da8a7aeb017487ff6fd051dfc885ba063415a3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FLASHTIM_40MHZ_CPU</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>gga0779e088e3fa7b3a18e66fa0949da8a7a617391e132f72d731806d6af10863407</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FLASHTIM_60MHZ_CPU</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>gga0779e088e3fa7b3a18e66fa0949da8a7af482ee6a465905fd232ff2da7f993514</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FLASHTIM_80MHZ_CPU</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>gga0779e088e3fa7b3a18e66fa0949da8a7aff2c5410ad123adc43eb1a781f71ffe8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FLASHTIM_100MHZ_CPU</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>gga0779e088e3fa7b3a18e66fa0949da8a7af386ac4dceedf99032479736a40c7257</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FLASHTIM_120MHZ_CPU</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>gga0779e088e3fa7b3a18e66fa0949da8a7a747d8440c3924f3546bbb895b101e085</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FLASHTIM_SAFE_SETTING</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>gga0779e088e3fa7b3a18e66fa0949da8a7a769f5cbe41f1f487aeaf3ca300be86fb</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>CHIP_SYSCTL_BOOT_MODE_REMAP_T</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ga1115b38ab39d6c5369e9b00c9387fbcc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>REMAP_BOOT_LOADER_MODE</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>gga1115b38ab39d6c5369e9b00c9387fbccac021b05380bf12e261025e5a86ae6fc4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>REMAP_USER_FLASH_MODE</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>gga1115b38ab39d6c5369e9b00c9387fbcca959725d695548ba6fe27fdbc006623e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>CHIP_SYSCTL_RESET_T</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>gaecf57441c240b2e654da2a7de6da0f7b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_LCD</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7baed98debf56f4be417183448fbbff92d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_TIMER0</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba3646ac9d3dd373fd587f5281e86143be</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_TIMER1</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7baa590b00b3c51dcd756e7b5951c1e525f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_UART0</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba8441e64c743b5241188005595b21219e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_UART1</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7bab41090743fac87a57b444896875ed959</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_PWM0</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba4706614acb98dc27a0e5866c96c46c50</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_PWM1</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7baea04ecce5d82d8226afb99da846deee9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_I2C0</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba1bed6e5be953651c0ef47d8cf904a8e9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_UART4</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba6ceb03615f8d28db71bd2d6177b6c45f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_RTC</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba48614c5d1c26d28c528b20318d42abef</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_SSP1</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba0d28a7a6f404c1d165997390d26d7284</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_EMC</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7baf1c841fe4b3099132d0e1b72ffd2e690</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_ADC</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba989c7156e441750fb330394490e6c320</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_CAN1</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba4bc1982378a526a7ab1b9bab4513233e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_CAN2</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba16f62d8235af020ae6ea4b5288ff15b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_GPIO</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba464cabe264192a35ca432b1dda796388</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_SPIFI</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7bacb3f3b67771790e282117d4ad33fc19c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_MCPWM</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba10bd86a7a5e273255a5a75e20adcf350</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_QEI</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba02b1bb91432fffccb96b03f862d0e1d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_I2C1</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba170c46420669c6a9b188f2def4324677</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_SSP2</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba2d825fb70573fbc92821326b6a844930</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_SSP0</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba68902860edbee49e9e481f92903693b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_TIMER2</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba4a424deff0df97116354dfeaf7d3df7b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_TIMER3</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7baa74a3061ccd70ed19f189d19793e1c07</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_UART2</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba7ae0f9c361aad8287f6e0108383d6d12</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_UART3</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba76f86efe400f2bf929dd41e25c6e72dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_I2C2</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7baed261cfe273b05189b77cf280896d699</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_I2S</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7bab46bf1f20826387cc2d5e881e59aa08d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_PCSDC</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba88dfb49974f2c29bdaa4a83c9e37d423</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_GPDMA</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba6b60e8e3c03464c2a32736186be4fec2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_ENET</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7baebefd16733d5531076972791fed8a248</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_USB</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba55a0845b870e64126a930a11856b51f7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_IOCON</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba27c83420589d4ba328bff407f8181864</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_DAC</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7baa4850057330c9eedac61bb1b9b217404</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>SYSCTL_RESET_CANACC</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ggaecf57441c240b2e654da2a7de6da0f7ba9095ea547e053380a83c10b3fdee5e47</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SYSCTL_SetFLASHAccess</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>gacaea88993f709565eb0166779426b6d8</anchor>
      <arglist>(FMC_FLASHTIM_T clks)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SYSCTL_Map</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>gab44b1cefe990081961a517994babfcbd</anchor>
      <arglist>(CHIP_SYSCTL_BOOT_MODE_REMAP_T remap)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_SYSCTL_GetSystemRSTStatus</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>gadd10c121cb32ddc55dd62e13de780c59</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SYSCTL_ClearSystemRSTStatus</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ga565050aac84c3cfa86874d42630d7eb8</anchor>
      <arglist>(uint32_t reset)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SYSCTL_EnableBOD</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ga68a4fd386020cb12d47b03f95903b9f1</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SYSCTL_DisableBOD</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ga20468997a597d6fcae205472c22a949c</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SYSCTL_EnableBODReset</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ga4b04d78cf397ffd3980b2f4ded6a055d</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SYSCTL_DisableBODReset</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>gaee6f6a45b42962728419d15328427564</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SYSCTL_EnableBODRPM</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ga6fa1a6e737b1ac612ee778894303a270</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SYSCTL_DisableBODRPM</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ga758135a9cf9c949acbee97291eb447bf</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_SYSCTL_GetClrSleepFlags</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>gab80482c3d12f51ef87131497cb67a326</anchor>
      <arglist>(uint32_t flags)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SYSCTL_EnableBoost</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>gaaf987cc6cf1398ae226aaebb975a6372</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_SYSCTL_DisableBoost</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ga3c49515056cbc9af430e33f9d8401487</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_SYSCTL_PeriphReset</name>
      <anchorfile>group__SYSCTL__17XX__40XX.html</anchorfile>
      <anchor>ga94ee3796eba43742e11809eacb88269b</anchor>
      <arglist>(CHIP_SYSCTL_RESET_T periph)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>TIMER_17XX_40XX</name>
    <title>CHIP: LPc17xx/40xx 16/32-bit Timer driver</title>
    <filename>group__TIMER__17XX__40XX.html</filename>
    <class kind="struct">LPC_TIMER_T</class>
    <member kind="define">
      <type>#define</type>
      <name>TIMER_IR_CLR</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga7c74d9b89f53a497dd1128e2f4b2670b</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TIMER_MATCH_INT</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga1f2ad401455a401aa8400fd343eadd1a</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TIMER_CAP_INT</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga085de5b6eb6d51b2ef555bd06f267f83</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TIMER_ENABLE</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>gace9bc6168bdb15fd3c88da899ad3f154</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TIMER_RESET</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga892a0fbd1ea794e5c6dbbc66e9703bd6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TIMER_INT_ON_MATCH</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga7fd49e94768d8d2016f72965f904f5e1</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TIMER_RESET_ON_MATCH</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga0d69103df0ceea6cb41286bc8e7536b3</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TIMER_STOP_ON_MATCH</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga7362986ae8e43df0e575eba1789722f4</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TIMER_CAP_RISING</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga34ab45ae29494cf8f92fdc4e6eb7bb06</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TIMER_CAP_FALLING</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>gad35ff35bf622cb530f2206ecdd861a7d</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TIMER_INT_ON_CAP</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga39a441a62cb174fd89ebc217417482c9</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>TIMER_PIN_MATCH_STATE_T</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga37bff84e8ff6aeda06f159e0229426d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>TIMER_EXTMATCH_DO_NOTHING</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>gga37bff84e8ff6aeda06f159e0229426d3aee61101edb52ea5ce70b74a41766467f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>TIMER_EXTMATCH_CLEAR</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>gga37bff84e8ff6aeda06f159e0229426d3a0fc4886f55eb7bcfda4baea4c08c551a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>TIMER_EXTMATCH_SET</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>gga37bff84e8ff6aeda06f159e0229426d3a059e5dedfdab474ecafba9827116f5ff</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>TIMER_EXTMATCH_TOGGLE</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>gga37bff84e8ff6aeda06f159e0229426d3aa7b4fdd349721213351a4bba29472215</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>TIMER_CAP_SRC_STATE_T</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>gac1ee4c4905934b36a45a016b1344e8aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>TIMER_CAPSRC_RISING_PCLK</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ggac1ee4c4905934b36a45a016b1344e8aaa9c110041557616b65aff5c20d03ae8be</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>TIMER_CAPSRC_RISING_CAPN</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ggac1ee4c4905934b36a45a016b1344e8aaae17c344c6e80cc8f2a0e6f5c4bc32269</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>TIMER_CAPSRC_FALLING_CAPN</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ggac1ee4c4905934b36a45a016b1344e8aaaf07e78a49ce7829f3a7975196aeb5a5c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>TIMER_CAPSRC_BOTH_CAPN</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ggac1ee4c4905934b36a45a016b1344e8aaaa09ba96751f9c750f9257050053ccb62</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_TIMER_Init</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>gac2ca0aff00ae8a651e129afba400c833</anchor>
      <arglist>(LPC_TIMER_T *pTMR)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_TIMER_DeInit</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>gaa52f3e33303d7d4f1e2325586a21a5c0</anchor>
      <arglist>(LPC_TIMER_T *pTMR)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE bool</type>
      <name>Chip_TIMER_MatchPending</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga0d61fd61d18ba82d44f1b5fec2e48a76</anchor>
      <arglist>(LPC_TIMER_T *pTMR, int8_t matchnum)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE bool</type>
      <name>Chip_TIMER_CapturePending</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>gae9a2575f38b3acaf6255caf15c5b65df</anchor>
      <arglist>(LPC_TIMER_T *pTMR, int8_t capnum)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_TIMER_ClearMatch</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>gae9ad45169f0511d27696923acfd6a17e</anchor>
      <arglist>(LPC_TIMER_T *pTMR, int8_t matchnum)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_TIMER_ClearCapture</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga0aebc9314c86b4e9a67e2d08bab38e24</anchor>
      <arglist>(LPC_TIMER_T *pTMR, int8_t capnum)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_TIMER_Enable</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>gaff500707a8a397daf29cc84f454802b2</anchor>
      <arglist>(LPC_TIMER_T *pTMR)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_TIMER_Disable</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>gae7fedfd4c2543991eddd7be1b32bd00b</anchor>
      <arglist>(LPC_TIMER_T *pTMR)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_TIMER_ReadCount</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga6050d4da70d679696b3af922b8c1a6ac</anchor>
      <arglist>(LPC_TIMER_T *pTMR)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_TIMER_ReadPrescale</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga52328278a0f1326c6ce7dc210ad010d3</anchor>
      <arglist>(LPC_TIMER_T *pTMR)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_TIMER_PrescaleSet</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>gaa2483e6483702140e11de3183d5271f9</anchor>
      <arglist>(LPC_TIMER_T *pTMR, uint32_t prescale)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_TIMER_SetMatch</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>gaff98bdf0254cd7783c7b42655fa43cd2</anchor>
      <arglist>(LPC_TIMER_T *pTMR, int8_t matchnum, uint32_t matchval)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_TIMER_ReadCapture</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga7eab047dd60eef7fb0f042266f9dae05</anchor>
      <arglist>(LPC_TIMER_T *pTMR, int8_t capnum)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_TIMER_Reset</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>gaba0336e88cc662505e2dde1eabff1aaf</anchor>
      <arglist>(LPC_TIMER_T *pTMR)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_TIMER_MatchEnableInt</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga21daeb9b42a0f7fe57ec52f3815ab223</anchor>
      <arglist>(LPC_TIMER_T *pTMR, int8_t matchnum)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_TIMER_MatchDisableInt</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga81252a6e24fddbf78d62f791f589306e</anchor>
      <arglist>(LPC_TIMER_T *pTMR, int8_t matchnum)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_TIMER_ResetOnMatchEnable</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga4b8a551b290e9f70ba8b8569e2937f58</anchor>
      <arglist>(LPC_TIMER_T *pTMR, int8_t matchnum)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_TIMER_ResetOnMatchDisable</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>gae203cf1a04cbbf63e966de3a5bd9c29e</anchor>
      <arglist>(LPC_TIMER_T *pTMR, int8_t matchnum)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_TIMER_StopOnMatchEnable</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>gaf76a0a1282598f976f04c00595b6332c</anchor>
      <arglist>(LPC_TIMER_T *pTMR, int8_t matchnum)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_TIMER_StopOnMatchDisable</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>gae1be576a72246ab2255f735245fb49a7</anchor>
      <arglist>(LPC_TIMER_T *pTMR, int8_t matchnum)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_TIMER_CaptureRisingEdgeEnable</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga7768003112560a8cbd06582fa8747fae</anchor>
      <arglist>(LPC_TIMER_T *pTMR, int8_t capnum)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_TIMER_CaptureRisingEdgeDisable</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga25bcfd101f052ee941da5bae28d84fcd</anchor>
      <arglist>(LPC_TIMER_T *pTMR, int8_t capnum)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_TIMER_CaptureFallingEdgeEnable</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga9938c5dc5cf1aa81064cf2e14ab29d44</anchor>
      <arglist>(LPC_TIMER_T *pTMR, int8_t capnum)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_TIMER_CaptureFallingEdgeDisable</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga521a3308abdffb693f4785b739dae98f</anchor>
      <arglist>(LPC_TIMER_T *pTMR, int8_t capnum)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_TIMER_CaptureEnableInt</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga141861bd1b18812fcfd231b8a42065d8</anchor>
      <arglist>(LPC_TIMER_T *pTMR, int8_t capnum)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_TIMER_CaptureDisableInt</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga75d24b5365354b481b285c7c718f3791</anchor>
      <arglist>(LPC_TIMER_T *pTMR, int8_t capnum)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_TIMER_ExtMatchControlSet</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>gaf58d175fd6011349b7331055422b9e28</anchor>
      <arglist>(LPC_TIMER_T *pTMR, int8_t initial_state, TIMER_PIN_MATCH_STATE_T matchState, int8_t matchnum)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_TIMER_TIMER_SetCountClockSrc</name>
      <anchorfile>group__TIMER__17XX__40XX.html</anchorfile>
      <anchor>ga676ac53fdb5dd31f0288ea5c5023709d</anchor>
      <arglist>(LPC_TIMER_T *pTMR, TIMER_CAP_SRC_STATE_T capSrc, int8_t capnum)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>UART_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx UART driver</title>
    <filename>group__UART__17XX__40XX.html</filename>
    <class kind="struct">LPC_USART_T</class>
    <member kind="define">
      <type>#define</type>
      <name>UART_RBR_MASKBIT</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga06774e65c2ca095c4373122ed9a390b8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LOAD_DLL</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga55a89461d99a43769772276e51a6710a</anchor>
      <arglist>(div)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_DLL_MASKBIT</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga85050a24048ffc2de997cd60ea67f9df</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LOAD_DLM</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gac53f4cc36f13edd3fdf7fd9bab1360e2</anchor>
      <arglist>(div)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_DLM_MASKBIT</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gaf4d480e07f82896893e45b572adeffcd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_IER_RBRINT</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gafe8ecd345fb121d6b0ce19f4ce6672ba</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_IER_THREINT</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gaa0d5f875782af503852f8b8f93292673</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_IER_RLSINT</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga79f6b167debe3a8e37d40e4d30f1daec</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_IER_MSINT</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga149affc2ce17a660a640f26d3212a624</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_IER_CTSINT</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga1dd8f89b44fd3a2b0d22dd7c6ad4fe5f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_IER_ABEOINT</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gaab2fcb3a77f3c010d56fccebf59b4cb1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_IER_ABTOINT</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga5ddbca0802fdc260ac7966455d53761b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_IER_BITMASK</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga101e57e41855d1262e9d9b747854542f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART1_IER_BITMASK</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga145046fd9bd1d318acffd4770a7432ec</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART2_IER_BITMASK</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gaab8a4f7c6e8d2cf24b5f7d58cc16f8f7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_IIR_INTSTAT_PEND</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gab5fadcd32fca709aece83c05f8be1901</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_IIR_FIFO_EN</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga29b20e73585acb416f112502d29554d7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_IIR_ABEO_INT</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga6ce7f02b02e196d84ef8f6066dd2b9d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_IIR_ABTO_INT</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga29486c78b0afdb4b3943defe36d5404c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_IIR_BITMASK</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gad443b74131fa7b7aecf0f1c581172faa</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_IIR_INTID_MASK</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga6f78952aec5835ac753718323b681910</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_IIR_INTID_RLS</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga4441660d2a99f6b17a79eafbfb0424dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_IIR_INTID_RDA</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gac646d8f797f3e71e01f4361997fc581b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_IIR_INTID_CTI</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga965ba229214955385f11277549b7ecce</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_IIR_INTID_THRE</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gafb93160677afbc9c90f7a0baa917a435</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_IIR_INTID_MODEM</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gaf02dabd5f0b60345c70379ab8df3e899</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_FCR_FIFO_EN</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gadec12ecfc7ae1198cee68f2cad982bcb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_FCR_RX_RS</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga246b37ccd6137c0bb51eb32760cb228e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_FCR_TX_RS</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga1c1a83fcacf333309330eea460d8a6a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_FCR_DMAMODE_SEL</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga996e144f7d08cb36aa729f28d74b5801</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_FCR_BITMASK</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga2dd6b12c7c237b0a52c6a82698f85b04</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_FCR_TRG_LEV0</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gaba4b4e15936a075bf5054776fbd59676</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_FCR_TRG_LEV1</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga264238c2dde9248a73d679c32a74004b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_FCR_TRG_LEV2</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga7b655aba90b695210e7ce9f7b00cea89</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_FCR_TRG_LEV3</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga9e6ef12c7a1f3514d6e30d7548ed3e46</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LCR_WLEN_MASK</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga0d00b51fd6ca0b80b89af4044d94bbc0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LCR_WLEN5</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga2c64fd92092b8ac1e64b6b1204927682</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LCR_WLEN6</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga916fcefe6db8651be1cb1c066726381d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LCR_WLEN7</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga7746eb5a2aac4b9f86e97ee82e5e2a10</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LCR_WLEN8</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga71ecde192fb0c9facb9ef9c6b77cc687</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LCR_SBS_MASK</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga99a703d7a010edb3c940b54537ccdb08</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LCR_SBS_1BIT</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga70ccdedb76a079b8e7c87e5c3709469c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LCR_SBS_2BIT</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga6d36ad770b49b2354ed5cefbc066b7e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LCR_PARITY_EN</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga4fba4b3d639bdfa713d12466d411f57c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LCR_PARITY_DIS</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga91bc2978f5af5ac9a1f18af284275b39</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LCR_PARITY_ODD</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga5ef9bdb85d3f5c3823d667190b19bb40</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LCR_PARITY_EVEN</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga48df31af63d9e3a65b13a32880bb0b36</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LCR_PARITY_F_1</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga17566959150e60563687a91817ddf844</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LCR_PARITY_F_0</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gaa5d9db8e53dae40ddaa70204fa1b60a3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LCR_BREAK_EN</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga2f83aa82aecd63cf457ea423be643d57</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LCR_DLAB_EN</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gaae9c53e30321d4cac13137c66b022e9e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LCR_BITMASK</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga28e31fe85eeeb124ff6a471978155356</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_MCR_DTR_CTRL</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga9fdc7f45b2fb3679b64164b34afb9350</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_MCR_RTS_CTRL</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gabf6b55840dea19f6dbc8c8c7077796b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_MCR_LOOPB_EN</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga879d10e97b9b5e01f2b25037aa3d3c96</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_MCR_AUTO_RTS_EN</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gae26cd92b527d6d6ec9f7fd98aeefd94a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_MCR_AUTO_CTS_EN</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga7769292aa692cb12dce90893fc992d2f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_MCR_BITMASK</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga0cc006116ea98bd8b00e948073b8d749</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LSR_RDR</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga3d83de31d722cd373ee69a2a38aaed43</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LSR_OE</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga85c4312a700f6033bf0a075ae41de57c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LSR_PE</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga3ae0ee26be22b855aa08d68a2801d3d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LSR_FE</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga18b1661d7c37ab40c9310311dd4f647d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LSR_BI</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gaaca4bb43e62c7085534b67576e1ddbeb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LSR_THRE</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gae05118527ef8873b9d7b1b0be0153019</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LSR_TEMT</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gadb3f8bb82f0a253700fdb88d8c609710</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LSR_RXFE</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga5972ac77db6249142b482356427dcf7c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LSR_TXFE</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga1abf066d0f8b3400880a1909373cf699</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_LSR_BITMASK</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga3643d58e12f1d3bf342d140a5e3cb1ae</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART1_LSR_BITMASK</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gad567d8ee9c41def9dea3d84c4633ac27</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_MSR_DELTA_CTS</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gad236b1cf377bf1b4600820b8a37c66ca</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_MSR_DELTA_DSR</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga2b5cee7872a43558a4c2631459198f9b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_MSR_LO2HI_RI</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga5b2defd6ffec805753fbf799838984ed</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_MSR_DELTA_DCD</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga0b880ad272a1356b38bb5ff30d972378</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_MSR_CTS</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga2cd867126cafb765b3d690e10f79b4c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_MSR_DSR</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gae4270c77bd681dee743930df8841765e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_MSR_RI</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga1f4efd8727007b41de36b8b6ab9d4f6b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_MSR_DCD</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga9a85f5379c5d15ebc486c4b174196afb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_MSR_BITMASK</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga79745d229ade663104e0a00c7597aa45</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_ACR_START</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gaf6a6a4cb65edff2871ba48d3f2b445dc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_ACR_MODE</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga706e927ee7abf7027eb88b1e13dd2a92</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_ACR_AUTO_RESTART</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga20674ae8e687d2161ef3fd88f2649036</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_ACR_ABEOINT_CLR</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga77450ebf0f86b6b7ea363927f0cd40c2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_ACR_ABTOINT_CLR</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga2e12222f359d7a5a41668cd729b0731d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_ACR_BITMASK</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gae83190d58b42771ee951dfe88aada715</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_ACR_MODE0</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga31933a99dfe9a8afac45c1f26b0cf021</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_ACR_MODE1</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga25d690e49a3ccc696e031e8a1480dc1d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_RS485CTRL_NMM_EN</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga0b5e5ccc3ad07acad2bfa3f0846cbfd0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_RS485CTRL_RX_DIS</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gacdaee14296a914ca14d877069414f88f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_RS485CTRL_AADEN</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga1e4adf900200efcdfaab657b180b30d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_RS485CTRL_SEL_DTR</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga0632053088b7e65c6000274a90a76091</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_RS485CTRL_DCTRL_EN</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gaa00bb66207fce982ed0dbd6325d8fb66</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_RS485CTRL_OINV_1</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gaadf3ec8419a76ba6c3ccd2a4eb9b233b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_RS485CTRL_BITMASK</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga4ab3c90d083989134e4881e0b82e7364</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_ICR_IRDAEN</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga716e4830450b44e4f290e6c99879ba99</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_ICR_IRDAINV</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gabee5ba619dd3c8f28a7d2ec488614f06</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_ICR_FIXPULSE_EN</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gae022dc3e5ad94f95d2805294d97594cd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_ICR_PULSEDIV</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga2c4fd2b4e5050b400349138942bfb307</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_ICR_BITMASK</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga822d618fad4a8a146fd8113f827b5d09</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_HDEN_HDEN</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga3affaa6bd622295bd6ea0c9f4a70b19c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_SCICTRL_SCIEN</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gac14d3bf09ef0b0956626afe3e0fcf83a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_SCICTRL_NACKDIS</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga49ad93a6f4c7175b6111716e329bfb36</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_SCICTRL_PROTSEL_T1</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga48e869912cabe85bef5d11cda3b773f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_SCICTRL_TXRETRY</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gadd17c8f0f6239c42d18e4af35ee3effc</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_SCICTRL_GUARDTIME</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gac74665d22f8c0c1b18f46a65bbe5031a</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_FDR_DIVADDVAL</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga08ae53568f606c894a5ffd764cef6171</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_FDR_MULVAL</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga728a262cba31ffd0d7b4fb172f6dead7</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_FDR_BITMASK</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga61a8f74c3fc22574793c6218b90fec50</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_TER1_TXEN</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga78f0ee43aa0e7c030a3cfa1dca5ff072</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_TER2_TXEN</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gac9ec0de9b40d5d9dd9fed4836a31122f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_SYNCCTRL_SYNC</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga03c124f3fc8b888e5ee5a3fc7660bb7d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_SYNCCTRL_CSRC_MASTER</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga4776a7aa288f3c34e56db78cfb7032eb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_SYNCCTRL_FES</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga5263f24b5804673c99e8c6f09c95aed4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_SYNCCTRL_TSBYPASS</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gaea12c85ea0d6a9fa91242904c707a285</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_SYNCCTRL_CSCEN</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga17ff94fa4369905ab4a5f3bf28f44ced</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_SYNCCTRL_STARTSTOPDISABLE</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga5ebb09ebb7c6794ae8b2d1cd2c5fb193</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UART_SYNCCTRL_CCCLR</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gaedc6fb3d7ba9d561d91f3fdcddcaccce</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_UART_SendByte</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gaa600b8621d1425b1b493238a68f38088</anchor>
      <arglist>(LPC_USART_T *pUART, uint8_t data)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint8_t</type>
      <name>Chip_UART_ReadByte</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga8eec9067080637eea7ecfedac6586fe9</anchor>
      <arglist>(LPC_USART_T *pUART)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_UART_IntEnable</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga5a816f48dc294f330cc2cc7b32f9e88b</anchor>
      <arglist>(LPC_USART_T *pUART, uint32_t intMask)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_UART_IntDisable</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gaf92136333a1b6efdfd40e96f97d4a24e</anchor>
      <arglist>(LPC_USART_T *pUART, uint32_t intMask)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_UART_GetIntsEnabled</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga090e960cbbcc79be17bf52a52ec3595c</anchor>
      <arglist>(LPC_USART_T *pUART)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_UART_ReadIntIDReg</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga6a69343d3d7025055dd2326a8fdd3c74</anchor>
      <arglist>(LPC_USART_T *pUART)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_UART_SetupFIFOS</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gac1a9d00d4f324e319e1486138b097874</anchor>
      <arglist>(LPC_USART_T *pUART, uint32_t fcr)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_UART_ConfigData</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga26626229fe35e820bf2daf6a87c43155</anchor>
      <arglist>(LPC_USART_T *pUART, uint32_t config)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_UART_EnableDivisorAccess</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga7a8e9260541ab5cacefcacbd94725d52</anchor>
      <arglist>(LPC_USART_T *pUART)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_UART_DisableDivisorAccess</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga81a3df6028c4b08dba38e4f6330d41d7</anchor>
      <arglist>(LPC_USART_T *pUART)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_UART_SetDivisorLatches</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga267fa73ca52d35a7f60f849727c3d2b6</anchor>
      <arglist>(LPC_USART_T *pUART, uint8_t dll, uint8_t dlm)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_UART_ReadModemControl</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gadef0e0d2ea30182cd99561efe9909707</anchor>
      <arglist>(LPC_USART_T *pUART)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_UART_SetModemControl</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gad617968b795061ad0e4578aa79c4537d</anchor>
      <arglist>(LPC_USART_T *pUART, uint32_t mcr)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_UART_ClearModemControl</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gabdcefa8f847cfa0de2f9292405827874</anchor>
      <arglist>(LPC_USART_T *pUART, uint32_t mcr)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_UART_ReadLineStatus</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gaf15ab7a9529d102b91760ed5587b279a</anchor>
      <arglist>(LPC_USART_T *pUART)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_UART_ReadModemStatus</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gacd3d5239dec0378e52602633c183e942</anchor>
      <arglist>(LPC_USART_T *pUART)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_UART_SetScratch</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga67cb62756a430156bc754a57c72372bd</anchor>
      <arglist>(LPC_USART_T *pUART, uint8_t data)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint8_t</type>
      <name>Chip_UART_ReadScratch</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gafa7b67254c1f19bb0b085736049d9879</anchor>
      <arglist>(LPC_USART_T *pUART)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_UART_SetAutoBaudReg</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga4ae4f3c36bdae1bf8a93c0420ee74b40</anchor>
      <arglist>(LPC_USART_T *pUART, uint32_t acr)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_UART_ClearAutoBaudReg</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga6a6d30f304047175780d16207bab2b09</anchor>
      <arglist>(LPC_USART_T *pUART, uint32_t acr)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_UART_SetRS485Flags</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga0bf41455dd390ae86f95f4bfe43a49a2</anchor>
      <arglist>(LPC_USART_T *pUART, uint32_t ctrl)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_UART_ClearRS485Flags</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga27379f3351109d53453dfe4833e3f350</anchor>
      <arglist>(LPC_USART_T *pUART, uint32_t ctrl)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_UART_SetRS485Addr</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga72a3d146a8d6f8a8276080f91c3703e1</anchor>
      <arglist>(LPC_USART_T *pUART, uint8_t addr)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint8_t</type>
      <name>Chip_UART_GetRS485Addr</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga1c96142bb767ca604ad468977c5e546c</anchor>
      <arglist>(LPC_USART_T *pUART)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_UART_SetRS485Delay</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga4aa983b3f076828ee460a1c7146b3eac</anchor>
      <arglist>(LPC_USART_T *pUART, uint8_t dly)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint8_t</type>
      <name>Chip_UART_GetRS485Delay</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga5394200f985c69d25fd0c7434588cf88</anchor>
      <arglist>(LPC_USART_T *pUART)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_UART_Init</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gaf024084be4068e407aab7c30e105f7af</anchor>
      <arglist>(LPC_USART_T *pUART)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_UART_DeInit</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gaa18c4ebd4be27643e6f848472e778989</anchor>
      <arglist>(LPC_USART_T *pUART)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_UART_TXEnable</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gafe3fef953086848a51b8ec148c9fff2a</anchor>
      <arglist>(LPC_USART_T *pUART)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_UART_TXDisable</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga96589453d52bc5e208ffbeced9a49dab</anchor>
      <arglist>(LPC_USART_T *pUART)</arglist>
    </member>
    <member kind="function">
      <type>FlagStatus</type>
      <name>Chip_UART_CheckBusy</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga1b5b93f60a5b0a28e5af5f3f3311bcba</anchor>
      <arglist>(LPC_USART_T *pUART)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>Chip_UART_Send</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gacbd726b1450510892272857e43854c4c</anchor>
      <arglist>(LPC_USART_T *pUART, const void *data, int numBytes)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>Chip_UART_Read</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga06dd61ee7d8483847fea746978cb1c93</anchor>
      <arglist>(LPC_USART_T *pUART, void *data, int numBytes)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_UART_SetBaud</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga19f24dcf53316cbfb204003f506d5be5</anchor>
      <arglist>(LPC_USART_T *pUART, uint32_t baudrate)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_UART_SetBaudFDR</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gad22ce7fc8360c02aab39b2dd586088fd</anchor>
      <arglist>(LPC_USART_T *pUART, uint32_t baudrate)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>Chip_UART_SendBlocking</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gad2e45d820abdd0e1790ebd61938c100a</anchor>
      <arglist>(LPC_USART_T *pUART, const void *data, int numBytes)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>Chip_UART_ReadBlocking</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga2b256d39f2fde9f8b923a2f341f5fea3</anchor>
      <arglist>(LPC_USART_T *pUART, void *data, int numBytes)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_UART_RXIntHandlerRB</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gafdd19a312a2bed6ff1ccb47d5b68c248</anchor>
      <arglist>(LPC_USART_T *pUART, RINGBUFF_T *pRB)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_UART_TXIntHandlerRB</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga74df5e39960a1535118fcfe2fbe90d30</anchor>
      <arglist>(LPC_USART_T *pUART, RINGBUFF_T *pRB)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>Chip_UART_SendRB</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga6ed43ed19b9d2a32ece3e50bc2f651a9</anchor>
      <arglist>(LPC_USART_T *pUART, RINGBUFF_T *pRB, const void *data, int bytes)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>Chip_UART_ReadRB</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>gab54219751466a0fa8d9e75f8689ac99d</anchor>
      <arglist>(LPC_USART_T *pUART, RINGBUFF_T *pRB, void *data, int bytes)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_UART_IRQRBHandler</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga8ab537af48951658e60af145690b656e</anchor>
      <arglist>(LPC_USART_T *pUART, RINGBUFF_T *pRXRB, RINGBUFF_T *pTXRB)</arglist>
    </member>
    <member kind="function">
      <type>FlagStatus</type>
      <name>Chip_UART_GetABEOStatus</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga6d1d74a73290b145868a88e6b5635093</anchor>
      <arglist>(LPC_USART_T *pUART)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_UART_ABCmd</name>
      <anchorfile>group__UART__17XX__40XX.html</anchorfile>
      <anchor>ga9aa733f176891043bb1dd4d87940187f</anchor>
      <arglist>(LPC_USART_T *pUART, uint32_t mode, bool autorestart, FunctionalState NewState)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>USB_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx USB Device, Host, &amp; OTG driver</title>
    <filename>group__USB__17XX__40XX.html</filename>
    <class kind="struct">LPC_USB_T</class>
  </compound>
  <compound kind="group">
    <name>USBD_CDC</name>
    <title>Communication Device Class (CDC) Function Driver</title>
    <filename>group__USBD__CDC.html</filename>
    <class kind="struct">USBD_CDC_INIT_PARAM_T</class>
    <class kind="struct">USBD_CDC_API_T</class>
    <docanchor file="group__USBD__CDC" title="Module Description">Sec_CDCModDescription</docanchor>
  </compound>
  <compound kind="group">
    <name>USBD_Core</name>
    <title>USB Core Layer</title>
    <filename>group__USBD__Core.html</filename>
    <class kind="struct">_WB_T</class>
    <class kind="union">__WORD_BYTE</class>
    <class kind="struct">_BM_T</class>
    <class kind="union">_REQUEST_TYPE</class>
    <class kind="struct">_USB_SETUP_PACKET</class>
    <class kind="struct">_USB_DEVICE_DESCRIPTOR</class>
    <class kind="struct">_USB_DEVICE_QUALIFIER_DESCRIPTOR</class>
    <class kind="struct">_USB_CONFIGURATION_DESCRIPTOR</class>
    <class kind="struct">_USB_IAD_DESCRIPTOR</class>
    <class kind="struct">_USB_INTERFACE_DESCRIPTOR</class>
    <class kind="struct">_USB_ENDPOINT_DESCRIPTOR</class>
    <class kind="struct">_USB_STRING_DESCRIPTOR</class>
    <class kind="struct">_USB_COMMON_DESCRIPTOR</class>
    <class kind="struct">_USB_OTHER_SPEED_CONFIGURATION</class>
    <class kind="struct">USB_CORE_DESCS_T</class>
    <class kind="struct">USBD_API_INIT_PARAM_T</class>
    <class kind="struct">USBD_CORE_API_T</class>
    <member kind="define">
      <type>#define</type>
      <name>USB_CONFIG_POWER_MA</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gadcf08dc7fdb6117608624203612b3104</anchor>
      <arglist>(mA)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_0_HS_MAXP</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gac8df859211824ddcf32db5c80b06290a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_0_LS_MAXP</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga2b0c6c2be1d518967da6c7b32458c894</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>USB_ENDPOINT_BULK_HS_MAXP</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gad4839d37e7a31a4d9e8ddf242b16608f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>void *</type>
      <name>USBD_HANDLE_T</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gafdbb2204d929cb9d75736bd2b42342ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>ErrorCode_t(*</type>
      <name>USB_CB_T</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga0404ce046312aa5c798cc4a05c417e46</anchor>
      <arglist>)(USBD_HANDLE_T hUsb)</arglist>
    </member>
    <member kind="typedef">
      <type>ErrorCode_t(*</type>
      <name>USB_PARAM_CB_T</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>ga7df622c61ebb152b83dd5972ac789b28</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, uint32_t param1)</arglist>
    </member>
    <member kind="typedef">
      <type>ErrorCode_t(*</type>
      <name>USB_EP_HANDLER_T</name>
      <anchorfile>group__USBD__Core.html</anchorfile>
      <anchor>gaa578d29a85226108ef62c6d5c325b742</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, void *data, uint32_t event)</arglist>
    </member>
    <docanchor file="group__USBD__Core" title="Module Description">Sec_CoreModDescription</docanchor>
  </compound>
  <compound kind="group">
    <name>USBD_DFU</name>
    <title>Device Firmware Upgrade (DFU) Class Function Driver</title>
    <filename>group__USBD__DFU.html</filename>
    <class kind="struct">USBD_DFU_INIT_PARAM_T</class>
    <class kind="struct">USBD_DFU_API_T</class>
    <docanchor file="group__USBD__MSC" title="Module Description">Sec_MSCModDescription</docanchor>
  </compound>
  <compound kind="group">
    <name>USBD_HID</name>
    <title>HID Class Function Driver</title>
    <filename>group__USBD__HID.html</filename>
    <class kind="struct">_HID_DESCRIPTOR</class>
    <class kind="struct">USB_HID_REPORT_T</class>
    <class kind="struct">USBD_HID_INIT_PARAM_T</class>
    <class kind="struct">USBD_HID_API_T</class>
    <docanchor file="group__USBD__HID" title="Module Description">Sec_HIDModDescription</docanchor>
  </compound>
  <compound kind="group">
    <name>USBD_HW</name>
    <title>USB Device Controller Driver</title>
    <filename>group__USBD__HW.html</filename>
    <class kind="struct">USBD_HW_API_T</class>
    <member kind="enumeration">
      <type></type>
      <name>USBD_EVENT_T</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>ga61dde6aa35d2912927ef1b185eedaa13</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_SETUP</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13a8d26afb2e56f4b1b788321a0d1633b33</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_OUT</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13ad720a106a796d05d1dd52fb0b021be67</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_IN</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13aa284007e2d06d2f9241618b55c1bcd3a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_OUT_NAK</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13a8966a8816143f41ac6aafd7215151f88</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_IN_NAK</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13af973a80c8f31a5c9aa320b15cd230434</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_OUT_STALL</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13a3e17b2dcf495ff4befaeba2718affae4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_IN_STALL</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13a1ba247a8c3cdc166098e817ec2808777</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_OUT_DMA_EOT</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13a3a3b0dc5766c4d17bd6d2daea5812fbc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_IN_DMA_EOT</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13aaddab8476ea17ac9fb4e10796bc7c633</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_OUT_DMA_NDR</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13a744297c96c9296f311a28da97df03001</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_IN_DMA_NDR</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13a6c66edac1510b9e9741a8f5019142214</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_OUT_DMA_ERR</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13ac469035908127847ebb32fd51a0a33bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_IN_DMA_ERR</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13a2e9c73676bebd4b0c4ebb412c2a53af4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_RESET</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13ac9da611df8ba920792bb1388c70fbc53</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_SOF</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13a327107e15682a2403deec540ca209670</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_DEV_STATE</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13ae30ace519ceb787d84de55ebf5e4901e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>USB_EVT_DEV_ERROR</name>
      <anchorfile>group__USBD__HW.html</anchorfile>
      <anchor>gga61dde6aa35d2912927ef1b185eedaa13a7e46c0177f969c8be4e4a40784c7c2f4</anchor>
      <arglist></arglist>
    </member>
    <docanchor file="group__USBD__HW" title="Module Description">Sec_HWModDescription</docanchor>
  </compound>
  <compound kind="group">
    <name>USBD_MSC</name>
    <title>Mass Storage Class (MSC) Function Driver</title>
    <filename>group__USBD__MSC.html</filename>
    <class kind="struct">USBD_MSC_INIT_PARAM_T</class>
    <class kind="struct">USBD_MSC_API_T</class>
    <docanchor file="group__USBD__MSC" title="Module Description">Sec_MSCModDescription</docanchor>
  </compound>
  <compound kind="group">
    <name>WWDT_17XX_40XX</name>
    <title>CHIP: LPC17xx/40xx Windowed Watchdog driver</title>
    <filename>group__WWDT__17XX__40XX.html</filename>
    <class kind="struct">LPC_WWDT_T</class>
    <member kind="define">
      <type>#define</type>
      <name>WDT_OSC</name>
      <anchorfile>group__WWDT__17XX__40XX.html</anchorfile>
      <anchor>ga628282c7bf5c28d5a859e4915c62b643</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>WWDT_WDMOD_BITMASK</name>
      <anchorfile>group__WWDT__17XX__40XX.html</anchorfile>
      <anchor>gac9ba0cf06012012875985842cabb5a11</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>WWDT_WDMOD_WDEN</name>
      <anchorfile>group__WWDT__17XX__40XX.html</anchorfile>
      <anchor>ga9c4d839b554cf8919c76bd613745967f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>WWDT_WDMOD_WDRESET</name>
      <anchorfile>group__WWDT__17XX__40XX.html</anchorfile>
      <anchor>gaa6b03ad5df847bc2241c0ea1eefd9431</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>WWDT_WDMOD_WDTOF</name>
      <anchorfile>group__WWDT__17XX__40XX.html</anchorfile>
      <anchor>ga9379872b1e184e20abf74f1abbdd8cb9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>WWDT_WDMOD_WDINT</name>
      <anchorfile>group__WWDT__17XX__40XX.html</anchorfile>
      <anchor>ga6530623c6535d2c7b65c9b50d320c5f1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>WWDT_WDMOD_WDPROTECT</name>
      <anchorfile>group__WWDT__17XX__40XX.html</anchorfile>
      <anchor>gac44132c9f40915e405e3cfedc2599586</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_WWDT_Init</name>
      <anchorfile>group__WWDT__17XX__40XX.html</anchorfile>
      <anchor>gab4cdf632b42394855a6b8cf969f49693</anchor>
      <arglist>(LPC_WWDT_T *pWWDT)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_WWDT_DeInit</name>
      <anchorfile>group__WWDT__17XX__40XX.html</anchorfile>
      <anchor>gaac3365ace1492b5ccc89472fb3f2cfc0</anchor>
      <arglist>(LPC_WWDT_T *pWWDT)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_WWDT_SetTimeOut</name>
      <anchorfile>group__WWDT__17XX__40XX.html</anchorfile>
      <anchor>ga3147a15042f4276588c83e98b0a1b996</anchor>
      <arglist>(LPC_WWDT_T *pWWDT, uint32_t timeout)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_WWDT_Feed</name>
      <anchorfile>group__WWDT__17XX__40XX.html</anchorfile>
      <anchor>ga88db6aef307efd5cbc629695c4678006</anchor>
      <arglist>(LPC_WWDT_T *pWWDT)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_WWDT_SetWarning</name>
      <anchorfile>group__WWDT__17XX__40XX.html</anchorfile>
      <anchor>ga83ecb3bc2ce68b3deb8343a7d76e3d7e</anchor>
      <arglist>(LPC_WWDT_T *pWWDT, uint32_t timeout)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_WWDT_SetWindow</name>
      <anchorfile>group__WWDT__17XX__40XX.html</anchorfile>
      <anchor>gaad121c7d4960ceec5626e8bad047c966</anchor>
      <arglist>(LPC_WWDT_T *pWWDT, uint32_t timeout)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_WWDT_SetOption</name>
      <anchorfile>group__WWDT__17XX__40XX.html</anchorfile>
      <anchor>gab1908a91ca65434f14402e4a8373091f</anchor>
      <arglist>(LPC_WWDT_T *pWWDT, uint32_t options)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_WWDT_UnsetOption</name>
      <anchorfile>group__WWDT__17XX__40XX.html</anchorfile>
      <anchor>gae71f59b4d6dfa847411bc74f5467946c</anchor>
      <arglist>(LPC_WWDT_T *pWWDT, uint32_t options)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE void</type>
      <name>Chip_WWDT_Start</name>
      <anchorfile>group__WWDT__17XX__40XX.html</anchorfile>
      <anchor>gacae3a80bfc9430604c434d073230f577</anchor>
      <arglist>(LPC_WWDT_T *pWWDT)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_WWDT_GetStatus</name>
      <anchorfile>group__WWDT__17XX__40XX.html</anchorfile>
      <anchor>ga9e5a34151326049c5485bb20c9f36fee</anchor>
      <arglist>(LPC_WWDT_T *pWWDT)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Chip_WWDT_ClearStatusFlag</name>
      <anchorfile>group__WWDT__17XX__40XX.html</anchorfile>
      <anchor>ga6e6453450170638f554e7ba3c548ec4a</anchor>
      <arglist>(LPC_WWDT_T *pWWDT, uint32_t status)</arglist>
    </member>
    <member kind="function">
      <type>STATIC INLINE uint32_t</type>
      <name>Chip_WWDT_GetCurrentCount</name>
      <anchorfile>group__WWDT__17XX__40XX.html</anchorfile>
      <anchor>ga4b1c8d2f48a8397d63c1c3c74dc7e82a</anchor>
      <arglist>(LPC_WWDT_T *pWWDT)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>ADC_CLOCK_SETUP_T</name>
    <filename>structADC__CLOCK__SETUP__T.html</filename>
    <member kind="variable">
      <type>uint32_t</type>
      <name>adcRate</name>
      <anchorfile>structADC__CLOCK__SETUP__T.html</anchorfile>
      <anchor>a8948fd266d15eee3d090cab24a64ff42</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bitsAccuracy</name>
      <anchorfile>structADC__CLOCK__SETUP__T.html</anchorfile>
      <anchor>aeb37891571691033d98087dabf6ddbd5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>burstMode</name>
      <anchorfile>structADC__CLOCK__SETUP__T.html</anchorfile>
      <anchor>a92bc50d4472a36de38dac15a5fd3109a</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="union">
    <name>APSR_Type</name>
    <filename>unionAPSR__Type.html</filename>
    <member kind="variable">
      <type>struct APSR_Type::@0</type>
      <name>b</name>
      <anchorfile>unionAPSR__Type.html</anchorfile>
      <anchor>a7dbc79a057ded4b11ca5323fc2d5ab14</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>w</name>
      <anchorfile>unionAPSR__Type.html</anchorfile>
      <anchor>ae4c2ef8c9430d7b7bef5cbfbbaed3a94</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>_BM_T</name>
    <filename>struct__BM__T.html</filename>
    <member kind="variable">
      <type>uint8_t</type>
      <name>Recipient</name>
      <anchorfile>struct__BM__T.html</anchorfile>
      <anchor>a0abcb925c51cc2072ca7e95dd59a93e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>Type</name>
      <anchorfile>struct__BM__T.html</anchorfile>
      <anchor>a0ff76090420ac878852d6b32d74df4a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>Dir</name>
      <anchorfile>struct__BM__T.html</anchorfile>
      <anchor>ae32424a89f7fc2a36d7baa96f774e3f0</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>CAN_EXT_ID_ENTRY_T</name>
    <filename>structCAN__EXT__ID__ENTRY__T.html</filename>
    <member kind="variable">
      <type>uint8_t</type>
      <name>CtrlNo</name>
      <anchorfile>structCAN__EXT__ID__ENTRY__T.html</anchorfile>
      <anchor>ad8ed0ed8438c467abe045573bd079190</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>ID_29</name>
      <anchorfile>structCAN__EXT__ID__ENTRY__T.html</anchorfile>
      <anchor>a3e67827310c81e3e17cb067dd25155b3</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>CAN_EXT_ID_RANGE_ENTRY_T</name>
    <filename>structCAN__EXT__ID__RANGE__ENTRY__T.html</filename>
    <member kind="variable">
      <type>CAN_EXT_ID_ENTRY_T</type>
      <name>LowerID</name>
      <anchorfile>structCAN__EXT__ID__RANGE__ENTRY__T.html</anchorfile>
      <anchor>a704cb0823b253cecdb54c0cff24f5fac</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>CAN_EXT_ID_ENTRY_T</type>
      <name>UpperID</name>
      <anchorfile>structCAN__EXT__ID__RANGE__ENTRY__T.html</anchorfile>
      <anchor>a6889adbc72854cce6891e7595a96bf3d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>CAN_MSG_T</name>
    <filename>structCAN__MSG__T.html</filename>
    <member kind="variable">
      <type>uint32_t</type>
      <name>ID</name>
      <anchorfile>structCAN__MSG__T.html</anchorfile>
      <anchor>a7fbb2453bed19c5b143fb7336a697622</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>Type</name>
      <anchorfile>structCAN__MSG__T.html</anchorfile>
      <anchor>a2c15ca6deba6d985f7d8d24caec2fb3b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>DLC</name>
      <anchorfile>structCAN__MSG__T.html</anchorfile>
      <anchor>a39963b19d3cb9885a5e984a6805f2674</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>Data</name>
      <anchorfile>structCAN__MSG__T.html</anchorfile>
      <anchor>adb227f0afb0c340bdc92b4098c2734de</anchor>
      <arglist>[CAN_MSG_MAX_DATA_LEN]</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>CAN_STD_ID_ENTRY_T</name>
    <filename>structCAN__STD__ID__ENTRY__T.html</filename>
    <member kind="variable">
      <type>uint8_t</type>
      <name>CtrlNo</name>
      <anchorfile>structCAN__STD__ID__ENTRY__T.html</anchorfile>
      <anchor>a5361d52d7fcdc1a8598550836b91bb97</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>Disable</name>
      <anchorfile>structCAN__STD__ID__ENTRY__T.html</anchorfile>
      <anchor>a02dc64b0ae7683331953979d1ed9dce7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>ID_11</name>
      <anchorfile>structCAN__STD__ID__ENTRY__T.html</anchorfile>
      <anchor>a7cb55fad6132fbd461a568ff724f9ca1</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>CAN_STD_ID_RANGE_ENTRY_T</name>
    <filename>structCAN__STD__ID__RANGE__ENTRY__T.html</filename>
    <member kind="variable">
      <type>CAN_STD_ID_ENTRY_T</type>
      <name>LowerID</name>
      <anchorfile>structCAN__STD__ID__RANGE__ENTRY__T.html</anchorfile>
      <anchor>a8e71d9944856371e26c1f42fcdc124f7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>CAN_STD_ID_ENTRY_T</type>
      <name>UpperID</name>
      <anchorfile>structCAN__STD__ID__RANGE__ENTRY__T.html</anchorfile>
      <anchor>a5c92aad98ca02e625a651b5509a4bd6e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>CANAF_LUT_T</name>
    <filename>structCANAF__LUT__T.html</filename>
    <member kind="variable">
      <type>CAN_STD_ID_ENTRY_T *</type>
      <name>FullCANSec</name>
      <anchorfile>structCANAF__LUT__T.html</anchorfile>
      <anchor>a7faf17c00ee110a91d7339aacd4fda41</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>FullCANEntryNum</name>
      <anchorfile>structCANAF__LUT__T.html</anchorfile>
      <anchor>ac8dfb97883a903ee82362da9abd4e863</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>CAN_STD_ID_ENTRY_T *</type>
      <name>SffSec</name>
      <anchorfile>structCANAF__LUT__T.html</anchorfile>
      <anchor>a34daf657f3d2a50e721daa0ab7dee994</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>SffEntryNum</name>
      <anchorfile>structCANAF__LUT__T.html</anchorfile>
      <anchor>aa5b3264e0fb44bbb7f3bf16ee367cc2e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>CAN_STD_ID_RANGE_ENTRY_T *</type>
      <name>SffGrpSec</name>
      <anchorfile>structCANAF__LUT__T.html</anchorfile>
      <anchor>abcfa9b72b2a981e655551810d52c5fff</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>SffGrpEntryNum</name>
      <anchorfile>structCANAF__LUT__T.html</anchorfile>
      <anchor>ae7d8ed9ef4c6d5b4bef0be1068ea0c8d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>CAN_EXT_ID_ENTRY_T *</type>
      <name>EffSec</name>
      <anchorfile>structCANAF__LUT__T.html</anchorfile>
      <anchor>a30b5814a21e36ca7d9711c0752ff3f4e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>EffEntryNum</name>
      <anchorfile>structCANAF__LUT__T.html</anchorfile>
      <anchor>a62d74093f87bfd355f53f143b022e3ce</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>CAN_EXT_ID_RANGE_ENTRY_T *</type>
      <name>EffGrpSec</name>
      <anchorfile>structCANAF__LUT__T.html</anchorfile>
      <anchor>a62a98cfd049ea67dd3b01d6e6f3871d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>EffGrpEntryNum</name>
      <anchorfile>structCANAF__LUT__T.html</anchorfile>
      <anchor>a48493bdab0f7818be7082e61284994d8</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>_CDC_ABSTRACT_CONTROL_MANAGEMENT_DESCRIPTOR</name>
    <filename>struct__CDC__ABSTRACT__CONTROL__MANAGEMENT__DESCRIPTOR.html</filename>
  </compound>
  <compound kind="struct">
    <name>_CDC_CALL_MANAGEMENT_DESCRIPTOR</name>
    <filename>struct__CDC__CALL__MANAGEMENT__DESCRIPTOR.html</filename>
  </compound>
  <compound kind="struct">
    <name>_CDC_HEADER_DESCRIPTOR</name>
    <filename>struct__CDC__HEADER__DESCRIPTOR.html</filename>
  </compound>
  <compound kind="struct">
    <name>_CDC_LINE_CODING</name>
    <filename>struct__CDC__LINE__CODING.html</filename>
  </compound>
  <compound kind="struct">
    <name>_CDC_UNION_1SLAVE_DESCRIPTOR</name>
    <filename>struct__CDC__UNION__1SLAVE__DESCRIPTOR.html</filename>
  </compound>
  <compound kind="struct">
    <name>_CDC_UNION_DESCRIPTOR</name>
    <filename>struct__CDC__UNION__DESCRIPTOR.html</filename>
  </compound>
  <compound kind="struct">
    <name>Chip_SSP_DATA_SETUP_T</name>
    <filename>structChip__SSP__DATA__SETUP__T.html</filename>
    <member kind="variable">
      <type>void *</type>
      <name>tx_data</name>
      <anchorfile>structChip__SSP__DATA__SETUP__T.html</anchorfile>
      <anchor>a490e82f2648097f22faa108b59d89848</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>tx_cnt</name>
      <anchorfile>structChip__SSP__DATA__SETUP__T.html</anchorfile>
      <anchor>ab37848675426612b2fd0e986dbd2d260</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>void *</type>
      <name>rx_data</name>
      <anchorfile>structChip__SSP__DATA__SETUP__T.html</anchorfile>
      <anchor>a06371dd1c78e3cb326a2ccc2811f0346</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>rx_cnt</name>
      <anchorfile>structChip__SSP__DATA__SETUP__T.html</anchorfile>
      <anchor>a74d6f8529c17e5e1ce7e78cdeb1a39b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>length</name>
      <anchorfile>structChip__SSP__DATA__SETUP__T.html</anchorfile>
      <anchor>ac5202ff04a3ed639dce258e31a9000d6</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="union">
    <name>CONTROL_Type</name>
    <filename>unionCONTROL__Type.html</filename>
    <member kind="variable">
      <type>struct CONTROL_Type::@3</type>
      <name>b</name>
      <anchorfile>unionCONTROL__Type.html</anchorfile>
      <anchor>adc6a38ab2980d0e9577b5a871da14eb9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>w</name>
      <anchorfile>unionCONTROL__Type.html</anchorfile>
      <anchor>a6b642cca3d96da660b1198c133ca2a1f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>CoreDebug_Type</name>
    <filename>structCoreDebug__Type.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>DHCSR</name>
      <anchorfile>structCoreDebug__Type.html</anchorfile>
      <anchor>a25c14c022c73a725a1736e903431095d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>DCRSR</name>
      <anchorfile>structCoreDebug__Type.html</anchorfile>
      <anchor>afefa84bce7497652353a1b76d405d983</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>DCRDR</name>
      <anchorfile>structCoreDebug__Type.html</anchorfile>
      <anchor>ab8f4bb076402b61f7be6308075a789c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>DEMCR</name>
      <anchorfile>structCoreDebug__Type.html</anchorfile>
      <anchor>a5cdd51dbe3ebb7041880714430edd52d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>_DFU_STATUS</name>
    <filename>struct__DFU__STATUS.html</filename>
  </compound>
  <compound kind="struct">
    <name>DWT_Type</name>
    <filename>structDWT__Type.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CTRL</name>
      <anchorfile>structDWT__Type.html</anchorfile>
      <anchor>a37964d64a58551b69ce4c8097210d37d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CYCCNT</name>
      <anchorfile>structDWT__Type.html</anchorfile>
      <anchor>a71680298e85e96e57002f87e7ab78fd4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CPICNT</name>
      <anchorfile>structDWT__Type.html</anchorfile>
      <anchor>a88cca2ab8eb1b5b507817656ceed89fc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>EXCCNT</name>
      <anchorfile>structDWT__Type.html</anchorfile>
      <anchor>ac0801a2328f3431e4706fed91c828f82</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>SLEEPCNT</name>
      <anchorfile>structDWT__Type.html</anchorfile>
      <anchor>a8afd5a4bf994011748bc012fa442c74d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>LSUCNT</name>
      <anchorfile>structDWT__Type.html</anchorfile>
      <anchor>aeba92e6c7fd3de4ba06bfd94f47f5b35</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>FOLDCNT</name>
      <anchorfile>structDWT__Type.html</anchorfile>
      <anchor>a35f2315f870a574e3e6958face6584ab</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>PCSR</name>
      <anchorfile>structDWT__Type.html</anchorfile>
      <anchor>abc5ae11d98da0ad5531a5e979a3c2ab5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>COMP0</name>
      <anchorfile>structDWT__Type.html</anchorfile>
      <anchor>a7cf71ff4b30a8362690fddd520763904</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MASK0</name>
      <anchorfile>structDWT__Type.html</anchorfile>
      <anchor>a5bb1c17fc754180cc197b874d3d8673f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>FUNCTION0</name>
      <anchorfile>structDWT__Type.html</anchorfile>
      <anchor>a5fbd9947d110cc168941f6acadc4a729</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>COMP1</name>
      <anchorfile>structDWT__Type.html</anchorfile>
      <anchor>a4a5bb70a5ce3752bd628d5ce5658cb0c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MASK1</name>
      <anchorfile>structDWT__Type.html</anchorfile>
      <anchor>a0c684438a24f8c927e6e01c0e0a605ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>FUNCTION1</name>
      <anchorfile>structDWT__Type.html</anchorfile>
      <anchor>a3345a33476ee58e165447a3212e6d747</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>COMP2</name>
      <anchorfile>structDWT__Type.html</anchorfile>
      <anchor>a8927aedbe9fd6bdae8983088efc83332</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MASK2</name>
      <anchorfile>structDWT__Type.html</anchorfile>
      <anchor>a8ecdc8f0d917dac86b0373532a1c0e2e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>FUNCTION2</name>
      <anchorfile>structDWT__Type.html</anchorfile>
      <anchor>acba1654190641a3617fcc558b5e3f87b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>COMP3</name>
      <anchorfile>structDWT__Type.html</anchorfile>
      <anchor>a3df15697eec279dbbb4b4e9d9ae8b62f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MASK3</name>
      <anchorfile>structDWT__Type.html</anchorfile>
      <anchor>ae3f01137a8d28c905ddefe7333547fba</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>FUNCTION3</name>
      <anchorfile>structDWT__Type.html</anchorfile>
      <anchor>a80bd242fc05ca80f9db681ce4d82e890</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>ENET_CONTROL_T</name>
    <filename>structENET__CONTROL__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>COMMAND</name>
      <anchorfile>structENET__CONTROL__T.html</anchorfile>
      <anchor>a5c6633c6ca37621d5f23513ebca2bd17</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>STATUS</name>
      <anchorfile>structENET__CONTROL__T.html</anchorfile>
      <anchor>aaaef8a4b3d2a27e488d5e2e8a236640a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>ENET_TRANSFER_INFO_T</type>
      <name>RX</name>
      <anchorfile>structENET__CONTROL__T.html</anchorfile>
      <anchor>a3b07c9a812059f852415f8b5a5261882</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>ENET_TRANSFER_INFO_T</type>
      <name>TX</name>
      <anchorfile>structENET__CONTROL__T.html</anchorfile>
      <anchor>a1010eaf24cd6af0d6355b4cd34d8e90a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>TSV0</name>
      <anchorfile>structENET__CONTROL__T.html</anchorfile>
      <anchor>adfd0bf0a173cb107271eba4fa8f20de8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>TSV1</name>
      <anchorfile>structENET__CONTROL__T.html</anchorfile>
      <anchor>a03b03ad79457f3f25cc3d1d2d564310a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>RSV</name>
      <anchorfile>structENET__CONTROL__T.html</anchorfile>
      <anchor>a5817f844b6195193d1536edbc770e72f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>FLOWCONTROLCOUNTER</name>
      <anchorfile>structENET__CONTROL__T.html</anchorfile>
      <anchor>a988aac51d07bd4d330945f74f3747084</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>FLOWCONTROLSTATUS</name>
      <anchorfile>structENET__CONTROL__T.html</anchorfile>
      <anchor>abe816b0eaea3b8dcb126cbe565d81ffb</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>ENET_MAC_T</name>
    <filename>structENET__MAC__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MAC1</name>
      <anchorfile>structENET__MAC__T.html</anchorfile>
      <anchor>a1e758365774fda3ac9c11922c4a7ef35</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MAC2</name>
      <anchorfile>structENET__MAC__T.html</anchorfile>
      <anchor>a925a92dbe2a48105e6497378a8cdfcac</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>IPGT</name>
      <anchorfile>structENET__MAC__T.html</anchorfile>
      <anchor>ac73d8d2cf290a3b9dc117340fc093d7e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>IPGR</name>
      <anchorfile>structENET__MAC__T.html</anchorfile>
      <anchor>adcccddf8623def0f5a9aeee408312664</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CLRT</name>
      <anchorfile>structENET__MAC__T.html</anchorfile>
      <anchor>afdf0e932f04510c6e77bb13a4592cf13</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MAXF</name>
      <anchorfile>structENET__MAC__T.html</anchorfile>
      <anchor>a5e6d2bc05984b815f245d2ebd3c067a3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>SUPP</name>
      <anchorfile>structENET__MAC__T.html</anchorfile>
      <anchor>ad7232a883f980074136d81f8a15d8bb5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>TEST</name>
      <anchorfile>structENET__MAC__T.html</anchorfile>
      <anchor>a3ba308b4a368f475aa4b2136faa88f6f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MCFG</name>
      <anchorfile>structENET__MAC__T.html</anchorfile>
      <anchor>a1e733caa47928bbd88205c10549db3b6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MCMD</name>
      <anchorfile>structENET__MAC__T.html</anchorfile>
      <anchor>af5015dd00c915a85f7aa387444349f59</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MADR</name>
      <anchorfile>structENET__MAC__T.html</anchorfile>
      <anchor>afc17c49a8ba00a4a4db6dc0b87d949b7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>MWTD</name>
      <anchorfile>structENET__MAC__T.html</anchorfile>
      <anchor>a588ac0beb769e9cabb7039582625a3e0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>MRDD</name>
      <anchorfile>structENET__MAC__T.html</anchorfile>
      <anchor>a98a3f303ec4276150d3d4b8586f2a71d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>MIND</name>
      <anchorfile>structENET__MAC__T.html</anchorfile>
      <anchor>a6d107736b77b59d7188fb6faa04ea549</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>SA</name>
      <anchorfile>structENET__MAC__T.html</anchorfile>
      <anchor>a25214ef3698b23b87fea95b7158a11ad</anchor>
      <arglist>[3]</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>ENET_MODULE_CTRL_T</name>
    <filename>structENET__MODULE__CTRL__T.html</filename>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>INTSTATUS</name>
      <anchorfile>structENET__MODULE__CTRL__T.html</anchorfile>
      <anchor>a7dac73c2fd05c5247f006717dbe6c9fa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>INTENABLE</name>
      <anchorfile>structENET__MODULE__CTRL__T.html</anchorfile>
      <anchor>a4d21be82461cf028b89fe0e314718352</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>INTCLEAR</name>
      <anchorfile>structENET__MODULE__CTRL__T.html</anchorfile>
      <anchor>a67c87b65d221dfc156c41c76d80aebdb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>INTSET</name>
      <anchorfile>structENET__MODULE__CTRL__T.html</anchorfile>
      <anchor>a7898d6a89c630ecd471a7a059ee376d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>POWERDOWN</name>
      <anchorfile>structENET__MODULE__CTRL__T.html</anchorfile>
      <anchor>a42b5ebd11c8549b2ba2bb072b5b42736</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>ENET_RXDESC_T</name>
    <filename>structENET__RXDESC__T.html</filename>
    <member kind="variable">
      <type>uint32_t</type>
      <name>Packet</name>
      <anchorfile>structENET__RXDESC__T.html</anchorfile>
      <anchor>a6fffb262a03d26e426701e6fba82a7ff</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>Control</name>
      <anchorfile>structENET__RXDESC__T.html</anchorfile>
      <anchor>a4e7065600418654b06d854ea66dfe259</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>ENET_RXFILTER_T</name>
    <filename>structENET__RXFILTER__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CONTROL</name>
      <anchorfile>structENET__RXFILTER__T.html</anchorfile>
      <anchor>a41f07a3db2876b8d2ad5da9a93dfc26b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>WOLSTATUS</name>
      <anchorfile>structENET__RXFILTER__T.html</anchorfile>
      <anchor>a40eedf8542d1e384d39cc5fca3fffffb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>WOLCLEAR</name>
      <anchorfile>structENET__RXFILTER__T.html</anchorfile>
      <anchor>acc4b5d489de8f522d567436de4b10d91</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>HashFilterL</name>
      <anchorfile>structENET__RXFILTER__T.html</anchorfile>
      <anchor>ad011f3c2f5381b81e57c42420bb98644</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>HashFilterH</name>
      <anchorfile>structENET__RXFILTER__T.html</anchorfile>
      <anchor>a2d82ff2319b5b50a63698fd062c2fe7d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>ENET_RXSTAT_T</name>
    <filename>structENET__RXSTAT__T.html</filename>
    <member kind="variable">
      <type>uint32_t</type>
      <name>StatusInfo</name>
      <anchorfile>structENET__RXSTAT__T.html</anchorfile>
      <anchor>a0d2de29791ff4358c5cc4dedd694857d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>StatusHashCRC</name>
      <anchorfile>structENET__RXSTAT__T.html</anchorfile>
      <anchor>a52d71f70dc193d59f6cb67241d92a4c9</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>ENET_TRANSFER_INFO_T</name>
    <filename>structENET__TRANSFER__INFO__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>DESCRIPTOR</name>
      <anchorfile>structENET__TRANSFER__INFO__T.html</anchorfile>
      <anchor>a9eabd8ba19da27477fd1a07efe01161e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>STATUS</name>
      <anchorfile>structENET__TRANSFER__INFO__T.html</anchorfile>
      <anchor>ae72bffa37688d592667997f276ff28e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>DESCRIPTORNUMBER</name>
      <anchorfile>structENET__TRANSFER__INFO__T.html</anchorfile>
      <anchor>a5ee3c6f15d43d48fe6de2e52faa66610</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>PRODUCEINDEX</name>
      <anchorfile>structENET__TRANSFER__INFO__T.html</anchorfile>
      <anchor>ab8f942719d586cb8d735dbe57341bc10</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CONSUMEINDEX</name>
      <anchorfile>structENET__TRANSFER__INFO__T.html</anchorfile>
      <anchor>a1704aa8577f168e0bb3565f68766e27c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>ENET_TXDESC_T</name>
    <filename>structENET__TXDESC__T.html</filename>
    <member kind="variable">
      <type>uint32_t</type>
      <name>Packet</name>
      <anchorfile>structENET__TXDESC__T.html</anchorfile>
      <anchor>aa32276326b2e5e4157e59d0f5553bae3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>Control</name>
      <anchorfile>structENET__TXDESC__T.html</anchorfile>
      <anchor>a7bbcf3f7943ab87b784dbca19e2698d5</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>ENET_TXSTAT_T</name>
    <filename>structENET__TXSTAT__T.html</filename>
    <member kind="variable">
      <type>uint32_t</type>
      <name>StatusInfo</name>
      <anchorfile>structENET__TXSTAT__T.html</anchorfile>
      <anchor>a8b19a43922a609a6b5e477c3d5dae72f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>GPDMA_CFG_T</name>
    <filename>structGPDMA__CFG__T.html</filename>
    <member kind="variable">
      <type>GPDMA_CBK_T</type>
      <name>Callback</name>
      <anchorfile>structGPDMA__CFG__T.html</anchorfile>
      <anchor>ab7f55f4c790d9a27b5506d2893de833d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>void *</type>
      <name>Arg</name>
      <anchorfile>structGPDMA__CFG__T.html</anchorfile>
      <anchor>a1b81fffebab14bf2be93f79430b37e19</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>XferWidth</name>
      <anchorfile>structGPDMA__CFG__T.html</anchorfile>
      <anchor>a675bedc34bc70ceefff17992b751d72a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>SrcConn</name>
      <anchorfile>structGPDMA__CFG__T.html</anchorfile>
      <anchor>a5126c066bdfe4cdcb01d9b62b84fc174</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>DstConn</name>
      <anchorfile>structGPDMA__CFG__T.html</anchorfile>
      <anchor>a2b2223ecc69a6fc270c6d841342815e3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>GPDMA_FLOW_CONTROL_T</type>
      <name>XferType</name>
      <anchorfile>structGPDMA__CFG__T.html</anchorfile>
      <anchor>a752392555d9ba0d2de9cda6d5043188f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>GPDMA_CH_T</name>
    <filename>structGPDMA__CH__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>SRCADDR</name>
      <anchorfile>structGPDMA__CH__T.html</anchorfile>
      <anchor>a734e1803144cdaa3cfab1507fa4f05d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>DESTADDR</name>
      <anchorfile>structGPDMA__CH__T.html</anchorfile>
      <anchor>ab61baba3fc08a7ba4b0502d043ed6ccd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>LLI</name>
      <anchorfile>structGPDMA__CH__T.html</anchorfile>
      <anchor>a240936dbac9f3a949572b3dd196bd83f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CONTROL</name>
      <anchorfile>structGPDMA__CH__T.html</anchorfile>
      <anchor>a7b2997d55e2bbe71c2f99d5879ca75c3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CONFIG</name>
      <anchorfile>structGPDMA__CH__T.html</anchorfile>
      <anchor>a4f74d11c01d0d3203ceeebd9db2a50a8</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>GPDMA_DESC_T</name>
    <filename>structGPDMA__DESC__T.html</filename>
    <member kind="variable">
      <type>uint32_t</type>
      <name>SrcAddr</name>
      <anchorfile>structGPDMA__DESC__T.html</anchorfile>
      <anchor>abf02844aacad11c28ceeec7443b81894</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>DstAddr</name>
      <anchorfile>structGPDMA__DESC__T.html</anchorfile>
      <anchor>af95d5e12d9011a7881951e520d00a220</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>LinkAddr</name>
      <anchorfile>structGPDMA__DESC__T.html</anchorfile>
      <anchor>a7f2ff00f5752bcaecb597efa6df3f00a</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>GPIOINT_PORT_T</name>
    <filename>structGPIOINT__PORT__T.html</filename>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>STATR</name>
      <anchorfile>structGPIOINT__PORT__T.html</anchorfile>
      <anchor>afff0c48e8b49a668e3f7125b5443280a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>STATF</name>
      <anchorfile>structGPIOINT__PORT__T.html</anchorfile>
      <anchor>a4439b8e7ddaac1768c0caf158a884375</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>CLR</name>
      <anchorfile>structGPIOINT__PORT__T.html</anchorfile>
      <anchor>aca139ec8cd0a157793ab02b63caaa32b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>ENR</name>
      <anchorfile>structGPIOINT__PORT__T.html</anchorfile>
      <anchor>aec2ea22900c6c655ffe5da100027c792</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>ENF</name>
      <anchorfile>structGPIOINT__PORT__T.html</anchorfile>
      <anchor>ac0330b9c737da49042c1318175e13038</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>_HID_DESCRIPTOR</name>
    <filename>struct__HID__DESCRIPTOR.html</filename>
    <class kind="struct">_HID_DESCRIPTOR::_HID_DESCRIPTOR_LIST</class>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bLength</name>
      <anchorfile>struct__HID__DESCRIPTOR.html</anchorfile>
      <anchor>a979bbf8e7cc5687972069636a3b33c20</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bDescriptorType</name>
      <anchorfile>struct__HID__DESCRIPTOR.html</anchorfile>
      <anchor>a2bbdd2858b320f94beb8b6f2785f2e17</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>bcdHID</name>
      <anchorfile>struct__HID__DESCRIPTOR.html</anchorfile>
      <anchor>a6adc2d5f876e919012d2951ff9b9f8ab</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bCountryCode</name>
      <anchorfile>struct__HID__DESCRIPTOR.html</anchorfile>
      <anchor>a9fc900321e0f039666048147577c305a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bNumDescriptors</name>
      <anchorfile>struct__HID__DESCRIPTOR.html</anchorfile>
      <anchor>a552eeff6b75d4a91dfb30e09e3c23f4a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>PRE_PACK struct POST_PACK _HID_DESCRIPTOR::_HID_DESCRIPTOR_LIST</type>
      <name>DescriptorList</name>
      <anchorfile>struct__HID__DESCRIPTOR.html</anchorfile>
      <anchor>a9ee990e1037af61b19834c58d73e6d9a</anchor>
      <arglist>[1]</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>_HID_DESCRIPTOR::_HID_DESCRIPTOR_LIST</name>
    <filename>struct__HID__DESCRIPTOR_1_1__HID__DESCRIPTOR__LIST.html</filename>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bDescriptorType</name>
      <anchorfile>struct__HID__DESCRIPTOR_1_1__HID__DESCRIPTOR__LIST.html</anchorfile>
      <anchor>a1e50d94dada08bb086c2b68e8bf427c6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>wDescriptorLength</name>
      <anchorfile>struct__HID__DESCRIPTOR_1_1__HID__DESCRIPTOR__LIST.html</anchorfile>
      <anchor>a3dd9dcf5f28f63e248c0b5cfda0bcfa3</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>I2C_BUFF_T</name>
    <filename>structI2C__BUFF__T.html</filename>
    <member kind="variable">
      <type>uint8_t *</type>
      <name>bufPtr</name>
      <anchorfile>structI2C__BUFF__T.html</anchorfile>
      <anchor>a0d2b23daf6e7c0aef2486bb736f5c145</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>bufSz</name>
      <anchorfile>structI2C__BUFF__T.html</anchorfile>
      <anchor>a1e35aa435d122a869402e30b3f032569</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct __I2C_BUFF_T *</type>
      <name>bufNext</name>
      <anchorfile>structI2C__BUFF__T.html</anchorfile>
      <anchor>a28289716673069ef46dc62ae84575849</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>i2c_interface</name>
    <filename>structi2c__interface.html</filename>
  </compound>
  <compound kind="struct">
    <name>i2c_slave_interface</name>
    <filename>structi2c__slave__interface.html</filename>
  </compound>
  <compound kind="struct">
    <name>I2C_XFER_T</name>
    <filename>structI2C__XFER__T.html</filename>
    <member kind="variable">
      <type>uint8_t</type>
      <name>slaveAddr</name>
      <anchorfile>structI2C__XFER__T.html</anchorfile>
      <anchor>a39393422071ba7bca5d22e15b110e723</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>I2C_BUFF_T *</type>
      <name>txBuff</name>
      <anchorfile>structI2C__XFER__T.html</anchorfile>
      <anchor>a02c83ac7273c8aad0c32cde045fd1988</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>I2C_BUFF_T *</type>
      <name>rxBuff</name>
      <anchorfile>structI2C__XFER__T.html</anchorfile>
      <anchor>a15327ef1630bc696e0c0f449b6b1d021</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>I2C_STATUS_T</type>
      <name>status</name>
      <anchorfile>structI2C__XFER__T.html</anchorfile>
      <anchor>aefc3cfe9bf7cdaeb2a632c42250602ad</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>I2S_AUDIO_FORMAT_T</name>
    <filename>structI2S__AUDIO__FORMAT__T.html</filename>
    <member kind="variable">
      <type>uint32_t</type>
      <name>SampleRate</name>
      <anchorfile>structI2S__AUDIO__FORMAT__T.html</anchorfile>
      <anchor>aef370fad5b70b5226a2f8a780bd934d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>ChannelNumber</name>
      <anchorfile>structI2S__AUDIO__FORMAT__T.html</anchorfile>
      <anchor>ad9856c3cfc176a9c5f25851dda4d848d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>WordWidth</name>
      <anchorfile>structI2S__AUDIO__FORMAT__T.html</anchorfile>
      <anchor>a5b21d5d739e0eb7e66c898618fcd4605</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>IP_CAN_001_CR_T</name>
    <filename>structIP__CAN__001__CR__T.html</filename>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>TXSR</name>
      <anchorfile>structIP__CAN__001__CR__T.html</anchorfile>
      <anchor>a7ce56eda4c7eabebb9597b8b9cd418ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>RXSR</name>
      <anchorfile>structIP__CAN__001__CR__T.html</anchorfile>
      <anchor>a421bed687294a71ec89ece1ea01fc808</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>MSR</name>
      <anchorfile>structIP__CAN__001__CR__T.html</anchorfile>
      <anchor>a4cfec80b7875ddddf16f1b7284280423</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>IP_CAN_001_RX_T</name>
    <filename>structIP__CAN__001__RX__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>RFS</name>
      <anchorfile>structIP__CAN__001__RX__T.html</anchorfile>
      <anchor>aa5c1bc243bb1763d92e9070d84b594cf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>RID</name>
      <anchorfile>structIP__CAN__001__RX__T.html</anchorfile>
      <anchor>a9b8287992a2f69010823997db04d7ef6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>RD</name>
      <anchorfile>structIP__CAN__001__RX__T.html</anchorfile>
      <anchor>a7b9fd4220efbbd69d33c81d887803b69</anchor>
      <arglist>[2]</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>IP_CAN_BUS_TIMING_T</name>
    <filename>structIP__CAN__BUS__TIMING__T.html</filename>
    <member kind="variable">
      <type>uint16_t</type>
      <name>BRP</name>
      <anchorfile>structIP__CAN__BUS__TIMING__T.html</anchorfile>
      <anchor>a18fb5e94652c78dd8468b7a374699643</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>SJW</name>
      <anchorfile>structIP__CAN__BUS__TIMING__T.html</anchorfile>
      <anchor>a6f3d35851d3119d9d04096d08d41d289</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>TESG1</name>
      <anchorfile>structIP__CAN__BUS__TIMING__T.html</anchorfile>
      <anchor>ad1295003e524b683f247ea0dd4372380</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>TESG2</name>
      <anchorfile>structIP__CAN__BUS__TIMING__T.html</anchorfile>
      <anchor>a67805cda8a3bb4be441ac2ebd25ed3a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>SAM</name>
      <anchorfile>structIP__CAN__BUS__TIMING__T.html</anchorfile>
      <anchor>a3313ced0f84d23e3caf050a530119fb9</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="union">
    <name>IPSR_Type</name>
    <filename>unionIPSR__Type.html</filename>
    <member kind="variable">
      <type>struct IPSR_Type::@1</type>
      <name>b</name>
      <anchorfile>unionIPSR__Type.html</anchorfile>
      <anchor>add0d6497bd50c25569ea22b48a03ec50</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>w</name>
      <anchorfile>unionIPSR__Type.html</anchorfile>
      <anchor>a4adca999d3a0bc1ae682d73ea7cfa879</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>ITM_Type</name>
    <filename>structITM__Type.html</filename>
    <member kind="variable">
      <type>__O union ITM_Type::@4</type>
      <name>PORT</name>
      <anchorfile>structITM__Type.html</anchorfile>
      <anchor>afe056e8c8f8c5519d9b47611fa3a4c46</anchor>
      <arglist>[32]</arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>TER</name>
      <anchorfile>structITM__Type.html</anchorfile>
      <anchor>a91a040e1b162e1128ac1e852b4a0e589</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>TPR</name>
      <anchorfile>structITM__Type.html</anchorfile>
      <anchor>a93b480aac6da620bbb611212186d47fa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>TCR</name>
      <anchorfile>structITM__Type.html</anchorfile>
      <anchor>a58f169e1aa40a9b8afb6296677c3bb45</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>IWR</name>
      <anchorfile>structITM__Type.html</anchorfile>
      <anchor>afd0e0c051acd3f6187794a4e8dc7e7ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>IRR</name>
      <anchorfile>structITM__Type.html</anchorfile>
      <anchor>a212a614a8d5f2595e5eb049e5143c739</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>IMCR</name>
      <anchorfile>structITM__Type.html</anchorfile>
      <anchor>ab2e87d8bb0e3ce9b8e0e4a6a6695228a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>LAR</name>
      <anchorfile>structITM__Type.html</anchorfile>
      <anchor>a97840d39a9c63331e3689b5fa69175e9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>LSR</name>
      <anchorfile>structITM__Type.html</anchorfile>
      <anchor>aaa0515b1f6dd5e7d90b61ef67d8de77b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>PID4</name>
      <anchorfile>structITM__Type.html</anchorfile>
      <anchor>accfc7de00b0eaba0301e8f4553f70512</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>PID5</name>
      <anchorfile>structITM__Type.html</anchorfile>
      <anchor>a9353055ceb7024e07d59248e54502cb9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>PID6</name>
      <anchorfile>structITM__Type.html</anchorfile>
      <anchor>a755c0ec919e7dbb5f7ff05c8b56a3383</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>PID7</name>
      <anchorfile>structITM__Type.html</anchorfile>
      <anchor>aa31ca6bb4b749201321b23d0dbbe0704</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>PID0</name>
      <anchorfile>structITM__Type.html</anchorfile>
      <anchor>ab69ade751350a7758affdfe396517535</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>PID1</name>
      <anchorfile>structITM__Type.html</anchorfile>
      <anchor>a30e87ec6f93ecc9fe4f135ca8b068990</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>PID2</name>
      <anchorfile>structITM__Type.html</anchorfile>
      <anchor>ae139d2e588bb382573ffcce3625a88cd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>PID3</name>
      <anchorfile>structITM__Type.html</anchorfile>
      <anchor>af006ee26c7e61c9a3712a80ac74a6cf3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>CID0</name>
      <anchorfile>structITM__Type.html</anchorfile>
      <anchor>a413f3bb0a15222e5f38fca4baeef14f6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>CID1</name>
      <anchorfile>structITM__Type.html</anchorfile>
      <anchor>a5f7d524b71f49e444ff0d1d52b3c3565</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>CID2</name>
      <anchorfile>structITM__Type.html</anchorfile>
      <anchor>adee4ccce1429db8b5db3809c4539f876</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>CID3</name>
      <anchorfile>structITM__Type.html</anchorfile>
      <anchor>a0e7aa199619cc7ac6baddff9600aa52e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_ADC_T</name>
    <filename>structLPC__ADC__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CR</name>
      <anchorfile>structLPC__ADC__T.html</anchorfile>
      <anchor>a751f1cbdd3d5242aea596dc18113fedc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>GDR</name>
      <anchorfile>structLPC__ADC__T.html</anchorfile>
      <anchor>a063e42ec8fcdcf5590579a8d1a888ca0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>INTEN</name>
      <anchorfile>structLPC__ADC__T.html</anchorfile>
      <anchor>a3ed883c378f817342d10cb7ed29a05ae</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>DR</name>
      <anchorfile>structLPC__ADC__T.html</anchorfile>
      <anchor>a10f521f172d10766bb189be671f8bd57</anchor>
      <arglist>[8]</arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>STAT</name>
      <anchorfile>structLPC__ADC__T.html</anchorfile>
      <anchor>a6fd64c9a5717b2adc106721eb9ab190b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_CAN_T</name>
    <filename>structLPC__CAN__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MOD</name>
      <anchorfile>structLPC__CAN__T.html</anchorfile>
      <anchor>a8bc39ce9227ace84de55a902d60cb11e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>CMR</name>
      <anchorfile>structLPC__CAN__T.html</anchorfile>
      <anchor>a2b22d55ceb4c934524714384f23cc87e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>GSR</name>
      <anchorfile>structLPC__CAN__T.html</anchorfile>
      <anchor>ae36d4c72254feab1bce53a7653612acc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>ICR</name>
      <anchorfile>structLPC__CAN__T.html</anchorfile>
      <anchor>a8ccbac222a59bb6da7ba81f8394dbb2e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>IER</name>
      <anchorfile>structLPC__CAN__T.html</anchorfile>
      <anchor>a2ab9628e17449ba6a3bfa9c8479c2e8a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>BTR</name>
      <anchorfile>structLPC__CAN__T.html</anchorfile>
      <anchor>a2ef6231da82a2f3f55f05797f8ac4fa0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>EWL</name>
      <anchorfile>structLPC__CAN__T.html</anchorfile>
      <anchor>ab79cd2d466c6f59c3bb43873fd556af6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>SR</name>
      <anchorfile>structLPC__CAN__T.html</anchorfile>
      <anchor>a16d0fa727f1fbcff03739b512b00806d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO IP_CAN_001_RX_T</type>
      <name>RX</name>
      <anchorfile>structLPC__CAN__T.html</anchorfile>
      <anchor>a9890a3ed561ea1e0fbce9a2d6321ac39</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO LPC_CAN_TX_T</type>
      <name>TX</name>
      <anchorfile>structLPC__CAN__T.html</anchorfile>
      <anchor>ad24f43de6447283b4379922cc1c83a1d</anchor>
      <arglist>[3]</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_CAN_TX_T</name>
    <filename>structLPC__CAN__TX__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>TFI</name>
      <anchorfile>structLPC__CAN__TX__T.html</anchorfile>
      <anchor>a10a4c2076dd63148d4e9e4ad32af1451</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>TID</name>
      <anchorfile>structLPC__CAN__TX__T.html</anchorfile>
      <anchor>a7a98a061fcb602592c5b6a17810a42c3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>TD</name>
      <anchorfile>structLPC__CAN__TX__T.html</anchorfile>
      <anchor>ab41475b9f2b0952da4e71f74cfe33c20</anchor>
      <arglist>[2]</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_CANAF_RAM_T</name>
    <filename>structLPC__CANAF__RAM__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MASK</name>
      <anchorfile>structLPC__CANAF__RAM__T.html</anchorfile>
      <anchor>aca1f20422319eb6c80082ba9fd8cc3bc</anchor>
      <arglist>[CANAF_RAM_ENTRY_NUM]</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_CANAF_T</name>
    <filename>structLPC__CANAF__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>AFMR</name>
      <anchorfile>structLPC__CANAF__T.html</anchorfile>
      <anchor>a987f2996cd4432665a2cbde52251f429</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>ENDADDR</name>
      <anchorfile>structLPC__CANAF__T.html</anchorfile>
      <anchor>ab62d75d561c2446b4ac8ce383e566498</anchor>
      <arglist>[CANAF_RAM_SECTION_NUM]</arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>LUTERRAD</name>
      <anchorfile>structLPC__CANAF__T.html</anchorfile>
      <anchor>ac98a6bb03e4031fbfee27c49b884fddd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>LUTERR</name>
      <anchorfile>structLPC__CANAF__T.html</anchorfile>
      <anchor>acd4acbf7638e632b78feaccdba530c88</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>FCANIE</name>
      <anchorfile>structLPC__CANAF__T.html</anchorfile>
      <anchor>aca9607036e23ed6523777c80e1ab1147</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>FCANIC</name>
      <anchorfile>structLPC__CANAF__T.html</anchorfile>
      <anchor>a89079a08d1150a7761dff98d0130d7e5</anchor>
      <arglist>[2]</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_DAC_T</name>
    <filename>structLPC__DAC__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CR</name>
      <anchorfile>structLPC__DAC__T.html</anchorfile>
      <anchor>a9e9bbab757da4f3ef793da168224eb9d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CTRL</name>
      <anchorfile>structLPC__DAC__T.html</anchorfile>
      <anchor>a602c1a7a50c8372683c0eef978e2c149</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CNTVAL</name>
      <anchorfile>structLPC__DAC__T.html</anchorfile>
      <anchor>adfd515a77f5d9e0e7ca6b6835a996c47</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_ENET_T</name>
    <filename>structLPC__ENET__T.html</filename>
    <member kind="variable">
      <type>ENET_MAC_T</type>
      <name>MAC</name>
      <anchorfile>structLPC__ENET__T.html</anchorfile>
      <anchor>a2f1d337b7206673d31a165cee9c6b1aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>ENET_CONTROL_T</type>
      <name>CONTROL</name>
      <anchorfile>structLPC__ENET__T.html</anchorfile>
      <anchor>a40bc06ba6a0ce4949f8a94550cfc9798</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>ENET_RXFILTER_T</type>
      <name>RXFILTER</name>
      <anchorfile>structLPC__ENET__T.html</anchorfile>
      <anchor>a4076c6a080ba8b6e6e7af7343408080b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>ENET_MODULE_CTRL_T</type>
      <name>MODULE_CONTROL</name>
      <anchorfile>structLPC__ENET__T.html</anchorfile>
      <anchor>a279749cd4018c81be5aa07edfe62d3e7</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_FMC_T</name>
    <filename>structLPC__FMC__T.html</filename>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>RESERVED1</name>
      <anchorfile>structLPC__FMC__T.html</anchorfile>
      <anchor>a17b10ffb216f33f76e0984f1bb97c3dd</anchor>
      <arglist>[8]</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_GPDMA_T</name>
    <filename>structLPC__GPDMA__T.html</filename>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>INTSTAT</name>
      <anchorfile>structLPC__GPDMA__T.html</anchorfile>
      <anchor>a744123eb8ee989a3c80570160cd0b91b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>INTTCSTAT</name>
      <anchorfile>structLPC__GPDMA__T.html</anchorfile>
      <anchor>a2ca8f15773342988a28ba333cf73c380</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>INTTCCLEAR</name>
      <anchorfile>structLPC__GPDMA__T.html</anchorfile>
      <anchor>a85a79ad19b52d9017a2fd7b15349a168</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>INTERRSTAT</name>
      <anchorfile>structLPC__GPDMA__T.html</anchorfile>
      <anchor>a6d563f8fa43b98a42090d51698735727</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>INTERRCLR</name>
      <anchorfile>structLPC__GPDMA__T.html</anchorfile>
      <anchor>ae278f6d236ca389e9aa9a8bcd68ba063</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>RAWINTTCSTAT</name>
      <anchorfile>structLPC__GPDMA__T.html</anchorfile>
      <anchor>a215d6f79819ceb03fd00ecae1324a7c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>RAWINTERRSTAT</name>
      <anchorfile>structLPC__GPDMA__T.html</anchorfile>
      <anchor>ae4bf6971c867a0892da2cf449c341122</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>ENBLDCHNS</name>
      <anchorfile>structLPC__GPDMA__T.html</anchorfile>
      <anchor>aa35d28a63f29b0e098d8e40dc294049a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>SOFTBREQ</name>
      <anchorfile>structLPC__GPDMA__T.html</anchorfile>
      <anchor>ac6bbc437741b88b7178437840f482bd7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>SOFTSREQ</name>
      <anchorfile>structLPC__GPDMA__T.html</anchorfile>
      <anchor>a2905a8988cfd38c6e44d121783c0bfe7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>SOFTLBREQ</name>
      <anchorfile>structLPC__GPDMA__T.html</anchorfile>
      <anchor>a565d4d2985feee1cf21acee1185373a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>SOFTLSREQ</name>
      <anchorfile>structLPC__GPDMA__T.html</anchorfile>
      <anchor>a0b7a85e9f40e19d368c8358ca48778de</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CONFIG</name>
      <anchorfile>structLPC__GPDMA__T.html</anchorfile>
      <anchor>a059b4997225adef9a773dca681a4b676</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>SYNC</name>
      <anchorfile>structLPC__GPDMA__T.html</anchorfile>
      <anchor>ac89c20888b14781ead9fe459f8f79853</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_GPIO_T</name>
    <filename>structLPC__GPIO__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>DIR</name>
      <anchorfile>structLPC__GPIO__T.html</anchorfile>
      <anchor>a753957fa7e1c261e57255c1db8d62bb7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MASK</name>
      <anchorfile>structLPC__GPIO__T.html</anchorfile>
      <anchor>a5b0ae096141efd7298aad95ca7b3370e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>PIN</name>
      <anchorfile>structLPC__GPIO__T.html</anchorfile>
      <anchor>a1311cc8cf63279d1bdfca5d48c6ccf0a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>SET</name>
      <anchorfile>structLPC__GPIO__T.html</anchorfile>
      <anchor>aaafd15e2c4eb4665f7a546dfeea25954</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>CLR</name>
      <anchorfile>structLPC__GPIO__T.html</anchorfile>
      <anchor>aba8161f685588a1ca79702207083e6ab</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_GPIOINT_T</name>
    <filename>structLPC__GPIOINT__T.html</filename>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>STATUS</name>
      <anchorfile>structLPC__GPIOINT__T.html</anchorfile>
      <anchor>a9547c1d447b762a819920fe2a10c5029</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>GPIOINT_PORT_T</type>
      <name>IO0</name>
      <anchorfile>structLPC__GPIOINT__T.html</anchorfile>
      <anchor>a4edac525fb01aae7b57c4bcc9563ae27</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>GPIOINT_PORT_T</type>
      <name>IO2</name>
      <anchorfile>structLPC__GPIOINT__T.html</anchorfile>
      <anchor>a1300843bc75ee56c6177f05f69b23ef8</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_I2C_T</name>
    <filename>structLPC__I2C__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CONSET</name>
      <anchorfile>structLPC__I2C__T.html</anchorfile>
      <anchor>a98ed6d816b3c7e00a29b32956fe5fa2d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>STAT</name>
      <anchorfile>structLPC__I2C__T.html</anchorfile>
      <anchor>ae806722ff38a93680338e5607d96156d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>DAT</name>
      <anchorfile>structLPC__I2C__T.html</anchorfile>
      <anchor>ab3babd27c9d202c1fefce13d5498d473</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>ADR0</name>
      <anchorfile>structLPC__I2C__T.html</anchorfile>
      <anchor>a900e0819e3f450e08a3566caf6f18851</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>SCLH</name>
      <anchorfile>structLPC__I2C__T.html</anchorfile>
      <anchor>a73be20e257e178ea0deafd0057cc7900</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>SCLL</name>
      <anchorfile>structLPC__I2C__T.html</anchorfile>
      <anchor>a1cd0d0f95f90d6e6c3380f112144085b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>CONCLR</name>
      <anchorfile>structLPC__I2C__T.html</anchorfile>
      <anchor>a091c12dd25fb7db692339da933c8b523</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MMCTRL</name>
      <anchorfile>structLPC__I2C__T.html</anchorfile>
      <anchor>ace2d21c2a5042f1355f98b2e687ae8c1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>ADR1</name>
      <anchorfile>structLPC__I2C__T.html</anchorfile>
      <anchor>ad948a871ac4d57bde1698f91ea554903</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>ADR2</name>
      <anchorfile>structLPC__I2C__T.html</anchorfile>
      <anchor>aad3c43c326c675b3c9a02936f7b906fa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>ADR3</name>
      <anchorfile>structLPC__I2C__T.html</anchorfile>
      <anchor>a554732a259fca555bcec4201d756e945</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>DATA_BUFFER</name>
      <anchorfile>structLPC__I2C__T.html</anchorfile>
      <anchor>a63283a528320f69703f42fcc919ab4bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MASK</name>
      <anchorfile>structLPC__I2C__T.html</anchorfile>
      <anchor>a9e734d7a24b7a02ac1887608552b9c69</anchor>
      <arglist>[4]</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_I2S_T</name>
    <filename>structLPC__I2S__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>DAO</name>
      <anchorfile>structLPC__I2S__T.html</anchorfile>
      <anchor>ad574a0e2f1cdec634e05ee8176c0450e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>DAI</name>
      <anchorfile>structLPC__I2S__T.html</anchorfile>
      <anchor>ab94e2f49e7d70f8b19c3d74f2e67878e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>TXFIFO</name>
      <anchorfile>structLPC__I2S__T.html</anchorfile>
      <anchor>aa3a381d218c4342917885fa3a51e82d8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>RXFIFO</name>
      <anchorfile>structLPC__I2S__T.html</anchorfile>
      <anchor>aac5c44248c1354eb4320eb5fa5b18baa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>STATE</name>
      <anchorfile>structLPC__I2S__T.html</anchorfile>
      <anchor>a35299409c86d4860e936a7ff3e5603bf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>DMA</name>
      <anchorfile>structLPC__I2S__T.html</anchorfile>
      <anchor>a2259283a5ad23aa09617861948df25d7</anchor>
      <arglist>[I2S_DMA_REQUEST_CHANNEL_NUM]</arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>IRQ</name>
      <anchorfile>structLPC__I2S__T.html</anchorfile>
      <anchor>a0ed73323cb1c577293f1416c6c55c47f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>TXRATE</name>
      <anchorfile>structLPC__I2S__T.html</anchorfile>
      <anchor>ab62a8a48830412bcf4cec1b1ed5a5b21</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>RXRATE</name>
      <anchorfile>structLPC__I2S__T.html</anchorfile>
      <anchor>a03d51065fa11a182809e189b919b034a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>TXBITRATE</name>
      <anchorfile>structLPC__I2S__T.html</anchorfile>
      <anchor>a16c60f11507aee3e4aef26c365a02ce2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>RXBITRATE</name>
      <anchorfile>structLPC__I2S__T.html</anchorfile>
      <anchor>a3f0c2587313eb723ac48ad06af157df9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>TXMODE</name>
      <anchorfile>structLPC__I2S__T.html</anchorfile>
      <anchor>a30384953ab71a349e60ed555b4ec4552</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>RXMODE</name>
      <anchorfile>structLPC__I2S__T.html</anchorfile>
      <anchor>a257e2de4bfa371adba9eeb4c2e95fe09</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_IOCON_T</name>
    <filename>structLPC__IOCON__T.html</filename>
  </compound>
  <compound kind="struct">
    <name>LPC_MCPWM_T</name>
    <filename>structLPC__MCPWM__T.html</filename>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>CON</name>
      <anchorfile>structLPC__MCPWM__T.html</anchorfile>
      <anchor>ae047b5f0f2ca06bbaa663ce2458ae7ad</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>CON_SET</name>
      <anchorfile>structLPC__MCPWM__T.html</anchorfile>
      <anchor>a3dc36e144bb33d1756f06c765726f15d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>CON_CLR</name>
      <anchorfile>structLPC__MCPWM__T.html</anchorfile>
      <anchor>a22b564b847caa28e9f0c53ff38375cc3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>CAPCON</name>
      <anchorfile>structLPC__MCPWM__T.html</anchorfile>
      <anchor>a47afe6c393cc7da61af1d48bfc0f709c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>CAPCON_SET</name>
      <anchorfile>structLPC__MCPWM__T.html</anchorfile>
      <anchor>a8511a69d57549e1765080274f4bd5c3c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>CAPCON_CLR</name>
      <anchorfile>structLPC__MCPWM__T.html</anchorfile>
      <anchor>aadd69d706d636d53ff8893d3a0be210a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>TC</name>
      <anchorfile>structLPC__MCPWM__T.html</anchorfile>
      <anchor>ae6960477ec37aa32fbb20956cf01edb7</anchor>
      <arglist>[3]</arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>LIM</name>
      <anchorfile>structLPC__MCPWM__T.html</anchorfile>
      <anchor>a01c2889381949b5b1f8c2bf8edf9148c</anchor>
      <arglist>[3]</arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MAT</name>
      <anchorfile>structLPC__MCPWM__T.html</anchorfile>
      <anchor>ac3b16a52a48e436af60d17ce27c5c879</anchor>
      <arglist>[3]</arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>DT</name>
      <anchorfile>structLPC__MCPWM__T.html</anchorfile>
      <anchor>a15d8f4f81c3e82a335d4abb02863b850</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CCP</name>
      <anchorfile>structLPC__MCPWM__T.html</anchorfile>
      <anchor>a8b75fa73064dfd26c78d2a4bfe5ff01a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>CAP</name>
      <anchorfile>structLPC__MCPWM__T.html</anchorfile>
      <anchor>a015a4607abaca03f64acaeae4a0ccaf7</anchor>
      <arglist>[3]</arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>INTEN</name>
      <anchorfile>structLPC__MCPWM__T.html</anchorfile>
      <anchor>a2f6d65b7d9772a735474d90311e738b6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>INTEN_SET</name>
      <anchorfile>structLPC__MCPWM__T.html</anchorfile>
      <anchor>ab678eeb9fa0e190473e58d179778a249</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>INTEN_CLR</name>
      <anchorfile>structLPC__MCPWM__T.html</anchorfile>
      <anchor>a5694122ceacd913b57424a90be8f7f63</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>CNTCON</name>
      <anchorfile>structLPC__MCPWM__T.html</anchorfile>
      <anchor>a7a400f1cfa5c66f9546a2df92acf0402</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>CNTCON_SET</name>
      <anchorfile>structLPC__MCPWM__T.html</anchorfile>
      <anchor>a726084d7661d7fc8bb579fef4ed5727f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>CNTCON_CLR</name>
      <anchorfile>structLPC__MCPWM__T.html</anchorfile>
      <anchor>a1130e338a71c6ee3a1c92084ced3aecd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>INTF</name>
      <anchorfile>structLPC__MCPWM__T.html</anchorfile>
      <anchor>a0bfcbbed2fabcf16419d014ff01e8e37</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>INTF_SET</name>
      <anchorfile>structLPC__MCPWM__T.html</anchorfile>
      <anchor>a4c129a22156572b5671ff0e8b9f1977d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>INTF_CLR</name>
      <anchorfile>structLPC__MCPWM__T.html</anchorfile>
      <anchor>acd49890931c9446a54e8070478cf7185</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>CAP_CLR</name>
      <anchorfile>structLPC__MCPWM__T.html</anchorfile>
      <anchor>a88da2eb831f09cf7145f57c45b224e91</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_PMU_T</name>
    <filename>structLPC__PMU__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>PCON</name>
      <anchorfile>structLPC__PMU__T.html</anchorfile>
      <anchor>a9b889ba60e56c881274ac9eaebecaf66</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_QEI_T</name>
    <filename>structLPC__QEI__T.html</filename>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>CON</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>af84cacebc1e4ba309cb8c1af4acca96f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>STAT</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>a842c67512c5c8c9a31e7ba2ec4771784</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CONF</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>a72886edbac2779ab5e2823fd0db339f7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>POS</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>a400314449370eff087c3c5e1e1c31cc1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MAXPOS</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>acaf54ed95adb4553f8be314b509f786b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CMPOS0</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>a4bb72cc566919075cb721bb123e72443</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CMPOS1</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>ad1272dc00c77777af8e58d6ac3b94902</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CMPOS2</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>aa2f3bddd189d57b8985ffca9ddb70ff2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>INXCNT</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>ae5f756b6037664715b3157f6b6953e42</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>INXCMP0</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>abb45974ac42d27676c2b3c08d9f9aad0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>LOAD</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>aab13bc1fd1ecc6539d985d00a2beb756</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>TIME</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>a73a528b77fc560d229dc287237a9879e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>VEL</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>a15e45a6723e72a83eb3c2671b1c1f43d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>CAP</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>a3496f3632323f5c9b09c816f57b90b2d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>VELCOMP</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>a5237f6f2f6ed6530bbf6a13d0e20151f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>FILTERPHA</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>ae8de561fff333fbf117036c7de063c36</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>FILTERPHB</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>a385bbf02e2aa20edde97366861e0ef8d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>FILTERINX</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>a92cbc5ae36dcd7194b4b1f3d448ca2e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>WINDOW</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>af75222d15c3a2744586a5f27a0549c18</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>INXCMP1</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>a69a6150f42d9c8f9ed52c305c72bd72f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>INXCMP2</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>a19ba8a85129824ec0259b6733baff660</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>IEC</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>ad4164e0990c509994c19caccf1f7cf61</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>IES</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>a2eab592cbea507455a7aaab9a5ab6d53</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>INTSTAT</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>a550a4e26c5e38589e8722dc3064478a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>IE</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>a89eace37d24d618b12322f0479b3aefd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>CLR</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>a909f2a48a6a21651d6baf9a3a94e6cb0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>SET</name>
      <anchorfile>structLPC__QEI__T.html</anchorfile>
      <anchor>a199af3383881188039525c15e18049a9</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_REGFILE_T</name>
    <filename>structLPC__REGFILE__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>REGFILE</name>
      <anchorfile>structLPC__REGFILE__T.html</anchorfile>
      <anchor>ae61f3de82503d032642cbdd27a4a160d</anchor>
      <arglist>[5]</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_ROM_API_T</name>
    <filename>structLPC__ROM__API__T.html</filename>
    <member kind="variable">
      <type>const uint32_t</type>
      <name>usbdApiBase</name>
      <anchorfile>structLPC__ROM__API__T.html</anchorfile>
      <anchor>a94bc8146ba697eeadc992d4985971550</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const uint32_t</type>
      <name>reserved0</name>
      <anchorfile>structLPC__ROM__API__T.html</anchorfile>
      <anchor>a93487c5be9000ba3b633bd6c354f8589</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const uint32_t</type>
      <name>reserved1</name>
      <anchorfile>structLPC__ROM__API__T.html</anchorfile>
      <anchor>a2fce9202473b5986d54262b3d5548e82</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const uint32_t</type>
      <name>reserved2</name>
      <anchorfile>structLPC__ROM__API__T.html</anchorfile>
      <anchor>a0ab9f56adf03f0f69b04baa60bcc271a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const uint32_t</type>
      <name>reserved3</name>
      <anchorfile>structLPC__ROM__API__T.html</anchorfile>
      <anchor>a70c7b14958f6d5af166224d04148e945</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const uint32_t</type>
      <name>reserved4</name>
      <anchorfile>structLPC__ROM__API__T.html</anchorfile>
      <anchor>a1d3193c8ed079cf1c5006dce51686598</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const uint32_t</type>
      <name>reserved5</name>
      <anchorfile>structLPC__ROM__API__T.html</anchorfile>
      <anchor>a1e6687c757f30d286d1ae661c0863026</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const uint32_t</type>
      <name>reserved6</name>
      <anchorfile>structLPC__ROM__API__T.html</anchorfile>
      <anchor>a425b87f3b0f5ce258583e9e965829606</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const uint32_t</type>
      <name>reserved7</name>
      <anchorfile>structLPC__ROM__API__T.html</anchorfile>
      <anchor>a7a9d7f0aa59c70ff085c59291594d22c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const uint32_t</type>
      <name>reserved8</name>
      <anchorfile>structLPC__ROM__API__T.html</anchorfile>
      <anchor>a81674fc569de19c3dd861c1f87df1d44</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const uint32_t</type>
      <name>reserved9</name>
      <anchorfile>structLPC__ROM__API__T.html</anchorfile>
      <anchor>aae524e6f969485072d45b577c7501abc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const uint32_t</type>
      <name>reserved10</name>
      <anchorfile>structLPC__ROM__API__T.html</anchorfile>
      <anchor>afb1226faead704ffe1e0c7c5efcb86f2</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_RTC_T</name>
    <filename>structLPC__RTC__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>ILR</name>
      <anchorfile>structLPC__RTC__T.html</anchorfile>
      <anchor>a6e1debaa7074a0fae4767a70f9abff29</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CCR</name>
      <anchorfile>structLPC__RTC__T.html</anchorfile>
      <anchor>ab2117371a628879dbc56f2c1774207b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CIIR</name>
      <anchorfile>structLPC__RTC__T.html</anchorfile>
      <anchor>ac9d2627afcf203dccde2675c6c74d673</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>AMR</name>
      <anchorfile>structLPC__RTC__T.html</anchorfile>
      <anchor>af8b21ae5aa8bedcb0a1dd918678ee389</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>CTIME</name>
      <anchorfile>structLPC__RTC__T.html</anchorfile>
      <anchor>a66b166ccd4abefe149e6e3ef6d833554</anchor>
      <arglist>[3]</arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>TIME</name>
      <anchorfile>structLPC__RTC__T.html</anchorfile>
      <anchor>adb4fbf67e8231188ed424c3ce226919e</anchor>
      <arglist>[RTC_TIMETYPE_LAST]</arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CALIBRATION</name>
      <anchorfile>structLPC__RTC__T.html</anchorfile>
      <anchor>ade5b98ca9e6ea8af2fd91e8a4f20503b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>GPREG</name>
      <anchorfile>structLPC__RTC__T.html</anchorfile>
      <anchor>a6624a04c6db73e0f9e53a9bc3cf50c1a</anchor>
      <arglist>[5]</arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>RTC_AUXEN</name>
      <anchorfile>structLPC__RTC__T.html</anchorfile>
      <anchor>af65651a26ceb3d710ccdba8f762a649f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>RTC_AUX</name>
      <anchorfile>structLPC__RTC__T.html</anchorfile>
      <anchor>ab476d508c9ea87e71724c30d36ece294</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>ALRM</name>
      <anchorfile>structLPC__RTC__T.html</anchorfile>
      <anchor>abeb0bd5d52ee9f40f515b9b007c7a832</anchor>
      <arglist>[RTC_TIMETYPE_LAST]</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_SSP_T</name>
    <filename>structLPC__SSP__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CR0</name>
      <anchorfile>structLPC__SSP__T.html</anchorfile>
      <anchor>af8ad155254cb659608f4c1c6bd83b62b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CR1</name>
      <anchorfile>structLPC__SSP__T.html</anchorfile>
      <anchor>a648b740833659e86e8be3a8f6c17147c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>DR</name>
      <anchorfile>structLPC__SSP__T.html</anchorfile>
      <anchor>a7a32964f3ca72981b80cf4012c515ea8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>SR</name>
      <anchorfile>structLPC__SSP__T.html</anchorfile>
      <anchor>a5f6421682b9a321abc8d4c91a6dbd964</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CPSR</name>
      <anchorfile>structLPC__SSP__T.html</anchorfile>
      <anchor>a48e7161fc6e6b91bd724a17df5435141</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>IMSC</name>
      <anchorfile>structLPC__SSP__T.html</anchorfile>
      <anchor>a5969572eaab7a02f4bbd7c898f93ca73</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>RIS</name>
      <anchorfile>structLPC__SSP__T.html</anchorfile>
      <anchor>a0aa742f8d7d4b2e2a6038b01dbc1aa5b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>MIS</name>
      <anchorfile>structLPC__SSP__T.html</anchorfile>
      <anchor>aa4b72809de09f83335e72d0d0844a90b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>ICR</name>
      <anchorfile>structLPC__SSP__T.html</anchorfile>
      <anchor>ad788fb9f7178c7a79588b429f74b9946</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>DMACR</name>
      <anchorfile>structLPC__SSP__T.html</anchorfile>
      <anchor>a6a74b0cbac37f424e198ccef9a208d65</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_SYSCTL_T</name>
    <filename>structLPC__SYSCTL__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>FLASHCFG</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>a8dcbc2e1c89d7bd42dbd19f66e2ddd8e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MEMMAP</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>a5a5d2dc160a256a110878af0c2757a64</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>SYSCTL_PLL_REGS_T</type>
      <name>PLL</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>aabc2ad04761cf7c1cbfa8a43ddc91c84</anchor>
      <arglist>[SYSCTL_USB_PLL+1]</arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>PCON</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>a92e2ea242fb2d75af44c1979a8cf9ce4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>PCONP</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>a27650e24e19bd3b8db140db39d9ab1c4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>PCONP1</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>a7e1241849ab9c934991342896ba7c296</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>EMCCLKSEL</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>a23152315a7ac650737bf1b3852bab194</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CCLKSEL</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>a5cebc138aa8f8591199efafea7bc316f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>USBCLKSEL</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>a6d004c6e6657a7fe78e75bd3e3ea00c4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CLKSRCSEL</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>a0ddcaa3fe7ae45dfee7e17f6ec93138e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CANSLEEPCLR</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>aa6166dc2b9f99af318dfb44c259cff81</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CANWAKEFLAGS</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>a4fee9b64e430f37988fe1fb566f6577e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>EXTINT</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>ac394af46f7dc8a036a574980c62ff769</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>EXTMODE</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>a22607cf2c4669efe70eae5746f74a33c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>EXTPOLAR</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>a7d0423c5e3f7482182c229bbc4b2b83b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>RSID</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>a50cd4f8d03ea4b39516add82ac2dfd1a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>SCS</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>a90bf168dbcc04b27d230b74b5e4d17a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>PCLKSEL</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>a55111e26d098f9e4ee0dbfd5b243a014</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>PBOOST</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>acc912b84bbe8dc170fdf50300d341acb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>LCD_CFG</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>aef5d629c590fd35e8da2e061ddcdf17e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>USBIntSt</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>a0505fe464c77b1421ebbd8f9d1cd9f2a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>DMAREQSEL</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>a1915d9b403571a380a581c82352b8e4a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CLKOUTCFG</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>aebd09c67da35ee48eb53ef129aa2b76e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>RSTCON</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>ae7fbeba7b7acf3f7759d1de9eaf39ea1</anchor>
      <arglist>[2]</arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>EMCDLYCTL</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>a3976afa12c3ebc2e7acfe59aef82acc4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>EMCCAL</name>
      <anchorfile>structLPC__SYSCTL__T.html</anchorfile>
      <anchor>aa7c40eaca1081f3133eda04395be6fa6</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_TIMER_T</name>
    <filename>structLPC__TIMER__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>IR</name>
      <anchorfile>structLPC__TIMER__T.html</anchorfile>
      <anchor>a3db61fe5ab56aeea0c27c6199da63b1a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>TCR</name>
      <anchorfile>structLPC__TIMER__T.html</anchorfile>
      <anchor>ab6f7507a9fa9ac19e6ffb35f766027bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>TC</name>
      <anchorfile>structLPC__TIMER__T.html</anchorfile>
      <anchor>ac92507831988bc15ddc0fbc30ab31bf9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>PR</name>
      <anchorfile>structLPC__TIMER__T.html</anchorfile>
      <anchor>ab803475dcfb9c751b2b8d02f02cb9d95</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>PC</name>
      <anchorfile>structLPC__TIMER__T.html</anchorfile>
      <anchor>aff47df94f3c3f882c742af874983ffb9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MCR</name>
      <anchorfile>structLPC__TIMER__T.html</anchorfile>
      <anchor>a7696d9896a932a78d6a6a60488332674</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MR</name>
      <anchorfile>structLPC__TIMER__T.html</anchorfile>
      <anchor>a26e8addd98189542175c5ecbcd8d3776</anchor>
      <arglist>[4]</arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CCR</name>
      <anchorfile>structLPC__TIMER__T.html</anchorfile>
      <anchor>ae97a68e845ea92e8c617bbdf1d867e48</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CR</name>
      <anchorfile>structLPC__TIMER__T.html</anchorfile>
      <anchor>aa352dc65884c0a7b8888736a90a4bb7b</anchor>
      <arglist>[4]</arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>EMR</name>
      <anchorfile>structLPC__TIMER__T.html</anchorfile>
      <anchor>a92df4dc0b947774e8cf040b5c2c2ae30</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CTCR</name>
      <anchorfile>structLPC__TIMER__T.html</anchorfile>
      <anchor>a8bee15636f5bce5c6d3f3fcd4d8cf513</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_USART_T</name>
    <filename>structLPC__USART__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>LCR</name>
      <anchorfile>structLPC__USART__T.html</anchorfile>
      <anchor>aee4d95152fdd5222436368295e4307c2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MCR</name>
      <anchorfile>structLPC__USART__T.html</anchorfile>
      <anchor>a3fca7faf2cc9ec38e5a8566538cadf48</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>LSR</name>
      <anchorfile>structLPC__USART__T.html</anchorfile>
      <anchor>a71f711aab2dc24f42d183abd449ce829</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>MSR</name>
      <anchorfile>structLPC__USART__T.html</anchorfile>
      <anchor>ac4372e0a659dc1f4b5503a8825ed1971</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>SCR</name>
      <anchorfile>structLPC__USART__T.html</anchorfile>
      <anchor>a28417e4b3d19fcbb6c6ef116376ed58b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>ACR</name>
      <anchorfile>structLPC__USART__T.html</anchorfile>
      <anchor>a443bb067899c7f269dc903a26522f44b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>ICR</name>
      <anchorfile>structLPC__USART__T.html</anchorfile>
      <anchor>a85c78774fcee5d86f82a4bd497db2b00</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>FDR</name>
      <anchorfile>structLPC__USART__T.html</anchorfile>
      <anchor>ae35ac03aeeafd5a54f2c2c675b6dca4f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>OSR</name>
      <anchorfile>structLPC__USART__T.html</anchorfile>
      <anchor>abd99c522dd0ccec4b7c7b1c08cc3c1fb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>TER1</name>
      <anchorfile>structLPC__USART__T.html</anchorfile>
      <anchor>afa3989925f4b5c3edd74531748ccccb2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>HDEN</name>
      <anchorfile>structLPC__USART__T.html</anchorfile>
      <anchor>aedc3d854bf59c2f0999a8ae22e3e7bb4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>SCICTRL</name>
      <anchorfile>structLPC__USART__T.html</anchorfile>
      <anchor>aa8ad1461441995d0b618ff7f5b0e42be</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>RS485CTRL</name>
      <anchorfile>structLPC__USART__T.html</anchorfile>
      <anchor>a6a7e49f50f49efbe8e11befcf6e7d6b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>RS485ADRMATCH</name>
      <anchorfile>structLPC__USART__T.html</anchorfile>
      <anchor>adfc2810503e8eb5f4d9bc4622500295e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>RS485DLY</name>
      <anchorfile>structLPC__USART__T.html</anchorfile>
      <anchor>a6e9fc8109a269820386a36c81dc943ab</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>TER2</name>
      <anchorfile>structLPC__USART__T.html</anchorfile>
      <anchor>afc422e0333356dc62b23d404bfdbd1de</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>LPC_USB_T</name>
    <filename>structLPC__USB__T.html</filename>
  </compound>
  <compound kind="struct">
    <name>LPC_WWDT_T</name>
    <filename>structLPC__WWDT__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MOD</name>
      <anchorfile>structLPC__WWDT__T.html</anchorfile>
      <anchor>a92ea6cd2032ed81f7c5f626b48c4c772</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>TC</name>
      <anchorfile>structLPC__WWDT__T.html</anchorfile>
      <anchor>a8390ac91aea3330b363ad2fd5dc97f98</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>FEED</name>
      <anchorfile>structLPC__WWDT__T.html</anchorfile>
      <anchor>a80b357a30f90ba82fd46ff1faa8fe4b8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>TV</name>
      <anchorfile>structLPC__WWDT__T.html</anchorfile>
      <anchor>a8257032e074282d5242e1d2a07537db1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>WARNINT</name>
      <anchorfile>structLPC__WWDT__T.html</anchorfile>
      <anchor>a6efaed3716fc4661ef3a7a52bcc9028a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>WINDOW</name>
      <anchorfile>structLPC__WWDT__T.html</anchorfile>
      <anchor>abcc1eca1d9cc366b693a5333fb75d1e0</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>_MSC_CBW</name>
    <filename>struct__MSC__CBW.html</filename>
  </compound>
  <compound kind="struct">
    <name>_MSC_CSW</name>
    <filename>struct__MSC__CSW.html</filename>
  </compound>
  <compound kind="struct">
    <name>NVIC_Type</name>
    <filename>structNVIC__Type.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>ISER</name>
      <anchorfile>structNVIC__Type.html</anchorfile>
      <anchor>af90c80b7c2b48e248780b3781e0df80f</anchor>
      <arglist>[8]</arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>ICER</name>
      <anchorfile>structNVIC__Type.html</anchorfile>
      <anchor>a1965a2e68b61d2e2009621f6949211a5</anchor>
      <arglist>[8]</arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>ISPR</name>
      <anchorfile>structNVIC__Type.html</anchorfile>
      <anchor>acf8e38fc2e97316242ddeb7ea959ab90</anchor>
      <arglist>[8]</arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>ICPR</name>
      <anchorfile>structNVIC__Type.html</anchorfile>
      <anchor>a46241be64208436d35c9a4f8552575c5</anchor>
      <arglist>[8]</arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>IABR</name>
      <anchorfile>structNVIC__Type.html</anchorfile>
      <anchor>a33e917b381e08dabe4aa5eb2881a7c11</anchor>
      <arglist>[8]</arglist>
    </member>
    <member kind="variable">
      <type>__IO uint8_t</type>
      <name>IP</name>
      <anchorfile>structNVIC__Type.html</anchorfile>
      <anchor>a6524789fedb94623822c3e0a47f3d06c</anchor>
      <arglist>[240]</arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>STIR</name>
      <anchorfile>structNVIC__Type.html</anchorfile>
      <anchor>a0b0d7f3131da89c659a2580249432749</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>PINMUX_GRP_T</name>
    <filename>structPINMUX__GRP__T.html</filename>
  </compound>
  <compound kind="union">
    <name>_REQUEST_TYPE</name>
    <filename>union__REQUEST__TYPE.html</filename>
    <member kind="variable">
      <type>uint8_t</type>
      <name>B</name>
      <anchorfile>union__REQUEST__TYPE.html</anchorfile>
      <anchor>a2640497b57e73729f061d60ffb14cce5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>BM_T</type>
      <name>BM</name>
      <anchorfile>union__REQUEST__TYPE.html</anchorfile>
      <anchor>abfaabce86a194581a5015db841f10f1f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>RINGBUFF_T</name>
    <filename>structRINGBUFF__T.html</filename>
  </compound>
  <compound kind="struct">
    <name>RTC_TIME_T</name>
    <filename>structRTC__TIME__T.html</filename>
  </compound>
  <compound kind="struct">
    <name>SCB_Type</name>
    <filename>structSCB__Type.html</filename>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>CPUID</name>
      <anchorfile>structSCB__Type.html</anchorfile>
      <anchor>afa7a9ee34dfa1da0b60b4525da285032</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>ICSR</name>
      <anchorfile>structSCB__Type.html</anchorfile>
      <anchor>a3e66570ab689d28aebefa7e84e85dc4a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>VTOR</name>
      <anchorfile>structSCB__Type.html</anchorfile>
      <anchor>a0faf96f964931cadfb71cfa54e051f6f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>AIRCR</name>
      <anchorfile>structSCB__Type.html</anchorfile>
      <anchor>a6ed3c9064013343ea9fd0a73a734f29d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>SCR</name>
      <anchorfile>structSCB__Type.html</anchorfile>
      <anchor>abfad14e7b4534d73d329819625d77a16</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CCR</name>
      <anchorfile>structSCB__Type.html</anchorfile>
      <anchor>a6d273c6b90bad15c91dfbbad0f6e92d8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint8_t</type>
      <name>SHP</name>
      <anchorfile>structSCB__Type.html</anchorfile>
      <anchor>af6336103f8be0cab29de51daed5a65f4</anchor>
      <arglist>[12]</arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>SHCSR</name>
      <anchorfile>structSCB__Type.html</anchorfile>
      <anchor>ae9891a59abbe51b0b2067ca507ca212f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CFSR</name>
      <anchorfile>structSCB__Type.html</anchorfile>
      <anchor>a2f94bf549b16fdeb172352e22309e3c4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>HFSR</name>
      <anchorfile>structSCB__Type.html</anchorfile>
      <anchor>a7bed53391da4f66d8a2a236a839d4c3d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>DFSR</name>
      <anchorfile>structSCB__Type.html</anchorfile>
      <anchor>ad7d61d9525fa9162579c3da0b87bff8d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>MMFAR</name>
      <anchorfile>structSCB__Type.html</anchorfile>
      <anchor>ac49b24b3f222508464f111772f2c44dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>BFAR</name>
      <anchorfile>structSCB__Type.html</anchorfile>
      <anchor>a31f79afe86c949c9862e7d5fce077c3a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>AFSR</name>
      <anchorfile>structSCB__Type.html</anchorfile>
      <anchor>aeb77053c84f49c261ab5b8374e8958ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>PFR</name>
      <anchorfile>structSCB__Type.html</anchorfile>
      <anchor>a3f51c43f952f3799951d0c54e76b0cb7</anchor>
      <arglist>[2]</arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>DFR</name>
      <anchorfile>structSCB__Type.html</anchorfile>
      <anchor>a586a5225467262b378c0f231ccc77f86</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>ADR</name>
      <anchorfile>structSCB__Type.html</anchorfile>
      <anchor>aaedf846e435ed05c68784b40d3db2bf2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>MMFR</name>
      <anchorfile>structSCB__Type.html</anchorfile>
      <anchor>aec2f8283d2737c6897188568a4214976</anchor>
      <arglist>[4]</arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>ISAR</name>
      <anchorfile>structSCB__Type.html</anchorfile>
      <anchor>acee8e458f054aac964268f4fe647ea4f</anchor>
      <arglist>[5]</arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CPACR</name>
      <anchorfile>structSCB__Type.html</anchorfile>
      <anchor>af460b56ce524a8e3534173f0aee78e85</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>SCnSCB_Type</name>
    <filename>structSCnSCB__Type.html</filename>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>ICTR</name>
      <anchorfile>structSCnSCB__Type.html</anchorfile>
      <anchor>ad99a25f5d4c163d9005ca607c24f6a98</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>SPI_Address_t</name>
    <filename>structSPI__Address__t.html</filename>
    <member kind="variable">
      <type>uint8_t</type>
      <name>port</name>
      <anchorfile>structSPI__Address__t.html</anchorfile>
      <anchor>a64ccbaf37a32da0f13b46325b4b42c4d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>pin</name>
      <anchorfile>structSPI__Address__t.html</anchorfile>
      <anchor>aa89aa381abb683131e6d7b3158b25155</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>SSP_ConfigFormat</name>
    <filename>structSSP__ConfigFormat.html</filename>
    <member kind="variable">
      <type>CHIP_SSP_BITS_T</type>
      <name>bits</name>
      <anchorfile>structSSP__ConfigFormat.html</anchorfile>
      <anchor>afaeb54b5e0525adda38644d3ef84dca1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>CHIP_SSP_CLOCK_MODE_T</type>
      <name>clockMode</name>
      <anchorfile>structSSP__ConfigFormat.html</anchorfile>
      <anchor>a910e709c7d9e6e367a26028a939b69cf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>CHIP_SSP_FRAME_FORMAT_T</type>
      <name>frameFormat</name>
      <anchorfile>structSSP__ConfigFormat.html</anchorfile>
      <anchor>ae702ac713d3aa81bffdf140ad8942ec7</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>SYSCTL_PLL_REGS_T</name>
    <filename>structSYSCTL__PLL__REGS__T.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>PLLCON</name>
      <anchorfile>structSYSCTL__PLL__REGS__T.html</anchorfile>
      <anchor>a1fb97445f7a92913133769bb098df316</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>PLLCFG</name>
      <anchorfile>structSYSCTL__PLL__REGS__T.html</anchorfile>
      <anchor>a7f6a92521303bcd5522acdac74af3210</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>PLLSTAT</name>
      <anchorfile>structSYSCTL__PLL__REGS__T.html</anchorfile>
      <anchor>a9cc1486fbc5a9888ccdd70df2b57ebec</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__O uint32_t</type>
      <name>PLLFEED</name>
      <anchorfile>structSYSCTL__PLL__REGS__T.html</anchorfile>
      <anchor>a658116aa66b1d9e27f12cf0e429663f7</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>SysTick_Type</name>
    <filename>structSysTick__Type.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CTRL</name>
      <anchorfile>structSysTick__Type.html</anchorfile>
      <anchor>af2ad94ac83e5d40fc6e34884bc1bec5f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>LOAD</name>
      <anchorfile>structSysTick__Type.html</anchorfile>
      <anchor>ae7bc9d3eac1147f3bba8d73a8395644f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>VAL</name>
      <anchorfile>structSysTick__Type.html</anchorfile>
      <anchor>a0997ff20f11817f8246e8f0edac6f4e4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>CALIB</name>
      <anchorfile>structSysTick__Type.html</anchorfile>
      <anchor>a9c9eda0ea6f6a7c904d2d75a6963e238</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>TPI_Type</name>
    <filename>structTPI__Type.html</filename>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>SSPSR</name>
      <anchorfile>structTPI__Type.html</anchorfile>
      <anchor>a158e9d784f6ee6398f4bdcb2e4ca0912</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CSPSR</name>
      <anchorfile>structTPI__Type.html</anchorfile>
      <anchor>aa723ef3d38237aa2465779b3cc73a94a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>ACPR</name>
      <anchorfile>structTPI__Type.html</anchorfile>
      <anchor>ad75832a669eb121f6fce3c28d36b7fab</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>SPPR</name>
      <anchorfile>structTPI__Type.html</anchorfile>
      <anchor>a3eb655f2e45d7af358775025c1a50c8e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>FFSR</name>
      <anchorfile>structTPI__Type.html</anchorfile>
      <anchor>ae67849b2c1016fe6ef9095827d16cddd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>FFCR</name>
      <anchorfile>structTPI__Type.html</anchorfile>
      <anchor>a3eb42d69922e340037692424a69da880</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>FSCR</name>
      <anchorfile>structTPI__Type.html</anchorfile>
      <anchor>a377b78fe804f327e6f8b3d0f37e7bfef</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>TRIGGER</name>
      <anchorfile>structTPI__Type.html</anchorfile>
      <anchor>aa4b603c71768dbda553da571eccba1fe</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>FIFO0</name>
      <anchorfile>structTPI__Type.html</anchorfile>
      <anchor>ae91ff529e87d8e234343ed31bcdc4f10</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>ITATBCTR2</name>
      <anchorfile>structTPI__Type.html</anchorfile>
      <anchor>a176d991adb4c022bd5b982a9f8fa6a1d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>ITATBCTR0</name>
      <anchorfile>structTPI__Type.html</anchorfile>
      <anchor>a20ca7fad4d4009c242f20a7b4a44b7d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>FIFO1</name>
      <anchorfile>structTPI__Type.html</anchorfile>
      <anchor>aebaa9b8dd27f8017dd4f92ecf32bac8e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>ITCTRL</name>
      <anchorfile>structTPI__Type.html</anchorfile>
      <anchor>ab49c2cb6b5fe082746a444e07548c198</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CLAIMSET</name>
      <anchorfile>structTPI__Type.html</anchorfile>
      <anchor>a2e4d5a07fabd771fa942a171230a0a84</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__IO uint32_t</type>
      <name>CLAIMCLR</name>
      <anchorfile>structTPI__Type.html</anchorfile>
      <anchor>a44efa6045512c8d4da64b0623f7a43ad</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>DEVID</name>
      <anchorfile>structTPI__Type.html</anchorfile>
      <anchor>a4b2e0d680cf7e26728ca8966363a938d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>__I uint32_t</type>
      <name>DEVTYPE</name>
      <anchorfile>structTPI__Type.html</anchorfile>
      <anchor>a16d12c5b1e12f764fa3ec4a51c5f0f35</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>_USB_COMMON_DESCRIPTOR</name>
    <filename>struct__USB__COMMON__DESCRIPTOR.html</filename>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bLength</name>
      <anchorfile>struct__USB__COMMON__DESCRIPTOR.html</anchorfile>
      <anchor>a97b6c23d8471b84abfd0f8a87b009227</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bDescriptorType</name>
      <anchorfile>struct__USB__COMMON__DESCRIPTOR.html</anchorfile>
      <anchor>a6a2665ee27191ce1fb64644349e5a363</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>_USB_CONFIGURATION_DESCRIPTOR</name>
    <filename>struct__USB__CONFIGURATION__DESCRIPTOR.html</filename>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bLength</name>
      <anchorfile>struct__USB__CONFIGURATION__DESCRIPTOR.html</anchorfile>
      <anchor>a2ba304d3cb0f6b506c190ca8391fd1a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bDescriptorType</name>
      <anchorfile>struct__USB__CONFIGURATION__DESCRIPTOR.html</anchorfile>
      <anchor>a28bd3308c28df6445b4b63bdaade83c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>wTotalLength</name>
      <anchorfile>struct__USB__CONFIGURATION__DESCRIPTOR.html</anchorfile>
      <anchor>a39499f3dd4e3f1ef1d8a2c9b430bc11b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bNumInterfaces</name>
      <anchorfile>struct__USB__CONFIGURATION__DESCRIPTOR.html</anchorfile>
      <anchor>abd647927a872ac856c9f5aaf624bb99f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bConfigurationValue</name>
      <anchorfile>struct__USB__CONFIGURATION__DESCRIPTOR.html</anchorfile>
      <anchor>a568d08f2ae98670a707489645a67746b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>iConfiguration</name>
      <anchorfile>struct__USB__CONFIGURATION__DESCRIPTOR.html</anchorfile>
      <anchor>a488103d763db8744459d2c94995458b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bmAttributes</name>
      <anchorfile>struct__USB__CONFIGURATION__DESCRIPTOR.html</anchorfile>
      <anchor>ac2f4c62d572fe371be5680962e42a8d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bMaxPower</name>
      <anchorfile>struct__USB__CONFIGURATION__DESCRIPTOR.html</anchorfile>
      <anchor>a3d6088be2999c1189f2a3eb56be51832</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>USB_CORE_DESCS_T</name>
    <filename>structUSB__CORE__DESCS__T.html</filename>
    <member kind="variable">
      <type>uint8_t *</type>
      <name>device_desc</name>
      <anchorfile>structUSB__CORE__DESCS__T.html</anchorfile>
      <anchor>a3b2f85222b38ee4ff99a99eecdb25be7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t *</type>
      <name>string_desc</name>
      <anchorfile>structUSB__CORE__DESCS__T.html</anchorfile>
      <anchor>a5795b0df1d18c2cd9f2a8c8963a4a85b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t *</type>
      <name>full_speed_desc</name>
      <anchorfile>structUSB__CORE__DESCS__T.html</anchorfile>
      <anchor>a305d78c60798193048450862410a6056</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t *</type>
      <name>high_speed_desc</name>
      <anchorfile>structUSB__CORE__DESCS__T.html</anchorfile>
      <anchor>a6e58a78be9819a9de95cfe2553c5a866</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t *</type>
      <name>device_qualifier</name>
      <anchorfile>structUSB__CORE__DESCS__T.html</anchorfile>
      <anchor>ac832dab0bd32c8abb71654b8506f8489</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>_USB_DEVICE_DESCRIPTOR</name>
    <filename>struct__USB__DEVICE__DESCRIPTOR.html</filename>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bLength</name>
      <anchorfile>struct__USB__DEVICE__DESCRIPTOR.html</anchorfile>
      <anchor>a951dd86a1fee895f356a0be0fc8c0d8f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bDescriptorType</name>
      <anchorfile>struct__USB__DEVICE__DESCRIPTOR.html</anchorfile>
      <anchor>aad465b71adef34630171e88b0a3664a3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>bcdUSB</name>
      <anchorfile>struct__USB__DEVICE__DESCRIPTOR.html</anchorfile>
      <anchor>a621148cd71148fb15e136010480a34ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bDeviceClass</name>
      <anchorfile>struct__USB__DEVICE__DESCRIPTOR.html</anchorfile>
      <anchor>afa7047f0318ab6df92a12291511cc1ce</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bDeviceSubClass</name>
      <anchorfile>struct__USB__DEVICE__DESCRIPTOR.html</anchorfile>
      <anchor>af01535d145805f4bfae66e21f1fc652d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bDeviceProtocol</name>
      <anchorfile>struct__USB__DEVICE__DESCRIPTOR.html</anchorfile>
      <anchor>af205ea8c5729e0ddbf4dcbdf1636fe57</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bMaxPacketSize0</name>
      <anchorfile>struct__USB__DEVICE__DESCRIPTOR.html</anchorfile>
      <anchor>a92d96e9016c96043dccf56872d13a628</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>idVendor</name>
      <anchorfile>struct__USB__DEVICE__DESCRIPTOR.html</anchorfile>
      <anchor>ae3c7088fe6f7b521132d8f2b95f958bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>idProduct</name>
      <anchorfile>struct__USB__DEVICE__DESCRIPTOR.html</anchorfile>
      <anchor>adaec05748124a104c3d49008433719ba</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>bcdDevice</name>
      <anchorfile>struct__USB__DEVICE__DESCRIPTOR.html</anchorfile>
      <anchor>aa4e565c598b6605cbcfe8cab66f99e65</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>iManufacturer</name>
      <anchorfile>struct__USB__DEVICE__DESCRIPTOR.html</anchorfile>
      <anchor>ad16528e2a4018962f8d7d97036386eac</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>iProduct</name>
      <anchorfile>struct__USB__DEVICE__DESCRIPTOR.html</anchorfile>
      <anchor>a63e19361f9fe25a6a2553f7d10569174</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>iSerialNumber</name>
      <anchorfile>struct__USB__DEVICE__DESCRIPTOR.html</anchorfile>
      <anchor>ac9d321b21797ac7a6190ae9a2ffa1e3e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bNumConfigurations</name>
      <anchorfile>struct__USB__DEVICE__DESCRIPTOR.html</anchorfile>
      <anchor>ab1b8db3992e0dceb2ba86f3bab1e5bca</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>_USB_DEVICE_QUALIFIER_DESCRIPTOR</name>
    <filename>struct__USB__DEVICE__QUALIFIER__DESCRIPTOR.html</filename>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bLength</name>
      <anchorfile>struct__USB__DEVICE__QUALIFIER__DESCRIPTOR.html</anchorfile>
      <anchor>a1fe853c915f99976ff910b6f083f99af</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bDescriptorType</name>
      <anchorfile>struct__USB__DEVICE__QUALIFIER__DESCRIPTOR.html</anchorfile>
      <anchor>a894a5931242f7169044c1337d6000820</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>bcdUSB</name>
      <anchorfile>struct__USB__DEVICE__QUALIFIER__DESCRIPTOR.html</anchorfile>
      <anchor>ae2c303a050439c863ff1fecc229c6eb8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bDeviceClass</name>
      <anchorfile>struct__USB__DEVICE__QUALIFIER__DESCRIPTOR.html</anchorfile>
      <anchor>a663279b4a9e5ee5b5a6112ca97d8d9d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bDeviceSubClass</name>
      <anchorfile>struct__USB__DEVICE__QUALIFIER__DESCRIPTOR.html</anchorfile>
      <anchor>a917ada5d945a1d9a5f5ae184e38f5e65</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bDeviceProtocol</name>
      <anchorfile>struct__USB__DEVICE__QUALIFIER__DESCRIPTOR.html</anchorfile>
      <anchor>aa302b9d208e7fb639d86242eede38886</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bMaxPacketSize0</name>
      <anchorfile>struct__USB__DEVICE__QUALIFIER__DESCRIPTOR.html</anchorfile>
      <anchor>a109a2e16a22098760d0d216758b8f79c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bNumConfigurations</name>
      <anchorfile>struct__USB__DEVICE__QUALIFIER__DESCRIPTOR.html</anchorfile>
      <anchor>ac0b3a27d7c0eb9702ffe8d1500378468</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bReserved</name>
      <anchorfile>struct__USB__DEVICE__QUALIFIER__DESCRIPTOR.html</anchorfile>
      <anchor>ae6d3fe18edd3c254a449daa816eaa74c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>_USB_DFU_FUNC_DESCRIPTOR</name>
    <filename>struct__USB__DFU__FUNC__DESCRIPTOR.html</filename>
  </compound>
  <compound kind="struct">
    <name>_USB_ENDPOINT_DESCRIPTOR</name>
    <filename>struct__USB__ENDPOINT__DESCRIPTOR.html</filename>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bLength</name>
      <anchorfile>struct__USB__ENDPOINT__DESCRIPTOR.html</anchorfile>
      <anchor>a55263a9cc9dc17abc37226ac8a471ad0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bDescriptorType</name>
      <anchorfile>struct__USB__ENDPOINT__DESCRIPTOR.html</anchorfile>
      <anchor>af39d036ab903a96b9d4d56b879555d6c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bEndpointAddress</name>
      <anchorfile>struct__USB__ENDPOINT__DESCRIPTOR.html</anchorfile>
      <anchor>a23fbb539d72d10a6cb12efedf7d3457c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bmAttributes</name>
      <anchorfile>struct__USB__ENDPOINT__DESCRIPTOR.html</anchorfile>
      <anchor>a8ee7f1bafac335258c6e742969387f57</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>wMaxPacketSize</name>
      <anchorfile>struct__USB__ENDPOINT__DESCRIPTOR.html</anchorfile>
      <anchor>af34105fa6bd0029d9c6b73d85f7853c6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bInterval</name>
      <anchorfile>struct__USB__ENDPOINT__DESCRIPTOR.html</anchorfile>
      <anchor>a95cd9669dee9cab375a7c5e62284a01b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>USB_HID_REPORT_T</name>
    <filename>structUSB__HID__REPORT__T.html</filename>
    <member kind="variable">
      <type>uint16_t</type>
      <name>len</name>
      <anchorfile>structUSB__HID__REPORT__T.html</anchorfile>
      <anchor>a94940b840d230071272a05099cea7b4d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>idle_time</name>
      <anchorfile>structUSB__HID__REPORT__T.html</anchorfile>
      <anchor>af6cea0cf62691e6d36cfc165cc636b1b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>__pad</name>
      <anchorfile>structUSB__HID__REPORT__T.html</anchorfile>
      <anchor>aed7a1eea84ca976184a13914dd4522d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t *</type>
      <name>desc</name>
      <anchorfile>structUSB__HID__REPORT__T.html</anchorfile>
      <anchor>a7ef482e285072e7abaaa875cde201f5a</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>_USB_IAD_DESCRIPTOR</name>
    <filename>struct__USB__IAD__DESCRIPTOR.html</filename>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bLength</name>
      <anchorfile>struct__USB__IAD__DESCRIPTOR.html</anchorfile>
      <anchor>ae2da032c28db4aa5f42d750fdb8c3b31</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bDescriptorType</name>
      <anchorfile>struct__USB__IAD__DESCRIPTOR.html</anchorfile>
      <anchor>a7acc23ddf37a6126d5cd0eb608083179</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bFirstInterface</name>
      <anchorfile>struct__USB__IAD__DESCRIPTOR.html</anchorfile>
      <anchor>a279136ee5e09ed6b246c0fca10cf0766</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bInterfaceCount</name>
      <anchorfile>struct__USB__IAD__DESCRIPTOR.html</anchorfile>
      <anchor>a4b38c1b555b0d89a72e6bdfebda7235e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bFunctionClass</name>
      <anchorfile>struct__USB__IAD__DESCRIPTOR.html</anchorfile>
      <anchor>abef9f2bc863b2d7266f5fe27702220a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bFunctionSubClass</name>
      <anchorfile>struct__USB__IAD__DESCRIPTOR.html</anchorfile>
      <anchor>ae45c43afce1fd5595a5aa3231d25e77e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bFunctionProtocol</name>
      <anchorfile>struct__USB__IAD__DESCRIPTOR.html</anchorfile>
      <anchor>a9c2098e37e8252fbf3e5df9dde9893e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>iFunction</name>
      <anchorfile>struct__USB__IAD__DESCRIPTOR.html</anchorfile>
      <anchor>ac9e9783900b10cd912f297bc042bdcbe</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>_USB_INTERFACE_DESCRIPTOR</name>
    <filename>struct__USB__INTERFACE__DESCRIPTOR.html</filename>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bLength</name>
      <anchorfile>struct__USB__INTERFACE__DESCRIPTOR.html</anchorfile>
      <anchor>ad1a0f98931b5e094d33c8c31d4e29ad6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bDescriptorType</name>
      <anchorfile>struct__USB__INTERFACE__DESCRIPTOR.html</anchorfile>
      <anchor>a2515bf5c49f849965030c94f50dea285</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bInterfaceNumber</name>
      <anchorfile>struct__USB__INTERFACE__DESCRIPTOR.html</anchorfile>
      <anchor>a3ae5da44842f99bbde5cca9b20c6911a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bAlternateSetting</name>
      <anchorfile>struct__USB__INTERFACE__DESCRIPTOR.html</anchorfile>
      <anchor>a0ddcf6c9bc46ed757e9e15086746b5a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bNumEndpoints</name>
      <anchorfile>struct__USB__INTERFACE__DESCRIPTOR.html</anchorfile>
      <anchor>a3268bc05bb0147e19f97e50e702fc141</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bInterfaceClass</name>
      <anchorfile>struct__USB__INTERFACE__DESCRIPTOR.html</anchorfile>
      <anchor>a4d352604355f617fef9509951d573356</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bInterfaceSubClass</name>
      <anchorfile>struct__USB__INTERFACE__DESCRIPTOR.html</anchorfile>
      <anchor>ad5b51ed2e6540dd58085f10a2440970e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bInterfaceProtocol</name>
      <anchorfile>struct__USB__INTERFACE__DESCRIPTOR.html</anchorfile>
      <anchor>afcce1d75b7b110cc0d11e3328d9c7204</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>iInterface</name>
      <anchorfile>struct__USB__INTERFACE__DESCRIPTOR.html</anchorfile>
      <anchor>a391ad3184a29ddbe1e02ff1feca8f64a</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>_USB_OTHER_SPEED_CONFIGURATION</name>
    <filename>struct__USB__OTHER__SPEED__CONFIGURATION.html</filename>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bLength</name>
      <anchorfile>struct__USB__OTHER__SPEED__CONFIGURATION.html</anchorfile>
      <anchor>a3e745290193597adaccfb2e9b1e5f6b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bDescriptorType</name>
      <anchorfile>struct__USB__OTHER__SPEED__CONFIGURATION.html</anchorfile>
      <anchor>a734018aa46b76f2210986d969664c878</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>wTotalLength</name>
      <anchorfile>struct__USB__OTHER__SPEED__CONFIGURATION.html</anchorfile>
      <anchor>a3d459fe17370260a6e2bf39dedb1a460</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bNumInterfaces</name>
      <anchorfile>struct__USB__OTHER__SPEED__CONFIGURATION.html</anchorfile>
      <anchor>a94d4be76355c7e95b4c9095a3ff07f18</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bConfigurationValue</name>
      <anchorfile>struct__USB__OTHER__SPEED__CONFIGURATION.html</anchorfile>
      <anchor>a9dd32da9d50bd23ebe1e6299c8416237</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>IConfiguration</name>
      <anchorfile>struct__USB__OTHER__SPEED__CONFIGURATION.html</anchorfile>
      <anchor>adc1344cdefc4201dfd9d7cda176faa96</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bmAttributes</name>
      <anchorfile>struct__USB__OTHER__SPEED__CONFIGURATION.html</anchorfile>
      <anchor>a2a38cea66cb9060424555263661c367d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bMaxPower</name>
      <anchorfile>struct__USB__OTHER__SPEED__CONFIGURATION.html</anchorfile>
      <anchor>a0ab6390abc0a97ad69c7accc8400eaa9</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>_USB_SETUP_PACKET</name>
    <filename>struct__USB__SETUP__PACKET.html</filename>
    <member kind="variable">
      <type>REQUEST_TYPE</type>
      <name>bmRequestType</name>
      <anchorfile>struct__USB__SETUP__PACKET.html</anchorfile>
      <anchor>a29d5d0163a7e2b32cdd0dd727f7f1d63</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bRequest</name>
      <anchorfile>struct__USB__SETUP__PACKET.html</anchorfile>
      <anchor>ae4d0935c2a5a6440b8135c4e96e834fb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>WORD_BYTE</type>
      <name>wValue</name>
      <anchorfile>struct__USB__SETUP__PACKET.html</anchorfile>
      <anchor>a45d5a04ff4a9a86ad485cd36a9769754</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>WORD_BYTE</type>
      <name>wIndex</name>
      <anchorfile>struct__USB__SETUP__PACKET.html</anchorfile>
      <anchor>a7b950e5d9f1391a334572aad824c8f77</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>wLength</name>
      <anchorfile>struct__USB__SETUP__PACKET.html</anchorfile>
      <anchor>afd38819037e7f1bc3de004f7e554cf32</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>_USB_STRING_DESCRIPTOR</name>
    <filename>struct__USB__STRING__DESCRIPTOR.html</filename>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bLength</name>
      <anchorfile>struct__USB__STRING__DESCRIPTOR.html</anchorfile>
      <anchor>a951a99e733e6cf21012d6e45d201b4d8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>bDescriptorType</name>
      <anchorfile>struct__USB__STRING__DESCRIPTOR.html</anchorfile>
      <anchor>a5feb4b82d9014780d6580f974ddaa28e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>bString</name>
      <anchorfile>struct__USB__STRING__DESCRIPTOR.html</anchorfile>
      <anchor>a4fcbbb2a93dea10d68984335796472a5</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>USBD_API_INIT_PARAM_T</name>
    <filename>structUSBD__API__INIT__PARAM__T.html</filename>
    <member kind="variable">
      <type>uint32_t</type>
      <name>usb_reg_base</name>
      <anchorfile>structUSBD__API__INIT__PARAM__T.html</anchorfile>
      <anchor>a2200b18c707f4547fe8dbe465d5a643a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>mem_base</name>
      <anchorfile>structUSBD__API__INIT__PARAM__T.html</anchorfile>
      <anchor>a8739f30e4b9914535d92ca2ce89aa7d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>mem_size</name>
      <anchorfile>structUSBD__API__INIT__PARAM__T.html</anchorfile>
      <anchor>a746219ea1a82b356ec966df059824f72</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>max_num_ep</name>
      <anchorfile>structUSBD__API__INIT__PARAM__T.html</anchorfile>
      <anchor>af9eae0114be0454c4cce831715faf585</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>USB_CB_T</type>
      <name>USB_Reset_Event</name>
      <anchorfile>structUSBD__API__INIT__PARAM__T.html</anchorfile>
      <anchor>a8e660f6bf25cdeefe2cec011fabe212c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>USB_CB_T</type>
      <name>USB_Suspend_Event</name>
      <anchorfile>structUSBD__API__INIT__PARAM__T.html</anchorfile>
      <anchor>aaf95eb8e3001c6da1ef8d72b26cdc1aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>USB_CB_T</type>
      <name>USB_Resume_Event</name>
      <anchorfile>structUSBD__API__INIT__PARAM__T.html</anchorfile>
      <anchor>ab58c48d9bfad77e56ef9ca73b7fc7e82</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>USB_CB_T</type>
      <name>reserved_sbz</name>
      <anchorfile>structUSBD__API__INIT__PARAM__T.html</anchorfile>
      <anchor>a17205439f8d1d4b4277e27a1f0e962ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>USB_CB_T</type>
      <name>USB_SOF_Event</name>
      <anchorfile>structUSBD__API__INIT__PARAM__T.html</anchorfile>
      <anchor>af2042bc4188d6ff6e1e56f419d6c6cae</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>USB_PARAM_CB_T</type>
      <name>USB_WakeUpCfg</name>
      <anchorfile>structUSBD__API__INIT__PARAM__T.html</anchorfile>
      <anchor>a741fd1c3d79894a4191c0155a9bd3022</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>USB_PARAM_CB_T</type>
      <name>USB_Power_Event</name>
      <anchorfile>structUSBD__API__INIT__PARAM__T.html</anchorfile>
      <anchor>aeccf0e29292aea05e398ff2d7f4c8990</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>USB_PARAM_CB_T</type>
      <name>USB_Error_Event</name>
      <anchorfile>structUSBD__API__INIT__PARAM__T.html</anchorfile>
      <anchor>a127b313434a74ef5ad6994d72c40e92e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>USB_CB_T</type>
      <name>USB_Configure_Event</name>
      <anchorfile>structUSBD__API__INIT__PARAM__T.html</anchorfile>
      <anchor>aa99c9b6ada00d0246878a2a392ab76f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>USB_CB_T</type>
      <name>USB_Interface_Event</name>
      <anchorfile>structUSBD__API__INIT__PARAM__T.html</anchorfile>
      <anchor>adf5dbf583a9517bb8bf109f7483bc894</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>USB_CB_T</type>
      <name>USB_Feature_Event</name>
      <anchorfile>structUSBD__API__INIT__PARAM__T.html</anchorfile>
      <anchor>a52e28e7ebed0bc27189cfbec8c4c64f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t(*</type>
      <name>virt_to_phys</name>
      <anchorfile>structUSBD__API__INIT__PARAM__T.html</anchorfile>
      <anchor>ada5bf5dcf8e164527e9b91656235dff4</anchor>
      <arglist>)(void *vaddr)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>cache_flush</name>
      <anchorfile>structUSBD__API__INIT__PARAM__T.html</anchorfile>
      <anchor>a663728635276a9fe35b2bb5e48cfdec2</anchor>
      <arglist>)(uint32_t *start_adr, uint32_t *end_adr)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>USBD_API_T</name>
    <filename>structUSBD__API__T.html</filename>
    <member kind="variable">
      <type>const USBD_HW_API_T *</type>
      <name>hw</name>
      <anchorfile>structUSBD__API__T.html</anchorfile>
      <anchor>a74090dd5dc824568b410d821de1ae02d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const USBD_CORE_API_T *</type>
      <name>core</name>
      <anchorfile>structUSBD__API__T.html</anchorfile>
      <anchor>a3329fde3dae844a3862155fa644d48d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const USBD_MSC_API_T *</type>
      <name>msc</name>
      <anchorfile>structUSBD__API__T.html</anchorfile>
      <anchor>a31566318258366b055ce64b9ce3d4c2a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const USBD_DFU_API_T *</type>
      <name>dfu</name>
      <anchorfile>structUSBD__API__T.html</anchorfile>
      <anchor>a58929dff8ab24c2522bc06cddaf8e9ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const USBD_HID_API_T *</type>
      <name>hid</name>
      <anchorfile>structUSBD__API__T.html</anchorfile>
      <anchor>a0548bd88d44460062433a3950e68ad06</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const USBD_CDC_API_T *</type>
      <name>cdc</name>
      <anchorfile>structUSBD__API__T.html</anchorfile>
      <anchor>aa70bb0209684585214fc2299d42c1da4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const uint32_t *</type>
      <name>reserved6</name>
      <anchorfile>structUSBD__API__T.html</anchorfile>
      <anchor>a7dbaab42998991c1dd69172a002e288d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const uint32_t</type>
      <name>version</name>
      <anchorfile>structUSBD__API__T.html</anchorfile>
      <anchor>ae88d4678d9f2958613e0b179590af851</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>USBD_CDC_API_T</name>
    <filename>structUSBD__CDC__API__T.html</filename>
    <member kind="variable">
      <type>uint32_t(*</type>
      <name>GetMemSize</name>
      <anchorfile>structUSBD__CDC__API__T.html</anchorfile>
      <anchor>a12bbadd1b283b48ce09819fd07f0ae73</anchor>
      <arglist>)(USBD_CDC_INIT_PARAM_T *param)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>init</name>
      <anchorfile>structUSBD__CDC__API__T.html</anchorfile>
      <anchor>a226378819e5b85e2b5d81ba8fd1a7be2</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, USBD_CDC_INIT_PARAM_T *param, USBD_HANDLE_T *phCDC)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>SendNotification</name>
      <anchorfile>structUSBD__CDC__API__T.html</anchorfile>
      <anchor>a946674150d65c2d17afe885fd6bd3c49</anchor>
      <arglist>)(USBD_HANDLE_T hCdc, uint8_t bNotification, uint16_t data)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>USBD_CDC_INIT_PARAM_T</name>
    <filename>structUSBD__CDC__INIT__PARAM__T.html</filename>
    <member kind="variable">
      <type>uint32_t</type>
      <name>mem_base</name>
      <anchorfile>structUSBD__CDC__INIT__PARAM__T.html</anchorfile>
      <anchor>ab48009ff0ec318d3f55f5f5637e0b9aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>mem_size</name>
      <anchorfile>structUSBD__CDC__INIT__PARAM__T.html</anchorfile>
      <anchor>a04e2cd9eb2eabd43c55340afdcebc3b9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t *</type>
      <name>cif_intf_desc</name>
      <anchorfile>structUSBD__CDC__INIT__PARAM__T.html</anchorfile>
      <anchor>ae60b2f8eda3000078ed624e163d54ea8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t *</type>
      <name>dif_intf_desc</name>
      <anchorfile>structUSBD__CDC__INIT__PARAM__T.html</anchorfile>
      <anchor>a3a842050479f7edd5b611dc6845cd9b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>CIC_GetRequest</name>
      <anchorfile>structUSBD__CDC__INIT__PARAM__T.html</anchorfile>
      <anchor>a3274f3238675f9fcffcd34bd5e7fed56</anchor>
      <arglist>)(USBD_HANDLE_T hHid, USB_SETUP_PACKET *pSetup, uint8_t **pBuffer, uint16_t *length)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>CIC_SetRequest</name>
      <anchorfile>structUSBD__CDC__INIT__PARAM__T.html</anchorfile>
      <anchor>af086ddf433905fea3f04d54c579d6fb2</anchor>
      <arglist>)(USBD_HANDLE_T hCdc, USB_SETUP_PACKET *pSetup, uint8_t **pBuffer, uint16_t length)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>CDC_BulkIN_Hdlr</name>
      <anchorfile>structUSBD__CDC__INIT__PARAM__T.html</anchorfile>
      <anchor>a8f3d4381ba0dea3cb65222292402baf7</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, void *data, uint32_t event)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>CDC_BulkOUT_Hdlr</name>
      <anchorfile>structUSBD__CDC__INIT__PARAM__T.html</anchorfile>
      <anchor>a74b50926fb2d8b461b264b35ab72edf2</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, void *data, uint32_t event)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>SendEncpsCmd</name>
      <anchorfile>structUSBD__CDC__INIT__PARAM__T.html</anchorfile>
      <anchor>ada42fea0632a8f3c700e0abefc8c1bdc</anchor>
      <arglist>)(USBD_HANDLE_T hCDC, uint8_t *buffer, uint16_t len)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>GetEncpsResp</name>
      <anchorfile>structUSBD__CDC__INIT__PARAM__T.html</anchorfile>
      <anchor>abcb997ba4da901d1ad95f2a884a38fde</anchor>
      <arglist>)(USBD_HANDLE_T hCDC, uint8_t **buffer, uint16_t *len)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>SetCommFeature</name>
      <anchorfile>structUSBD__CDC__INIT__PARAM__T.html</anchorfile>
      <anchor>aa2155bb779f23ada4f8dee64c6d0ad23</anchor>
      <arglist>)(USBD_HANDLE_T hCDC, uint16_t feature, uint8_t *buffer, uint16_t len)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>GetCommFeature</name>
      <anchorfile>structUSBD__CDC__INIT__PARAM__T.html</anchorfile>
      <anchor>a4e4f9ea204fbf0cf5f372fe09e5cc82d</anchor>
      <arglist>)(USBD_HANDLE_T hCDC, uint16_t feature, uint8_t **pBuffer, uint16_t *len)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>ClrCommFeature</name>
      <anchorfile>structUSBD__CDC__INIT__PARAM__T.html</anchorfile>
      <anchor>addd9387cc563f2172e4d4fa640f037e5</anchor>
      <arglist>)(USBD_HANDLE_T hCDC, uint16_t feature)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>SetCtrlLineState</name>
      <anchorfile>structUSBD__CDC__INIT__PARAM__T.html</anchorfile>
      <anchor>ae5a9f6351313899e95e7b657ec4d1c92</anchor>
      <arglist>)(USBD_HANDLE_T hCDC, uint16_t state)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>SendBreak</name>
      <anchorfile>structUSBD__CDC__INIT__PARAM__T.html</anchorfile>
      <anchor>ae0ba966363701bae70aecf2b8265e1f0</anchor>
      <arglist>)(USBD_HANDLE_T hCDC, uint16_t mstime)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>SetLineCode</name>
      <anchorfile>structUSBD__CDC__INIT__PARAM__T.html</anchorfile>
      <anchor>a4eccdbee017aa1b7c7a9b24fcabc9439</anchor>
      <arglist>)(USBD_HANDLE_T hCDC, CDC_LINE_CODING *line_coding)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>CDC_InterruptEP_Hdlr</name>
      <anchorfile>structUSBD__CDC__INIT__PARAM__T.html</anchorfile>
      <anchor>a32c9b649ba44af3a64e5abb1525eea1b</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, void *data, uint32_t event)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>CDC_Ep0_Hdlr</name>
      <anchorfile>structUSBD__CDC__INIT__PARAM__T.html</anchorfile>
      <anchor>a2991ba9338093ca552d64f25251f751d</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, void *data, uint32_t event)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>USBD_CORE_API_T</name>
    <filename>structUSBD__CORE__API__T.html</filename>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>RegisterClassHandler</name>
      <anchorfile>structUSBD__CORE__API__T.html</anchorfile>
      <anchor>a651891ae3d6612f6f566384b3d681e36</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, USB_EP_HANDLER_T pfn, void *data)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>RegisterEpHandler</name>
      <anchorfile>structUSBD__CORE__API__T.html</anchorfile>
      <anchor>a305d05fbc2d0c748b87edcd986bfb767</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, uint32_t ep_index, USB_EP_HANDLER_T pfn, void *data)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>SetupStage</name>
      <anchorfile>structUSBD__CORE__API__T.html</anchorfile>
      <anchor>a373376ee1855114095288c907687b421</anchor>
      <arglist>)(USBD_HANDLE_T hUsb)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>DataInStage</name>
      <anchorfile>structUSBD__CORE__API__T.html</anchorfile>
      <anchor>ab1e05c41e0070d3e706ae98e048e4384</anchor>
      <arglist>)(USBD_HANDLE_T hUsb)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>DataOutStage</name>
      <anchorfile>structUSBD__CORE__API__T.html</anchorfile>
      <anchor>aff6e7a43f616cdcbf9e193da25df684a</anchor>
      <arglist>)(USBD_HANDLE_T hUsb)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>StatusInStage</name>
      <anchorfile>structUSBD__CORE__API__T.html</anchorfile>
      <anchor>a714fc392b856668bf6422cfb1ef1d74c</anchor>
      <arglist>)(USBD_HANDLE_T hUsb)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>StatusOutStage</name>
      <anchorfile>structUSBD__CORE__API__T.html</anchorfile>
      <anchor>a3c49c88a0f39ad432b7ddd03ae449403</anchor>
      <arglist>)(USBD_HANDLE_T hUsb)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>StallEp0</name>
      <anchorfile>structUSBD__CORE__API__T.html</anchorfile>
      <anchor>adbfe3149090b63dd0334634f5f08b002</anchor>
      <arglist>)(USBD_HANDLE_T hUsb)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>USBD_DFU_API_T</name>
    <filename>structUSBD__DFU__API__T.html</filename>
    <member kind="variable">
      <type>uint32_t(*</type>
      <name>GetMemSize</name>
      <anchorfile>structUSBD__DFU__API__T.html</anchorfile>
      <anchor>a0c6e1f4b40bb5ffa70f4256d9e8277c1</anchor>
      <arglist>)(USBD_DFU_INIT_PARAM_T *param)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>init</name>
      <anchorfile>structUSBD__DFU__API__T.html</anchorfile>
      <anchor>a51de17420c2e5a682a66d9984e925fce</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, USBD_DFU_INIT_PARAM_T *param, uint32_t init_state)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>USBD_DFU_INIT_PARAM_T</name>
    <filename>structUSBD__DFU__INIT__PARAM__T.html</filename>
    <member kind="variable">
      <type>uint32_t</type>
      <name>mem_base</name>
      <anchorfile>structUSBD__DFU__INIT__PARAM__T.html</anchorfile>
      <anchor>a492c4cb17874d0c51ed7c7c0d01d05c7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>mem_size</name>
      <anchorfile>structUSBD__DFU__INIT__PARAM__T.html</anchorfile>
      <anchor>ad80d6de873114d4cf494c7e542d37cd6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>wTransferSize</name>
      <anchorfile>structUSBD__DFU__INIT__PARAM__T.html</anchorfile>
      <anchor>a13622f03c7523aa22d65e52d1606757b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t *</type>
      <name>intf_desc</name>
      <anchorfile>structUSBD__DFU__INIT__PARAM__T.html</anchorfile>
      <anchor>a2e5f31b4c872dad5f481796b42755330</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t(*</type>
      <name>DFU_Write</name>
      <anchorfile>structUSBD__DFU__INIT__PARAM__T.html</anchorfile>
      <anchor>ac9f06c80d4abbe134ecd8993806f0910</anchor>
      <arglist>)(uint32_t block_num, uint8_t **src, uint32_t length, uint8_t *bwPollTimeout)</arglist>
    </member>
    <member kind="variable">
      <type>uint32_t(*</type>
      <name>DFU_Read</name>
      <anchorfile>structUSBD__DFU__INIT__PARAM__T.html</anchorfile>
      <anchor>a09999334b7d2cebd3336178178da8fa7</anchor>
      <arglist>)(uint32_t block_num, uint8_t **dst, uint32_t length)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>DFU_Done</name>
      <anchorfile>structUSBD__DFU__INIT__PARAM__T.html</anchorfile>
      <anchor>acf2c82a59477fb69bd902d778e325db7</anchor>
      <arglist>)(void)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>DFU_Detach</name>
      <anchorfile>structUSBD__DFU__INIT__PARAM__T.html</anchorfile>
      <anchor>ae9454762b22860fe980058085f992228</anchor>
      <arglist>)(USBD_HANDLE_T hUsb)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>DFU_Ep0_Hdlr</name>
      <anchorfile>structUSBD__DFU__INIT__PARAM__T.html</anchorfile>
      <anchor>a58d8ce3f3dd24933c4b32f0482e83af5</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, void *data, uint32_t event)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>USBD_HID_API_T</name>
    <filename>structUSBD__HID__API__T.html</filename>
    <member kind="variable">
      <type>uint32_t(*</type>
      <name>GetMemSize</name>
      <anchorfile>structUSBD__HID__API__T.html</anchorfile>
      <anchor>a876a04f27441acaff5c04b2e4ec4f4e0</anchor>
      <arglist>)(USBD_HID_INIT_PARAM_T *param)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>init</name>
      <anchorfile>structUSBD__HID__API__T.html</anchorfile>
      <anchor>a594d3014a9e4a33dfe23e53b6c383a2c</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, USBD_HID_INIT_PARAM_T *param)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>USBD_HID_INIT_PARAM_T</name>
    <filename>structUSBD__HID__INIT__PARAM__T.html</filename>
    <member kind="variable">
      <type>uint32_t</type>
      <name>mem_base</name>
      <anchorfile>structUSBD__HID__INIT__PARAM__T.html</anchorfile>
      <anchor>a72d038d9f30b525482c7b7c82b68ac00</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>mem_size</name>
      <anchorfile>structUSBD__HID__INIT__PARAM__T.html</anchorfile>
      <anchor>a1a9e0c9e4db38404329009120202c8b7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>max_reports</name>
      <anchorfile>structUSBD__HID__INIT__PARAM__T.html</anchorfile>
      <anchor>ad4e75a05cb02d51e3684ce1fd9217ad7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t *</type>
      <name>intf_desc</name>
      <anchorfile>structUSBD__HID__INIT__PARAM__T.html</anchorfile>
      <anchor>a0ed94e0f954d4adcc82172a2cda097bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>USB_HID_REPORT_T *</type>
      <name>report_data</name>
      <anchorfile>structUSBD__HID__INIT__PARAM__T.html</anchorfile>
      <anchor>a73ba1e535bc64be3d555564944323ea2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>HID_GetReport</name>
      <anchorfile>structUSBD__HID__INIT__PARAM__T.html</anchorfile>
      <anchor>a8b8b4e95325693135bfbbb25c586835a</anchor>
      <arglist>)(USBD_HANDLE_T hHid, USB_SETUP_PACKET *pSetup, uint8_t **pBuffer, uint16_t *length)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>HID_SetReport</name>
      <anchorfile>structUSBD__HID__INIT__PARAM__T.html</anchorfile>
      <anchor>a482ad53a1672d22e1fbcb46db9841035</anchor>
      <arglist>)(USBD_HANDLE_T hHid, USB_SETUP_PACKET *pSetup, uint8_t **pBuffer, uint16_t length)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>HID_GetPhysDesc</name>
      <anchorfile>structUSBD__HID__INIT__PARAM__T.html</anchorfile>
      <anchor>a03bf939278987775343694fbdc0330b5</anchor>
      <arglist>)(USBD_HANDLE_T hHid, USB_SETUP_PACKET *pSetup, uint8_t **pBuf, uint16_t *length)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>HID_SetIdle</name>
      <anchorfile>structUSBD__HID__INIT__PARAM__T.html</anchorfile>
      <anchor>a41c26b7da74dd21f5032b95e1a0ba294</anchor>
      <arglist>)(USBD_HANDLE_T hHid, USB_SETUP_PACKET *pSetup, uint8_t idleTime)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>HID_SetProtocol</name>
      <anchorfile>structUSBD__HID__INIT__PARAM__T.html</anchorfile>
      <anchor>a3328698fbf767f49d0948a002e0cd3be</anchor>
      <arglist>)(USBD_HANDLE_T hHid, USB_SETUP_PACKET *pSetup, uint8_t protocol)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>HID_EpIn_Hdlr</name>
      <anchorfile>structUSBD__HID__INIT__PARAM__T.html</anchorfile>
      <anchor>a3457762a27a837bec9383263596f912a</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, void *data, uint32_t event)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>HID_EpOut_Hdlr</name>
      <anchorfile>structUSBD__HID__INIT__PARAM__T.html</anchorfile>
      <anchor>a78490085cbbf9a3608b6e1e2b9bd24ad</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, void *data, uint32_t event)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>HID_GetReportDesc</name>
      <anchorfile>structUSBD__HID__INIT__PARAM__T.html</anchorfile>
      <anchor>a322548a116679eee194a812826fbf22d</anchor>
      <arglist>)(USBD_HANDLE_T hHid, USB_SETUP_PACKET *pSetup, uint8_t **pBuf, uint16_t *length)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>HID_Ep0_Hdlr</name>
      <anchorfile>structUSBD__HID__INIT__PARAM__T.html</anchorfile>
      <anchor>ab16c430a5a744cb5220a41e1c61ab775</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, void *data, uint32_t event)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>USBD_HW_API_T</name>
    <filename>structUSBD__HW__API__T.html</filename>
    <member kind="variable">
      <type>uint32_t(*</type>
      <name>GetMemSize</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>a0139f290b9605de953245f12b5a0ff26</anchor>
      <arglist>)(USBD_API_INIT_PARAM_T *param)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>Init</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>adf7b0f0dc37399c73b3cb03224a6715c</anchor>
      <arglist>)(USBD_HANDLE_T *phUsb, USB_CORE_DESCS_T *pDesc, USBD_API_INIT_PARAM_T *param)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>Connect</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>a94f866b8fdcb6c08b622badfa5703545</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, uint32_t con)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>ISR</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>a00d85b9c68f1ec92c8092dafe524fe2b</anchor>
      <arglist>)(USBD_HANDLE_T hUsb)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>Reset</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>aa8865aa13798d88076d1f60e4aa05ffd</anchor>
      <arglist>)(USBD_HANDLE_T hUsb)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>ForceFullSpeed</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>ac4fda1a2255a941af2defbaf43ec6fc2</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, uint32_t cfg)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>WakeUpCfg</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>a303ebdddeba4fbe77a9bf82d10829d06</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, uint32_t cfg)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>SetAddress</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>aac740eff46c33a2e55505d38b78bbc15</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, uint32_t adr)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>Configure</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>a14ff45a427052dd46f20f01e00ec759c</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, uint32_t cfg)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>ConfigEP</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>a05143cf85edf626e786ae3a24be105e2</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, USB_ENDPOINT_DESCRIPTOR *pEPD)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>DirCtrlEP</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>ab95b4205de9657a5f5bba6aafd5c9b1b</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, uint32_t dir)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>EnableEP</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>a2ed5cd50bbf5612c2ca6515482119f14</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, uint32_t EPNum)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>DisableEP</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>a1f346a7f9024f17e4def61e15b5db1b6</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, uint32_t EPNum)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>ResetEP</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>aaf39abc4c1d2bbf0de4800fcaee74f7e</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, uint32_t EPNum)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>SetStallEP</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>a120a026986cd54f2ebac307bfe7298ed</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, uint32_t EPNum)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>ClrStallEP</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>a8f1c9298a6c41d175a26e6c7cfa644a8</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, uint32_t EPNum)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>SetTestMode</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>ad29373feb1c7b6a6be1eb82884a48478</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, uint8_t mode)</arglist>
    </member>
    <member kind="variable">
      <type>uint32_t(*</type>
      <name>ReadEP</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>a563550e0a99ee45b29c4aaa7a1f57e1d</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, uint32_t EPNum, uint8_t *pData)</arglist>
    </member>
    <member kind="variable">
      <type>uint32_t(*</type>
      <name>ReadReqEP</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>a9430c42695d6d31f626dc72d69fb69ad</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, uint32_t EPNum, uint8_t *pData, uint32_t len)</arglist>
    </member>
    <member kind="variable">
      <type>uint32_t(*</type>
      <name>ReadSetupPkt</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>a4cdd89766fb47d106014234d6fc96092</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, uint32_t EPNum, uint32_t *pData)</arglist>
    </member>
    <member kind="variable">
      <type>uint32_t(*</type>
      <name>WriteEP</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>aac0f044c14779dca4425403e2a50991c</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, uint32_t EPNum, uint8_t *pData, uint32_t cnt)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>WakeUp</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>aa4e8e27a0733b22a2ac797a462bd52be</anchor>
      <arglist>)(USBD_HANDLE_T hUsb)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>EnableEvent</name>
      <anchorfile>structUSBD__HW__API__T.html</anchorfile>
      <anchor>ac8ba5c7f74d1861430a067375b4af885</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, uint32_t EPNum, uint32_t event_type, uint32_t enable)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>USBD_MSC_API_T</name>
    <filename>structUSBD__MSC__API__T.html</filename>
    <member kind="variable">
      <type>uint32_t(*</type>
      <name>GetMemSize</name>
      <anchorfile>structUSBD__MSC__API__T.html</anchorfile>
      <anchor>a009217842ce3a3faaae6e62d3ca80ec1</anchor>
      <arglist>)(USBD_MSC_INIT_PARAM_T *param)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>init</name>
      <anchorfile>structUSBD__MSC__API__T.html</anchorfile>
      <anchor>a3d421532e7d54500f285f8c2e0813e7e</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, USBD_MSC_INIT_PARAM_T *param)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>USBD_MSC_INIT_PARAM_T</name>
    <filename>structUSBD__MSC__INIT__PARAM__T.html</filename>
    <member kind="variable">
      <type>uint32_t</type>
      <name>mem_base</name>
      <anchorfile>structUSBD__MSC__INIT__PARAM__T.html</anchorfile>
      <anchor>a902b52aa7d1dd89f422f5f978843716a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>mem_size</name>
      <anchorfile>structUSBD__MSC__INIT__PARAM__T.html</anchorfile>
      <anchor>ae7060f25d07d31d86e0fed9e5445111d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t *</type>
      <name>InquiryStr</name>
      <anchorfile>structUSBD__MSC__INIT__PARAM__T.html</anchorfile>
      <anchor>af9782ea548038354c4186fc40468bef7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>BlockCount</name>
      <anchorfile>structUSBD__MSC__INIT__PARAM__T.html</anchorfile>
      <anchor>aaa75be184d1f12f0263b88753785fdf9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>BlockSize</name>
      <anchorfile>structUSBD__MSC__INIT__PARAM__T.html</anchorfile>
      <anchor>a244585ff2624c53e0b33af595ea96891</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>MemorySize</name>
      <anchorfile>structUSBD__MSC__INIT__PARAM__T.html</anchorfile>
      <anchor>a922243906b903d11f384d78619a95d4d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t *</type>
      <name>intf_desc</name>
      <anchorfile>structUSBD__MSC__INIT__PARAM__T.html</anchorfile>
      <anchor>af004840b822fbae7e101d796015c2f12</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>MSC_Write</name>
      <anchorfile>structUSBD__MSC__INIT__PARAM__T.html</anchorfile>
      <anchor>ab6c19e690ec06a1ca80769c831f52318</anchor>
      <arglist>)(uint32_t offset, uint8_t **src, uint32_t length, uint32_t high_offset)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>MSC_Read</name>
      <anchorfile>structUSBD__MSC__INIT__PARAM__T.html</anchorfile>
      <anchor>ad65d0c4adc1cfb0179ca6dbc4437a247</anchor>
      <arglist>)(uint32_t offset, uint8_t **dst, uint32_t length, uint32_t high_offset)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>MSC_Verify</name>
      <anchorfile>structUSBD__MSC__INIT__PARAM__T.html</anchorfile>
      <anchor>ac569452a1df268b0c5ba09906b182886</anchor>
      <arglist>)(uint32_t offset, uint8_t buf[], uint32_t length, uint32_t high_offset)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>MSC_GetWriteBuf</name>
      <anchorfile>structUSBD__MSC__INIT__PARAM__T.html</anchorfile>
      <anchor>a497b2cf909115f98261c6b98b9ab4224</anchor>
      <arglist>)(uint32_t offset, uint8_t **buff_adr, uint32_t length, uint32_t high_offset)</arglist>
    </member>
    <member kind="variable">
      <type>ErrorCode_t(*</type>
      <name>MSC_Ep0_Hdlr</name>
      <anchorfile>structUSBD__MSC__INIT__PARAM__T.html</anchorfile>
      <anchor>a768c09f89844bdc0913663873bae1e4a</anchor>
      <arglist>)(USBD_HANDLE_T hUsb, void *data, uint32_t event)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>_WB_T</name>
    <filename>struct__WB__T.html</filename>
    <member kind="variable">
      <type>uint8_t</type>
      <name>L</name>
      <anchorfile>struct__WB__T.html</anchorfile>
      <anchor>a972f1009cd3bf1e1eca3b09fe8519528</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>H</name>
      <anchorfile>struct__WB__T.html</anchorfile>
      <anchor>af4f9ff0c4f4127861c5e4d1c0e7276de</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="union">
    <name>__WORD_BYTE</name>
    <filename>union____WORD__BYTE.html</filename>
    <member kind="variable">
      <type>uint16_t</type>
      <name>W</name>
      <anchorfile>union____WORD__BYTE.html</anchorfile>
      <anchor>a4cbdbd1a6f1f891f7b07909b68fa3762</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>WB_T</type>
      <name>WB</name>
      <anchorfile>union____WORD__BYTE.html</anchorfile>
      <anchor>a10c955122c6733864ad448942c130e8b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="union">
    <name>xPSR_Type</name>
    <filename>unionxPSR__Type.html</filename>
    <member kind="variable">
      <type>struct xPSR_Type::@2</type>
      <name>b</name>
      <anchorfile>unionxPSR__Type.html</anchorfile>
      <anchor>a3b1063bb5cdad67e037cba993b693b70</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>w</name>
      <anchorfile>unionxPSR__Type.html</anchorfile>
      <anchor>a1a47176768f45f79076c4f5b1b534bc2</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="dir">
    <name>inc</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/chip/inc/</path>
    <filename>dir_bfccd401955b95cf8c75461437045ac0.html</filename>
    <dir>inc/usbd</dir>
    <file>adc_17xx_40xx.h</file>
    <file>can_17xx_40xx.h</file>
    <file>chip.h</file>
    <file>chip_lpc175x_6x.h</file>
    <file>chip_lpc177x_8x.h</file>
    <file>chip_lpc407x_8x.h</file>
    <file>clock_17xx_40xx.h</file>
    <file>cmp_17xx_40xx.h</file>
    <file>cmsis.h</file>
    <file>cmsis_175x_6x.h</file>
    <file>core_cm3.h</file>
    <file>core_cmFunc.h</file>
    <file>core_cmInstr.h</file>
    <file>crc_17xx_40xx.h</file>
    <file>dac_17xx_40xx.h</file>
    <file>eeprom_17xx_40xx.h</file>
    <file>emc_17xx_40xx.h</file>
    <file>enet_17xx_40xx.h</file>
    <file>error.h</file>
    <file>fmc_17xx_40xx.h</file>
    <file>gpdma_17xx_40xx.h</file>
    <file>gpio_17xx_40xx.h</file>
    <file>gpioint_17xx_40xx.h</file>
    <file>i2c_17xx_40xx.h</file>
    <file>i2s_17xx_40xx.h</file>
    <file>iap.h</file>
    <file>iocon_17xx_40xx.h</file>
    <file>lcd_17xx_40xx.h</file>
    <file>lpc_types.h</file>
    <file>mcpwm_17xx_40xx.h</file>
    <file>pmu_17xx_40xx.h</file>
    <file>qei_17xx_40xx.h</file>
    <file>ring_buffer.h</file>
    <file>ritimer_17xx_40xx.h</file>
    <file>romapi_17xx_40xx.h</file>
    <file>rtc_17xx_40xx.h</file>
    <file>sdc_17xx_40xx.h</file>
    <file>sdmmc_17xx_40xx.h</file>
    <file>spi_17xx_40xx.h</file>
    <file>spifi_17xx_40xx.h</file>
    <file>ssp_17xx_40xx.h</file>
    <file>stopwatch.h</file>
    <file>sys_config.h</file>
    <file>sysctl_17xx_40xx.h</file>
    <file>timer_17xx_40xx.h</file>
    <file>uart_17xx_40xx.h</file>
    <file>usb_17xx_40xx.h</file>
    <file>wwdt_17xx_40xx.h</file>
  </compound>
  <compound kind="dir">
    <name>src</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/chip/src/</path>
    <filename>dir_68267d1309a1af8e8297ef4c3efbcdba.html</filename>
    <file>adc_17xx_40xx.c</file>
    <file>can_17xx_40xx.c</file>
    <file>chip_17xx_40xx.c</file>
    <file>clock_17xx_40xx.c</file>
    <file>cmp_17xx_40xx.c</file>
    <file>crc_17xx_40xx.c</file>
    <file>dac_17xx_40xx.c</file>
    <file>eeprom_17xx_40xx.c</file>
    <file>emc_17xx_40xx.c</file>
    <file>enet_17xx_40xx.c</file>
    <file>gpdma_17xx_40xx.c</file>
    <file>gpio_17xx_40xx.c</file>
    <file>gpioint_17xx_40xx.c</file>
    <file>i2c_17xx_40xx.c</file>
    <file>i2s_17xx_40xx.c</file>
    <file>iap.c</file>
    <file>iocon_17xx_40xx.c</file>
    <file>lcd_17xx_40xx.c</file>
    <file>pmu_17xx_40xx.c</file>
    <file>ring_buffer.c</file>
    <file>ritimer_17xx_40xx.c</file>
    <file>rtc_17xx_40xx.c</file>
    <file>sdc_17xx_40xx.c</file>
    <file>sdmmc_17xx_40xx.c</file>
    <file>spi_17xx_40xx.c</file>
    <file>ssp_17xx_40xx.c</file>
    <file>stopwatch_17xx_40xx.c</file>
    <file>sysctl_17xx_40xx.c</file>
    <file>sysinit_17xx_40xx.c</file>
    <file>timer_17xx_40xx.c</file>
    <file>uart_17xx_40xx.c</file>
    <file>wwdt_17xx_40xx.c</file>
  </compound>
  <compound kind="dir">
    <name>inc/usbd</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/chip/inc/usbd/</path>
    <filename>dir_12bf91aec4b1cd2b35367a8d2b14813c.html</filename>
    <file>usbd.h</file>
    <file>usbd_adc.h</file>
    <file>usbd_cdc.h</file>
    <file>usbd_cdcuser.h</file>
    <file>usbd_core.h</file>
    <file>usbd_desc.h</file>
    <file>usbd_dfu.h</file>
    <file>usbd_dfuuser.h</file>
    <file>usbd_hid.h</file>
    <file>usbd_hiduser.h</file>
    <file>usbd_hw.h</file>
    <file>usbd_msc.h</file>
    <file>usbd_mscuser.h</file>
    <file>usbd_rom_api.h</file>
  </compound>
  <compound kind="page">
    <name>index</name>
    <title>Hardware Support Package</title>
    <filename>index</filename>
    <docanchor file="index">chip_pkg</docanchor>
    <docanchor file="index" title="Overview">chip_overview</docanchor>
  </compound>
</tagfile>
