/*
 * @brief LPC17xx/40xx General Purpose DMA driver
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2014
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights. NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#ifndef __GPDMA_17XX_40XX_H_
#define __GPDMA_17XX_40XX_H_

#ifdef __cplusplus
extern "C" {
#endif
  
/** @defgroup GPDMA_17XX_40XX CHIP: LPC17xx/40xx General Purpose DMA driver
 * @ingroup CHIP_17XX_40XX_Drivers
 * @{
 */
  
/**
 * @brief Number of channels on GPDMA
 */
#define GPDMA_NUMBER_CHANNELS 8

/**
 * @brief Channel bitmask on GPDMA
 */
#define GPDMA_DMACHx_BitMask(n)   ((((1UL << (n)) & 0xFF)))
/**
 * @brief GPDMA Channel register block structure
 */
typedef struct {
  __IO uint32_t  SRCADDR;				/*!< DMA Channel Source Address Register */
  __IO uint32_t  DESTADDR;			/*!< DMA Channel Destination Address Register */
  __IO uint32_t  LLI;					/*!< DMA Channel Linked List Item Register */
  __IO uint32_t  CONTROL;				/*!< DMA Channel Control Register */
  __IO uint32_t  CONFIG;				/*!< DMA Channel Configuration Register */
  __I  uint32_t  RESERVED1[3];
} GPDMA_CH_T;
  
/**
 * @brief GPDMA register block
 */
typedef struct {						/*!< GPDMA Structure */
  __I  uint32_t  INTSTAT;				/*!< DMA Interrupt Status Register */
  __I  uint32_t  INTTCSTAT;			/*!< DMA Interrupt Terminal Count Request Status Register */
  __O  uint32_t  INTTCCLEAR;			/*!< DMA Interrupt Terminal Count Request Clear Register */
  __I  uint32_t  INTERRSTAT;			/*!< DMA Interrupt Error Status Register */
  __O  uint32_t  INTERRCLR;			/*!< DMA Interrupt Error Clear Register */
  __I  uint32_t  RAWINTTCSTAT;		/*!< DMA Raw Interrupt Terminal Count Status Register */
  __I  uint32_t  RAWINTERRSTAT;		/*!< DMA Raw Error Interrupt Status Register */
  __I  uint32_t  ENBLDCHNS;			/*!< DMA Enabled Channel Register */
  __IO uint32_t  SOFTBREQ;			/*!< DMA Software Burst Request Register */
  __IO uint32_t  SOFTSREQ;			/*!< DMA Software Single Request Register */
  __IO uint32_t  SOFTLBREQ;			/*!< DMA Software Last Burst Request Register */
  __IO uint32_t  SOFTLSREQ;			/*!< DMA Software Last Single Request Register */
  __IO uint32_t  CONFIG;				/*!< DMA Configuration Register */
  __IO uint32_t  SYNC;				/*!< DMA Synchronization Register */
  __I  uint32_t  RESERVED0[50];
  GPDMA_CH_T     CH[GPDMA_NUMBER_CHANNELS];
} LPC_GPDMA_T;

/**
 * @brief Macro defines for DMA channel control registers
 */
#define GPDMA_DMACCxControl_TransferSize(n) (((n & 0xFFF) << 0))	/*!< Transfer size*/
#define GPDMA_DMACCxControl_SBSize(n)       (((n & 0x07) << 12))	/*!< Source burst size*/
#define GPDMA_DMACCxControl_DBSize(n)       (((n & 0x07) << 15))	/*!< Destination burst size*/
#define GPDMA_DMACCxControl_SWidth(n)       (((n & 0x07) << 18))	/*!< Source transfer width*/
#define GPDMA_DMACCxControl_DWidth(n)       (((n & 0x07) << 21))	/*!< Destination transfer width*/
#define GPDMA_DMACCxControl_SI              ((1UL << 26))			/*!< Source increment*/
#define GPDMA_DMACCxControl_DI              ((1UL << 27))			/*!< Destination increment*/
#define GPDMA_DMACCxControl_SrcTransUseAHBMaster1   0
#define GPDMA_DMACCxControl_DestTransUseAHBMaster1  0
#define GPDMA_DMACCxControl_Prot1           ((1UL << 28))			/*!< Indicates that the access is in user mode or privileged mode*/
#define GPDMA_DMACCxControl_Prot2           ((1UL << 29))			/*!< Indicates that the access is bufferable or not bufferable*/
#define GPDMA_DMACCxControl_Prot3           ((1UL << 30))			/*!< Indicates that the access is cacheable or not cacheable*/
#define GPDMA_DMACCxControl_I               ((1UL << 31))			/*!< Terminal count interrupt enable bit */

/**
 * @brief Macro defines for DMA Configuration register
 */
#define GPDMA_DMACConfig_E              ((0x01))	/*!< DMA Controller enable*/
#define GPDMA_DMACConfig_M              ((0x02))	/*!< AHB Master endianness configuration*/
#define GPDMA_DMACConfig_BITMASK        ((0x03))


/**
 * @brief Macro defines for DMA Channel Configuration registers
 */
#define GPDMA_DMACCxConfig_E                    ((1UL << 0))			/*!< DMA control enable*/
#define GPDMA_DMACCxConfig_SrcPeripheral(n)     (((n & 0x1F) << 1))		/*!< Source peripheral*/
#define GPDMA_DMACCxConfig_DestPeripheral(n)    (((n & 0x1F) << 6))		/*!< Destination peripheral*/
#define GPDMA_DMACCxConfig_TransferType(n)      (((n & 0x7) << 11))		/*!< This value indicates the type of transfer*/
#define GPDMA_DMACCxConfig_IE                   ((1UL << 14))			/*!< Interrupt error mask*/
#define GPDMA_DMACCxConfig_ITC                  ((1UL << 15))			/*!< Terminal count interrupt mask*/
#define GPDMA_DMACCxConfig_L                    ((1UL << 16))			/*!< Lock*/
#define GPDMA_DMACCxConfig_A                    ((1UL << 17))			/*!< Active*/
#define GPDMA_DMACCxConfig_H                    ((1UL << 18))			/*!< Halt*/

/**
 * @brief GPDMA Interrupt Clear Status
 */
typedef enum {
  GPDMA_STATCLR_INTTC,	/*!< GPDMA Interrupt Terminal Count Request Clear */
  GPDMA_STATCLR_INTERR	/*!< GPDMA Interrupt Error Clear */
} GPDMA_STATECLEAR_T;

/**
 * @brief GPDMA Type of Interrupt Status
 */
typedef enum {
  GPDMA_STAT_TC,		/*!< GPDMA Interrupt Terminal Count Request Status */
  GPDMA_STAT_ERR		/*!< GPDMA Interrupt Error Status */
} GPDMA_STATUS_T;
  
/**
 * @brief GPDMA Type of DMA controller
 */
typedef enum {
  GPDMA_TRANSFERTYPE_M2M_CONTROLLER_DMA              = ((0UL)),	/*!< Memory to memory - DMA control */
  GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA              = ((1UL)),	/*!< Memory to peripheral - DMA control */
  GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA              = ((2UL)),	/*!< Peripheral to memory - DMA control */
  GPDMA_TRANSFERTYPE_P2P_CONTROLLER_DMA              = ((3UL)),	/*!< Source peripheral to destination peripheral - DMA control */
  GPDMA_TRANSFERTYPE_P2P_CONTROLLER_DestPERIPHERAL   = ((4UL)),	/*!< Source peripheral to destination peripheral - destination peripheral control */
  GPDMA_TRANSFERTYPE_M2P_CONTROLLER_PERIPHERAL       = ((5UL)),	/*!< Memory to peripheral - peripheral control */
  GPDMA_TRANSFERTYPE_P2M_CONTROLLER_PERIPHERAL       = ((6UL)),	/*!< Peripheral to memory - peripheral control */
  GPDMA_TRANSFERTYPE_P2P_CONTROLLER_SrcPERIPHERAL    = ((7UL))	/*!< Source peripheral to destination peripheral - source peripheral control */
} GPDMA_FLOW_CONTROL_T;
  
/**
 * @brief GPDMA callback for DMA operation
 */
typedef void(*GPDMA_CBK_T)(GPDMA_STATUS_T, void *);

/**
 * @brief GPDMA structure using for DMA configuration
 */
typedef struct {
  GPDMA_CBK_T Callback; /*!< Callback for transfer status  */
  void *Arg;            /*!< Additional argument to be passed  */

  uint32_t XferWidth;	/*!< Transfer width - used for TransferType is GPDMA_TRANSFERTYPE_M2M only */
  uint32_t SrcConn;	/*!< Source Connection ID */
  uint32_t DstConn;	/*!< Destination Connection ID */
  GPDMA_FLOW_CONTROL_T XferType;/*!< Transfer Type, should be one of the following:
				 * - GPDMA_TRANSFERTYPE_M2M: Memory to memory - DMA control
				 * - GPDMA_TRANSFERTYPE_M2P: Memory to peripheral - DMA control
				 * - GPDMA_TRANSFERTYPE_P2M: Peripheral to memory - DMA control
				 * - GPDMA_TRANSFERTYPE_P2P: Source peripheral to destination peripheral - DMA control
				 */  
} GPDMA_CFG_T;

/**
 * @brief GPDMA array request connection helpers
 */
#define GPDMA_CONN_ARR(base, index)  (((index)<<8)|(base))
#define GPDMA_CONN_BASE(conn)   ((conn)&0xFF)
#define GPDMA_CONN_IDX(conn)    (((conn)>>8)&0xFF)

#if defined(CHIP_LPC177X_8X) || defined(CHIP_LPC40XX)
/**
 * @brief GPDMA request connections
 */
#define GPDMA_CONN_MEMORY           ((0UL))     /*!< Memory */
#define GPDMA_CONN_SDC              ((1UL))	/*!< SD card */
#define GPDMA_CONN_SSP0_Tx          ((2UL))	/*!< SSP0 Tx */
#define GPDMA_CONN_SSP0_Rx          ((3UL))	/*!< SSP0 Rx */
#define GPDMA_CONN_SSP1_Tx          ((4UL))	/*!< SSP1 Tx */
#define GPDMA_CONN_SSP1_Rx          ((5UL))	/*!< SSP1 Rx */
#define GPDMA_CONN_SSP2_Tx          ((6UL))	/*!< SSP2 Tx */
#define GPDMA_CONN_SSP2_Rx          ((7UL))	/*!< SSP2 Rx */
#define GPDMA_CONN_ADC              ((8UL))	/*!< ADC */
#define GPDMA_CONN_DAC              ((9UL))	/*!< DAC */
#define GPDMA_CONN_UART0_Tx         ((10UL))	/*!< UART0 Tx */
#define GPDMA_CONN_UART0_Rx         ((11UL))	/*!< UART0 Rx */
#define GPDMA_CONN_UART1_Tx         ((12UL))	/*!< UART1 Tx */
#define GPDMA_CONN_UART1_Rx         ((13UL))	/*!< UART1 Rx */
#define GPDMA_CONN_UART2_Tx         ((14UL))		/*!< UART2 Tx */
#define GPDMA_CONN_UART2_Rx         ((15UL))		/*!< UART2 Rx */
#define GPDMA_CONN_MAT0_0           ((16UL))		/*!< MAT0.0 */
#define GPDMA_CONN_MAT0_1           ((17UL))		/*!< MAT0.1 */
#define GPDMA_CONN_MAT1_0           ((18UL))		/*!< MAT1.0 */
#define GPDMA_CONN_MAT1_1           ((19UL))		/*!< MAT1.1 */
#define GPDMA_CONN_MAT2_0           ((20UL))		/*!< MAT2.0 */
#define GPDMA_CONN_MAT2_1           ((21UL))		/*!< MAT2.1 */
#define GPDMA_CONN_I2S_Channel_0    ((22UL))		/*!< I2S channel 0 */
#define GPDMA_CONN_I2S_Channel_1    ((23UL))		/*!< I2S channel 1 */
#define GPDMA_CONN_UART3_Tx         ((26UL))		/*!< UART3 Tx */
#define GPDMA_CONN_UART3_Rx         ((27UL))		/*!< UART3 Rx */
#define GPDMA_CONN_UART4_Tx         ((28UL))		/*!< UART3 Tx */
#define GPDMA_CONN_UART4_Rx         ((29UL))		/*!< UART3 Rx */
#define GPDMA_CONN_MAT3_0           ((30UL))		/*!< MAT3.0 */
#define GPDMA_CONN_MAT3_1           ((31UL))		/*!< MAT3.1 */

#elif defined(CHIP_LPC175X_6X)
/**
 * @brief GPDMA request connections
 */
#define GPDMA_CONN_SSP0_Tx          ((0UL))			/**< SSP0 Tx */
#define GPDMA_CONN_SSP0_Rx          ((1UL))			/**< SSP0 Rx */
#define GPDMA_CONN_SSP1_Tx          ((2UL))			/**< SSP1 Tx */
#define GPDMA_CONN_SSP1_Rx          ((3UL))			/**< SSP1 Rx */
#define GPDMA_CONN_ADC0             (GPDMA_CONN_ARR((4UL),0))	/**< ADC Channel 0*/
#define GPDMA_CONN_ADC1             (GPDMA_CONN_ARR((4UL),1))	/**< ADC Channel 1*/
#define GPDMA_CONN_ADC2             (GPDMA_CONN_ARR((4UL),2))	/**< ADC Channel 2*/
#define GPDMA_CONN_ADC3             (GPDMA_CONN_ARR((4UL),3))	/**< ADC Channel 3*/
#define GPDMA_CONN_ADC4             (GPDMA_CONN_ARR((4UL),4))	/**< ADC Channel 4*/
#define GPDMA_CONN_ADC5             (GPDMA_CONN_ARR((4UL),5))	/**< ADC Channel 5*/
#define GPDMA_CONN_ADC6             (GPDMA_CONN_ARR((4UL),6))	/**< ADC Channel 6*/
#define GPDMA_CONN_ADC7             (GPDMA_CONN_ARR((4UL),7))	/**< ADC Channel 7*/
#define GPDMA_CONN_I2S_Channel_0    ((5UL))			/**< I2S channel 0 */
#define GPDMA_CONN_I2S_Channel_1    ((6UL))			/**< I2S channel 1 */
#define GPDMA_CONN_DAC              ((7UL))			/**< DAC */
#define GPDMA_CONN_UART0_Tx         ((8UL))			/**< UART0 Tx */
#define GPDMA_CONN_UART0_Rx         ((9UL))			/**< UART0 Rx */
#define GPDMA_CONN_UART1_Tx         ((10UL))		/**< UART1 Tx */
#define GPDMA_CONN_UART1_Rx         ((11UL))		/**< UART1 Rx */
#define GPDMA_CONN_UART2_Tx         ((12UL))		/**< UART2 Tx */
#define GPDMA_CONN_UART2_Rx         ((13UL))		/**< UART2 Rx */
#define GPDMA_CONN_UART3_Tx         ((14UL))		/**< UART3 Tx */
#define GPDMA_CONN_UART3_Rx         ((15UL))		/**< UART3 Rx */
#define GPDMA_CONN_MAT0_0           ((16UL))		/**< MAT0.0 */
#define GPDMA_CONN_MAT0_1           ((17UL))		/**< MAT0.1 */
#define GPDMA_CONN_MAT1_0           ((18UL))		/**< MAT1.0 */
#define GPDMA_CONN_MAT1_1           ((19UL))		/**< MAT1.1 */
#define GPDMA_CONN_MAT2_0           ((20UL))		/**< MAT2.0 */
#define GPDMA_CONN_MAT2_1           ((21UL))		/**< MAT2.1 */
#define GPDMA_CONN_MAT3_0           ((22UL))		/**< MAT3.0 */
#define GPDMA_CONN_MAT3_1           ((23UL))		/**< MAT3.1 */
#define GPDMA_CONN_MEMORY           ((24UL))
#endif

/**
 * @brief GPDMA Burst size in Source and Destination definitions
 */
#define GPDMA_BSIZE_1   ((0UL))	/*!< Burst size = 1 */
#define GPDMA_BSIZE_4   ((1UL))	/*!< Burst size = 4 */
#define GPDMA_BSIZE_8   ((2UL))	/*!< Burst size = 8 */
#define GPDMA_BSIZE_16  ((3UL))	/*!< Burst size = 16 */
#define GPDMA_BSIZE_32  ((4UL))	/*!< Burst size = 32 */
#define GPDMA_BSIZE_64  ((5UL))	/*!< Burst size = 64 */
#define GPDMA_BSIZE_128 ((6UL))	/*!< Burst size = 128 */
#define GPDMA_BSIZE_256 ((7UL))	/*!< Burst size = 256 */

/**
 * @brief Width in Source transfer width and Destination transfer width definitions
 */
#define GPDMA_WIDTH_BYTE        ((0UL))	/*!< Width = 1 byte */
#define GPDMA_WIDTH_HALFWORD    ((1UL))	/*!< Width = 2 bytes */
#define GPDMA_WIDTH_WORD        ((2UL))	/*!< Width = 4 bytes */

/**
 * @brief Flow control definitions
 */
#define GPDMA_CONTROLLER 0		/*!< Flow control is DMA controller*/
#define SRC_PER_CONTROLLER 1	/*!< Flow control is Source peripheral controller*/
#define DST_PER_CONTROLLER 2	/*!< Flow control is Destination peripheral controller*/

/**
 * @brief Transfer Descriptor structure typedef
 */
typedef struct {
  uint32_t SrcAddr;	/*!< Source address */
  uint32_t DstAddr;	/*!< Destination address */
  uint32_t LinkAddr;	/*!< Pointer to next descriptor structure */
  union {
    uint32_t CtrlWord;  /*!< Control word that has transfer size, type etc. */
    uint32_t Size;      /*!< Size for ease of use. */
  };
} GPDMA_DESC_T;

/**
 * @brief Transfer Handle typedef
 */
typedef uint32_t GPDMA_HANDLE_T;

/**
 * @brief Null Transfer Handle
 */
#define GPDMA_HANDLE_NULL  0xFFFFFFFF

/**
 * @brief	Initialize the GPDMA
 * @param	pGPDMA	: The base of GPDMA on the chip
 * @return	Nothing
 */
void Chip_GPDMA_Init(LPC_GPDMA_T *pGPDMA);

/**
 * @brief	Shutdown the GPDMA
 * @param	pGPDMA	: The base of GPDMA on the chip
 * @return	Nothing
 */
void Chip_GPDMA_DeInit(LPC_GPDMA_T *pGPDMA);

/**
 * @brief	The GPDMA stream interrupt handler
 * @param	pGPDMA		: The base of GPDMA on the chip
 * @return	Nothing
 */
void  Chip_GPDMA_Interrupt(LPC_GPDMA_T *pGPDMA);
 
/**
 * @brief	Prepare a single DMA descriptor
 * @param	pGPDMA		: The base of GPDMA on the chip
 * @param	pCFG	        : Configuration of DMA to be performed
 * @param	pDESC		: Descriptor for DMA  
 * @return	Always SUCCESS
 */
Status Chip_GPDMA_PrepareDescriptor(LPC_GPDMA_T *pGPDMA, GPDMA_CFG_T *pCFG, GPDMA_DESC_T *pDESC);
						     
/**
 * @brief	Do a DMA transfer 
 * @param	pGPDMA		: The base of GPDMA on the chip
 * @param	pCFG	        : Configuration of DMA to be performed
 * @param	pDESC		: Descriptor chain for DMA  
 * @param       minPriority     : 
 * @return      Transfer handle, null one on error
 */
GPDMA_HANDLE_T Chip_GPDMA_Transfer(LPC_GPDMA_T *pGPDMA, GPDMA_CFG_T *pCFG, GPDMA_DESC_T *pDESC, uint32_t minPriority);

/**
 * @brief	Stop a DMA transfer
 * @param	pGPDMA		: The base of GPDMA on the chip
 * @param	Handle	        : Transfer handle
 * @return	ERROR if old handle, SUCCESS otherwise
 */
Status Chip_GPDMA_Stop(LPC_GPDMA_T *pGPDMA, GPDMA_HANDLE_T Handle);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __GPDMA_17XX_40XX_H_ */
