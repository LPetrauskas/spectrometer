#include "scpi.h"

typedef struct _scpi_flat_t {
	SCPI_OBJECT_PROLOGUE;
	char *value;
	int size;
} scpi_flat_t;

#define FLAT_VALUE(obj)     (((scpi_flat_t *)obj)->value)
#define FLAT_SIZE(obj) 		(((scpi_flat_t *)obj)->size)

static int scpi_flat_delete(scpi_object_t *this) {		
	free(FLAT_VALUE(this));		
	free(this);
	return 0;
}

/*** Exported symbols ***/

const scpi_type_t scpi_mutable_flat_type = {

	sizeof(scpi_flat_t), /* object size */

	/* base primitives */
	&scpi_base_type.new,	/* new method */
	&scpi_flat_clone, 		/* clone method */
	&scpi_flat_delete,		/* delete method */
	
	/* attr primitives */
	NULL,					/* get method */
	NULL,					/* set method */
	
//	/*serialize primitives */
//	&scpi_flat_load			/* load method */
//	&scpi_flat_dump			/* dump method */
	
	/* expression primitives */
	NULL,					/*binary operation method*/
	NULL,					/*unary operation method*/
	
	/*command primitives */
	NULL,					/* perform method */ 
	NULL,					/* query method */ 
}


const scpi_type_t scpi_immutable_flat_type = {

	sizeof(scpi_flat_t), /* object size */

	/* base primitives */
	&scpi_base_type.new,	/* new method */
	&scpi_base_type.new, 		/* clone method */
	&scpi_base_type.new,		/* delete method */
	
	/* attr primitives */
	NULL,					/* get method */
	NULL,					/* set method */
	
//	/*serialize primitives */
//	&scpi_flat_load			/* load method */
//	&scpi_flat_dump			/* dump method */
	
	/* expression primitives */
	NULL,					/*binary operation method*/
	NULL,					/*unary operation method*/
	
	/*command primitives */
	NULL,					/* perform method */ 
	NULL,					/* query method */ 
}

int scpi_flat_as_bytes(scpi_object_t *this, void **data) {
	if (SCPI_INSTANCE_OF(other, scpi_flat_type)) {		
		scpi_error_set();
		return -1;
	}	

	*data = FLAT_VALUE(this);
	return FLAT_SIZE(this);
}

scpi_object_t *scpi_flat_from_const_bytes(const void *data, int size) {
	scpi_object_t *object = scpi_object_new(&scpi_immutable_flat_type);
	if (object != NULL) {
		FLAT_VALUE(object) = data;
		FLAT_SIZE(object) = size;
	}
	return object;
}

scpi_object_t *scpi_flat_from_bytes(const void *data, int size) {
	scpi_object_t *object = scpi_object_new(&scpi_mutable_flat_type);	
	if (object != NULL) {
		FLAT_VALUE(object) = malloc(size);
		if(FLAT_VALUE(object) == NULL) {
			scpi_object_delete(object);	
			scpi_error_set();
			return NULL;
		}				
		memcpy(FLAT_VALUE(object), data, size);
		FLAT_SIZE(object) = size;		
	}	
	return object;
}
