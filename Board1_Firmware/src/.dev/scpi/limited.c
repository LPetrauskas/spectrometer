#include "scpi.h"

#define LIMITED_MAX(obj) (((scpi_limited_t *)obj)->max)
#define LIMITED_MIN(obj) (((scpi_limited_t *)obj)->min)
#define LIMITED_DEF(obj) (((scpi_limited_t *)obj)->def)

typedef struct _scpi_limited_t{
	SCPI_OBJECT_PROLOGUE;
	scpi_object_t *max;
	scpi_object_t *min;
	scpi_object_t *def;
} scpi_limited_t;

static int scpi_limited_perform(scpi_object_t *this, scpi_object_t *args) {
	int len; scpi_object_t *arg;
	
	len = scpi_array_len(args);
	while(len--) {
		arg = scpi_array_pop(args);
		if (arg == scpi_max) {
			arg = LIMITED_MAX(this);
		}
		if (arg == scpi_min) {
			arg = LIMITED_MIN(this);	
		}
		if (arg == scpi_default) {
			arg = LIMITED_DEF(this);
		}
		scpi_array_push(args, arg);
	}
	return scpi_object_perform(SCPI_PROTO(this), args);
}

static scpi_object_t *scpi_limited_query(scpi_object_t *this, scpi_object_t *args) {
	scpi_object_t *arg;

	if (scpi_array_len(args) == 1) {
		arg = scpi_array_index(args, 0);
		if (arg == scpi_max) {
			SCPI_INCREF(LIMITED_MAX(this));
			return LIMITED_MAX(this);
		}
		if (arg == scpi_min) {
			SCPI_INCREF(LIMITED_MIN(this));
			return LIMITED_MIN(this);
		}
		if (arg == scpi_default) {
			SCPI_INCREF(LIMITED_DEF(this));
			return LIMITED_DEF(this);
		}
	}
	return scpi_object_query(SCPI_PROTO(this), args);
}


/*** Exported symbols ***/

const scpi_type_t scpi_limited_type = {

	sizeof(scpi_limited_t), /* object size */

	/* base primitives */
	&scpi_base_type.new,	/* new method */
	&scpi_base_type.clone,  /* clone method */
	&scpi_base_type.del,	/* delete method */
	
	/* attr primitives */
	NULL,					/* get method */
	NULL,					/* set method */
	
	// /*serialize primitives */
	// &scpi_integer_load		/* load method */
	// &scpi_integer_dump		/* dump method */
	
	/* expression primitives */
	NULL,		/*binary operation method*/
	NULL,		/*unary operation method*/
	
	/*command primitives */
	&scpi_limited_perform,		/* perform method */ 
	&scpi_limited_query,		/* query method */ 
}

scpi_object_t *scpi_set_limits(scpi_object_t *obj, scpi_object_t *max, scpi_object_t *min, scpi_object_t *def) {
	scpi_object *new = scpi_object_new(&scpi_limited_type);
	if (new != NULL) {		
		SCPI_PROTO(new) = obj;
		LIMITED_MAX(new) = max;
		LIMITED_MIN(min) = min;
		LIMITED_DEF(def) = def;
	}
	return new;
}