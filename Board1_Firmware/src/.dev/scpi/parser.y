{%
	#include "scpi_types.h"
%}

/* Declarations section */

/* Operators tokens */
%left  OR EXOR
%left  AND
%right NOT
%left  "+" "-"
%left  "*" "/" MOD DIV
%left  "^"
%right UPLUS UMINUS 

/* Literal tokens */
%token RAW
%token UNIT
%token DECIMAL	
%token INTEGER
%token STRING
%token MNEMONIC
%token WHITESPACE

%% 
/* Rules section */

term_prog_msg:  prog_msg prog_msg_term
				;

prog_msg:  	 	prog_msg_unit
			|	prog_msg_unit prog_msg_unit_sep prog_msg
			;

prog_msg_unit:		cmd_msg_unit
				|	qry_msg_unit
				;

cmd_msg_unit:		prog_hdr { scpi_object_perform($1, scpi_nul); } 	
				|   prog_hdr prog_hdr_sep prog_data { scpi_object_perform($1, $3); }
				;

qry_msg_unit:		prog_hdr "?" { $$ = scpi_object_query($1, scpi_nul); }
				|   prog_hdr "?" prog_hdr_sep prog_data { $$ = scpi_object_query($1, $4); }
				;
			
prog_data:		prog_data_elem	{ $$ = scpi_array_push(scpi_array_new(), $1); }
			|	prog_data prog_data_sep prog_data_elem { $$ = scpi_array_push($1, $3); }
			;
			
prog_data_elem:		char_prog_data 
				|	dec_prog_data
				|	dec_prog_data sfx_prog_data { $$ = scpi_magnitude_compose($1, $2); }				
				| 	nondec_prog_data 				
				|	str_prog_data 
				|	arb_prog_data 
				|	expr_prog_data 
				|	sfx_prog_data 
				|	qry_msg_unit 
				;

prog_msg_unit_sep:		";"
					|	WHITESPACE prog_msg_unit_sep
					;	

prog_data_sep:			","
					|	WHITESPACE  prog_data_sep
					|	prog_data_sep WHITESPACE
					;
					
prog_hdr_sep:		WHITESPACE
					;

prog_msg_term:		"\n"
				|	WHITESPACE prog_msg_term
				;

prog_hdr:		simple_prog_hdr
			|	cmpoud_prog_hdr
			|	common_prog_hdr
			|	WHITESPACE prog_hdr
			;

simple_prog_hdr:	MNEMONIC
				    ;

cmpoud_prog_hdr:		simple_prog_hdr { scpi_array }
					|	cmpoud_prog_hdr ":" { scpi_array_push(scpi_scopes, $1); } cmpoud_prog_hdr  
					|	":" { scpi_array_clean(scpi_scopes); scpi_array_push(scpi_scopes, scpi_root); } cmpoud_prog_hdr
					;

common_prog_hdr:	"*"	{ scpi_array_push(scpi_scopes, scpi_builtin); } MNEMONIC { scpi_array_pop(scpi_scopes); }
					;
						
char_prog_data:		MNEMONIC
					;

dec_prog_data:		DECIMAL
					;

sfx_prog_data:		sfx_expr
					;

sfx_elem:	UNIT							
			;

sfx_expr:		sfx_elem
			|	sfx_expr "*" sfx_elem	{ $$ = scpi_object_binary_op($1, $3, '*'); }
			|	sfx_expr "/" sfx_elem	{ $$ = scpi_object_binary_op($1, $3, '/'); }
			|	sfx_expr "^" INTEGER	{ $$ = scpi_object_binary_op($1, $3, '^'); }
			|	"(" sfx_expr ")" 		{ $$ = $2; }
			;

nondec_prog_data:	INTEGER
					;

str_prog_data:		STRING										
					;

arb_prog_data:		RAW 
					;
					
expr_prog_data:		"(" prog_expr ")" { $$ = $2; }
					;

prog_expr:		prog_expr_elem	{ $$ = scpi_array_push(scpi_array_new(), $1); }
			|	prog_expr prog_data_sep prog_expr_elem  { $$ = scpi_array_push($1, $3); }
			;

prog_expr_elem:		prog_data_elem
				|	prog_expr "+" prog_data_elem 	{ $$ = scpi_object_binary_op($1, $3, '+'); }
				|	prog_expr "-" prog_data_elem	{ $$ = scpi_object_binary_op($1, $3, '-'); }
				|	prog_expr "*" prog_data_elem	{ $$ = scpi_object_binary_op($1, $3, '*'); }
				|	prog_expr "/" prog_data_elem 	{ $$ = scpi_object_binary_op($1, $3, '/'); }
				|	prog_expr MOD prog_data_elem	{ $$ = scpi_object_binary_op($1, $3, MOD); }  			
				|	prog_expr DIV prog_data_elem	{ $$ = scpi_object_binary_op($1, $3, DIV); }  			
				|	prog_expr AND prog_data_elem	{ $$ = scpi_object_binary_op($1, $3, AND); }  			
				|	prog_expr OR  prog_data_elem	{ $$ = scpi_object_binary_op($1, $3, OR); }  			
				|	prog_expr EXOR  prog_data_elem	{ $$ = scpi_object_binary_op($1, $3, EXOR); }  
				|	prog_expr "^" prog_data_elem 	{ $$ = scpi_object_binary_op($1, $3, '^'); }
				|	"-" prog_expr %prec UMINUS		{ $$ = scpi_object_unary_op($2, '-'); }
				|	"+" prog_expr %prec UPLUS		{ $$ = scpi_object_unary_op($2, '+'); }
				|	NOT prog_expr					{ $$ = scpi_object_unary_op($2, NOT); }
				|   "(" prog_expr ")"				{ $$ = $2; }
				;
				
%% 	
/* User code */