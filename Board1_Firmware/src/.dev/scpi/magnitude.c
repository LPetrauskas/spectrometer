#include "scpi.h"

#define MAGN_NUMBER(obj)  	(((scpi_magnitude_t *)obj)->number)
#define MAGN_UNIT(obj)		(((scpi_magnitude_t *)obj)->unit)	

typedef _scpi_magnitude_t{
	SCPI_OBJECT_PROLOGUE;
	scpi_object_t *number;
	scpi_object_t *unit;
} scpi_magnitude_t;

scpi_object_t *scpi_magnitude_magnitude_binary_op(scpi_object_t *this, scpi_object_t *other, char op) {
	scpi_object_t *new, *u, *n, *op1, *op2;	
	unit = scpi_unit_binary_op(MAGN_UNIT(this), MAGN_UNIT(other), op);
	if (unit != NULL) {
		switch(op) {
			case '-':
			case '+':
				op1 = scpi_unit_convert(MAGN_NUMBER(this), unit, MAGN_UNIT(this));
				op2 = scpi_unit_convert(MAGN_NUMBER(this), unit, MAGN_UNIT(other));
			case '*':
			case '/':
				if (scpi_unit_is_db(unit)) {
					op1 = scpi_unit_db_deref(MAGN_NUMBER(this), MAGN_UNIT(this));
					op2 = scpi_unit_db_deref(MAGN_NUMBER(other), MAGN_UNIT(other));	
				}				
			default:
				break;
		}
		num = scpi_object_binary_op(MAGN_NUMBER(this), MAGN_NUMBER(other), op);	 
		if (num != NULL) {
			new = scpi_object_new(&scpi_magnitude_type);
			if (new != NULL) {
				MAGN_NUMBER(new) = num;
				MAGN_UNIT(new) = unit;
				return new;
			}
			SCPI_DECREF(num);
		}
		SCPI_DECREF(unit);
	}		
	return NULL;
}

scpi_object_t *scpi_magnitude_numeric_binary_op(scpi_object_t *this, scpi_object_t *other, char op) {
	scpi_object_t *new = NULL; 
	scpi_object_t *n = scpi_object_binary_op(MAGN_NUMBER(this), other, op);
	if (n != NULL) {
		new = scpi_object_new(&scpi_magnitude_type);
		if (new != NULL) {
			MAGN_NUMBER(new) = n;
			MAGN_INCREF(MAGN_UNIT(this));		
			MAGN_UNIT(new) = MAGN_UNIT(this);
		}
	}
	return new;
}

scpi_object_t *scpi_magnitude_binary_op(scpi_object_t *this, scpi_object_t *other, char op) {
	if (SCPI_INSTANCE_OF(other, scpi_magnitude_type)) {
		return scpi_magnitude_magnitude_binary_op(this, other, op);
	}
	if (SCPI_INSTANCE_OF(other, scpi_numeric_type)) {
		return scpi_magnitude_numeric_binary_op(this, other, op);
	}
	scpi_error_set();
	return NULL;
}

scpi_object_t *scpi_magnitude_binary_op(scpi_object_t *this, char op) {
	scpi_object_t *new, *n, *u;	
	if (op != NOT) {				
		n = scpi_object_unary_op(MAGN_NUMBER(this), op);
		if (n != NULL) {
			new = scpi_object_new(&scpi_magnitude_type);
			if (new != NULL) {				
				u = MAGN_UNIT(this);
				SCPI_INCREF(u);
				MAGN_UNIT(new) = u;
				MAGN_NUMBER(new) = n;							
			}
			return new;			
		}
		return NULL;		
	}
	scpi_error_set();
	return NULL;
}

int scpi_magnitude_delete(scpi_object_t *this) {
	SCPI_DECREF(MAGN_NUMBER(this));
	SCPI_DECREF(MAGN_UNIT(this));
	free(this);
	return 0;
}

/*** Exported symbols ***/

const scpi_type_t scpi_magnitude_type = {

	sizeof(scpi_magnitude_t), /* object size */

	/* base primitives */
	&scpi_base_type.new,	/* new method */
	&scpi_base_type.clone,  /* clone method */
	&scpi_magnitude_delete,	/* delete method */
	
	/* attr primitives */
	NULL,					/* get method */
	NULL,					/* set method */
	
//	/*serialize primitives */
//	&scpi_magnitude_load		/* load method */
//	&scpi_magnitude_dump		/* dump method */
	
	/* expression primitives */
	&scpi_magnitude_binary_op,	/*binary operation method*/
	&scpi_magnitude_unary_op,	/*unary operation method*/
	
	/*command primitives */
	NULL,						/* perform method */ 
	NULL,						/* query method */ 
};

scpi_object_t *scpi_magnitude_compose(scpi_object_t *number, scpi_object_t *unit) {
	scpi_object_t *object = scpi_object_new(&scpi_magnitude_type);
	if (object != NULL) {
		SCPI_INCREF(number);
		SCPI_INCREF(unit);
		object->number = number;
		object->unit = unit;
	}
	return object;
}

double scpi_magnitude_express(scpi_object_t *magn, scpi_object_t unit) {

}



