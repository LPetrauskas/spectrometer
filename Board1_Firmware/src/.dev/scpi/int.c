#include "scpi.h"

// static scpi_object_t *scpi_integer_load(scpi_type_t *type, void *data, int *size) {
// 	long value;	
	
// 	if (*size < sizeof(long)) {		
// 		scpi_error_set();
// 		return NULL;
// 	}	
// 	*size = sizeof(long);
// 	memcpy(data, value, sizeof(long));		
	
// 	return scpi_integer_from_long(value);
// }

// static int scpi_integer_dump(scpi_object_t *this, void *data, int *size) {
// 	long value;
// 	if (*size < sizeof(long)) {
// 		*size = sizeof(long);
// 		scpi_error_set();
// 		return -1;
// 	}	
// 	value = scpi_integer_as_long(this);
// 	memcpy(&value, data, sizeof(long));
// 	*size = sizeof(long);
// 	return 0;
// }

#define INT_VALUE(obj) (((scpi_integer_t *)obj)->value)

typedef struct _scpi_integer_t
{
	SCPI_OBJECT_PROLOGUE;
	long value;
} scpi_integer_t;

static scpi_object_t *scpi_integer_integer_binary_op(scpi_object_t *this, scpi_object_t *other, char op) {
	long lvalue = INT_VALUE(this); 
	long rvalue = INT_VALUE(other);
	switch(op) {
		case '+':
			lvalue += rvalue;
			break;
		case '-':
			lvalue -= rvalue;
			break;
		case '*':
			lvalue *= rvalue;
			break;
		case '/':
			lvalue /= rvalue;
			break;
		case DIV:
			lvalue /= rvalue;			
			break;
		case REM:
			lvalue %= rvalue;
			break;
		case '^':
			lvalue = lpow(lvalue, rvalue);
			break;
		case AND:
			lvalue &= rvalue;
			break;
		case OR:
			lvalue |= rvalue;
			break;
		case EXOR:
			lvalue ^= rvalue;
			break;	
	}
	return scpi_integer_from_long(lvalue);
}

static scpi_object_t *scpi_integer_numeric_binary_op(scpi_object_t *this, scpi_object_t *other, char op) {
	double lvalue = (double)INT_VALUE(this);	
	double rvalue = scpi_numeric_as_double(other);
	switch(op) {
		case '+':
			lvalue += rvalue;
			break;
		case '-':
			lvalue -= rvalue;
			break;
		case '*':
			lvalue *= rvalue;
			break;
		case '/':
			lvalue /= rvalue;
			break;
		case DIV:
			lvalue = (int)(lvalue / rvalue);
			break;		
		case REM:
			lvalue %= rvalue;
			break;
		case '^':
			lvalue = pow(lvalue, rvalue);
			break;
		case AND:
			lvalue &= rvalue;
			break;
		case OR:
			lvalue |= rvalue;
			break;
		case EXOR:
			lvalue ^= rvalue;
			break;			
	}
	return scpi_numeric_from_double(lvalue);
}

static scpi_object_t *scpi_integer_string_binary_op(scpi_object_t *this, scpi_object_t *other, char op) {	
	long lvalue = ((scpi_integer_t *)this)->value;
	char *rvalue = scpi_string_as_chars(other);
	long rsize = strlen(rvalue);
	char *lvalue = malloc(rsize * i + 1);	

	switch(op) {		
		case '*':
			while (i--) {
				strcpy(&lvalue[i * rsize], rvalue)
			}			
			break;
		default:
			scpi_error_set("");
			return NULL:
	}
	return scpi_string_from_chars(svalue);
}

static scpi_object_t *scpi_integer_binary_op(scpi_object_t *this, scpi_object_t *other, char op) {	
	if (SCPI_INSTANCE_OF(other, scpi_integer_type)) {	
		return scpi_integer_integer_binary_op(this, other, op);
	}
	if (SCPI_INSTANCE_OF(other, scpi_numeric_type)) {
		return scpi_integer_numeric_binary_op(this, other, op);
	}
	if (SCPI_INSTANCE_OF(other, scpi_string_type)) {
		return scpi_integer_string_binary_op(this, other, op);
	}
	scpi_error_set();
	return NULL;	
}

static scpi_object_t *scpi_integer_unary_op(scpi_object_t *this, char op) {	
	long lvalue =INT_VALUE(this);	
	switch (op) {
		case '-':
			lvalue = -lvalue;
		case NOT:
			lvalue = ~lvalue;
		default:
			break;
	}
	return scpi_integer_from_long(lvalue);	
}

/*** Exported symbols ***/

const scpi_type_t scpi_integer_type = {

	sizeof(scpi_integer_t), /* object size */

	/* base primitives */
	&scpi_base_type.new,	/* new method */
	&scpi_base_type.clone,  /* clone method */
	&scpi_base_type.del,	/* delete method */
	
	/* attr primitives */
	NULL,					/* get method */
	NULL,					/* set method */
	
	// /*serialize primitives */
	// &scpi_integer_load		/* load method */
	// &scpi_integer_dump		/* dump method */
	
	/* expression primitives */
	&scpi_integer_binary_op,	/*binary operation method*/
	&scpi_integer_unary_op,		/*unary operation method*/
	
	/*command primitives */
	NULL,	/* perform method */ 
	NULL,	/* query method */ 
}


long scpi_integer_as_long(scpi_object_t *object) {
	if (SCPI_INSTANCE_OF(object, scpi_integer_type)) {
		scpi_error_set();
		return -1;
	}	
	return INT_VALUE(object);
}

scpi_object_t *scpi_integer_from_long(long value) {		
	scpi_object_t *new = scpi_object_new(&scpi_integer_type);	
	if (object != NULL) {
		INT_VALUE(new) = value;	
	}	
	return object;
}
