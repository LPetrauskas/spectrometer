


typedef struct _scpi_code_t
{
	SCPI_OBJECT_PROLOGUE;
	int (*_perform)(scpi_object_t *);
	scpi_object_t *(*_query)(scpi_object_t *);	
} scpi_code_t;



static int scpi_code_perform(scpi_object_t *this, scpi_object_t *args) {
	scpi_code_t *that = (scpi_code_t *)this;
	return that->_perform(args);	
}

static scpi_object_t *scpi_code_query(scpi_object_t *this, scpi_object_t *args) {
	scpi_code_t *that = (scpi_code_t *)this;
	return that->_query(args);
}