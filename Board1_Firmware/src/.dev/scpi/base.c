
typedef _scpi_base_t {
	SCPI_OBJECT_PROLOGUE;	
} scpi_base_t;


const scpi_type_t scpi_base_type = {

	sizeof(scpi_base_t), /* object size */

	/* base primitives */
	&scpi_base_new,	/* new method */
	&scpi_base_clone, 		/* clone method */
	&scpi_base_delete,		/* delete method */
	
	/* attr primitives */
	NULL,					/* get method */
	NULL,					/* set method */
	
//	/*serialize primitives */
//	&scpi_flat_load			/* load method */
//	&scpi_flat_dump			/* dump method */
	
	/* expression primitives */
	NULL,					/*binary operation method*/
	NULL,					/*unary operation method*/
	
	/*command primitives */
	NULL,					/* perform method */ 
	NULL,					/* query method */ 
}

static scpi_object_t *scpi_base_new(scpi_type_t *type) {
	scpi_object_t *object = malloc(type->_size);
	if (object == NULL) {
		scpi_error_set();
		return NULL;
	}
	object->_type = type;	
	object->_ref = 1;
	return object;
}

static scpi_object_t scpi_base_clone(scpi_object_t *this) {
	scpi_object_t *new = malloc(this->_type->_size);
	if (new == NULL) {
		scpi_error_set();
		return NULL;
	}
	memcpy(new, this, this->_type->_size);
	return 0;	
}

static int scpi_base_delete(scpi_object_t *this) {
	free(this);
	return 0;	
}