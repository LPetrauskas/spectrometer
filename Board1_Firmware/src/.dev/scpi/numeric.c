#include "scpi.h"
#include "math.h"
/*** Internal symbols ***/

// static scpi_object_t *scpi_numeric_load(scpi_type_t *type, void *data, int *size) {
// 	double value;
	
// 	if (*size < sizeof(value)) {		
// 		scpi_error_set();
// 		return NULL;
// 	}	
// 	*size = sizeof(value);
// 	memcpy(data, &value, sizeof(value));		
		
// 	return scpi_numeric_from_double(value);
// }

// static int scpi_numeric_dump(scpi_object_t *this, void *data, int *size) {
// 	double value;

// 	if (*size < sizeof(value)) {
// 		*size = sizeof(value);
// 		scpi_error_set();
// 		return -1;
// 	}	
// 	memcpy(&value, data, sizeof(value));
// 	*size = sizeof(value);
// 	return 0;
// }

typedef struct _scpi_numeric_t
{
	SCPI_OBJECT_PROLOGUE;
	double value;
} scpi_numeric_t;

static scpi_object_t *scpi_numeric_numeric_binary_op(scpi_object_t *this, scpi_object_t *other, scpi_bin_ops_t op) {
	double lvalue = ((scpi_numeric_t *)this)->value;	
	double rvalue = ((scpi_numeric_t *)other)->value;
	switch(op) {
		case '+':
			lvalue += rvalue;
			break;
		case '-':
			lvalue -= rvalue;
			break;
		case '*':
			lvalue *= rvalue;
			break;
		case '/':
			lvalue /= rvalue;
			break;		
		case DIV:
			lvalue /= rvalue;				
			lvalue = floor(lvalue);
			break;		
		case REM:
			lvalue %= rvalue;
			break;
		case '^':
			lvalue = pow(lvalue, rvalue);
			break;
		case AND:
			lvalue &= rvalue;
			break;
		case OR:
			lvalue |= rvalue;
			break;
		case EXOR:
			lvalue ^= rvalue;
			break;
		default:

	}
	return scpi_numeric_from_double(lvalue);
}

static scpi_object_t *scpi_numeric_integer_binary_op(scpi_object_t *this, scpi_object_t *other, scpi_bin_ops_t op) {
	double rvalue = ((scpi_numeric_t *)this)->value;	
	double lvalue = (double)scpi_integer_as_long(other);
	switch(op) {
		case '+':
			lvalue += rvalue;
			break;
		case '-':
			lvalue -= rvalue;
			break;
		case '*':
			lvalue *= rvalue;
			break;
		case '/':
			lvalue /= rvalue;
			break;
		case DIV:
			lvalue /= rvalue;
			lvalue = floor(lvalue);
			break;		
		case MOD:
			lvalue %= rvalue;
			break;
		case '^':
			lvalue = pow(lvalue, rvalue);
			break;
		case AND:
			lvalue &= rvalue;
			break;
		case OR:
			lvalue |= rvalue;
			break;
		case EXOR:
			lvalue ^= rvalue;
			break;		
	}
	return scpi_numeric_from_double(lvalue);
}

static scpi_object_t *scpi_numeric_binary_op(scpi_object_t *this, scpi_object_t *other, scpi_bin_ops_t op) {	
	if (scpi_object_type(other) == scpi_numeric_type)) {	
		return scpi_numeric_numeric_binary_op(this, other, op);
	}
	if (scpi_object_type(other) == scpi_integer_type)) {
		return scpi_numeric_integer_binary_op(this, other, op);
	}	
	scpi_error_set();
	return NULL;	
}

static scpi_object_t *scpi_numeric_unary_op(scpi_object_t *this, scpi_un_ops_t op) {	
	double lvalue = ((scpi_numeric_t *)this)->value;	
	switch (op) {
		case '-':
			lvalue = -lvalue;
		case NOT:
			lvalue = ~lvalue;
		default:
			break;
	}
	return scpi_numeric_from_double(lvalue);	
}

static int scpi_numeric_perform(scpi_object_t *this, scpi_object_t *args) {	
	double value;
	scpi_object_t *other;
		
	if (scpi_array_len(args) < 1) {
		scpi_error_set();
		return -1;
	}
	other = scpi_array_index(args, 0);

	if (scpi_object_type(other) == scpi_numeric_type) {
		value = ((scpi_numeric_t *)other)->value;		
	}else if (scpi_object_type(other) == scpi_integer_type) {
		value = (double)scpi_integer_as_long(other);		
	}else {
		scpi_error_set();
		return -1;
	}		
		
	((scpi_numeric_t *)this)->value = value;
	return 0;	
}

/*** Exported symbols ***/

const scpi_type_t scpi_numeric_type = {

	sizeof(scpi_numeric_t), /* object size */

	/* base primitives */
	&scpi_base_type.new,	/* new method */	
	&scpi_base_type.del,	/* delete method */
	
	/* attr primitives */
	NULL,					/* attr method */
	NULL,					/* set method */

//	/*serialize primitives */
//	&scpi_numeric_load		/* load method */
//	&scpi_numeric_dump		/* dump method */
	
	/* expression primitives */
	&scpi_numeric_binary_op,	/*binary operation method*/
	&scpi_numeric_unary_op,		/*unary operation method*/
	
	/*command primitives */
	NULL,	/* perform method */ 
	NULL,	/* query method */ 
}

double scpi_numeric_as_double(scpi_object_t *object) {
	if (SCPI_TYPE(object) != scpi_numeric_type) {
		scpi_error_set();
		return -1;
	}
	
	return ((scpi_numeric_t *)object)->value;
}

scpi_object_t *scpi_numeric_from_double(double value) {		
	scpi_object_t *object = scpi_object_new(&scpi_numeric_type);
	
	if (object != NULL) {
		((scpi_numeric_t *)object)->value = value;	
	}
	
	return object;
}


