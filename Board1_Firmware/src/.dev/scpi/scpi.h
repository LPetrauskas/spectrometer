	


struct _scpi_type_t;
typedef struct _scpi_type_t scpi_type_t;

struct _scpi_object_t;
typedef struct _scpi_object_t scpi_object_t;

#define SCPI_OBJECT_PROLOGUE \
	struct _scpi_type_t *_type;
	struct _scpi_object_t *_proto;
	int _ref

struct _scpi_object_t {
	SCPI_OBJECT_PROLOGUE;
};

struct _scpi_type_t {	

	int _size;

	scpi_object_t *(_new)(scpi_type_t *type);	
	int(*_del)(scpi_object_t *this);

	scpi_object_t *(_attr)(scpi_object_t *this, scpi_object_t *name);	
	
	scpi_object_t *(*_b_op)(scpi_object_t *this, scpi_object_t *other, int op);
	scpi_object_t *(*_u_op)(scpi_object_t *this, int op);
	
	int (*_perform)(scpi_object_t *this, scpi_object_t *params);
	scpi_object_t *(*_query)(scpi_object_t *this, scpi_object_t *params);
};




#define __DEFINE_SCPI_SYSTEM(name)  CONST scpi_table_object_t scpi_##name##_system[] =

#define DEFINE_SCPI_ROOT_SYSTEM   __DEFINE_SCPI_SYSTEM(root)
#define DEFINE_SCPI_SYSTEM(name)  __DEFINE_SCPI_SYSTEM(user##_##name)

#define __DECLARE_SCPI_SYSTEM(name) {#name, &scpi_##name##_system, 1}
#defien DECLARE_SCPI_SYSTEM(name) __DECLARE_SCPI_SYSTEM(user##name)


#define __DECLARE_SCPI_OBJECT(name)	{#name, &scpi_##name##_object, 0}
#define DECLARE_SCPI_OBJECT(name)	__DECLARE_SCPI_OBJECT(user##name)



#define DEFINE_SCPI_ROOT_OBJECTS
#define DEFINE_SCPI_OBJECT(name)  __DEFINE_SCPI_OBJECT()  	 


#define DECLARE_SCPI_OBJECT(name) {#name, &scpi_user_##name##_object, 0}




DEFINE_SCPI_ROOT_SYSTEM {
	DECLARE_SCPI_SYSTEM(AMPLITUDE),
	DECLARE_SCPI_SYSTEM(FREQUENCY),
	DECLARE_SCPI_ROOT_OBJECTS
};

DEFINE_SCPI_SYSTEM(AMPLITUDE) {
	DECLARE_SCPI_OBJECT(AUTO) 
}


#define DEFINE_OBJECT(type,name) CONST type #name = 
#define DEFINE_SUBTYPE(type, name, ) {
	
}

#define DEFINE_INTEGER_OBJECT(name) 
#define DEFINE_INTEGER_SUBTYPE


#define DECLARE_SCPI_OBJECT(name) {#name, &scpi_on_object}
#define DECLARE_SCPI_ROOT_OBJECTS \
	{"ON", &scpi_on_object}, \
	{"OFF", &scpi_off_object}, \
	{"MIN", &scpi_min_object}, \
	{"MAX", &scpi_max_object}, \
	{"DEF", &scpi_def_object} \
	

#define DECLARE_SCPI_ROOT_OBJECTS \
	DECLARE_SCPI_OBJECT(ON),  \
	DECLARE_SCPI_OBJECT(OFF),  \
	DECLARE_SCPI_OBJECT(MAX),   \
	DECLARE_SCPI_OBJECT(MIN),   \
	DECLARE_SCPI_OBJECT(DEF),   

#define DEFINE_SCPI_ROOT_OBJECTS \
	DEFINE_SCPI_INTEGER_OBJECT(ON) { 0xFFFFFFFF }; \
	DEFINE_SCPI_INTEGER_OBJECT(OFF) { 0x00000000 }; \
	DEFINE_SCPI_INTEGER_OBJECT(MAX) { 0x0F0F0F0F }; \
	DEFINE_SCPI_INTEGER_OBJECT(MIN) { 0xF0F0F0F0 }; \
	DEFINE_SCPI_INTEGER_OBJECT(DEF) { 0x55555555 };

	