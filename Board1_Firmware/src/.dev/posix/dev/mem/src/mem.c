#include "i2c.h"
#include "chip.h"


static const DevOperations_t xI2CGenericOperations = {

}

struct I2CMemDescriptor_t
{
	I2C_ID_T xId;	/*!< Id de bus I2C. Ver @ref I2C_ID_T.*/

};

static const struct I2CDevDescriptor_t I2C0PrivateDevice = 
{
	.xId = I2C0,
	.xIrq = I2C0_IRQn,
	.xLines = {{0, 20, FUNC3}, {0, 19, FUNC3}}
};

static const struct I2CDevDescriptor_t I2C1PrivateDevice = 
{
	.xId = I2C1,
	.xIrq = I2C1_IRQn,
	.xLines = {{0, 20, FUNC3}, {0, 19, FUNC3}}
};

static const struct I2CDevDescriptor_t I2C2PrivateDevice = 
{
	.xId = I2C2,
	.xIrq = I2C2_IRQn,
	.xLines = {{0, 20, FUNC3}, {0, 19, FUNC3}}
};

const struct DevDescriptor_t xI2C0Device = 
{
	.pxPrivate = &I2C0PrivateDevice,
	.pxDescriptor = &xI2COperations
};

const struct DevDescriptor_t xI2C1Device = 
{
	.pxPrivate = &I2C1PrivateDevice,
	.pxDescriptor = &xI2COperations
};

const struct DevDescriptor_t xI2C2Device = 
{
	.pxPrivate = &I2C1PrivateDevice,
	.pxDescriptor = &xI2COperations
};

static BaseType_t xMemDeviceInit(const DevDescriptor_t * pxDescriptor)
{
	I2CMemDevDescriptor_t *pxPrivDescriptor;

	if (/*initialized?*/) {
		return ;
	}

	pxPrivDescriptor = (I2CMemDevDescriptor_t *)pxDescriptor;

	Board_I2C_Init(pxPrivDescriptor->xBus);

	return pdPass;
}

static BaseType_t xI2CMemDeviceOpen(const DevDescriptor_t * pxDescriptor, Dev_t *pxDev, UBaseType_t usMode, UBaseType_t usFlags)
{
	Board_E2PROM_Init()

	
}

static BaseType_t xI2CMemDeviceWrite(Dev_t *pxDev, void *pvData, UBaseType_t usSize)
{
	I2CMemDevDescriptor_t *pxPrivDescriptor;
	I2C_XFER_T xfer = {0};
	I2C_BUFF_T xAddress, xData;
	
	xAddress = 

	pxPrivDescriptor = (I2CMemDevDescriptor_t *)pxDev->pxDescriptor->pxPrivate;
	
	xferpxPrivDescriptor->xSlaveAddr


	Board_I2C_Transfer(pxPrivDescriptor->xId, &xfer);
}

static BaseType_t xI2CMemDeviceRead(Dev_t *pxDev, void *pvData, UBaseType_t usSize)
{
	return -1;
}

static BaseType_t xI2CMemDeviceSeek(Dev_t *pxDev, BaseType_t sOffset, BaseType_t sWhence)
{
	return sOffset;
}

static BaseType_t xI2CMemDeviceIoctl(Dev_t *pxDev, BaseType_t sCmd, ...)
{
	return pdPass;
}

static BaseType_t xI2CMemDeviceClose(Dev_t *pxDev)
{
	return pdPass;
}

static BaseType_t xI2CMemDeviceDeinit(const DevDescriptor_t * pxDescriptor)
{
	return pdPass;
}
