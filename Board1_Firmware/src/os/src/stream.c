#include "stream.h"

portTASK_FUNCTION_PROTO(vStreamerTask, pvParameters) {
   uint32_t ulData;
   StreamParameters_t *pxParams;
   eStreamAction eAction;
   eStreamState eState;

   pxParams = pvParameters;
   pxParams->pxSource->xLock();
   pxParams->pxSink->xLock();   
   ulData = 0; eState = eStreaming;

   while (ulData != pdEOF) {
      /* Input */
      xTaskNotifyWait(0x00, 0xFF, ((uint32_t *)&eAction), (eState != eStreaming)? portMAX_DELAY:0);       
      /* State */
      switch (eState) {
	 case eStalled:
	    if (eAction == eResume) {
	       eState = eStreaming;
	    }
	 case eStreaming:
	    if (eAction == eSuspend) {
	       pxParams->pxSource->vUnlock();
	       pxParams->pxSink->vUnlock();
	       eState = eStopped;	       
	    } 
	    if (eAction == eStall) {
	       eState = eStalled;
	    }
	    break;
	 case eStopped:	    	   
	    if (eAction == eResume) {
	       pxParams->pxSource->xLock();
	       pxParams->pxSink->xLock();
	       eState = eStreaming;		     
	    }
	    break;
      } 
      /* Output */
      if (eState == eStreaming) {
	 ulData = pxParams->pxSource->ulGet(pxParams->xFlags);
	 pxParams->pxSink->vPut(ulData, pxParams->xFlags);	 
      }
   } 
   pxParams->pxSource->vUnlock();
   pxParams->pxSink->vUnlock();   
   vTaskDelete(NULL);	 
}
