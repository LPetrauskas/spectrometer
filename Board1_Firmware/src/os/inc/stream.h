#ifndef STREAM_H
#define STREAM_H

#include "FreeRTOS.h"
#include "task.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Stream states
 * \ingroup Streams
 */
typedef enum {
  eStreaming, /**< Streaming ongoing */
  eStalled,   /**< Streaming has stopped due to an error */
  eStopped    /**< Streaming completed */
} eStreamState;

/**
 * Stream actions
 * \ingroup Streams
 */
typedef enum {
  eResume,  /**< Resume a (suspended) streaming */
  eStall,   /**< Stall (abort) streaming */
  eSuspend  /**< Suspend a streaming */
} eStreamAction;


/**
 * Flags for stream operation
 *
 * \defgroup StreamFlags Stream flags
 * \ingroup Streams
 */
#define WRAPUP    0x01
#define NOSTALL   0x02


/**
 * Streaming source descriptor
 * \ingroup Streams
 */
typedef struct {
  BaseType_t (*xLock)(void); /**< Resource locking callback */
  void (*vUnlock)(void);     /**< Resource unlocking callback */

  uint32_t (*ulGet)(BaseType_t); /**< Get data callback. Accepts some \ref StreamFlags flags, returns data */
} StreamSource_t;

/**
 * Streaming sink descriptor
 * \ingroup Streams
 */
typedef struct {
  BaseType_t (*xLock)(void); /**< Resource locking callback */
  void (*vUnlock)(void);     /**< Resource unlocking callback */

  void (*vPut)(uint32_t, BaseType_t); /**< Put data callback. Accepts some \ref StreamFlags flags and data */
} StreamSink_t;


/**
 * Streaming parameters.
 * \ingroup Streams
 */
typedef struct {
  BaseType_t xFlags;    /**< Default \ref StreamFlags flags */
  const StreamSource_t *const pxSource; /**< Source descriptor */
  const StreamSink_t *const pxSink;     /**< Sink descriptor */
} StreamParameters_t;

/**
 * Type by which streams are referenced.
 * \ingroup Streams
 */
typedef TaskHandle_t StreamHandle_t;

/**
 * Streaming task definition.
 * \ingroup Streams
 */
EXTERN portTASK_FUNCTION_PROTO(vStreamerTask, pvParameters);

/**
 * Create a new stream and start it.   
 * @param pcName A descriptive name for the stream. 
 * @param pxParameters Pointer to \ref StreamParameters_t "stream parameters"
 * @param uxPriority  Priority at which the streaming task should run 
 * @param pxCreateStream Used to pass back a handle to the stream.
 * @return pdTRUE if succeded, pdFALSE otherwise
 * \ingroup Streams
 */
#define xStreamCreate( pcName, pxParameters, uxPriority, pxCreatedStream ) xTaskCreate( (&vStreamerTask), ( pcName ), ( portMINIMAL_STACK ), ( pxParameters ), ( uxPriority ), ( pxCreatedStream) )   

/**
 * Resume a stream
 * @param xStream Handle to to the stream to be resumed.
 * @return pdTRUE if succeded, pdFALSE otherwise
 * \ingroup Streams
 */
#define xStreamResume( xStream ) xTaskNotify( ( xStream ), ( (uint32_t) eResume ), ( eSetValueWithoutOverwrite ) ) 

/**
 * Suspend a stream
 * @param xStream Handle to to the stream to be suspended.
 * @return pdTRUE if succeded, pdFALSE otherwise
 * \ingroup Streams
 */
#define xStreamSuspend( xStream ) xTaskNotify( ( xStream ), ( (uint32_t) eSuspend ), ( eSetValueWithoutOverwrite ) ) 

/**
 * Stall a stream
 * @param xStream Handle to to the stream to be stalled.
 * @return pdTRUE if succeded, pdFALSE otherwise
 * \ingroup Streams
 */
#define xStreamStall( xStream ) xTaskNotify( ( xStream ), ( (uint32_t) eStall ), ( eSetValueWithOverwrite ) ) 

#ifdef __cplusplus
}
#endif

#endif /* STREAM_H */
