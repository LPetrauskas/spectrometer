<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="group">
    <name>xCoRoutineCreate</name>
    <title>xCoRoutineCreate</title>
    <filename>group__xCoRoutineCreate.html</filename>
  </compound>
  <compound kind="group">
    <name>vCoRoutineSchedule</name>
    <title>vCoRoutineSchedule</title>
    <filename>group__vCoRoutineSchedule.html</filename>
  </compound>
  <compound kind="group">
    <name>crSTART</name>
    <title>crSTART</title>
    <filename>group__crSTART.html</filename>
  </compound>
  <compound kind="group">
    <name>crDELAY</name>
    <title>crDELAY</title>
    <filename>group__crDELAY.html</filename>
  </compound>
  <compound kind="group">
    <name>crQUEUE_SEND</name>
    <title>crQUEUE_SEND</title>
    <filename>group__crQUEUE__SEND.html</filename>
  </compound>
  <compound kind="group">
    <name>crQUEUE_RECEIVE</name>
    <title>crQUEUE_RECEIVE</title>
    <filename>group__crQUEUE__RECEIVE.html</filename>
  </compound>
  <compound kind="group">
    <name>crQUEUE_SEND_FROM_ISR</name>
    <title>crQUEUE_SEND_FROM_ISR</title>
    <filename>group__crQUEUE__SEND__FROM__ISR.html</filename>
  </compound>
  <compound kind="group">
    <name>crQUEUE_RECEIVE_FROM_ISR</name>
    <title>crQUEUE_RECEIVE_FROM_ISR</title>
    <filename>group__crQUEUE__RECEIVE__FROM__ISR.html</filename>
  </compound>
  <compound kind="group">
    <name>EventGroup</name>
    <title>EventGroup</title>
    <filename>group__EventGroup.html</filename>
    <subgroup>EventGroupHandle_t</subgroup>
    <subgroup>xEventGroupCreate</subgroup>
    <subgroup>xEventGroupWaitBits</subgroup>
    <subgroup>xEventGroupClearBits</subgroup>
    <subgroup>xEventGroupSetBitsFromISR</subgroup>
    <subgroup>xEventGroupSetBits</subgroup>
    <subgroup>xEventGroupSync</subgroup>
    <subgroup>xEventGroupGetBits</subgroup>
    <subgroup>xEventGroupGetBitsFromISR</subgroup>
  </compound>
  <compound kind="group">
    <name>EventGroupHandle_t</name>
    <title>EventGroupHandle_t</title>
    <filename>group__EventGroupHandle__t.html</filename>
  </compound>
  <compound kind="group">
    <name>xEventGroupCreate</name>
    <title>xEventGroupCreate</title>
    <filename>group__xEventGroupCreate.html</filename>
  </compound>
  <compound kind="group">
    <name>xEventGroupWaitBits</name>
    <title>xEventGroupWaitBits</title>
    <filename>group__xEventGroupWaitBits.html</filename>
  </compound>
  <compound kind="group">
    <name>xEventGroupClearBits</name>
    <title>xEventGroupClearBits</title>
    <filename>group__xEventGroupClearBits.html</filename>
  </compound>
  <compound kind="group">
    <name>xEventGroupSetBitsFromISR</name>
    <title>xEventGroupSetBitsFromISR</title>
    <filename>group__xEventGroupSetBitsFromISR.html</filename>
  </compound>
  <compound kind="group">
    <name>xEventGroupSetBits</name>
    <title>xEventGroupSetBits</title>
    <filename>group__xEventGroupSetBits.html</filename>
  </compound>
  <compound kind="group">
    <name>xEventGroupSync</name>
    <title>xEventGroupSync</title>
    <filename>group__xEventGroupSync.html</filename>
  </compound>
  <compound kind="group">
    <name>xEventGroupGetBits</name>
    <title>xEventGroupGetBits</title>
    <filename>group__xEventGroupGetBits.html</filename>
  </compound>
  <compound kind="group">
    <name>xEventGroupGetBitsFromISR</name>
    <title>xEventGroupGetBitsFromISR</title>
    <filename>group__xEventGroupGetBitsFromISR.html</filename>
  </compound>
  <compound kind="group">
    <name>xQueueCreate</name>
    <title>xQueueCreate</title>
    <filename>group__xQueueCreate.html</filename>
  </compound>
  <compound kind="group">
    <name>xQueueSend</name>
    <title>xQueueSend</title>
    <filename>group__xQueueSend.html</filename>
  </compound>
  <compound kind="group">
    <name>xQueueOverwrite</name>
    <title>xQueueOverwrite</title>
    <filename>group__xQueueOverwrite.html</filename>
  </compound>
  <compound kind="group">
    <name>xQueueReceive</name>
    <title>xQueueReceive</title>
    <filename>group__xQueueReceive.html</filename>
  </compound>
  <compound kind="group">
    <name>xQueuePeekFromISR</name>
    <title>xQueuePeekFromISR</title>
    <filename>group__xQueuePeekFromISR.html</filename>
  </compound>
  <compound kind="group">
    <name>uxQueueMessagesWaiting</name>
    <title>uxQueueMessagesWaiting</title>
    <filename>group__uxQueueMessagesWaiting.html</filename>
  </compound>
  <compound kind="group">
    <name>vQueueDelete</name>
    <title>vQueueDelete</title>
    <filename>group__vQueueDelete.html</filename>
  </compound>
  <compound kind="group">
    <name>xQueueSendFromISR</name>
    <title>xQueueSendFromISR</title>
    <filename>group__xQueueSendFromISR.html</filename>
  </compound>
  <compound kind="group">
    <name>xQueueOverwriteFromISR</name>
    <title>xQueueOverwriteFromISR</title>
    <filename>group__xQueueOverwriteFromISR.html</filename>
  </compound>
  <compound kind="group">
    <name>xQueueReceiveFromISR</name>
    <title>xQueueReceiveFromISR</title>
    <filename>group__xQueueReceiveFromISR.html</filename>
  </compound>
  <compound kind="group">
    <name>vSemaphoreCreateBinary</name>
    <title>vSemaphoreCreateBinary</title>
    <filename>group__vSemaphoreCreateBinary.html</filename>
  </compound>
  <compound kind="group">
    <name>xSemaphoreTake</name>
    <title>xSemaphoreTake</title>
    <filename>group__xSemaphoreTake.html</filename>
  </compound>
  <compound kind="group">
    <name>xSemaphoreTakeRecursive</name>
    <title>xSemaphoreTakeRecursive</title>
    <filename>group__xSemaphoreTakeRecursive.html</filename>
  </compound>
  <compound kind="group">
    <name>xSemaphoreGive</name>
    <title>xSemaphoreGive</title>
    <filename>group__xSemaphoreGive.html</filename>
  </compound>
  <compound kind="group">
    <name>xSemaphoreGiveRecursive</name>
    <title>xSemaphoreGiveRecursive</title>
    <filename>group__xSemaphoreGiveRecursive.html</filename>
  </compound>
  <compound kind="group">
    <name>xSemaphoreGiveFromISR</name>
    <title>xSemaphoreGiveFromISR</title>
    <filename>group__xSemaphoreGiveFromISR.html</filename>
  </compound>
  <compound kind="group">
    <name>vSemaphoreCreateMutex</name>
    <title>vSemaphoreCreateMutex</title>
    <filename>group__vSemaphoreCreateMutex.html</filename>
  </compound>
  <compound kind="group">
    <name>xSemaphoreCreateCounting</name>
    <title>xSemaphoreCreateCounting</title>
    <filename>group__xSemaphoreCreateCounting.html</filename>
  </compound>
  <compound kind="group">
    <name>vSemaphoreDelete</name>
    <title>vSemaphoreDelete</title>
    <filename>group__vSemaphoreDelete.html</filename>
  </compound>
  <compound kind="group">
    <name>StreamFlags</name>
    <title>Stream flags</title>
    <filename>group__StreamFlags.html</filename>
  </compound>
  <compound kind="group">
    <name>TaskHandle_t</name>
    <title>TaskHandle_t</title>
    <filename>group__TaskHandle__t.html</filename>
  </compound>
  <compound kind="group">
    <name>taskYIELD</name>
    <title>taskYIELD</title>
    <filename>group__taskYIELD.html</filename>
  </compound>
  <compound kind="group">
    <name>taskENTER_CRITICAL</name>
    <title>taskENTER_CRITICAL</title>
    <filename>group__taskENTER__CRITICAL.html</filename>
  </compound>
  <compound kind="group">
    <name>taskEXIT_CRITICAL</name>
    <title>taskEXIT_CRITICAL</title>
    <filename>group__taskEXIT__CRITICAL.html</filename>
  </compound>
  <compound kind="group">
    <name>taskDISABLE_INTERRUPTS</name>
    <title>taskDISABLE_INTERRUPTS</title>
    <filename>group__taskDISABLE__INTERRUPTS.html</filename>
  </compound>
  <compound kind="group">
    <name>taskENABLE_INTERRUPTS</name>
    <title>taskENABLE_INTERRUPTS</title>
    <filename>group__taskENABLE__INTERRUPTS.html</filename>
  </compound>
  <compound kind="group">
    <name>xTaskCreate</name>
    <title>xTaskCreate</title>
    <filename>group__xTaskCreate.html</filename>
  </compound>
  <compound kind="group">
    <name>xTaskCreateRestricted</name>
    <title>xTaskCreateRestricted</title>
    <filename>group__xTaskCreateRestricted.html</filename>
  </compound>
  <compound kind="group">
    <name>vTaskDelete</name>
    <title>vTaskDelete</title>
    <filename>group__vTaskDelete.html</filename>
  </compound>
  <compound kind="group">
    <name>vTaskDelay</name>
    <title>vTaskDelay</title>
    <filename>group__vTaskDelay.html</filename>
  </compound>
  <compound kind="group">
    <name>vTaskDelayUntil</name>
    <title>vTaskDelayUntil</title>
    <filename>group__vTaskDelayUntil.html</filename>
  </compound>
  <compound kind="group">
    <name>uxTaskPriorityGet</name>
    <title>uxTaskPriorityGet</title>
    <filename>group__uxTaskPriorityGet.html</filename>
  </compound>
  <compound kind="group">
    <name>vTaskPrioritySet</name>
    <title>vTaskPrioritySet</title>
    <filename>group__vTaskPrioritySet.html</filename>
  </compound>
  <compound kind="group">
    <name>vTaskSuspend</name>
    <title>vTaskSuspend</title>
    <filename>group__vTaskSuspend.html</filename>
  </compound>
  <compound kind="group">
    <name>vTaskResume</name>
    <title>vTaskResume</title>
    <filename>group__vTaskResume.html</filename>
  </compound>
  <compound kind="group">
    <name>vTaskResumeFromISR</name>
    <title>vTaskResumeFromISR</title>
    <filename>group__vTaskResumeFromISR.html</filename>
  </compound>
  <compound kind="group">
    <name>vTaskStartScheduler</name>
    <title>vTaskStartScheduler</title>
    <filename>group__vTaskStartScheduler.html</filename>
  </compound>
  <compound kind="group">
    <name>vTaskEndScheduler</name>
    <title>vTaskEndScheduler</title>
    <filename>group__vTaskEndScheduler.html</filename>
  </compound>
  <compound kind="group">
    <name>vTaskSuspendAll</name>
    <title>vTaskSuspendAll</title>
    <filename>group__vTaskSuspendAll.html</filename>
  </compound>
  <compound kind="group">
    <name>xTaskResumeAll</name>
    <title>xTaskResumeAll</title>
    <filename>group__xTaskResumeAll.html</filename>
  </compound>
  <compound kind="group">
    <name>xTaskGetTickCount</name>
    <title>xTaskGetTickCount</title>
    <filename>group__xTaskGetTickCount.html</filename>
  </compound>
  <compound kind="group">
    <name>xTaskGetTickCountFromISR</name>
    <title>xTaskGetTickCountFromISR</title>
    <filename>group__xTaskGetTickCountFromISR.html</filename>
  </compound>
  <compound kind="group">
    <name>uxTaskGetNumberOfTasks</name>
    <title>uxTaskGetNumberOfTasks</title>
    <filename>group__uxTaskGetNumberOfTasks.html</filename>
  </compound>
  <compound kind="group">
    <name>pcTaskGetTaskName</name>
    <title>pcTaskGetTaskName</title>
    <filename>group__pcTaskGetTaskName.html</filename>
  </compound>
  <compound kind="group">
    <name>vTaskList</name>
    <title>vTaskList</title>
    <filename>group__vTaskList.html</filename>
  </compound>
  <compound kind="group">
    <name>vTaskGetRunTimeStats</name>
    <title>vTaskGetRunTimeStats</title>
    <filename>group__vTaskGetRunTimeStats.html</filename>
  </compound>
  <compound kind="group">
    <name>xTaskNotify</name>
    <title>xTaskNotify</title>
    <filename>group__xTaskNotify.html</filename>
  </compound>
  <compound kind="group">
    <name>xTaskNotifyWait</name>
    <title>xTaskNotifyWait</title>
    <filename>group__xTaskNotifyWait.html</filename>
  </compound>
  <compound kind="group">
    <name>xTaskNotifyGive</name>
    <title>xTaskNotifyGive</title>
    <filename>group__xTaskNotifyGive.html</filename>
  </compound>
  <compound kind="group">
    <name>ulTaskNotifyTake</name>
    <title>ulTaskNotifyTake</title>
    <filename>group__ulTaskNotifyTake.html</filename>
  </compound>
  <compound kind="struct">
    <name>CRCB_t</name>
    <filename>structCRCB__t.html</filename>
  </compound>
  <compound kind="struct">
    <name>EventGroup_t</name>
    <filename>structEventGroup__t.html</filename>
  </compound>
  <compound kind="struct">
    <name>HeapRegion_t</name>
    <filename>structHeapRegion__t.html</filename>
  </compound>
  <compound kind="struct">
    <name>List_t</name>
    <filename>structList__t.html</filename>
  </compound>
  <compound kind="struct">
    <name>xLIST_ITEM</name>
    <filename>structxLIST__ITEM.html</filename>
  </compound>
  <compound kind="struct">
    <name>MemoryRegion_t</name>
    <filename>structMemoryRegion__t.html</filename>
  </compound>
  <compound kind="struct">
    <name>xMINI_LIST_ITEM</name>
    <filename>structxMINI__LIST__ITEM.html</filename>
  </compound>
  <compound kind="struct">
    <name>StreamParameters_t</name>
    <filename>structStreamParameters__t.html</filename>
    <member kind="variable">
      <type>BaseType_t</type>
      <name>xFlags</name>
      <anchorfile>structStreamParameters__t.html</anchorfile>
      <anchor>a710d10f711b295b7cdc4f9d8b3a0e377</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const StreamSource_t *const</type>
      <name>pxSource</name>
      <anchorfile>structStreamParameters__t.html</anchorfile>
      <anchor>a07afaa5dedfc900d765d6375069b5034</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const StreamSink_t *const</type>
      <name>pxSink</name>
      <anchorfile>structStreamParameters__t.html</anchorfile>
      <anchor>a433c508600427ca3545907061d2d6f8c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>StreamSink_t</name>
    <filename>structStreamSink__t.html</filename>
    <member kind="variable">
      <type>BaseType_t(*</type>
      <name>xLock</name>
      <anchorfile>structStreamSink__t.html</anchorfile>
      <anchor>a625b43aab31f584d3df9b58e8260774e</anchor>
      <arglist>)(void)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>vUnlock</name>
      <anchorfile>structStreamSink__t.html</anchorfile>
      <anchor>a01ef0002b37a130deb6d3667802b85f0</anchor>
      <arglist>)(void)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>vPut</name>
      <anchorfile>structStreamSink__t.html</anchorfile>
      <anchor>a7bcdf31b8bdd1a4f163ddcce6035b044</anchor>
      <arglist>)(uint32_t, BaseType_t)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>StreamSource_t</name>
    <filename>structStreamSource__t.html</filename>
    <member kind="variable">
      <type>BaseType_t(*</type>
      <name>xLock</name>
      <anchorfile>structStreamSource__t.html</anchorfile>
      <anchor>a499874bc1aba16c73118a9ea05ba7431</anchor>
      <arglist>)(void)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>vUnlock</name>
      <anchorfile>structStreamSource__t.html</anchorfile>
      <anchor>af363083220e56343d804684d1a9477ff</anchor>
      <arglist>)(void)</arglist>
    </member>
    <member kind="variable">
      <type>uint32_t(*</type>
      <name>ulGet</name>
      <anchorfile>structStreamSource__t.html</anchorfile>
      <anchor>a52e57b09fe475838dd711bc0138f40c9</anchor>
      <arglist>)(BaseType_t)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>TaskParameters_t</name>
    <filename>structTaskParameters__t.html</filename>
  </compound>
  <compound kind="struct">
    <name>TaskStatus_t</name>
    <filename>structTaskStatus__t.html</filename>
  </compound>
  <compound kind="struct">
    <name>TimeOut_t</name>
    <filename>structTimeOut__t.html</filename>
  </compound>
  <compound kind="struct">
    <name>tskTCB</name>
    <filename>structtskTCB.html</filename>
  </compound>
  <compound kind="struct">
    <name>xQUEUE</name>
    <filename>structxQUEUE.html</filename>
  </compound>
  <compound kind="dir">
    <name>inc</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/os/inc/</path>
    <filename>dir_bfccd401955b95cf8c75461437045ac0.html</filename>
    <file>croutine.h</file>
    <file>deprecated_definitions.h</file>
    <file>event_groups.h</file>
    <file>FreeRTOS.h</file>
    <file>FreeRTOSCommonHooks.h</file>
    <file>FreeRTOSConfig.h</file>
    <file>list.h</file>
    <file>mpu_wrappers.h</file>
    <file>os.h</file>
    <file>portable.h</file>
    <file>portmacro.h</file>
    <file>projdefs.h</file>
    <file>queue.h</file>
    <file>semphr.h</file>
    <file>StackMacros.h</file>
    <file>stream.h</file>
    <file>task.h</file>
    <file>timers.h</file>
  </compound>
  <compound kind="dir">
    <name>src</name>
    <path>/home/notebook/Documentos/spectrum/spectrum_analyzer/Firmware/src/os/src/</path>
    <filename>dir_68267d1309a1af8e8297ef4c3efbcdba.html</filename>
    <file>croutine.c</file>
    <file>event_groups.c</file>
    <file>FreeRTOSCommonHooks.c</file>
    <file>heap_3.c</file>
    <file>list.c</file>
    <file>port.c</file>
    <file>queue.c</file>
    <file>stream.c</file>
    <file>tasks.c</file>
    <file>timers.c</file>
  </compound>
  <compound kind="page">
    <name>index</name>
    <title>Operating System Package</title>
    <filename>index</filename>
    <docanchor file="index">os_pkg</docanchor>
    <docanchor file="index" title="Overview">os_overview</docanchor>
  </compound>
</tagfile>
